//
//  WebViewController.m
//  UFSApp
//
//  Created by SONIYA VISHWAKARMA on 25/08/15.
//  Copyright (c) 2015 Epictenet. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController ()
@property (weak, nonatomic) IBOutlet UINavigationBar *navBar;
@property (weak, nonatomic) IBOutlet UINavigationItem *navItem;

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UISegmentedControl *historySegmentControl;

@end

@implementation WebViewController

#pragma mark - View Controller Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    if (_pagetitle.length!=0) {
        _navItem.title = _pagetitle;
    }
    _activityIndicator.color = [ConfigurationFile getThemeColor];
    self.navBar.frame = CGRectMake(0, 0, self.navBar.frame.size.width, 64);
      _historySegmentControl.hidden = self.hideHistorySegment;
    if (_urlAddress!=nil) {
        [self reloadFeed];
    }
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Remove References Methods
-(void)removeRefencesOfWebView{
    [_webView stopLoading];
    _webView.delegate=nil;
    _webView=nil;
    _activityIndicator=nil;
    _historySegmentControl=nil;
    _urlAddress=nil;
}

#pragma mark - Operational Methods

- (IBAction)cancelClicked:(id)sender {
    [self removeRefencesOfWebView];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)loadHTMLOfLocalHTMLFile:(NSString*)fileName{
     NSString *htmlFile= [[NSBundle mainBundle] pathForResource:fileName ofType:@"html"];
    NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
    [self.webView loadHTMLString:htmlString baseURL:nil];

}

//// @Description : This method is used to open Url in Browser
//
//-(IBAction)openUrlinBrowser:(id)sender
//
//{
//    [_webView stopLoading];
//    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:_urlAddress]];
//}
//- (IBAction)reloadWebView
//{
//    [_webView reload];
//}

// @Description : This method is reload the WebView

- (void)reloadFeed
{
    self.webView.scalesPageToFit = YES;
     _urlAddress = [SupportingClass getValidUrl:_urlAddress];
    //Create a URL object.
    NSURL *url = [NSURL URLWithString:_urlAddress];
   
    //URL Requst Object
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:60];
    
    //Load the request in the UIWebView.
    [_webView loadRequest:requestObj];
}

// @Description : This method is used go Forward and backward in History

-(IBAction)goForwardBackward:(UISegmentedControl*)sender
{
    [_webView stopLoading];
    if (sender.selectedSegmentIndex==0) {
        
        [_webView goBack];
        
    }
    else
    {
        [_webView goForward];
        
    }
}

#pragma mark - WebViewDelegate

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [_activityIndicator stopAnimating];
}

- (void)webViewDidStartLoad:(UIWebView *)webView1 {
    
    [_activityIndicator startAnimating];
    [_historySegmentControl setEnabled:[_webView canGoBack] forSegmentAtIndex:0];
    [_historySegmentControl setEnabled:[_webView canGoForward] forSegmentAtIndex:1];
    
}

- (BOOL)webView:(UIWebView *)webView1 shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    [_historySegmentControl setEnabled:[_webView canGoBack] forSegmentAtIndex:0];
    [_historySegmentControl setEnabled:[_webView canGoForward] forSegmentAtIndex:1];
    //    if ( navigationType == UIWebViewNavigationTypeLinkClicked ) {
    //        btnBackHistory.enabled=YES;
    //    }
    return YES;
}


@end
