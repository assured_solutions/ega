/*
 Class Name : WebViewController
 Author : Soniya
 Dated : 24/08/15
 Description : This class is basically used to open any link from the application without leaving the application.
 Copyright (c) 2015 Epictenet. All rights reserved.
 */


#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface WebViewController: BaseViewController <UIWebViewDelegate>
@property(nonatomic,strong)NSString *urlAddress;
@property(nonatomic,strong)NSString *pagetitle;
@property(nonatomic,readwrite) BOOL hideHistorySegment;
-(void)loadHTMLOfLocalHTMLFile:(NSString*)fileName;
@end
