//
//  BaseViewController.m
//  
//
//  Created by Soniya on 16/08/15.
//  Copyright (c) 2015 iBeam. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()
{
    UIBarButtonItem* prev;
    UIBarButtonItem* next;
}
@end

@implementation BaseViewController
#pragma mark - Memory Management
-(void)removeBaseReferences{
    accessoryToolBar = nil;
    activeTextField = nil;
    prev = nil;
    next = nil;
    [self removeReferences];
 }
-(void)removeReferences{
    
}

#pragma mark - View Controller Life Cycle
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}
- (void)viewDidLoad {
     [super viewDidLoad];
    
    self.view.backgroundColor = [ConfigurationFile getBackGroundColor];
    appDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    screenWidth = screenSize.width;
    screenHeight = screenSize.height;
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    swipe.numberOfTouchesRequired = 1;
    [swipe setDirection:UISwipeGestureRecognizerDirectionRight];
    swipe.delegate = (id)self;
    [self.view addGestureRecognizer:swipe];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];

    
    
  
   
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)didMoveToParentViewController:(UIViewController *)parent
{
    if (![parent isEqual:self.parentViewController]) {
        [self removeReferences];
    }
}
#pragma mark - General Operational Methods
-(void)setCompnyLogoInNavigationBar{
    UIImageView *headerImage = [[UIImageView alloc]initWithImage:[[UIImage imageNamed:@"EGALogo"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
    headerImage.frame = CGRectMake(0, 0, 92, 32);
    headerImage.contentMode = UIViewContentModeScaleAspectFit;
    [self.navigationItem setTitleView:headerImage];

}
-(void)showListMenu{
      self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ListICon"] style:UIBarButtonItemStylePlain target:self action:@selector(listClicked:)];
}

-(void) createKeyboardAccessoryToolBar:(BOOL)havingNextPrevButton{
    accessoryToolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    accessoryToolBar.barTintColor = [ConfigurationFile getThemeColor];
    accessoryToolBar.tintColor = [UIColor whiteColor];
    UIBarButtonItem* flexSpace = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
     UIBarButtonItem* doneButton = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneEditingButtonClicked)];
    
    
    NSMutableArray* items = [[NSMutableArray alloc]init];
    if (havingNextPrevButton)
    {
         prev = [[UIBarButtonItem alloc]initWithTitle:@"Prev" style:UIBarButtonItemStyleDone target:self action:@selector(prevButtonClicked)];
         next = [[UIBarButtonItem alloc]initWithTitle:@"Next" style:UIBarButtonItemStyleDone target:self action:@selector(nextButtonClicked)];
  
        [items addObject:prev];
         [items addObject:next];
     }
    [items addObject:flexSpace];
    [items addObject:doneButton];
    accessoryToolBar.items = items;
    [accessoryToolBar sizeToFit];
    
    
}

-(void)hidePrevNext:(BOOL)hide{
    if (hide) {
         prev.enabled = false;
         prev.title = @"";
        next.enabled = false;
        next.title = @"";
    } else {
        prev.enabled = true;
        prev.title = @"Prev";
        next.enabled = true;
        next.title = @"Next";
    }}

- (void)handleSwipe:(UISwipeGestureRecognizer *)recognizer {
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)doneEditingButtonClicked
{
  
}
-(void)prevButtonClicked
{
    
}
-(void)nextButtonClicked
{
    
}
-(void)listClicked:(UIBarButtonItem*)sender{
    
  //  [_viewSlideMenu showSlideMenu];
    
}

-(void)goBackToHome:(id)sender{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
-(void)setTitle:(NSString *)title SubTitle:(NSString*)subTitle{
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.font = [UIFont boldSystemFontOfSize:18];
    titleLabel.text = title;
    [titleLabel sizeToFit];
    
    UILabel *subTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, 0, 0)];
    subTitleLabel.backgroundColor = [UIColor clearColor];
    subTitleLabel.textColor = [UIColor whiteColor];
    subTitleLabel.font = [UIFont systemFontOfSize:13];
    subTitleLabel.text = subTitle;
    [subTitleLabel sizeToFit];
    
    UIView *twoLineTitleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MAX(subTitleLabel.frame.size.width, titleLabel.frame.size.width), 30)];
    [twoLineTitleView addSubview:titleLabel];
    [twoLineTitleView addSubview:subTitleLabel];
    
    float widthDiff = subTitleLabel.frame.size.width - titleLabel.frame.size.width;
    
    if (widthDiff > 0) {
        CGRect frame = titleLabel.frame;
        frame.origin.x = widthDiff / 2;
        titleLabel.frame = CGRectIntegral(frame);
    }else{
        CGRect frame = subTitleLabel.frame;
        frame.origin.x = fabsf(widthDiff) / 2;
        subTitleLabel.frame = CGRectIntegral(frame);
    }
    
    self.navigationItem.titleView = twoLineTitleView;

}
@end
