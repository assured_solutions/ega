//
//  MyDietPlanViewController.m
//  EGA
//
//  Created by animesh on 12/6/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "MyDietPlanViewController.h"
#import "TableViewCell.h"
#import "MyDietPlanDM.h"
#import "AttemptedDietPlanViewController.h"


@interface MyDietPlanViewController ()<UITableViewDataSource,UITableViewDelegate,WebServiceCallerDelegate>
{
    NSMutableArray *arrMyDietPlan;
    __weak IBOutlet UITableView *tblDietPlan;
    __weak IBOutlet UILabel *lblHeading;
     WebServiceCaller *webServiceCaller;
    NSInteger SelectedIndexOFDiet;
    __weak IBOutlet UIView *vwHeader;
    
    UIView *viewHeader;
}
@end

@implementation MyDietPlanViewController
#pragma mark - Memory Management

-(void)removeReferences{
    
    [webServiceCaller cancelWebserviceCall];
    webServiceCaller.delegate = nil;
    webServiceCaller = nil;
    
    
    [ProgressHUD dismiss];
}

#pragma mark - View Controller Life Cycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
         // Do any additional setup after loading the view, typically from a nib.
    [self screenDesignOfMyDietPlan];
    
    
    
   
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - ScreenDesigning Methods
-(void)screenDesignOfMyDietPlan{
    self.navigationItem.title = @"My Diet Plan";
    webServiceCaller = [[WebServiceCaller alloc]init];
    webServiceCaller.delegate = self;
 //   [self filingDataOfDietPlan];
    if(_goToHome){
        // Create a UIBarButtonItem
            UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Home" style:UIBarButtonItemStylePlain target:self action:@selector(goBackToHome:)];
        
            // Associate the barButtonItem to the previous view
            [self.navigationItem setLeftBarButtonItem:barButtonItem];
    }
        
    [self getDataForDietPlan];
    viewHeader = vwHeader;
}

//calling web services to get diet plans

-(void)getDataForDietPlan{
    if (appDelegate.isNetAvailable) {
        [ProgressHUD show:@"Getting your diet plan" Interaction:NO];
        [webServiceCaller getListOfDietPlan:appDelegate.userData.str_userID];
    }
    else{
        [SupportingClass showErrorMessage:NETWORKERRORMESSAGE];
    }
    
}
#pragma mark - Operational Methods







-(void)filingDataOfDietPlan{
    
    MyDietPlanDM *db=[[MyDietPlanDM alloc]init];
    
    arrMyDietPlan = [db GetMyDietPlans];
    
    NSLog(@"here is data %@",arrMyDietPlan);
  
    if (arrMyDietPlan.count>0) {
        NSMutableDictionary *dict = [arrMyDietPlan objectAtIndex:0];;
        NSLog(@"%@",dict);
        NSString * fromDate = [SupportingClass getStringDate:[dict valueForKey:@"Startdate"] From:[SupportingClass dateFormat] to:DisplayDateFormat];
         NSString * toDate = [SupportingClass getStringDate:[dict valueForKey:@"Enddate"] From:[SupportingClass dateFormat] to:DisplayDateFormat];
        NSString *strDietPlanDate=[NSString stringWithFormat:@"From %@ To %@",fromDate,toDate];
        
        
        
        [lblHeading setText:strDietPlanDate];
    }
    
    [tblDietPlan reloadData];
}



//-(void)breakfastConsumedOfUser:(NSString*)userid StartDate:(NSString*)startDate isConsumed:(NSInteger)consumed;
//
//-(void)lunchConsumedOfUser:(NSString*)userid StartDate:(NSString*)startDate isConsumed:(NSInteger)consumed;
//
//-(void)dinnerConsumedOfUser:(NSString*)userid StartDate:(NSString*)startDate isConsumed:(NSInteger)consumed;






-(void)breakFastConsumed:(UIButton*)sender
{
    NSLog(@"Button Clicked index %ld",(long)sender.tag);
    
    NSInteger tag=sender.tag;
    NSInteger selectedIndexRow = tag-1000;
    NSMutableDictionary *tempDict=[arrMyDietPlan objectAtIndex:selectedIndexRow];
    
    NSLog(@"selected dict data %@",tempDict);
    //this index is global
    SelectedIndexOFDiet =selectedIndexRow;
    [ProgressHUD show:@"Getting your diet plan" Interaction:NO];
    
    //Calling web services to get diet plan web services
    [webServiceCaller breakfastConsumedOfUser:appDelegate.userData.str_userID StartDate:[tempDict valueForKey:@"Startdate"] isConsumed:1];
    
    
      }




-(void)breakfastConsumedCompletesSuccessfully:(BOOL)success WithMessage:(NSString*)message{
    
    
    if (success) {
         [ProgressHUD dismiss];
        
        NSMutableDictionary *tempDict=[arrMyDietPlan objectAtIndex:SelectedIndexOFDiet];
        NSLog(@"selected dict data %@",tempDict);

        
        MyDietPlanDM *dbMyDietPlanDM =[[MyDietPlanDM alloc]init];
        [dbMyDietPlanDM updateTableOFMyDietPlanBreakFastDetails:[tempDict valueForKey:@"Startdate"] andbreakFastID:[tempDict valueForKey:@"idBreakfast"] anduserID:appDelegate.userData.str_userID];
        
        [tempDict setObject:@"1" forKey:@"isConsumedBreakFast"];
        
        
        [arrMyDietPlan replaceObjectAtIndex:SelectedIndexOFDiet withObject:tempDict];
        
        NSLog(@"here is array %@",arrMyDietPlan);
        
        
        [tblDietPlan reloadData];

        
        
    }
    else{
        [SupportingClass showErrorMessage:message];
    }

    
    
}




-(void)lunchConsumed:(UIButton*)sender{
    
    NSLog(@"Button Clicked index %ld",(long)sender.tag);
    
    
    NSInteger tag=sender.tag;
    NSInteger selectedIndexRow = tag-2000;
    sender.selected=!sender.selected;
    NSMutableDictionary *tempDict=[arrMyDietPlan objectAtIndex:selectedIndexRow];
    NSLog(@"selected dict data %@",tempDict);
    
    SelectedIndexOFDiet =selectedIndexRow;
    
    [ProgressHUD show:@"Getting your diet plan" Interaction:NO];

     [webServiceCaller lunchConsumedOfUser:appDelegate.userData.str_userID StartDate:[tempDict valueForKey:@"Startdate"] isConsumed:1];
    
    
}

-(void)lunchConsumedCompletesSuccessfully:(BOOL)success WithMessage:(NSString*)message{
    
    if (success) {
        [ProgressHUD dismiss];
        NSMutableDictionary *tempDict=[arrMyDietPlan objectAtIndex:SelectedIndexOFDiet];

        MyDietPlanDM *dbMyDietPlanDM =[[MyDietPlanDM alloc]init];
        [dbMyDietPlanDM updateTableOFMyDietPlanLunchDetails:[tempDict valueForKey:@"Startdate"] andLunchID:[tempDict valueForKey:@"idLunch"] anduserID:appDelegate.userData.str_userID];
        [tempDict setObject:@"1" forKey:@"isConsumedLunch"];
        [arrMyDietPlan replaceObjectAtIndex:SelectedIndexOFDiet withObject:tempDict];
        NSLog(@"here is array %@",arrMyDietPlan);
        
        [tblDietPlan reloadData];

    }
    else{
        [SupportingClass showErrorMessage:message];

    }

    
}





-(void)dinnerConsumed:(UIButton*)sender{
    
    NSLog(@"Button Clicked index %ld",(long)sender.tag);
    
    //idLunch,idDinner
    //isConsumedLunch,txtDinner,isConsumedDinner
    NSInteger tag=sender.tag;
    NSInteger selectedIndexRow = tag-3000;
    sender.selected=!sender.selected;
    NSMutableDictionary *tempDict=[arrMyDietPlan objectAtIndex:selectedIndexRow];
    
    NSLog(@"selected dict data %@",tempDict);
    SelectedIndexOFDiet =selectedIndexRow;
    
    [ProgressHUD show:@"Getting your diet plan" Interaction:NO];
    
    [webServiceCaller dinnerConsumedOfUser:appDelegate.userData.str_userID StartDate:[tempDict valueForKey:@"Startdate"] isConsumed:1];
        
}

-(void)dinnerConsumedCompletesSuccessfully:(BOOL)success WithMessage:(NSString*)message{
    
     if (success) {
         [ProgressHUD dismiss];

         NSMutableDictionary *tempDict=[arrMyDietPlan objectAtIndex:SelectedIndexOFDiet];

    MyDietPlanDM *dbMyDietPlanDM =[[MyDietPlanDM alloc]init];
    
    [dbMyDietPlanDM updateTableOFMyDietPlanDinnerDetails:[tempDict valueForKey:@"Startdate"] andDinnerID:[tempDict valueForKey:@"idDinner"] anduserID:appDelegate.userData.str_userID];
    [tempDict setObject:@"1" forKey:@"isConsumedDinner"];
    [arrMyDietPlan replaceObjectAtIndex:SelectedIndexOFDiet withObject:tempDict];
    NSLog(@"here is array %@",arrMyDietPlan);
    
    [tblDietPlan reloadData];
     }
     else{
         [SupportingClass showErrorMessage:message];

     }

}




-(void)goBackToHome:(id)sender{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrMyDietPlan.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSMutableDictionary *dict = [arrMyDietPlan objectAtIndex:indexPath.row];
    NSString *txtbreakfast = [[dict objectForKey:@"textBreakFast"] stringByReplacingOccurrencesOfString:@"," withString:@"\n\n"];
    NSString *txtLunch = [[dict objectForKey:@"textLunch"] stringByReplacingOccurrencesOfString:@"," withString:@"\n\n"];
    NSString *txtDinner = [[dict objectForKey:@"txtDinner"] stringByReplacingOccurrencesOfString:@"," withString:@"\n\n"];
    
       NSInteger width = screenWidth/3-8;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize: 14.0 weight:UIFontWeightLight]};
    // NSString class method: boundingRectWithSize:options:attributes:context is
    // available only on ios7.0 sdk.
    CGRect rectbreakfast = [txtbreakfast boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX)
                                                      options:NSStringDrawingUsesLineFragmentOrigin
                                                   attributes:attributes
                                                      context:nil];
    
    NSLog(@"here is breakfast height %f",rectbreakfast.size.height);
    
    
    CGRect rectlunch = [txtLunch boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX)
                                              options:NSStringDrawingUsesLineFragmentOrigin
                                           attributes:attributes
                                              context:nil];
    
    NSLog(@"here is lunch height %f",rectlunch.size.height);
    
    
    CGRect rectDinner = [txtDinner boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX)
                                                options:NSStringDrawingUsesLineFragmentOrigin
                                             attributes:attributes
                                                context:nil];
    NSLog(@"here is dinner height %f",rectDinner.size.height);
    
    
    CGFloat MaxHeight = rectbreakfast.size.height;
    
    if (MaxHeight<rectlunch.size.height)
    {
        MaxHeight = rectlunch.size.height;
    }
    if (MaxHeight<rectDinner.size.height)
    {
        MaxHeight = rectDinner.size.height;
    }
        
//    if (MaxHeight<180)
//    {
//        return 180;
//    }
//    
    return MaxHeight+150;
     
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
     return 60;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{

    return viewHeader;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier =[NSString stringWithFormat:@"TableViewCell%ld",(long)indexPath.row];
    
    TableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"TableViewCell" owner:self options:nil];
        for (id oneObject in nib)
            if ([oneObject isKindOfClass:[TableViewCell class]])
                cell = (TableViewCell *)oneObject;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
     NSMutableDictionary *dict = [arrMyDietPlan objectAtIndex:indexPath.row];;
    NSLog(@"%@",dict);
    
     [cell setValueOfCell:dict];
    
    cell.btnBreakfast.tag=1000+indexPath.row;
    cell.btnLunch.tag=2000+indexPath.row;
    cell.btnDinner.tag =3000+indexPath.row;
  
    [cell.btnBreakfast addTarget:self action:@selector(breakFastConsumed:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnLunch addTarget:self action:@selector(lunchConsumed:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnDinner addTarget:self action:@selector(dinnerConsumed:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
    }

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark -  Webservice delegate

-(void)getListOfDietPlanCompletesSuccessfully:(BOOL)success StatusCode:(NSInteger)statusCode WithList:(NSMutableArray*)arrDiets WithMessage:(NSString*)message{
    [ProgressHUD dismiss];
    
    if (statusCode == 401) {
        
        
        if (arrDiets.count>0) {
            // diet plan available
            MyDietPlanDM *db=[[MyDietPlanDM alloc]init];
            [db insertMyDietPlans:arrDiets];
        }
        [self filingDataOfDietPlan];
        
        
        
        }
    else if (statusCode == 402 )
    {
     //  diet plan expire with date
        
        AttemptedDietPlanViewController * objAttempted=[self.storyboard instantiateViewControllerWithIdentifier:@"AttemptedDietPlanViewController"];
        
        [self.navigationController pushViewController:objAttempted animated:YES];
        
        
        [SupportingClass showErrorMessage:message];

        
        
    }
    else if (statusCode == 403)
    {
     //  diet plan not available
        
        [SupportingClass showErrorMessage:message];

        
        
    }
    else{
        [SupportingClass showErrorMessage:message];
    }
}
@end
