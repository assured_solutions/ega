//
//  AttemptedDietPlanViewController.m
//  EGA
//
//  Created by Soniya on 19/12/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "AttemptedDietPlanViewController.h"
#import "ReduceWeightViewController.h"
#import "MyDietPlanViewController.h"

@interface AttemptedDietPlanViewController ()<WebServiceCallerDelegate>
{
    WebServiceCaller *webServiceCaller;

}
@end

@implementation AttemptedDietPlanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)doyouWantToGenerateSystemGeneratedDietPlan:(id)sender{
    [ProgressHUD show:@"Loading..." Interaction:NO];

    webServiceCaller = [[WebServiceCaller alloc]init];
    webServiceCaller.delegate = self;
    [webServiceCaller autoDietPlanInsertStatus:appDelegate.userData.str_userID];

}


-(void)autoDietPlanInsertStatusCompletesSuccessfully:(BOOL)success StatusCode:(NSInteger)statusCode WithMessage:(NSString*)message{
    [ProgressHUD dismiss];
    
    if (statusCode==403) {
        
        MyDietPlanViewController * objAttempted=[self.storyboard instantiateViewControllerWithIdentifier:@"MyDietPlanViewController"];
        objAttempted.goToHome = YES;
        [self.navigationController pushViewController:objAttempted animated:YES];

    }
    else
    {
        [SupportingClass showErrorMessage:message];
 
    }
    

}

-(IBAction)doYouWantToReviewYourBodyAnalysisQuestions:(id)sender{
    
    ReduceWeightViewController * objAttempted=[self.storyboard instantiateViewControllerWithIdentifier:@"ReduceWeightViewController"];
    
    [self.navigationController pushViewController:objAttempted animated:YES];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
