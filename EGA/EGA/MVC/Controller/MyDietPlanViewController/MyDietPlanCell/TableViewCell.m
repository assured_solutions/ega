//
//  TableViewCell.m
//  Practice
//
//  Created by animesh on 11/22/15.
//  Copyright © 2015 Tanzanite. All rights reserved.
//

#import "TableViewCell.h"

@implementation TableViewCell
@synthesize lblbreakfast;
@synthesize lblLunch;
@synthesize lblDinner;

@synthesize btnBreakfast;
@synthesize btnLunch;
@synthesize btnDinner;


@synthesize viewbreakfast;
@synthesize viewDinner;
@synthesize viewLunch;
@synthesize lblDate;

- (void)awakeFromNib {
    // Initialization code
}

-(void)setValueOfCell:(NSDictionary*)dictCellDetail{
    
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    CGFloat screenWidth = screenSize.width;
    NSString *txtbreakfast = [[dictCellDetail objectForKey:@"textBreakFast"] stringByReplacingOccurrencesOfString:@"," withString:@"\n\n"];
    NSString *txtLunch = [[dictCellDetail objectForKey:@"textLunch"] stringByReplacingOccurrencesOfString:@"," withString:@"\n\n"];
    NSString *txtDinner = [[dictCellDetail objectForKey:@"txtDinner"] stringByReplacingOccurrencesOfString:@"," withString:@"\n\n"];
    NSString *txtDate= [dictCellDetail objectForKey:@"Startdate"];
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize: 14.0 weight:UIFontWeightLight]};
    // NSString class method: boundingRectWithSize:options:attributes:context is
    // available only on ios7.0 sdk.
    CGRect rectbreakfast = [txtbreakfast boundingRectWithSize:CGSizeMake(screenWidth, CGFLOAT_MAX)
                                                      options:NSStringDrawingUsesLineFragmentOrigin
                                                   attributes:attributes
                                                      context:nil];
    
    NSLog(@"here is height %f",rectbreakfast.size.height);
    
    
    CGRect rectlunch = [txtLunch boundingRectWithSize:CGSizeMake(screenWidth, CGFLOAT_MAX)
                                              options:NSStringDrawingUsesLineFragmentOrigin
                                           attributes:attributes
                                              context:nil];
    
    NSLog(@"here is height %f",rectlunch.size.height);
    
    
    CGRect rectDinner = [txtDinner boundingRectWithSize:CGSizeMake(screenWidth, CGFLOAT_MAX)
                                                options:NSStringDrawingUsesLineFragmentOrigin
                                             attributes:attributes
                                                context:nil];
    NSLog(@"here is height %f",rectDinner.size.height);
    
    
    CGFloat MaxHeight = rectbreakfast.size.height;
    
    if (MaxHeight<rectlunch.size.height)
    {
        MaxHeight = rectlunch.size.height;
    }
    if (MaxHeight<rectDinner.size.height)
    {
        MaxHeight = rectDinner.size.height;
    }
    
    MaxHeight = MaxHeight+100;

    lblbreakfast.text = txtbreakfast;
    lblLunch.text = txtLunch;
    lblDinner.text = txtDinner;
    lblDate.text = [SupportingClass getStringDate:txtDate From:[SupportingClass dateFormat] to:DisplayDateFormat];
    
    NSInteger width = screenWidth/3-2;
    
    //Date Label
    CGRect viewFrame = lblDate.frame;
    viewFrame.size.height = 25;
    lblDate.frame = viewFrame;
    
    //Breakfast
    
    viewFrame = CGRectMake(0, 25, width, MaxHeight+50);
    viewbreakfast.frame=viewFrame;
    
    CGRect lblFrame =  CGRectMake(3, 0, width-6, MaxHeight);
    lblbreakfast.frame = lblFrame;
    
    CGFloat buttonWidth = width-20;
    CGRect btnFrame =  CGRectMake(width/2-buttonWidth/2,MaxHeight+10,buttonWidth, 30);
    btnBreakfast.frame = btnFrame;
    
    //Breakfast
    
    viewFrame.origin.x = width+1;
    viewLunch.frame=viewFrame;
 
    lblLunch.frame = lblFrame;
    btnLunch.frame = btnFrame;
    
    //Dinner
    
    viewFrame.origin.x = width*2+2;
    viewDinner.frame=viewFrame;
    
    lblDinner.frame = lblFrame;
    btnDinner.frame = btnFrame;
    
    NSInteger isBreakFastConsumed = [[dictCellDetail objectForKey:@"isConsumedBreakFast"] boolValue];
    if (isBreakFastConsumed) {
        btnBreakfast.backgroundColor = [UIColor colorWithHexString:GREENCOLOR];
        btnBreakfast.enabled = NO;
        [btnBreakfast setTitle:@"Consumed" forState:UIControlStateNormal];
    }
    else{
        btnBreakfast.backgroundColor = [UIColor colorWithHexString:THEMECOLOR];
        btnBreakfast.enabled = YES;
        [btnBreakfast setTitle:@"Consume" forState:UIControlStateNormal];
    }
    
    NSInteger isLunchConsumed = [[dictCellDetail objectForKey:@"isConsumedLunch"] boolValue];
    if (isLunchConsumed) {
        btnLunch.backgroundColor = [UIColor colorWithHexString:GREENCOLOR];
        btnLunch.enabled = NO;
        [btnLunch setTitle:@"Consumed" forState:UIControlStateNormal];
    }
    else{
        btnLunch.backgroundColor = [UIColor colorWithHexString:THEMECOLOR];
        btnLunch.enabled = YES;
        [btnLunch setTitle:@"Consume" forState:UIControlStateNormal];
    }
    
    NSInteger isDinnerConsumed = [[dictCellDetail objectForKey:@"isConsumedDinner"] boolValue];
    if (isDinnerConsumed) {
        btnDinner.backgroundColor = [UIColor colorWithHexString:GREENCOLOR];
        btnDinner.enabled = NO;
        [btnDinner setTitle:@"Consumed" forState:UIControlStateNormal];
    }
    else{
        btnDinner.backgroundColor = [UIColor colorWithHexString:THEMECOLOR];
        btnDinner.enabled = YES;
        [btnDinner setTitle:@"Consume" forState:UIControlStateNormal];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
@end
