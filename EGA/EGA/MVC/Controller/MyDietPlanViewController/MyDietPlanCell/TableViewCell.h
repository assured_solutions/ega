//
//  TableViewCell.h
//  Practice
//
//  Created by animesh on 11/22/15.
//  Copyright © 2015 Tanzanite. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblbreakfast;
@property (strong, nonatomic) IBOutlet UILabel *lblLunch;
@property (strong, nonatomic) IBOutlet UILabel *lblDinner;

@property (strong, nonatomic) IBOutlet UIButton *btnBreakfast;
@property (strong, nonatomic) IBOutlet UIButton *btnLunch;
@property (strong, nonatomic) IBOutlet UIButton *btnDinner;

@property(strong,nonatomic)IBOutlet UILabel *lblDate;

-(void)setValueOfCell:(NSDictionary*)dictCellDetail;

@property (weak, nonatomic) IBOutlet UIView *viewbreakfast;
@property (weak, nonatomic) IBOutlet UIView *viewLunch;

@property (weak, nonatomic) IBOutlet UIView *viewDinner;

@end
