//
//  TermsAndConditionViewController.m
//  EGA
//
//  Created by Soniya on 20/12/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "TermsAndConditionViewController.h"
#import "LoginEmailViewController.h"
@interface TermsAndConditionViewController ()

@end

@implementation TermsAndConditionViewController

#pragma mark - Memory Management
-(void)removeReferences{
}

#pragma mark - View Controller Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self screenDesignOfTermsAndCondition];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - ScreenDesigning Methods
-(void)screenDesignOfTermsAndCondition{
     self.navigationController.navigationBar.barTintColor = [ConfigurationFile getThemeColor];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma mark - Operational Methods

-(IBAction)showTCPrivacy:(UIButton*)sender{
    NSString *urlString = TERMSANDCONDITION;
    if (sender.tag == 201) {
        urlString = PRIVACY;
    }
    WebViewController * webViewController = [[WebViewController alloc]initWithNibName:@"WebViewController" bundle:nil];
    webViewController.urlAddress = urlString;
    webViewController.hideHistorySegment = YES;
    [self presentViewController:webViewController animated:YES completion:nil];
}

-(IBAction)agree:(id)sender{
    LoginEmailViewController *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginEmailViewController"];
    NSArray *viewController = [NSArray arrayWithObjects:loginVC, nil];
    [self.navigationController setViewControllers:viewController animated:YES];

}

@end
