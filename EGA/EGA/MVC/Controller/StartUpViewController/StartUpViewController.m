//
//  StartUpViewController.m
//  EGA
//
//  Created by Soniya on 26/09/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "StartUpViewController.h"
#import "TermsAndConditionViewController.h"
#import "DDPageControl.h"
#import "UserDM.h"
#import "DashboardViewController.h"

@interface StartUpViewController ()<UIScrollViewDelegate,WebServiceCallerDelegate>{
    WebServiceCaller *webServiceCaller;
}
@property (weak, nonatomic) IBOutlet UIImageView *imgEGALogo;
@end

@implementation StartUpViewController
#pragma mark - Memory Management
-(void)removeReferences{
    [webServiceCaller cancelWebserviceCall];
    webServiceCaller.delegate = nil;
    webServiceCaller = nil;
}

#pragma mark - View Controller Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
  [self screenDesignOfStartUp];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = NO;
}
#pragma mark - ScreenDesigning Methods
-(void)screenDesignOfStartUp{
    [self animateControlOutside];
   self.navigationController.navigationBar.barTintColor = [ConfigurationFile getThemeColor];
//  [self performSelector:@selector(animateControlOutside) withObject:nil afterDelay:0.2];
    webServiceCaller = [[WebServiceCaller alloc]init];
    webServiceCaller.delegate = self;

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - Operational Methods

-(void) animateControlOutside{

    [UIView animateWithDuration:1.5
                     animations:^
     {
         CGPoint center = self.imgEGALogo.center;
         self.imgEGALogo.transform = CGAffineTransformMakeScale(10,10);
         self.imgEGALogo.center = center;
            }
                     completion:^(BOOL finished) {
                         [self performSelector:@selector(goToViewController) withObject:nil afterDelay:1];
                     } ];
    }

-(IBAction)goToViewController{
   
    NSUserDefaults * standardUserDefaults = [NSUserDefaults standardUserDefaults];
   NSString *userid =  [standardUserDefaults objectForKey:@"LoggedInUserID"];

    
     UserDM *userDM = [[UserDM alloc]init];
   UserOM *userData = [userDM selectUserOfID:userid];
    if (userData==nil) {
        // LoginViewController *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
        [self navigateToTC];
    }
    else{
        
        if (appDelegate.isNetAvailable){
            
            NSDictionary *dictUser = [[NSDictionary alloc]initWithObjectsAndKeys:userData.str_email,@"emailid",userData.str_pwd,@"password", nil];
            [webServiceCaller loginUserWithData:dictUser];
            
        }
        else{
            [SupportingClass showErrorMessage:NETWORKERRORMESSAGE];
            
        }
        
     
    }
}
-(void)navigateToTC{
    TermsAndConditionViewController *tcVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TermsAndConditionViewController"];
    NSArray *viewController = [NSArray arrayWithObjects:tcVC, nil];
    [self.navigationController setViewControllers:viewController animated:YES];
    
    [self removeReferences];
}
-(void)navigateToDashboard{
   
    UIViewController *dashboardVC= [self.storyboard instantiateViewControllerWithIdentifier:@"DashboardViewController"];
    NSArray *viewController=[[NSArray alloc]initWithObjects:dashboardVC, nil];
    //self.navigationController.viewControllers=nil;
    [self.navigationController setViewControllers:viewController];
    dashboardVC=nil;
    [self removeReferences];
    
    
}
#pragma mark -  Webservice delegate

-(void)loginUserCompletesSuccessfully:(BOOL)success WithData:(NSMutableDictionary*)dictUserData WithMessage:(NSString*)message{
    [ProgressHUD dismiss];
    if (success) {
        UserDM *userDM = [[UserDM alloc]init];
        NSString *userID = [dictUserData objectForKey:@"user_id"];
        [userDM insertUser:dictUserData];
        
        appDelegate.userData = [userDM selectUserOfID:userID];
        
        NSUserDefaults * standardUserDefaults = [NSUserDefaults standardUserDefaults];
        [standardUserDefaults setObject:userID forKey:@"LoggedInUserID"];
        [self navigateToDashboard];
    }
    else{
        if ([message isEqualToString:NETWORKERRORMESSAGE]) {
            [SupportingClass showErrorMessage:message];
        }
        else {
            //[SupportingClass showAlertWithTitle:@"" Message:message CancelButtonTitle:@"OK"];
            [self navigateToTC];
        }
    }
}



@end
