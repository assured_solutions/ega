//
//  HealthTipsViewController.m
//  EGA
//
//  Created by Soniya on 12/12/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "HealthTipsViewController.h"
#import "HealthTipDetailViewController.h"

@interface HealthTipsViewController ()<WebServiceCallerDelegate>{
    WebServiceCaller *webServiceCaller;
    NSMutableArray *arrHealthTipsList;
}
@property (weak, nonatomic) IBOutlet UITableView *tblHealthTips;
@property (weak, nonatomic) IBOutlet UIView *vwMessage;

@end

@implementation HealthTipsViewController
#pragma mark - Memory Management

-(void)removeReferences{
    [webServiceCaller cancelWebserviceCall];
    webServiceCaller.delegate = nil;
    webServiceCaller = nil;
    
    
    [ProgressHUD dismiss];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self screenDesignOfHealthTip];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - ScreenDesigning Methods
-(void)screenDesignOfHealthTip{
    self.navigationItem.title = @"Health Tips";
    webServiceCaller = [[WebServiceCaller alloc]init];
    webServiceCaller.delegate = self;
    [self getHealthTips];
}
#pragma mark - Operational Methods
-(void)getHealthTips{
    if (appDelegate.isNetAvailable) {
        [ProgressHUD show:@"Loading..." Interaction:NO];
        [webServiceCaller cancelAllCalls];
        [webServiceCaller getHealthTipsForPageNo:1 PageCount:50];
        
    }
    else{
        [SupportingClass showErrorMessage:NETWORKERRORMESSAGE];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(CGFloat)getHeightForText:(NSInteger)section{
    NSDictionary *dictTips = [arrHealthTipsList objectAtIndex:section];
    NSString *quesText = [dictTips objectForKey:@"health_tips_title"];
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:16 weight:UIFontWeightLight]};
    // NSString class method: boundingRectWithSize:options:attributes:context is
    // available only on ios7.0 sdk.
    CGRect rect = [quesText boundingRectWithSize:CGSizeMake(_tblHealthTips.frame.size.width, CGFLOAT_MAX)
                                         options:NSStringDrawingUsesLineFragmentOrigin
                                      attributes:attributes
                                         context:nil];
    CGFloat height = rect.size.height+40;
    
    if (height<=80)
        return 80;
    return height;
}


#pragma mark - Table View DataSource And Delegate Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return arrHealthTipsList.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
       return [self getHeightForText:indexPath.row];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"HealthTipsCellIdentifier"];
    NSDictionary *dictTips = [arrHealthTipsList objectAtIndex:indexPath.row];
    NSString *quesText = [dictTips objectForKey:@"health_tips_title"];
    cell.textLabel.text = quesText;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dictTips = [arrHealthTipsList objectAtIndex:indexPath.row];
    HealthTipDetailViewController *healthDetail = [self.storyboard instantiateViewControllerWithIdentifier:@"HealthTipDetailViewController"];
    healthDetail.dictTipDetails = dictTips;
    [self.navigationController pushViewController:healthDetail animated:YES];
}
#pragma mark -  Webservice delegate

-(void)getHealthTipsCompletesSuccessfully:(BOOL)success WithList:(NSMutableArray*)arrTips WithMessage:(NSString*)message{
    [ProgressHUD dismiss];
   
    if (success) {
        arrHealthTipsList = arrTips;
        if (arrHealthTipsList.count==0) {
            _vwMessage.hidden = NO;
            _tblHealthTips.hidden = YES;
        }
        else{
            _vwMessage.hidden = YES;
            _tblHealthTips.hidden = NO;
           [_tblHealthTips reloadData];
        }
    }
    else{
        [SupportingClass showErrorMessage:message];
    }
   
}

@end
