//
//  HealthTipDetailViewController.m
//  EGA
//
//  Created by Soniya on 12/12/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "HealthTipDetailViewController.h"
#import "AsyncImageView.h"
@interface HealthTipDetailViewController ()
{
    AsyncImageView *asyncImageView;
}
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UIImageView *imgHealthTip;

@end

@implementation HealthTipDetailViewController
@synthesize dictTipDetails;
#pragma mark - View Controller Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self screenDesignOfHealthTipDetail];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - ScreenDesigning Methods
-(void)screenDesignOfHealthTipDetail{
     [self setTitle:@"Health Tips" SubTitle:@"Tip for you"];
   
    _lblTitle.text = [dictTipDetails objectForKey:@"health_tips_title"];
    [_lblTitle sizeToFit];
    
    _lblDescription.text = [dictTipDetails objectForKey:@"health_tips_description"];
    [_lblDescription sizeToFit];
    
    CGFloat yAxis = 15;
    CGRect frame = _lblTitle.frame;
    frame.origin.x = 10;
    frame.origin.y = yAxis;
    frame.size.width = screenWidth - 20;
    _lblTitle.frame = frame;
    
    yAxis+=frame.size.height+15;
    
    frame = _lblDescription.frame;
     frame.origin.x = 10;
    frame.origin.y = yAxis;
    frame.size.width = screenWidth - 20;
    _lblDescription.frame = frame;
    
     yAxis+=frame.size.height+15;
    
    frame = _imgHealthTip.frame;
     frame.origin.x = 10;
    frame.origin.y = yAxis;
    frame.size.width = frame.size.height = screenWidth-20;
    _imgHealthTip.frame=frame;
    
     yAxis+=frame.size.height+15;
    
    _scrollView.contentSize = CGSizeMake(screenWidth, yAxis);
    
    NSString *imgUrl = [dictTipDetails objectForKey:@"health_image"];
    asyncImageView = [[AsyncImageView alloc]init];
    [asyncImageView loadImageFromURL:imgUrl imageview:_imgHealthTip];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
