//
//  HealthTipDetailViewController.h
//  EGA
//
//  Created by Soniya on 12/12/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "BaseViewController.h"

@interface HealthTipDetailViewController : BaseViewController

@property(nonatomic,strong)NSDictionary *dictTipDetails;
@end
