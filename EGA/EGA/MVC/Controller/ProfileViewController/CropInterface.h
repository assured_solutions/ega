
//
//  PhotoCrop.h
//  SmartProfiler
//
//  Created by animesh bansal on 5/19/15.
//  Copyright (c) 2015 Mahindra Comviva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CropInterface : UIImageView {
    BOOL isPanning;
    NSInteger currentTouches;
    CGPoint panTouch;
    CGFloat scaleDistance;
    UIView *currentDragView; 
    
    UIView *topView;
    UIView *bottomView;
    UIView *leftView;
    UIView *rightView;
    
    UIView *topLeftView;
    UIView *topRightView;
    UIView *bottomLeftView;
    UIView *bottomRightView;
}
@property (nonatomic, assign) CGRect crop;
@property (nonatomic, strong) UIView *cropView;
@property (nonatomic, strong) UIColor *shadowColor;
@property (nonatomic, strong) UIColor *borderColor;

- (id)initWithFrame:(CGRect)frame andImage:(UIImage *)image;
- (UIImage*)getCroppedImage;

@end
