//
//  ProfileViewController.m
//  EGA
//
//  Created by Soniya on 18/11/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "ProfileViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "ProfileSubViewController.h"
#import "UserDM.h"
#import "PhotoCropViewController.h"

#define Water @"Water"
#define Fire @"Fire"
#define Air @"Air"

@interface ProfileViewController ()<WebServiceCallerDelegate,UIImagePickerControllerDelegate,ProfileSubViewControllerDelegate,PhotoCropViewControllerDelegate>
{
    PhotoCropViewController *cropPhoto;
    WebServiceCaller *webServiceCaller;
    UIImagePickerController *pickerController;
    NSString *strProfilePicName;
    BOOL webServiceCalled;
    NSDictionary *dictProfileAnalysisDetails;
    __weak IBOutlet UIButton *btnProfile;
    
    __weak IBOutlet UIButton *btnBiologicalAnalytics;
    
}

@property (weak, nonatomic) IBOutlet UIView *viewBackContentTextfields;
@property (weak, nonatomic) IBOutlet UIView *vwBodyAnalysis;
@property (weak, nonatomic) IBOutlet UITextField *txtFName;
@property (weak, nonatomic) IBOutlet UITextField *txtLName;
@property (weak, nonatomic) IBOutlet UITextField *txtEmailAddr;
@property (weak, nonatomic) IBOutlet UITextField *txtDOB;
@property (weak, nonatomic) IBOutlet UITextField *txtWeight;
@property (weak, nonatomic) IBOutlet UITextField *txtHeight;
@property (weak, nonatomic) IBOutlet UISegmentedControl *genderSegmentControl;
@property (weak, nonatomic) NSString *strPassword;
@property(weak,nonatomic)IBOutlet UIScrollView *scrollview;

//Photo page codeing

@property (strong, nonatomic) IBOutlet UIButton *btnProfilePic;


//Body Analysis
@property (weak, nonatomic) IBOutlet UIScrollView *BAScrollView;
@property (weak, nonatomic) IBOutlet UILabel *lblBodyType;
@property (weak, nonatomic) IBOutlet UIImageView *imgBodyType;
@property (weak, nonatomic) IBOutlet UITextField *txtBAHeight;
@property (weak, nonatomic) IBOutlet UITextField *txtBAWeight;
@property (weak, nonatomic) IBOutlet UITextField *txtBAIdealWeight;
@property (weak, nonatomic) IBOutlet UITextField *txtBAOverWeight;
@property (weak, nonatomic) IBOutlet UILabel *lblBAOverWeight;
@property (weak, nonatomic) IBOutlet UITextField *txtBATargetWeightLoss;
@end

@implementation ProfileViewController

@synthesize delegate;
@synthesize txtFName;
@synthesize txtLName;
@synthesize txtEmailAddr;
@synthesize txtDOB;
@synthesize txtWeight;
@synthesize txtHeight;
@synthesize genderSegmentControl;
@synthesize scrollview;
@synthesize viewBackContentTextfields;
@synthesize btnProfilePic;

#pragma mark - Memory Management
-(void)removeReferences{
    txtFName=nil;
    txtLName=nil;
    txtEmailAddr=nil;
    txtDOB=nil;
    txtWeight=nil;
    txtHeight=nil;
    genderSegmentControl=nil;
    
    [webServiceCaller cancelWebserviceCall];
    webServiceCaller.delegate = nil;
    webServiceCaller = nil;
    
    
    [ProgressHUD dismiss];
}

#pragma mark - View Controller Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self screenDesigningForProfile];
 
    // Do any additional setup after loading the view.
}
- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self setProfileImage];
    
    // Do any additional setup after loading the view.
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
     if ([[segue identifier] isEqualToString:@"ProfileSubViewPasswordSegue"])
     {
         ProfileSubViewController *profileSubView = segue.destinationViewController;
         profileSubView.delegate = self;
         profileSubView.isChangePassword = YES;
     }
 }

#pragma mark - ScreenDesigning Methods
-(void)screenDesigningForProfile{
    
     self.navigationItem.title = @"My profile";

    
    strProfilePicName = [NSString stringWithFormat:@"%@ProfilePic.png",appDelegate.userData.str_userID];
    
    _strPassword = appDelegate.userData.str_pwd;
    
    txtFName.text=appDelegate.userData.str_firstname;
    txtLName.text=appDelegate.userData.str_lastname;
    txtEmailAddr.text=appDelegate.userData.str_email;
    txtDOB.text= [SupportingClass getStringDate:appDelegate.userData.str_dob From:[SupportingClass dateFormat] to:@"dd MMM yyyy"];
    txtWeight.text=appDelegate.userData.str_weight;
    txtHeight.text=appDelegate.userData.str_height;
    genderSegmentControl.selectedSegmentIndex=[appDelegate.userData.str_gender isEqualToString:@"f"]?1:0;
    [self createKeyboardAccessoryToolBar:YES];
    
    txtFName.inputAccessoryView = accessoryToolBar;
    txtLName.inputAccessoryView = accessoryToolBar;
    txtEmailAddr.inputAccessoryView = accessoryToolBar;
    
    txtWeight.inputAccessoryView = accessoryToolBar;
    txtHeight.inputAccessoryView = accessoryToolBar;
    
    UIDatePicker *datePicker = [[UIDatePicker alloc]init];
    datePicker.datePickerMode = UIDatePickerModeDate;
    datePicker.maximumDate = [NSDate date];
    [datePicker addTarget:self action:@selector(updateDOB:) forControlEvents:UIControlEventValueChanged];
    txtDOB.inputView = datePicker;
    
    webServiceCaller = [[WebServiceCaller alloc]init];
    webServiceCaller.delegate = self;
    
    // Picker Controller Init
    pickerController = [[UIImagePickerController alloc] init];
    pickerController.delegate = self;
    pickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
   
    
    scrollview.contentSize=CGSizeMake(viewBackContentTextfields.frame.size.width,viewBackContentTextfields.frame.size.height);
    
    if (_completeYourProfile) {
        [SupportingClass showAlertWithTitle:@"" Message:@"Please complete your profile, It will help us to know you more." CancelButtonTitle:@"OK"];
    }
    
    [self setProfileImage];
}
/*
 {
 "height": "167.00"
 "weight": "67.00"
 "bmi": "45"
 "ideal_weight": "50.00"
 "over_weight": "10.00"
 "reduce_weight": "5.00"
 "bodytype": "Air"
 "total_score": "29.00"
 "crnt_date": "2015-12-12"
 "status": "1"
 "message": "Successfully Uploaded..."
 }
 */
-(void)screenDesigningForBiologicalAnalysis{
     _BAScrollView.contentSize=CGSizeMake(_vwBodyAnalysis.frame.size.width,_vwBodyAnalysis.frame.size.height);
    
    NSString *value = [dictProfileAnalysisDetails objectForKey:@"height"];
    _txtBAHeight.text = [NSString stringWithFormat:@"%@ cm", value.length>0?value:@"--"];
    value = [dictProfileAnalysisDetails objectForKey:@"weight"];
     _txtBAWeight.text = [NSString stringWithFormat:@"%@ kg", value.length>0?value:@"--"];
    value = [dictProfileAnalysisDetails objectForKey:@"ideal_weight"];
     _txtBAIdealWeight.text = [NSString stringWithFormat:@"%@ kg", value.length>0?value:@"--"];
    
      value = [dictProfileAnalysisDetails objectForKey:@"over_weight"];
    NSString *weight = @"over";
    float diff = [[dictProfileAnalysisDetails objectForKey:@"ideal_weight"] floatValue]-[[dictProfileAnalysisDetails objectForKey:@"weight"] floatValue];
    if (diff>0) {
        weight = @"under";
    }

    _lblBAOverWeight.text = [NSString stringWithFormat:@"You are %@ weight by",weight];
     _txtBAOverWeight.text = [NSString stringWithFormat:@"%@ kg",value.length>0?value:@"0"];
    
    value = [dictProfileAnalysisDetails objectForKey:@"reduce_weight"];
    value = value.length>0?[NSString stringWithFormat:@"%0.1f",[value floatValue]]:@"--";
    _txtBATargetWeightLoss.text = [NSString stringWithFormat:@"%@ kg/fortnight", value];
    
    NSString *bodyType = [dictProfileAnalysisDetails objectForKey:@"bodytype"];
    _lblBodyType.text = [[NSString stringWithFormat:@"YOUR BODY TYPE IS \"%@\"",bodyType] uppercaseString];
    
    NSString *imageName = @"BodyTypeAir";
    if ([bodyType isEqualToString:Water]) {
        
        imageName = @"BodyTypeWater";
    }
    else if([bodyType isEqualToString:Fire]){
        imageName = @"BodyTypeFire";
    }
    _imgBodyType.image = [UIImage imageNamed:imageName];

}
#pragma mark - Operational Methods
- (IBAction)submitClicked:(id)sender {
    if (appDelegate.isNetAvailable){
        if ([self validateAllFields]){
            [ProgressHUD show:@"Updating Profile..." Interaction:NO];
            NSString *gender = genderSegmentControl.selectedSegmentIndex==0?@"m":@"f";
            NSDateFormatter *dateFomratter = [[NSDateFormatter alloc]init];
            dateFomratter.dateFormat = @"dd MMM yyyy";
            NSDate *date = [dateFomratter dateFromString:txtDOB.text];
            dateFomratter.dateFormat = @"yyyy-MM-dd";
            NSString *selectedDate = [dateFomratter stringFromDate:date];
            NSDictionary *dictUser = [[NSDictionary alloc]initWithObjectsAndKeys:txtEmailAddr.text,@"emailid",_strPassword,@"password",txtFName.text,@"firstname",txtLName.text,@"lastname",selectedDate,@"dob",gender,@"gender",txtWeight.text,@"weight",txtHeight.text,@"height",nil];//"","phone","","bloodgroup","","address","","phone", nil];
            [webServiceCaller updateProfile:dictUser OfUser:appDelegate.userData.str_userID];
        }
    }
    else{
        [SupportingClass showErrorMessage:NETWORKERRORMESSAGE];
        
    }
}
-(BOOL)validateAllFields{
    
    txtFName.text=[txtFName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    txtLName.text=[txtLName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    txtEmailAddr.text=[txtEmailAddr.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    txtDOB.text=[txtDOB.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    txtWeight.text=[txtWeight.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    txtHeight.text=[txtHeight.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSString* fname = txtFName.text;
    NSString* lname = txtLName.text;
   // NSString* email = txtEmailAddr.text;
    NSString* dob = txtDOB.text;
    NSString* weight = txtWeight.text;
    NSString* height = txtHeight.text;
 
    //Check whether textField are left empty or not
    NSString *errorMessage = nil;
    
    if (fname.length<=0) {
        errorMessage = @"Please provide your first name";
    }
    else  if (lname.length<=0) {
        errorMessage = @"Please provide your last name";
    }
    else if([SupportingClass isHavingSpecialCharacter:fname] || [SupportingClass isHavingSpecialCharacter:lname]){
        errorMessage = [SupportingClass specialCharacterErrorMessage];
    }
//    else if(email.length<=0){
//        errorMessage = [SupportingClass blankEmailIDErrorMessage];
//    }
//    else if(![SupportingClass isNSStringValidEmail:email]){
//        errorMessage = [SupportingClass invalidEmaildIDFormatErrorMessage];
//    }
    else if(dob.length<=0){
        errorMessage = [SupportingClass blankDOBErrorMessage];
    }
    
    else if (weight.length<=0) {
        errorMessage = @"Please provide your weight";
    }
    else if (height.length<=0) {
        errorMessage = @"Please provide your height";
    }
    
    [self doneEditingButtonClicked];
    if (errorMessage!=nil) {
        [SupportingClass showErrorMessage:errorMessage];
        return NO;
    }
    return YES;
}
-(void)updateDOB:(UIDatePicker*)sender{
    NSDateFormatter *dateFomratter = [[NSDateFormatter alloc]init];
    dateFomratter.dateFormat = @"dd MMM yyyy";
    txtDOB.text = [dateFomratter stringFromDate:sender.date];
}
- (IBAction)profileClicked:(UIButton*)sender {
        btnProfile.backgroundColor = [UIColor colorWithHexString:SUBTHEMECOLOR];
        btnBiologicalAnalytics.backgroundColor = [UIColor colorWithHexString:THEMECOLOR];
    
    scrollview.hidden = NO;
    _BAScrollView.hidden = YES;
    
    
}
- (IBAction)bodyAnalysisClicked:(UIButton*)sender {
    btnProfile.backgroundColor = [UIColor colorWithHexString:THEMECOLOR];
    btnBiologicalAnalytics.backgroundColor = [UIColor colorWithHexString:SUBTHEMECOLOR];

    scrollview.hidden = YES;
    _BAScrollView.hidden = NO;
    if (!webServiceCalled) {
        [ProgressHUD  show:@"" Interaction:NO];
        [webServiceCaller getAnalyticalUserDetailsForUserID:appDelegate.userData.str_userID];
    }
}


#pragma mark - Profile Pic

-(void)showPictureCropperForImage:(UIImage*)image{
    cropPhoto = [self.storyboard instantiateViewControllerWithIdentifier:@"PhotoCropViewController"];
    cropPhoto.callback=self;
    cropPhoto.originalImage = image;
    self.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:cropPhoto animated:YES completion:nil];
}
-(void)sendProfilePic:(UIImage*)image{
    if (appDelegate.isNetAvailable){
            [ProgressHUD show:@"Updating..." Interaction:NO];
            [webServiceCaller updateProfileImage:image OfUser:appDelegate.userData.str_userID];
    }
    else{
        [SupportingClass showErrorMessage:NETWORKERRORMESSAGE];
        
    }

}
// Getting Profile image if exist
-(void)setProfileImage{
    
    UIImage *img = [UIImage imageWithContentsOfFile:[self returnImageWithName:strProfilePicName]];
     if (img==nil) {
         img=[UIImage imageNamed:@"ProfilePicDefault"];
    }
    CGRect frame = btnProfilePic.frame;
    CGFloat width = frame.size.width;
    if (frame.size.height>width) {
        width = frame.size.height;
    }
    
    frame.size.height = frame.size.width = width;
    btnProfilePic.frame=frame;
    btnProfilePic.imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    [btnProfilePic setBackgroundImage:img forState:UIControlStateNormal];
    
    btnProfilePic.layer.cornerRadius = btnProfilePic.frame.size.width/2;//half of the width
    
    btnProfilePic.layer.borderColor = [UIColor colorWithHexString:BORDERCOLOR].CGColor;
    btnProfilePic.layer.borderWidth=1.0f;
    
    btnProfilePic.layer.masksToBounds = YES;

}


- (IBAction)picProfilePhoto:(UIButton*)sender {
UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Choose Profile Pic" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];

//    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
//        
//        // Camera  button tappped.
//        [SupportingClass showErrorMessage:@"Under Development"];
//    }]];
//
    
[actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
    
    // Camera  button tappped.
    [self openImagePickers:YES];
}]];

[actionSheet addAction:[UIAlertAction actionWithTitle:@"Photo Library" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
    
    // PhotoLibrary button tapped.
    [self openImagePickers:NO];
    
}]];

[actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
    
    // Cancel button tapped.
    
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}]];


// Present action sheet.
[self presentViewController:actionSheet animated:YES completion:nil];
}
-(void)openImagePickers:(BOOL)isCamera{
    if (isCamera) {
        pickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    else{
        pickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
//    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    [self presentViewController:pickerController animated:YES completion:nil];
}
//getting path of image from document folder

- (NSString *) returnImageWithName:(NSString *)strImageName
{
    NSArray  *paths	= NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docuPath = [paths objectAtIndex:0];
    NSString *newDocumentPath=[NSString stringWithFormat:@"%@/ProfilePics/%@",docuPath,strImageName];
    return newDocumentPath;
}
#pragma mark - ImagePicker delegate methods

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
    UIImage *originalImage, *editedImage, *imageToUse;
    
    if (CFStringCompare ((CFStringRef) mediaType, kUTTypeImage, 0) == kCFCompareEqualTo) {
        
        editedImage = (UIImage *) [info objectForKey: UIImagePickerControllerEditedImage]; // Edited image if available
        originalImage = (UIImage *) [info objectForKey: UIImagePickerControllerOriginalImage];
        imageToUse = editedImage ? editedImage : originalImage;
        
        [picker dismissViewControllerAnimated:YES completion:nil];
       // [self sendProfilePic:imageToUse];
        [self showPictureCropperForImage:imageToUse];
        
    }
    
}

#pragma mark - TextField Navigation Methodsbase
-(void)doneEditingButtonClicked
{
    [self.txtFName resignFirstResponder];
    [self.txtLName resignFirstResponder];
    [self.txtEmailAddr resignFirstResponder];
    [self.txtDOB resignFirstResponder];
    [self.txtWeight resignFirstResponder];
    [self.txtHeight resignFirstResponder];
    
}
-(void)prevButtonClicked
{
    if (activeTextField==self.txtHeight) {
        [self.txtWeight becomeFirstResponder];
    }
    else if (activeTextField==self.txtWeight) {
        [self.txtDOB becomeFirstResponder];
    }
//    else if (activeTextField==self.txtDOB) {
//        [self.txtEmailAddr becomeFirstResponder];
//    }
    else if (activeTextField==self.txtDOB) {
        [self.txtLName becomeFirstResponder];
    }
    else if (activeTextField==self.txtLName) {
        [self.txtFName becomeFirstResponder];
    }
    else if (activeTextField==self.txtFName) {
        [self.txtHeight becomeFirstResponder];
    }
    
}
-(void)nextButtonClicked
{
    if (activeTextField==self.txtFName) {
        [self.txtLName becomeFirstResponder];
    }
    else if (activeTextField==self.txtLName) {
        [self.txtDOB becomeFirstResponder];
    }
//    else if (activeTextField==self.txtEmailAddr) {
//        [self.txtDOB becomeFirstResponder];
//    }
    else if (activeTextField==self.txtDOB) {
        [self.txtWeight becomeFirstResponder];
    }
    else if (activeTextField==self.txtWeight) {
        [self.txtHeight becomeFirstResponder];
    }
   
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    activeTextField = textField;
}

#pragma mark - Profile delegate

-(void)passwordChangedTo:(NSString*)password{
    _strPassword = password;
}

#pragma mark - Photo Crop delegate

-(void)imageWhichisGoingToUploadedToServer:(UIImage *)image{
     [self sendProfilePic:image];
}
#pragma mark -  Webservice delegate

-(void)updateProfileCompletesSuccessfully:(BOOL)success WithData:(NSMutableDictionary*)dictUserData WithMessage:(NSString*)message{
    [ProgressHUD dismiss];
    if (success) {
        [ProgressHUD showSuccess:message Interaction:NO];
        UserDM *userDM = [[UserDM alloc]init];
        NSString *userID = appDelegate.userData.str_userID;
        [userDM updateProfile:dictUserData OfUser:userID];
        
        appDelegate.userData = [userDM selectUserOfID:userID];
         _strPassword = appDelegate.userData.str_pwd;
           }
    else{
        if ([message isEqualToString:NETWORKERRORMESSAGE]) {
            [SupportingClass showErrorMessage:message];
        }
        else {
            [SupportingClass showAlertWithTitle:@"" Message:message CancelButtonTitle:@"OK"];
        }
    }

}
-(void)updateProfileImageCompletesSuccessfully:(BOOL)success WithData:(UIImage*)img WithMessage:(NSString*)message{
    [ProgressHUD dismiss];
    if (success) {
      BOOL success =  [SupportingClass saveImageInDocumentFolder:img FolderName:@"ProfilePics" FileName:strProfilePicName];
        if (success) {
            [self setProfileImage];
        }
        else{
             [SupportingClass showErrorMessage:@"Not able to save your profile image, Please try later"];
        }
    }
    else{
        if ([message isEqualToString:NETWORKERRORMESSAGE]) {
            [SupportingClass showErrorMessage:message];
        }
        else {
            [SupportingClass showAlertWithTitle:@"" Message:message CancelButtonTitle:@"OK"];
        }
    }
    
}
-(void)getAnalyticalUserDetailsCompletesSuccessfully:(BOOL)success WithData:(NSDictionary*)dictResult WithMessage:(NSString*)message{
    [ProgressHUD dismiss];
    if (success) {
        dictProfileAnalysisDetails = dictResult;
        _BAScrollView.hidden = NO;
        [self screenDesigningForBiologicalAnalysis];
        webServiceCalled = YES;
    }
    else{
        if ([message isEqualToString:NETWORKERRORMESSAGE]) {
            [SupportingClass showErrorMessage:message];
        }
        else {
            [SupportingClass showAlertWithTitle:@"" Message:message CancelButtonTitle:@"OK"];
        }
        _BAScrollView.hidden = YES;
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
