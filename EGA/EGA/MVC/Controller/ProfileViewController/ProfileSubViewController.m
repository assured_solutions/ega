//
//  ProfileSubViewController.m
//  EGA
//
//  Created by Soniya on 11/12/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "ProfileSubViewController.h"

@interface ProfileSubViewController ()
@property (weak, nonatomic) IBOutlet UITextField *txtOldPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtNewPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtRetypePassword;

@end

@implementation ProfileSubViewController
@synthesize isChangePassword;

#pragma mark - View Controller Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self screenDesigningForProfile];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - ScreenDesigning Methods
-(void)screenDesigningForProfile{
    
    [self setTitle:@"My profile" SubTitle:@"Change Password"];
    [self createKeyboardAccessoryToolBar:YES];
    if (isChangePassword) {
        _txtOldPassword.inputAccessoryView = accessoryToolBar;
        _txtNewPassword.inputAccessoryView = accessoryToolBar;
        _txtRetypePassword.inputAccessoryView = accessoryToolBar;
    }
   }
#pragma mark - Operational Methods

-(BOOL)validateAllPasswordFields{
    
    _txtOldPassword.text=[_txtOldPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    _txtNewPassword.text=[_txtNewPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    _txtRetypePassword.text=[_txtRetypePassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
   
    
    NSString* oldPwd = _txtOldPassword.text;
    NSString* newPwd = _txtNewPassword.text;
    NSString* retypePwd = _txtRetypePassword.text;
  
    
    //Check whether textField are left empty or not
    NSString *errorMessage = nil;
    
    if (oldPwd.length<=0) {
        errorMessage = @"Old password can't left empty";
    }
    else if(![oldPwd isEqualToString:appDelegate.userData.str_pwd]){
        errorMessage = @"Please enter correct old password";
    }
    else  if (newPwd.length<=0) {
        errorMessage = @"New password can't left empty";
    }
    else if (newPwd.length<=5) {
        errorMessage = [SupportingClass invalidDigitsInPasswordErrorMessage];
    }
    else  if (retypePwd.length<=0) {
        errorMessage = @"Retype New password can't left empty";
    }
    else if(![newPwd isEqualToString:retypePwd]){
        errorMessage =  [SupportingClass passwordMissMatchErrorMessage];
    }
    
    [self doneEditingButtonClicked];
    if (errorMessage!=nil) {
        [SupportingClass showErrorMessage:errorMessage];
        return NO;
    }
    return YES;
}
- (IBAction)cancelController:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)changePasswordClicked:(UIButton *)sender {
          if ([self validateAllPasswordFields]){
              if(self.delegate!=nil && [(id)[self delegate] respondsToSelector:@selector(passwordChangedTo:)])
              {
                  [(id)[self delegate] passwordChangedTo:_txtNewPassword.text];
              }
              [self cancelController:nil];
        }

}
#pragma mark - TextField Navigation Methodsbase
-(void)doneEditingButtonClicked
{
    [self.txtOldPassword resignFirstResponder];
    [self.txtNewPassword resignFirstResponder];
    [self.txtRetypePassword resignFirstResponder];
 }
-(void)prevButtonClicked
{
    if (isChangePassword) {
        if (activeTextField==self.txtRetypePassword) {
            [self.txtNewPassword becomeFirstResponder];
        }
        else if (activeTextField==self.txtNewPassword) {
            [self.txtOldPassword becomeFirstResponder];
        }
        else if (activeTextField==self.txtOldPassword) {
            [self.txtRetypePassword becomeFirstResponder];
        }
    }
   
}
-(void)nextButtonClicked
{
     if (isChangePassword) {
         if (activeTextField==self.txtOldPassword) {
             [self.txtNewPassword becomeFirstResponder];
         }
         else if (activeTextField==self.txtNewPassword) {
             [self.txtRetypePassword becomeFirstResponder];
         }
         else if (activeTextField==self.txtRetypePassword) {
             [self.txtOldPassword becomeFirstResponder];
         }
     }
  
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    activeTextField = textField;
}

@end
