//
//  PhotoCropViewController.h
//  SmartProfiler
//
//  Created by animesh bansal on 5/19/15.
//  Copyright (c) 2015 Mahindra Comviva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CropInterface.h"

@protocol PhotoCropViewControllerDelegate;
@interface PhotoCropViewController : UIViewController
@property (nonatomic, strong) IBOutlet UIImageView *displayImage;
@property (nonatomic, strong) UIImage *originalImage;
@property (nonatomic, strong) CropInterface *cropper;
@property(nonatomic, assign) id <PhotoCropViewControllerDelegate>callback;

- (IBAction)cropPressed:(id)sender;
- (IBAction)originalPressed:(id)sender;

@end
@protocol PhotoCropViewControllerDelegate <NSObject>
-(void)imageWhichisGoingToUploadedToServer:(UIImage *)image;
@end

