//
//  photoPage.m
//  SmartProfiler
//
//  Created by  Animesh Bansal on 19/05/15.
//  Copyright (c) 2015 Mahindra Comviva. All rights reserved.
//
#import "CropInterface.h"
#import <QuartzCore/QuartzCore.h>

#define IMAGE_CROPPER_OUTSIDE_STILL_TOUCHABLE 40.0f
#define IMAGE_CROPPER_INSIDE_STILL_EDGE 20.0f

#ifndef CGWidth
#define CGWidth(rect)                   rect.size.width
#endif

#ifndef CGHeight
#define CGHeight(rect)                  rect.size.height
#endif

#ifndef CGOriginX
#define CGOriginX(rect)                 rect.origin.x
#endif

#ifndef CGOriginY
#define CGOriginY(rect)                 rect.origin.y
#endif

@implementation CropInterface

- (id)initWithFrame:(CGRect)frame andImage:(UIImage *)image
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.contentMode = UIViewContentModeScaleAspectFit;
        self.userInteractionEnabled = YES;

        // set image to crop
        self.image = image;

        topView = [self newEdgeView];
        bottomView = [self newEdgeView];
        leftView = [self newEdgeView];
        rightView = [self newEdgeView];
        topLeftView = [self newCornerView];
        topRightView = [self newCornerView];
        bottomLeftView = [self newCornerView];
        bottomRightView = [self newCornerView];
        
        [self initialCropView];
    }
    return self;
}

- (void)initialCropView {
    CGFloat width;
    CGFloat height;
    CGFloat x;
    CGFloat y;
    
    width  = 200;
    height = 200;
    x      = (self.frame.size.width - width) / 2;
    y      = (self.frame.size.height - height) / 2;
    
    UIView* cropView = [[UIView alloc] initWithFrame:CGRectMake(x, y, width, height)];
    cropView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    cropView.layer.borderColor =[UIColor colorWithRed:64/255.0 green:176.0/255.0 blue:251.0/255.0 alpha:1.0].CGColor; 
    cropView.layer.borderWidth = 2.0;
    cropView.backgroundColor = [UIColor clearColor];
    
    UIImageView *tlnode = [[UIImageView alloc]init];
    UIImageView *trnode =[[UIImageView alloc]init];
    UIImageView *blnode = [[UIImageView alloc]init];
    UIImageView *brnode =[[UIImageView alloc]init];
    tlnode.frame = CGRectMake(cropView.bounds.origin.x - 13, cropView.bounds.origin.y -13, 26, 26);
    trnode.frame = CGRectMake(cropView.frame.size.width - 13, cropView.bounds.origin.y -13, 26, 26);
    blnode.frame = CGRectMake(cropView.bounds.origin.x - 13, cropView.frame.size.height - 13, 26, 26);
    brnode.frame = CGRectMake(cropView.frame.size.width - 13, cropView.frame.size.height - 13, 26, 26);
    
    tlnode.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
    trnode.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin;
    blnode.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
    brnode.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin;
    
    [cropView addSubview:tlnode];
    [cropView addSubview:trnode];
    [cropView addSubview:blnode];
    [cropView addSubview:brnode];
    
    self.cropView = cropView;
    [self addSubview:self.cropView];
    
    [self updateBounds];
}

#pragma mark - setters

- (void)setShadowColor:(UIColor *)shadowColor {
    _shadowColor = shadowColor;
    topView.backgroundColor = _shadowColor;
    bottomView.backgroundColor = _shadowColor;
    leftView.backgroundColor = _shadowColor;
    rightView.backgroundColor = _shadowColor;
    topLeftView.backgroundColor = _shadowColor;
    topRightView.backgroundColor = _shadowColor;
    bottomLeftView.backgroundColor = _shadowColor;
    bottomRightView.backgroundColor = _shadowColor;
}

- (void)setBorderColor:(UIColor *)borderColor {
    _borderColor = borderColor;
    self.cropView.layer.borderColor = _borderColor.CGColor;
}

#pragma mark - motion

- (CGFloat)distanceBetweenTwoPoints:(CGPoint)fromPoint toPoint:(CGPoint)toPoint {
    float x = toPoint.x - fromPoint.x;
    float y = toPoint.y - fromPoint.y;
    
    return sqrt(x * x + y * y);
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self willChangeValueForKey:@"crop"];
    NSSet *allTouches = [event allTouches];
    
    switch ([allTouches count]) {
        case 1: {
            currentTouches = 1;
            isPanning = NO;
            CGFloat insetAmount = IMAGE_CROPPER_INSIDE_STILL_EDGE;
            
            CGPoint touch = [[allTouches anyObject] locationInView:self];
            if (CGRectContainsPoint(CGRectInset(self.cropView.frame, insetAmount, insetAmount), touch)) {
                isPanning = YES;
                panTouch = touch;
                return;
            }
            
                     currentDragView = nil;
            
            // We start dragging if we're within the rect + the inset amount
            // If we're definitively in the rect we actually start moving right to the point
        }
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    [self willChangeValueForKey:@"crop"];
    NSSet *allTouches = [event allTouches];
    
    switch ([allTouches count])
    {
        case 1: {
            CGPoint touch = [[allTouches anyObject] locationInView:self];
            
            if (isPanning) {
                CGPoint touchCurrent = [[allTouches anyObject] locationInView:self];
                CGFloat x = touchCurrent.x - panTouch.x;
                CGFloat y = touchCurrent.y - panTouch.y;
                
                self.cropView.center = CGPointMake(self.cropView.center.x + x, self.cropView.center.y + y);
                
                panTouch = touchCurrent;
            }
            else if ((CGRectContainsPoint(self.bounds, touch))) {
                CGRect frame = self.cropView.frame;
                CGFloat x = touch.x;
                CGFloat y = touch.y;
                
                if (x > self.frame.size.width)
                    x = self.frame.size.width;
                
                if (y > self.frame.size.height)
                    y = self.frame.size.height;
                
                if (currentDragView == topView) {
                    frame.size.height += CGOriginY(frame) - y;
                    frame.origin.y = y;
                }
                else if (currentDragView == bottomView) {
                    //currentDragView = bottomView;
                    frame.size.height = y - CGOriginY(frame);
                }
                else if (currentDragView == leftView) {
                    frame.size.width += CGOriginX(frame) - x;
                    frame.origin.x = x;
                }
                else if (currentDragView == rightView) {
                    //currentDragView = rightView;
                    frame.size.width = x - CGOriginX(frame);
                }
                else if (currentDragView == topLeftView) {
                    frame.size.width += CGOriginX(frame) - x;
                    frame.size.height += CGOriginY(frame) - y;
                    frame.origin = touch;
                }
                else if (currentDragView == topRightView) {
                    frame.size.height += CGOriginY(frame) - y;
                    frame.origin.y = y;
                    frame.size.width = x - CGOriginX(frame);
                }
                else if (currentDragView == bottomLeftView) {
                    frame.size.width += CGOriginX(frame) - x;
                    frame.size.height = y - CGOriginY(frame);
                    frame.origin.x =x;
                }
                else if ( currentDragView == bottomRightView) {
                    frame.size.width = x - CGOriginX(frame);
                    frame.size.height = y - CGOriginY(frame);
                }
                
                self.cropView.frame = frame;
            }
        } break;
  
    }
    
    [self updateBounds];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    scaleDistance = 0;
    currentTouches = [[event allTouches] count];
}

- (UIImage*)getCroppedImage {
    CGRect rect = self.cropView.frame;
    CGRect drawRect = [self cropRectForFrame:rect];
    UIImage *croppedImage = [self imageByCropping:self.image toRect:drawRect];
    
    return croppedImage;
}

- (UIImage *)imageByCropping:(UIImage *)image toRect:(CGRect)rect
{
    if (UIGraphicsBeginImageContextWithOptions) {
        UIGraphicsBeginImageContextWithOptions(rect.size,
                                               /* opaque */ NO,
                                               /* scaling factor */ 0.0);
    } else {
        UIGraphicsBeginImageContext(rect.size);
    }
    
    // stick to methods on UIImage so that orientation etc. are automatically
    // dealt with for us
    [image drawAtPoint:CGPointMake(-rect.origin.x, -rect.origin.y)];
    
    UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return result;
}

-(CGRect)cropRectForFrame:(CGRect)frame
{
    NSAssert(self.contentMode == UIViewContentModeScaleAspectFit, @"content mode must be aspect fit");
    
    CGFloat widthScale = self.bounds.size.width / self.image.size.width;
    CGFloat heightScale = self.bounds.size.height / self.image.size.height;
    
    float x, y, w, h, offset;
    if (widthScale<heightScale) {
        offset = (self.bounds.size.height - (self.image.size.height*widthScale))/2;
        x = frame.origin.x / widthScale;
        y = (frame.origin.y-offset) / widthScale;
        w = frame.size.width / widthScale;
        h = frame.size.height / widthScale;
    } else {
        offset = (self.bounds.size.width - (self.image.size.width*heightScale))/2;
        x = (frame.origin.x-offset) / heightScale;
        y = frame.origin.y / heightScale;
        w = frame.size.width / heightScale;
        h = frame.size.height / heightScale;
    }
    return CGRectMake(x, y, w, h);
}

- (void)updateBounds {
    [self constrainCropToImage];
    
    [self didChangeValueForKey:@"crop"];
}

- (void)constrainCropToImage {
    CGRect frame = self.cropView.frame;
    
    if (CGRectEqualToRect(frame, CGRectZero)) return;
    
    BOOL change = NO;
    
    do {
        change = NO;
        
        if (CGOriginX(frame) < 0) {
            frame.origin.x = 0;
            change = YES;
        }
        
        if (CGWidth(frame) > CGWidth(self.cropView.superview.frame)) {
            frame.size.width = CGWidth(self.cropView.superview.frame);
            change = YES;
        }
        
        if (CGWidth(frame) < 20) {
            frame.size.width = 20;
            change = YES;
        }
        
        if (CGOriginX(frame) + CGWidth(frame) > CGWidth(self.cropView.superview.frame)) {
            frame.origin.x = CGWidth(self.cropView.superview.frame) - CGWidth(frame);
            change = YES;
        }
        
        if (CGOriginY(frame) < 0) {
            frame.origin.y = 0;
            change = YES;
        }
        
        if (CGHeight(frame) > CGHeight(self.cropView.superview.frame)) {
            frame.size.height = CGHeight(self.cropView.superview.frame);
            change = YES;
        }
        
        if (CGHeight(frame) < 20) {
            frame.size.height = 20;
            change = YES;
        }
        
        if (CGOriginY(frame) + CGHeight(frame) > CGHeight(self.cropView.superview.frame)) {
            frame.origin.y = CGHeight(self.cropView.superview.frame) - CGHeight(frame);
            change = YES;
        }
    } while (change);
    
    self.cropView.frame = frame;
}

- (UIView*)newEdgeView {
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.60];
    [self addSubview:view];
    return view;
}

- (UIView*)newCornerView {
    UIView *view = [self newEdgeView];
    view.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.60];
    return view;
}

@end
