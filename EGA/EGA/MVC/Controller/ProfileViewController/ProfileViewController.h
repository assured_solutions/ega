//
//  ProfileViewController.h
//  EGA
//
//  Created by Soniya on 18/11/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "BaseViewController.h"



@protocol ProfileViewControllerDelegate;
@interface ProfileViewController : BaseViewController
@property(nonatomic, assign) id <ProfileViewControllerDelegate> delegate;
@property (nonatomic)BOOL completeYourProfile;
@end


@protocol ProfileViewControllerDelegate <NSObject>

@optional
//Calls when Registration is Done Succesfully
-(void)UserProfileUpdatedSuccessfully:(NSString *)strEmailAddress;

@end