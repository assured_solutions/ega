//
//  PhotoCropViewController.m
//  SmartProfiler
//
//  Created by animesh bansal on 5/19/15.
//  Copyright (c) 2015 Mahindra Comviva. All rights reserved.
//

#import "PhotoCropViewController.h"

@interface PhotoCropViewController ()

@end

@implementation PhotoCropViewController


#pragma mark - View Controller Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
     [self screenDesigningOFCrop];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - ScreenDesigning Methods

-(void)screenDesigningOFCrop{
  
    // make your image view content mode == aspect fit
    // yields best results
    self.displayImage.contentMode = UIViewContentModeScaleAspectFit;
    
    // must have user interaction enabled on view that will hold crop interface
    self.displayImage.userInteractionEnabled = YES;
    self.displayImage.image = self.originalImage;
    self.displayImage.layer.borderWidth=1.0;
    // ** this is where the magic happens
    
    // allocate crop interface with frame and image being cropped
    self.cropper = [[CropInterface alloc]initWithFrame:self.displayImage.bounds andImage:self.originalImage];
    // this is the default color even if you don't set it
    self.cropper.shadowColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.60];
    // white is the default border color.
    self.cropper.borderColor =  [ConfigurationFile getThemeColor];
    

    // add interface to superview. here we are covering the main image view.
    [self.displayImage addSubview:self.cropper];
    
}

#pragma mark - Operational Methods
- (IBAction)cropPressed:(id)sender {
    // crop image
    UIImage *croppedImage = [self.cropper getCroppedImage];
    croppedImage = [self generatePhotoThumbnail:croppedImage];
    croppedImage = [self compressPhoto:croppedImage];
    // remove crop interface from superview
    [self.cropper removeFromSuperview];
    self.cropper = nil;
    
    // display new cropped image
//    self.displayImage.image = croppedImage;
    if(self.callback!=nil && [(id)[self callback] respondsToSelector:@selector(imageWhichisGoingToUploadedToServer:)])
    {
        
        [(id)[self callback] performSelectorOnMainThread:@selector(imageWhichisGoingToUploadedToServer:) withObject:croppedImage waitUntilDone:NO];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)originalPressed:(id)sender {
    // set main image view to original image and add cropper if not already added
   [self dismissViewControllerAnimated:YES completion:nil];
}
-(UIImage*)compressPhoto:(UIImage*)image{
      NSData *imgData = UIImagePNGRepresentation(image);
   return [UIImage imageWithData:imgData];
}

/// generating thumbnail of of the pick capture
-(UIImage*)generatePhotoThumbnail:(UIImage *)image
{
    CGRect rect = CGRectMake(0,0,200,200);
    UIGraphicsBeginImageContext( rect.size );
    [image drawInRect:rect];
    UIImage *picture1 = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    NSData *imageData = UIImagePNGRepresentation(picture1);
    UIImage *thumbnail=[UIImage imageWithData:imageData];
    return thumbnail;
 }



@end
