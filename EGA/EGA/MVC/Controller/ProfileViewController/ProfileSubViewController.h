//
//  ProfileSubViewController.h
//  EGA
//
//  Created by Soniya on 11/12/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol ProfileSubViewControllerDelegate;
@interface ProfileSubViewController : BaseViewController
@property(nonatomic)BOOL isChangePassword;
@property(nonatomic, assign) id <ProfileSubViewControllerDelegate> delegate;
@end

@protocol ProfileSubViewControllerDelegate <NSObject>

@optional

-(void)passwordChangedTo:(NSString*)password;
@end
