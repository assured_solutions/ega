//
//  LoginViewController.m
//  EGA
//
//  Created by Soniya on 26/09/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "LoginViewController.h"
#import "ASImagePickerViewController.h"
@interface LoginViewController ()

@end

@implementation LoginViewController
#pragma mark - Memory Management
-(void)removeReferences{
}

#pragma mark - View Controller Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = NO;
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = NO;
}

#pragma mark - ScreenDesigning Methods
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - Operational Methods
-(IBAction)showImagePicker:(id)sender{
    ASImagePickerViewController *multipleImagePicker = [[ASImagePickerViewController alloc] init];
    [self.navigationController pushViewController:multipleImagePicker animated:YES];
    return;
}
@end
