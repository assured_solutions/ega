//
//  ForgotViewController.m
//  EGA
//
//  Created by Soniya on 28/09/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "ForgotViewController.h"

@interface ForgotViewController ()<WebServiceCallerDelegate>{
    WebServiceCaller *webServiceCaller;
}

@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@end

@implementation ForgotViewController
#pragma mark - Memory Management
-(void)removeReferences{
     _txtEmail=nil;
    
    [webServiceCaller cancelWebserviceCall];
    webServiceCaller.delegate = nil;
    webServiceCaller = nil;
    
    
    [ProgressHUD dismiss];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Reset Password";
    webServiceCaller = [[WebServiceCaller alloc]init];
    webServiceCaller.delegate = self;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - Operational Methods
- (IBAction)forgotPasswordClicked:(id)sender {
    if (appDelegate.isNetAvailable){
        if ([self validateAllFields]){
            [ProgressHUD show:@"Sending..." Interaction:NO];
           [webServiceCaller forgotPasswordWithEmailID:_txtEmail.text];
            
        }
    }
    else{
        [SupportingClass showErrorMessage:NETWORKERRORMESSAGE];
        
    }
}


-(BOOL)validateAllFields{
    _txtEmail.text=[_txtEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSString* loginID = _txtEmail.text;
    
    //Check whether textField are left empty or not
    NSString *message=nil;
    if (loginID.length<=0) {
        message = [SupportingClass blankEmailIDErrorMessage];
    }
    else if(![SupportingClass isNSStringValidEmail:loginID]){
        message = [SupportingClass invalidEmaildIDFormatErrorMessage];
    }
    
    
    [_txtEmail resignFirstResponder];
 
    if (message!=nil) {
        [SupportingClass showErrorMessage:message];
        return NO;
    }
    return YES;
}
#pragma mark -  Webservice delegate

-(void)forgotPasswordCompletesSuccessfully:(BOOL)success WithMessage:(NSString*)message{
    [ProgressHUD dismiss];
    if (success) {
        [SupportingClass showAlertWithTitle:@"" Message:message CancelButtonTitle:@"OK"];
        [self.navigationController popViewControllerAnimated:YES];

     }
    else{
        if ([message isEqualToString:NETWORKERRORMESSAGE]) {
            [SupportingClass showErrorMessage:message];
        }
        else {
            [SupportingClass showAlertWithTitle:@"" Message:message CancelButtonTitle:@"OK"];
        }
    }
}
@end
