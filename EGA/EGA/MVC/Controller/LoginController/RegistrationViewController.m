//
//  RegistrationViewController.m
//  EGA
//
//  Created by Soniya on 03/11/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "RegistrationViewController.h"
#import "ProfileViewController.h"
#import "UserDM.h"

@interface RegistrationViewController ()<WebServiceCallerDelegate>{
    WebServiceCaller *webServiceCaller;
}
@property (weak, nonatomic) IBOutlet UITextField *txtFName;
@property (weak, nonatomic) IBOutlet UITextField *txtLName;
@property (weak, nonatomic) IBOutlet UITextField *txtEmailAddr;
@property (weak, nonatomic) IBOutlet UITextField *txtDOB;
@property (weak, nonatomic) IBOutlet UITextField *txtWeight;
@property (weak, nonatomic) IBOutlet UITextField *txtHeight;
@property (weak, nonatomic) IBOutlet UISegmentedControl *genderSegmentControl;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtConfirmPassword;


@end

@implementation RegistrationViewController

#pragma mark - Memory Management
-(void)removeReferences{
    _txtFName=nil;
    _txtLName=nil;
    _txtEmailAddr=nil;
    _txtDOB=nil;
    _txtWeight=nil;
    _txtHeight=nil;
    _genderSegmentControl=nil;
    _txtPassword=nil;
    _txtConfirmPassword=nil;
    
    [webServiceCaller cancelWebserviceCall];
    webServiceCaller.delegate = nil;
    webServiceCaller = nil;
    
    
    [ProgressHUD dismiss];
}

#pragma mark - View Controller Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self screenDesigningForRegistration];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - ScreenDesigning Methods
-(void)screenDesigningForRegistration{
    self.navigationItem.title = @"Create your EGA account";
    [self createKeyboardAccessoryToolBar:YES];
   
    _txtFName.inputAccessoryView = accessoryToolBar;
    _txtLName.inputAccessoryView = accessoryToolBar;
    _txtEmailAddr.inputAccessoryView = accessoryToolBar;
     _txtDOB.inputAccessoryView = accessoryToolBar;
    
    _txtWeight.inputAccessoryView = accessoryToolBar;
    _txtHeight.inputAccessoryView = accessoryToolBar;
    _txtPassword.inputAccessoryView = accessoryToolBar;
    _txtConfirmPassword.inputAccessoryView = accessoryToolBar;
    
    UIDatePicker *datePicker = [[UIDatePicker alloc]init];
    datePicker.datePickerMode = UIDatePickerModeDate;
    datePicker.maximumDate = [NSDate date];
    [datePicker addTarget:self action:@selector(updateDOB:) forControlEvents:UIControlEventValueChanged];
    _txtDOB.inputView = datePicker;
    
    webServiceCaller = [[WebServiceCaller alloc]init];
    webServiceCaller.delegate = self;
}
#pragma mark - Operational Methods
- (IBAction)signUpClicked:(id)sender {
      if (appDelegate.isNetAvailable){
        if ([self validateAllFields]){
            [ProgressHUD show:@"Register..." Interaction:NO];
            NSString *gender = _genderSegmentControl.selectedSegmentIndex==0?@"m":@"f";
            NSDateFormatter *dateFomratter = [[NSDateFormatter alloc]init];
            dateFomratter.dateFormat = @"dd MMM yyyy";
            NSDate *date = [dateFomratter dateFromString:_txtDOB.text];
            dateFomratter.dateFormat = @"yyyy-MM-dd";
            NSString *selectedDate = [dateFomratter stringFromDate:date];
            NSDictionary *dictUser = [[NSDictionary alloc]initWithObjectsAndKeys:_txtEmailAddr.text,@"emailid",_txtPassword.text,@"password",_txtFName.text,@"firstname",_txtLName.text,@"lastname",selectedDate,@"dob",gender,@"gender",_txtWeight.text,@"weight",_txtHeight.text,@"height", nil];
            [webServiceCaller registerUserWithData:dictUser];
        }
    }
    else{
        [SupportingClass showErrorMessage:NETWORKERRORMESSAGE];
        
    }
}
-(BOOL)validateAllFields{
    
    _txtFName.text=[_txtFName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    _txtLName.text=[_txtLName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    _txtEmailAddr.text=[_txtEmailAddr.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    //_txtDOB.text=[_txtDOB.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    //_txtWeight.text=[_txtWeight.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    //_txtHeight.text=[_txtHeight.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    _txtPassword.text=[_txtPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    _txtConfirmPassword.text=[_txtConfirmPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSString* fname = _txtFName.text;
    NSString* lname = _txtLName.text;
    NSString* email = _txtEmailAddr.text;
    //NSString* dob = _txtDOB.text;
    //NSString* weight = _txtWeight.text;
   // NSString* height = _txtHeight.text;
    NSString* pwd = _txtPassword.text;
    NSString* confPwd = _txtConfirmPassword.text;
    
    //Check whether textField are left empty or not
    NSString *errorMessage = nil;
    
    if (fname.length<=0) {
        errorMessage = @"Please provide your first name";
    }
    else  if (lname.length<=0) {
        errorMessage = @"Please provide your last name";
    }
    else if([SupportingClass isHavingSpecialCharacter:fname] || [SupportingClass isHavingSpecialCharacter:lname]){
        errorMessage = [SupportingClass specialCharacterErrorMessage];
    }
    else if(email.length<=0){
        errorMessage = [SupportingClass blankEmailIDErrorMessage];
    }
    else if(![SupportingClass isNSStringValidEmail:email]){
        errorMessage = [SupportingClass invalidEmaildIDFormatErrorMessage];
    }
//    else if(dob.length<=0){
//        errorMessage = [SupportingClass blankDOBErrorMessage];
//    }
//    
//    else if (weight.length<=0) {
//        errorMessage = @"Please provide your weight";
//    }
//    else if (height.length<=0) {
//        errorMessage = @"Please provide your height";
//    }
    else if (pwd.length<=0) {
        errorMessage = [SupportingClass blankPasswordErrorMessage];
    }
    else if (confPwd.length<=0) {
        errorMessage = [SupportingClass blankConfirmPasswordErrorMessage];
    }
    else if (pwd.length<=5) {
        errorMessage = [SupportingClass invalidDigitsInPasswordErrorMessage];
    }
    else if (![pwd isEqualToString:confPwd]) {
        errorMessage = [SupportingClass passwordMissMatchErrorMessage];
    }

    [self doneEditingButtonClicked];
    if (errorMessage!=nil) {
        [SupportingClass showErrorMessage:errorMessage];
        return NO;
    }
      return YES;
}
-(void)updateDOB:(UIDatePicker*)sender{
    NSDateFormatter *dateFomratter = [[NSDateFormatter alloc]init];
    dateFomratter.dateFormat = @"dd MMM yyyy";
    _txtDOB.text = [dateFomratter stringFromDate:sender.date];
}
-(void)loginWithUser:(NSDictionary*)dictUserData{
    UserDM *userDM = [[UserDM alloc]init];
    NSString *userID = [dictUserData objectForKey:@"user_id"];
    [userDM deleteAllUserAndTheirData];
    [userDM insertUser:dictUserData];
     appDelegate.userData = [userDM selectUserOfID:userID];
    
    NSUserDefaults * standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [standardUserDefaults setObject:userID forKey:@"LoggedInUserID"];
    [self navigateToProfile];
}

-(void)navigateToProfile
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [standardUserDefaults setObject:appDelegate.userData.str_email forKey:@"UserEmailID"];
    [self removeReferences];
    UIViewController *dashboardVC= [self.storyboard instantiateViewControllerWithIdentifier:@"DashboardViewController"];
    ProfileViewController *profileVC= [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    profileVC.completeYourProfile = YES;
    NSArray *viewController=[[NSArray alloc]initWithObjects:dashboardVC,profileVC, nil];
    //self.navigationController.viewControllers=nil;
    [self.navigationController setViewControllers:viewController];
    dashboardVC=nil;
    
}

#pragma mark - TextField Navigation Methodsbase
-(void)doneEditingButtonClicked
{
    [self.txtFName resignFirstResponder];
    [self.txtLName resignFirstResponder];
    [self.txtEmailAddr resignFirstResponder];
    [self.txtDOB resignFirstResponder];
    [self.txtWeight resignFirstResponder];
    [self.txtHeight resignFirstResponder];
    [self.txtPassword resignFirstResponder];
    [self.txtConfirmPassword resignFirstResponder];

}
-(void)prevButtonClicked
{
    if (activeTextField==self.txtConfirmPassword) {
        [self.txtPassword becomeFirstResponder];
    }
    else if (activeTextField==self.txtPassword) {
//        [self.txtHeight becomeFirstResponder];
//    }
//    else if (activeTextField==self.txtHeight) {
//        [self.txtWeight becomeFirstResponder];
//    }
//    else if (activeTextField==self.txtWeight) {
//        [self.txtDOB becomeFirstResponder];
//    }
//    else if (activeTextField==self.txtDOB) {
        [self.txtEmailAddr becomeFirstResponder];
    }
    else if (activeTextField==self.txtEmailAddr) {
        [self.txtLName becomeFirstResponder];
    }
    else if (activeTextField==self.txtLName) {
        [self.txtFName becomeFirstResponder];
    }
    else if (activeTextField==self.txtFName) {
        [self.txtConfirmPassword becomeFirstResponder];
    }
    
}
-(void)nextButtonClicked
{
    if (activeTextField==self.txtFName) {
        [self.txtLName becomeFirstResponder];
    }
    else if (activeTextField==self.txtLName) {
        [self.txtEmailAddr becomeFirstResponder];
    }
    else if (activeTextField==self.txtEmailAddr) {
//        [self.txtDOB becomeFirstResponder];
//    }
//    else if (activeTextField==self.txtDOB) {
//        [self.txtWeight becomeFirstResponder];
//    }
//    else if (activeTextField==self.txtWeight) {
//        [self.txtHeight becomeFirstResponder];
//    }
//    else if (activeTextField==self.txtHeight) {
        [self.txtPassword becomeFirstResponder];
    }
    else if (activeTextField==self.txtPassword) {
        [self.txtConfirmPassword becomeFirstResponder];
    }
    else if (activeTextField==self.txtConfirmPassword) {
        [self.txtFName becomeFirstResponder];
    }
    
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    activeTextField = textField;
}

#pragma mark -  Webservice delegate

-(void)registerUserCompletesSuccessfully:(BOOL)success WithData:(NSMutableDictionary*)dictUserData WithMessage:(NSString*)message{
   
    if (success) {
        NSDictionary *dictUser = [[NSDictionary alloc]initWithObjectsAndKeys:_txtEmailAddr.text,@"emailid",_txtPassword.text,@"password", nil];
        [webServiceCaller loginUserWithData:dictUser];
    }
    else{
         [ProgressHUD dismiss];
        if ([message isEqualToString:NETWORKERRORMESSAGE]) {
            [SupportingClass showErrorMessage:message];
        }
        else {
            [SupportingClass showAlertWithTitle:@"" Message:message CancelButtonTitle:@"OK"];
        }
    }
}

#pragma mark -  Webservice delegate

-(void)loginUserCompletesSuccessfully:(BOOL)success WithData:(NSMutableDictionary*)dictUserData WithMessage:(NSString*)message{
    [ProgressHUD dismiss];
    if (success) {
        [self loginWithUser:dictUserData];
    }
    else{
        if ([message isEqualToString:NETWORKERRORMESSAGE]) {
            [SupportingClass showErrorMessage:message];
        }
        else {
            [SupportingClass showAlertWithTitle:@"" Message:message CancelButtonTitle:@"OK"];
        }
    }
}

@end
