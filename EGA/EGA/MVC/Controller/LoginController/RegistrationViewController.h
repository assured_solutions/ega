//
//  RegistrationViewController.h
//  EGA
//
//  Created by Soniya on 03/11/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol RegisterViewControllerDelegate;
@interface RegistrationViewController : BaseViewController
@property(nonatomic, assign) id <RegisterViewControllerDelegate> delegate;
@end

@protocol RegisterViewControllerDelegate <NSObject>

@optional
//Calls when Registration is Done Succesfully
-(void)RegisteredUserSuccessfully:(NSString *)strUserName;

@end