//
//  LoginEmailViewController.m
//  EGA
//
//  Created by Soniya on 26/09/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "LoginEmailViewController.h"
#import "DDPageControl.h"
#import "RegistrationViewController.h"
#import "UserDM.h"
#import "ReminderDM.h"

@interface LoginEmailViewController ()<RegisterViewControllerDelegate,WebServiceCallerDelegate>
{
    DDPageControl *pageControl;
    NSArray *arrImages;
    NSArray *arrText;
    NSInteger selectedIndex;
    WebServiceCaller *webServiceCaller;
    NSTimer *timer;
}
@property (weak, nonatomic) IBOutlet UIView *vwSliderContainer;
@property (weak, nonatomic) IBOutlet UILabel *lblSliderTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgSlider;
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@end

@implementation LoginEmailViewController

#pragma mark - Memory Management
-(void)removeReferences{
    _txtEmail=nil;
    _txtPassword=nil;
    
    _vwSliderContainer =nil;
    _lblSliderTitle=nil;
    _imgSlider=nil;
    
    [webServiceCaller cancelWebserviceCall];
    webServiceCaller.delegate = nil;
    webServiceCaller = nil;
    
    
    [ProgressHUD dismiss];
}

#pragma mark - View Controller Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
//   _txtEmail.text =@"notgud2bdown@gmail.com";
//    _txtPassword.text = @"123456";
    [self ScreenDesigningForSplashOfLogin];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];

}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [super viewWillDisappear:animated];
    timer=[NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(scrollingTimer) userInfo:nil repeats:YES];

    webServiceCaller.delegate =self;
    [self rememberUserNameToSave:NO];
}
-(void)viewWillDisappear:(BOOL)animated{
    webServiceCaller.delegate = nil;
       [timer invalidate];
    timer=nil;
    self.navigationController.navigationBarHidden = NO;
}
#pragma mark - ScreenDesigning Methods

-(void)ScreenDesigningForSplashOfLogin{
    
    arrImages = [[NSArray alloc]initWithObjects:[UIImage imageNamed:@"BiologicalAgeLogin"],[UIImage imageNamed:@"ReverseYourAgeLogin"],[UIImage imageNamed:@"StayHealthyLogin"], nil];
    arrText = [[NSArray alloc]initWithObjects:@"Know your Biological age",@"Reverse Your Age",@"Stay Healthy", nil];
  
    selectedIndex = 0;
    
    _imgSlider.image = [arrImages objectAtIndex:selectedIndex];
    _lblSliderTitle.text = [[arrText objectAtIndex:selectedIndex] uppercaseString];

    pageControl = [[DDPageControl alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 37)] ;
    [pageControl setCenter: CGPointMake(self.view.center.x, _vwSliderContainer.frame.origin.y+_vwSliderContainer.frame.size.height+20.0f)] ;
    [pageControl setNumberOfPages: arrImages.count] ;
    [pageControl setCurrentPage: 0] ;
    [pageControl setDefersCurrentPageDisplay: YES] ;
    [pageControl setType: DDPageControlTypeOnEmptyOffEmpty] ;
    [pageControl setOnColor: [ConfigurationFile getThemeColor]];
    [pageControl setOffColor: [UIColor colorWithHexString:BORDERCOLOR]] ;
    [pageControl setIndicatorDiameter: 15.0f] ;
    [pageControl setIndicatorSpace: 15.0f] ;
    [_scrollView addSubview: pageControl] ;
    
    [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(scrollingTimer) userInfo:nil repeats:YES];
    
    webServiceCaller = [[WebServiceCaller alloc]init];
    webServiceCaller.delegate = self;
}
#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    //  Get the new view controller using [segue destinationViewController].
    //  Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"RegistrationViewController"])
    {
        RegistrationViewController *registerController = [segue destinationViewController];
        registerController.delegate = self;
        
    }
    
}

#pragma mark - Operational Methods
- (void)scrollingTimer {
    
    selectedIndex++;
    if (selectedIndex==3) {
        selectedIndex = 0;
    }
    
    _imgSlider.image = [arrImages objectAtIndex:selectedIndex];
    _lblSliderTitle.text = [[arrText objectAtIndex:selectedIndex] uppercaseString];
    [SupportingClass animateViewWithType:kCATransitionFade SubType:kCATransitionFromRight OnView:_vwSliderContainer Duration:0.8];
    pageControl.currentPage=selectedIndex;
    [pageControl updateCurrentPageDisplay] ;
}

- (void)rememberUserNameToSave:(BOOL)saveUsername
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    if (standardUserDefaults) {
        if (saveUsername) {
            
                [standardUserDefaults setObject:self.txtEmail.text forKey:@"UserEmailID"];
            
            }
        }
        else
        {
            self.txtEmail.text=[standardUserDefaults objectForKey:@"UserEmailID"];
        }
        
    }

- (IBAction)signInClicked:(id)sender {
    if (appDelegate.isNetAvailable){
        if ([self validateAllFields]){
            [ProgressHUD show:@""];
            NSDictionary *dictUser = [[NSDictionary alloc]initWithObjectsAndKeys:_txtEmail.text,@"emailid",_txtPassword.text,@"password", nil];
            [webServiceCaller loginUserWithData:dictUser];
        }
    }
    else{
        [SupportingClass showErrorMessage:NETWORKERRORMESSAGE];
        
    }
}
-(BOOL)validateAllFields{
     _txtEmail.text=[_txtEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    _txtPassword.text=[_txtPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSString* loginID = _txtEmail.text;
    NSString* password = _txtPassword.text;
    
    //Check whether textField are left empty or not
    NSString *message=nil;
    if (loginID.length<=0) {
         message = [SupportingClass blankEmailIDErrorMessage];
    }
    else if(![SupportingClass isNSStringValidEmail:loginID]){
         message = [SupportingClass invalidEmaildIDFormatErrorMessage];
    }
    else if(password.length<=0){
         message = [SupportingClass blankPasswordErrorMessage];
    }
   
  
    [_txtEmail resignFirstResponder];
    [_txtPassword resignFirstResponder];
    
    if (message!=nil) {
        [SupportingClass showErrorMessage:message];
        return NO;
    }
    return YES;
}
-(IBAction)sendFocusToPassword
{
    [_txtEmail resignFirstResponder];
    [_txtPassword becomeFirstResponder];
}

-(void)navigateToDashboard
{
    [self rememberUserNameToSave:YES];
  [self removeReferences];
    UIViewController *dashboardVC= [self.storyboard instantiateViewControllerWithIdentifier:@"DashboardViewController"];
    NSArray *viewController=[[NSArray alloc]initWithObjects:dashboardVC, nil];
    //self.navigationController.viewControllers=nil;
    [self.navigationController setViewControllers:viewController];
    dashboardVC=nil;
   
}
-(void)loginWithUser:(NSDictionary*)dictUserData{
    UserDM *userDM = [[UserDM alloc]init];
    NSString *userID = [dictUserData objectForKey:@"user_id"];
    BOOL isUserExists = [userDM isUserExistWithUserID:userID];
   if (!isUserExists) {
        [userDM deleteAllUserAndTheirData];
   }
   else{
       ReminderDM *reminderDM = [[ReminderDM alloc]init];
       NSArray *arrReminder = [reminderDM getNotification];
       
       for(NSDictionary *dict in arrReminder){
       UILocalNotification *localNotification = [[UILocalNotification alloc] init];
       NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
       [dateFormat setDateFormat:[SupportingClass dateStandardFormat]];
           NSDate *date = [dateFormat dateFromString:[dict objectForKey:@"DateTime"]];
       localNotification.fireDate = date;
       localNotification.alertBody = [dict objectForKey:@"Title"];
       localNotification.repeatInterval=[[dict objectForKey:@"RepeatTime"] integerValue];
       
       //UILocalNotificationDefaultSoundName
       localNotification.soundName =[NSString stringWithFormat:@"%@.mp3",[dict objectForKey:@"AlarmTone"]];
    
        NSDictionary *infoDict = [NSDictionary dictionaryWithObjectsAndKeys:[dict objectForKey:@"NotificationID"], @"ID", nil];
        
        localNotification.userInfo = infoDict;
        
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
        }
   }
    [userDM insertUser:dictUserData];

    appDelegate.userData = [userDM selectUserOfID:userID];

    NSUserDefaults * standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [standardUserDefaults setObject:userID forKey:@"LoggedInUserID"];
    [self navigateToDashboard];
}
#pragma mark - Delegate Methods

-(void)RegisteredUserSuccessfully:(NSString *)strUserName{
    
    _txtEmail.text = strUserName;
}

#pragma mark -  Webservice delegate

-(void)loginUserCompletesSuccessfully:(BOOL)success WithData:(NSMutableDictionary*)dictUserData WithMessage:(NSString*)message{
    [ProgressHUD dismiss];
    if (success) {
        [self loginWithUser:dictUserData];
    }
    else{
        if ([message isEqualToString:NETWORKERRORMESSAGE]) {
            [SupportingClass showErrorMessage:message];
        }
        else {
            [SupportingClass showAlertWithTitle:@"" Message:message CancelButtonTitle:@"OK"];
        }
    }
}

@end
