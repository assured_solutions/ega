//
//  ReminderDateCell.m
//  EGA
//
//  Created by Soniya on 17/10/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "ReminderDateCell.h"

@implementation ReminderDateCell
@synthesize lblDate;
@synthesize vwSelection;
@synthesize lblDay;
@synthesize lblMonth;

- (void)awakeFromNib {
    // Initialization code
}
- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    self.contentView.backgroundColor = [UIColor clearColor];
    if (selected) {
        lblDate.font = [UIFont boldSystemFontOfSize:17];
        vwSelection.hidden = NO;
       
    } else {
        lblDate.font = [UIFont systemFontOfSize:17];
        vwSelection.hidden = YES;
    }
}
@end
