//
//  ReminderDateCell.h
//  EGA
//
//  Created by Soniya on 17/10/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReminderDateCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIView *vwSelection;
@property (strong, nonatomic) IBOutlet UILabel *lblDay;
@property (strong, nonatomic) IBOutlet UILabel *lblDate;
@property (strong, nonatomic) IBOutlet UILabel *lblMonth;
@end
