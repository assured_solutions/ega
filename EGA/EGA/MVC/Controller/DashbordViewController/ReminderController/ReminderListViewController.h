//
//  ReminderListViewController.h
//  EGA
//
//  Created by Soniya on 17/10/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "TGLStackedViewController.h"
@protocol ReminderListDelegate;

@interface ReminderListViewController : TGLStackedViewController
@property (nonatomic, assign) BOOL doubleTapToClose;
@property (nonatomic) NSMutableArray *arrReminder;
@property(nonatomic, assign) id <ReminderListDelegate> delegate;
-(void)reloadData;
@end

@protocol ReminderListDelegate <NSObject>


//SMTP Mail Delegate Methods
@required
-(void)allItemsGetDeleted;
@optional
-(void)mailSentWithSuccessfully:(BOOL)success Message:(NSString*)message;
@end