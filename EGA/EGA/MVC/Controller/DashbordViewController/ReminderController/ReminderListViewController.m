//
//  ReminderListViewController.m
//  EGA
//
//  Created by Soniya on 17/10/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "ReminderListViewController.h"
#import "ReminderDetailCell.h"
#import "ReminderDM.h"

@interface ReminderListViewController ()
{
    NSInteger notificationTagToDelete;
}
@end

@implementation ReminderListViewController

#pragma mark - View Controller Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self screenDesigningOfReminderList];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ScreenDesigning Methods
-(void)screenDesigningOfReminderList{
    // Set to NO to prevent a small number
    // of cards from filling the entire
    // view height evenly and only show
    // their -topReveal amount
    //
    
    [self.view setBackgroundColor:[UIColor colorWithHexString:BACKGROUNDCOLOR]];
    self.stackedLayout.fillHeight = YES;
    
    // Set to NO to prevent a small number
    // of cards from being scrollable and
    // bounce
    //
    self.stackedLayout.alwaysBounce = YES;
    
    // Set to NO to prevent unexposed
    // items at top and bottom from
    // being selectable
    //
    self.unexposedItemsAreSelectable = YES;
    
    self.exposedBottomOverlapCount = 4;
    
    self.exposedTopPinningCount = 2;
    self.exposedBottomPinningCount = 5;
    
    self.exposedItemSize = self.stackedLayout.itemSize = CGSizeMake(0.0, 240.0);
    self.exposedPinningMode = TGLExposedLayoutPinningModeBelow;
    self.exposedTopOverlap = 5.0;
    self.exposedBottomOverlap = 5.0;
    
    self.stackedLayout.layoutMargin = UIEdgeInsetsZero;
    self.exposedLayoutMargin = self.exposedPinningMode ? UIEdgeInsetsMake(40.0, 0.0, 0.0, 0.0) : UIEdgeInsetsMake(20.0, 0.0, 0.0, 0.0);
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    if (self.doubleTapToClose) {
        
        UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
        
        recognizer.delaysTouchesBegan = YES;
        recognizer.numberOfTapsRequired = 2;
        
        [self.collectionView addGestureRecognizer:recognizer];
    }
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma mark - Actions

- (IBAction)handleDoubleTap:(UITapGestureRecognizer *)recognizer {
    
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}
-(void)reloadData{
    [self.collectionView reloadData];
}
-(void)deleteNotification:(UIButton*)sender{
    notificationTagToDelete = sender.tag;
    [SupportingClass showAlertWithTitle:@"Are You Sure?" Message:@"Delete Reminder" CancelButtonTitle:@"NO" OtherButton:@"YES" Delegate:self];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        NSDictionary *dict=[_arrReminder objectAtIndex:notificationTagToDelete];
        
        //        NSArray *notificationsArray = [[UIApplication sharedApplication] scheduledLocalNotifications];
        //        NSString *dictID=[dict objectForKey:@"NotificationID"];
        //
        
        NSArray *notificationsArray = [[UIApplication sharedApplication] scheduledLocalNotifications];
        NSString *dictID=[dict objectForKey:@"NotificationID"];
        
        
        int i;
        
        for (i=0; i<notificationsArray.count; i++)
        {
            UILocalNotification *localNotification=[notificationsArray objectAtIndex:i];
            NSDictionary *notiDict=[localNotification userInfo];
            NSString *notiDictID=[notiDict objectForKey:@"ID"];
            
            if ([notiDictID isEqualToString:dictID])
            {
                NSString*key= [NSString stringWithFormat:@"%d",i];
                [[UIApplication sharedApplication] cancelLocalNotification:localNotification];
                
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:key];
                ReminderDM *reminderDM=[[ReminderDM alloc]init];
                [reminderDM deleteEvent:[dictID intValue]];
                reminderDM=nil;
                [_arrReminder removeObjectAtIndex:notificationTagToDelete];
                break;
            }
        }
        
        if (i==notificationsArray.count)
        {
            ReminderDM *reminderDM=[[ReminderDM alloc]init];
            
            
            [reminderDM deleteEvent:[dictID intValue]];
            reminderDM=nil;
            [_arrReminder removeObjectAtIndex:notificationTagToDelete];
        }
        
        if (_arrReminder.count==0) {
            if(self.delegate!=nil && [(id)[self delegate] respondsToSelector:@selector(allItemsGetDeleted)])
            {
                [(id)[self delegate] allItemsGetDeleted];
            }
        }
        [self.collectionView reloadData];
        
    }
}
#pragma mark - CollectionViewDataSource protocol

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return _arrReminder.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ReminderDetailCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ReminderDetailCell" forIndexPath:indexPath];
    NSMutableDictionary *reminder = [_arrReminder objectAtIndex:indexPath.row];
    UIColor *color = (UIColor*) [reminder objectForKey:@"color"];
    if (!color) {
        color = [self getRandomColor:indexPath.row];
        [reminder setObject:color forKey:@"color"];
    }
    cell.lblDate.text = [NSString stringWithFormat:@"Timing : %@", [SupportingClass getStringDate:[reminder objectForKey:@"DateTime"] From:[SupportingClass dateStandardFormat] to:@"hh:mm a"]];
    
    // cell.lblType.text = @"--";
    cell.lblType.text = [reminder objectForKey:@"Title"];
    NSLog(@"details are here %@",[reminder objectForKey:@"AlarmDetail"]);
    
    cell.txtVwDetail.text = [reminder objectForKey:@"AlarmDetail"];
    
    [cell.lblType sizeToFit];
    cell.btnDelete.tag = indexPath.row;
    [cell.btnDelete addTarget:self action:@selector(deleteNotification:) forControlEvents:UIControlEventTouchUpInside];
    cell.backgroundColor = color;
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath{
    cell.layer.cornerRadius =20;
}
#pragma mark - Overloaded methods

- (void)moveItemAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
    
    // Update data source when moving cards around
    
    NSDictionary *card = [_arrReminder objectAtIndex:fromIndexPath.item];
    
    [self.arrReminder removeObjectAtIndex:fromIndexPath.item];
    [self.arrReminder insertObject:card atIndex:toIndexPath.item];
}
-(UIColor*)getRandomColor:(NSInteger)row{
    
    int randNum = row % 8; //create the random number.
    UIColor *color;
    switch (randNum) {
        case 0:
            color = [UIColor colorWithHexString:@"#8000FF"];
            break;
        case 1:
            color = [UIColor colorWithHexString:@"#FF8000"];
            break;
        case 2:
            color = [UIColor colorWithHexString:@"#008000"];
            break;
        case 3:
            color = [UIColor colorWithHexString:@"#FF0080"];
            break;
        case 4:
            color = [UIColor colorWithHexString:@"#800000"];
            break;
        case 5:
            color = [UIColor colorWithHexString:@"#007AFF"];
            break;
        case 6:
            color = [UIColor colorWithHexString:@"#003366"];
            break;
        case 7:
            color = [UIColor colorWithHexString:@"#FF0000"];
            break;
        default:
            color = [UIColor colorWithHexString:@"#8000FF"];
            break;
    }
    return color;
}

@end
