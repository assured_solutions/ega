//
//  ReminderDetailViewController.h
//  EGA
//
//  Created by Soniya on 13/10/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "ViewController.h"


@interface ReminderDetailViewController : BaseViewController

@property (nonatomic) NSString *selectedDate;
@property (nonatomic) NSMutableArray *arrDateOfReminders;
@end
