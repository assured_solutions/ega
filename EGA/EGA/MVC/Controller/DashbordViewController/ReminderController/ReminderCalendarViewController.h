//
//  ReminderCalendarViewController.h
//  EGA
//
//  Created by Soniya on 13/10/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "ViewController.h"
#import <JTCalendar/JTCalendar.h>

@interface ReminderCalendarViewController : BaseViewController<JTCalendarDelegate>
@property (weak, nonatomic) IBOutlet JTCalendarMenuView *calendarMenuView;
@property (weak, nonatomic) IBOutlet JTHorizontalCalendarView *calendarContentView;

@property (strong, nonatomic) JTCalendarManager *calendarManager;

@end
