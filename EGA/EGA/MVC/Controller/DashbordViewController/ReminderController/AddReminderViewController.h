//
//  AddReminderViewController.h
//  EGA
//
//  Created by Soniya on 28/10/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddReminderViewController : BaseViewController
<UITextViewDelegate,UITextFieldDelegate>


@property(nonatomic,strong)IBOutlet UITextField *txtTitle;
@property(nonatomic,strong)IBOutlet UITextView *txtVwDetails;



@property (strong, nonatomic) IBOutlet UIDatePicker *dateTime;
@property (readwrite, nonatomic) BOOL isEdit;
@property (weak, nonatomic) IBOutlet UIButton *btnAdd;
@property (weak, nonatomic) NSDictionary *notificationDict;
@property(weak,nonatomic)NSString *strUserId;


@end
