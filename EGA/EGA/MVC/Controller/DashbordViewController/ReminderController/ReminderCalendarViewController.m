//
//  ReminderCalendarViewController.m
//  EGA
//
//  Created by Soniya on 13/10/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "ReminderCalendarViewController.h"
#import "ReminderDetailViewController.h"
#import "ReminderDM.h"

@interface ReminderCalendarViewController (){
    NSMutableArray *arrNotificationDate;
}
@end

@implementation ReminderCalendarViewController

#pragma mark - View Controller Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self ScreenDesigningForReminderCalendar];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self getAllNotifications];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma mark - ScreenDesigning Methods

-(void)ScreenDesigningForReminderCalendar{
    self.navigationItem.title = @"Reminder";
    _calendarManager = [JTCalendarManager new];
    _calendarManager.delegate = self;
    
    // Generate random events sort by date using a dateformatter for the demonstration
    
    [_calendarManager setMenuView:_calendarMenuView];
    [_calendarManager setContentView:_calendarContentView];
    [_calendarManager setDate:[NSDate date]];
    
}
#pragma mark - CalendarManager delegate

// Exemple of implementation of prepareDayView method
// Used to customize the appearance of dayView
- (void)calendar:(JTCalendarManager *)calendar prepareDayView:(JTCalendarDayView *)dayView
{
    // Today
    if([_calendarManager.dateHelper date:[NSDate date] isTheSameDayThan:dayView.date]){
        dayView.circleView.hidden = NO;
        dayView.circleView.backgroundColor = [ConfigurationFile getThemeColor];
        dayView.dotView.backgroundColor = [UIColor whiteColor];
        dayView.textLabel.textColor = [UIColor whiteColor];
    }
    // Other month
    else if(![_calendarManager.dateHelper date:_calendarContentView.date isTheSameMonthThan:dayView.date]){
        dayView.circleView.hidden = YES;
        dayView.dotView.backgroundColor = [UIColor redColor];
        dayView.textLabel.textColor = [UIColor lightGrayColor];
    }
    // Another day of the current month
    else{
        dayView.circleView.hidden = YES;
        dayView.dotView.backgroundColor = [UIColor redColor];
        dayView.textLabel.textColor = [UIColor blackColor];
    }
    NSString *dateToShow = [[SupportingClass dateFormatter:[SupportingClass dateFormat]] stringFromDate:dayView.date];
    
    if([self haveEventForDay:dateToShow]){
        dayView.dotView.hidden = NO;
        
    }
    else{
        dayView.dotView.hidden = YES;
    }
}

- (void)calendar:(JTCalendarManager *)calendar didTouchDayView:(JTCalendarDayView *)dayView
{
    NSString *dateSelected = [[SupportingClass dateFormatter:[SupportingClass dateFormat]] stringFromDate:dayView.date];
    if([self haveEventForDay:dateSelected]){
        
        ReminderDetailViewController *reminderDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ReminderDetailViewController"];
        reminderDetailVC.arrDateOfReminders = arrNotificationDate;
        reminderDetailVC.selectedDate = dateSelected;
        [self.navigationController pushViewController:reminderDetailVC animated:YES];
    }
    else{
        if(![_calendarManager.dateHelper date:_calendarContentView.date isTheSameMonthThan:dayView.date]){
            if([_calendarContentView.date compare:dayView.date] == NSOrderedAscending){
                [_calendarContentView loadNextPageWithAnimation];
            }
            else{
                [_calendarContentView loadPreviousPageWithAnimation];
            }
        }
    }
    
    
}



#pragma mark - Operational Methods

-(void)getAllNotifications{
    ReminderDM *reminderDM = [[ReminderDM alloc]init];
    arrNotificationDate = [reminderDM getAllNotificationOfAllDates];
    [_calendarManager reload];
}
#pragma mark - Fake data

// Used only to have a key for arrNotificationDate


- (BOOL)haveEventForDay:(NSString *)date
{
    
    if ([arrNotificationDate containsObject:date]) {
        return YES;
    }
    
    return NO;
    
}

- (void)createRandomEvents
{
    arrNotificationDate = [NSMutableArray new];
    
    for(int i = 0; i < 30; ++i){
        // Generate 30 random dates between now and 60 days later
        NSDate *randomDate = [NSDate dateWithTimeInterval:(rand() % (3600 * 24 * 60)) sinceDate:[NSDate date]];
        
        // Use the date as key for eventsByDate
        NSString *key = [[SupportingClass dateFormatter:[SupportingClass dateFormat]] stringFromDate:randomDate];
        
        //        if(!arrNotificationDate[key]){
        //            arrNotificationDate[key] = [NSMutableArray new];
        //        }
        //
        [arrNotificationDate addObject:key];
    }
}

@end
