//
//  ReminderDetailViewController.m
//  EGA
//
//  Created by Soniya on 13/10/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "ReminderDetailViewController.h"
#import "ReminderListViewController.h"
#import "ReminderDateCell.h"
#import "ReminderDM.h"


@interface ReminderDetailViewController ()<UICollectionViewDataSource, UICollectionViewDelegate,ReminderListDelegate>{
    ReminderListViewController *reminderList;
    NSInteger selectedDateIndex;
}
@property (nonatomic) IBOutlet UICollectionView *collViewDates;
@property (weak, nonatomic) IBOutlet UIView *vwContainer;


@end

@implementation ReminderDetailViewController
@synthesize arrDateOfReminders;
#pragma mark - View Controller Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self screenDesigningOfReminderDetail];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - ScreenDesigning Methods
-(void)screenDesigningOfReminderDetail{
   
    [self setTitle:@"Reminder" SubTitle:@"Your reminders"];
    
    reminderList = [self.storyboard instantiateViewControllerWithIdentifier:@"ReminderListViewController"];
    reminderList.view.frame = CGRectMake(0, 0, _vwContainer.frame.size.width, _vwContainer.frame.size.height);
    reminderList.delegate = self;
    [_vwContainer addSubview:reminderList.view];
    [arrDateOfReminders sortUsingComparator:^NSComparisonResult(NSString *date1, NSString *date2) {
        // return date2 compare date1 for descending. Or reverse the call for ascending.
        return [date1 compare:date2];
    }];
    selectedDateIndex = [arrDateOfReminders indexOfObject:_selectedDate];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:selectedDateIndex inSection:0];
    [_collViewDates selectItemAtIndexPath:indexPath animated:YES scrollPosition:UICollectionViewScrollPositionCenteredHorizontally];
    [self selectDateOfIndex:selectedDateIndex];
    // Set to NO to prevent a small number
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
-(void)getAllNotifications{
    ReminderDM *reminderDM = [[ReminderDM alloc]init];
    arrDateOfReminders = [reminderDM getAllNotificationOfAllDates];
    [_collViewDates reloadData];
}
#pragma mark - CollectionViewDataSource protocol

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.arrDateOfReminders.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ReminderDateCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ReminderDateCell" forIndexPath:indexPath];
    NSString *selectedDate =[arrDateOfReminders objectAtIndex:indexPath.row];
    NSString  *strDate=@"";
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormatter dateFromString:selectedDate];
    dateFormatter = [SupportingClass dateFormatter:@"dd"];
    strDate = [dateFormatter stringFromDate:date];
    cell.lblDate.text = strDate;
    dateFormatter = [SupportingClass dateFormatter:@"MMM"];
    strDate = [dateFormatter stringFromDate:date];
    cell.lblMonth.text = strDate;
    dateFormatter = [SupportingClass dateFormatter:@"EE"];
    strDate = [dateFormatter stringFromDate:date];
    cell.lblDay.text = strDate;
    
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    selectedDateIndex=indexPath.row;
    [self selectDateOfIndex:indexPath.row];
}

-(void)selectDateOfIndex:(NSInteger)index{
    ReminderDM *reminderDM=[[ReminderDM alloc]init];
    NSString *selectedDate =[arrDateOfReminders objectAtIndex:index];
    NSMutableArray *arrItem = [reminderDM getNotificationOfDate:selectedDate];
    reminderList.arrReminder = arrItem;
    [reminderList reloadData];
}
-(void)allItemsGetDeleted{
    
    [arrDateOfReminders removeObjectAtIndex:selectedDateIndex];
    if (arrDateOfReminders.count>0) {
        if (selectedDateIndex>=arrDateOfReminders.count) {
            selectedDateIndex--;
        }
        [_collViewDates reloadData];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:selectedDateIndex inSection:0];
        [_collViewDates selectItemAtIndexPath:indexPath animated:YES scrollPosition:UICollectionViewScrollPositionCenteredHorizontally];
        [self selectDateOfIndex:selectedDateIndex];
        
    }
    else{
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

@end
