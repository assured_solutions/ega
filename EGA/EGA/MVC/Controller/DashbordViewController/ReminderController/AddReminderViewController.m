//
//  AddReminderViewController.m
//  EGA
//
//  Created by Soniya on 28/10/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "AddReminderViewController.h"

#import "ReminderDM.h"


@interface AddReminderViewController ()
@end

@implementation AddReminderViewController
@synthesize txtTitle;
@synthesize txtVwDetails;
@synthesize dateTime;
@synthesize isEdit;
@synthesize btnAdd;
@synthesize notificationDict;
@synthesize strUserId;


#pragma mark - Memory Management
-(void)removeReferences{
}
#pragma mark - View Controller Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.}
     [self ScreenDesigningForAddReminder];
   }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - ScreenDesigning Methods

-(void)ScreenDesigningForAddReminder{
    
    [self setTitle:@"Reminder" SubTitle:@"Add a reminder"];
    
    UIUserNotificationType types = UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
    UIUserNotificationSettings *mySettings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:mySettings];
    
    
    txtVwDetails.layer.borderWidth = 1.0f;
    txtVwDetails.layer.borderColor = [UIColor colorWithHexString:BORDERCOLOR].CGColor;
    txtVwDetails.layer.cornerRadius = 2.0f;
    txtVwDetails.textColor=[UIColor lightGrayColor];
    txtVwDetails.text = @"Detail Description....";
    
    dateTime.backgroundColor=[UIColor clearColor];
    dateTime.layer.borderColor = [UIColor colorWithHexString:BORDERCOLOR].CGColor;
    dateTime.layer.borderWidth = 1 ;
    
    [self.dateTime setMinimumDate:[NSDate date]];
    [self createKeyboardAccessoryToolBar:YES];
    txtTitle.inputAccessoryView = accessoryToolBar;
    txtVwDetails.inputAccessoryView = accessoryToolBar;
    if (isEdit)
    {
        [btnAdd setTitle:@"Edit" forState:UIControlStateNormal];
        txtTitle.text= [notificationDict objectForKey:@"Title"];
        NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
        [dateFormat setDateFormat:@"dd-MM-yyyy hh:mm a"];
        dateTime.date=[dateFormat dateFromString:[notificationDict objectForKey:@"DateTime"]];
        
    }
    else
    {
        [btnAdd setTitle:@"Add" forState:UIControlStateNormal];
        
    }
    
}
#pragma mark - Operational Methods

-(BOOL)validateAllFields{
    txtTitle.text=[txtTitle.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSString* title = txtTitle.text;
    
    //Check whether textField are left empty or not
    NSString *message=nil;
    if (title.length<=0) {
        message = @"Title can't be left blank.";
    }
    
    [txtTitle resignFirstResponder];
    [txtVwDetails resignFirstResponder];
    
    if (message!=nil) {
        [SupportingClass showErrorMessage:message];
        return NO;
    }
    return YES;
}

-(IBAction)addNotification:(id)sender
{
    
    if ([txtVwDetails.text isEqualToString:@"Detail Description...."]) {
        txtVwDetails.text = @"No Description Available";
    }
     
    if ([self validateAllFields]) {
        NSInteger IsRepeatAlarm = 0 ;
        NSString *str_Mp3name= @"notification";
        NSDate *dateSel=self.dateTime.date;
        
        NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
        [dateFormat setDateFormat:[SupportingClass dateStandardFormat]];
        
        NSString *currentDate=[dateFormat stringFromDate:[NSDate date]];
        NSString *currentSelectedDate=[dateFormat stringFromDate:dateSel];
        
        if ([[dateFormat dateFromString:currentDate] isEqualToDate:[dateFormat dateFromString:currentSelectedDate]])
        {
            
            [SupportingClass showAlertWithTitle:@"EGA Reminder Alert" Message:@"Please not select current running time" CancelButtonTitle:@"OK"];
            return ;
            
        }
        else
        {
            UILocalNotification *localNotification = [[UILocalNotification alloc] init];
            
            localNotification.fireDate = dateSel;
            localNotification.alertBody = self.txtTitle.text;
            localNotification.repeatInterval=IsRepeatAlarm;
            
            //UILocalNotificationDefaultSoundName
            localNotification.soundName =[NSString stringWithFormat:@"%@.mp3",str_Mp3name];
            
            ReminderDM *db=[[ReminderDM alloc]init];
            
            if ([btnAdd.titleLabel.text isEqualToString:@"Edit"])
            {
                
                NSString *strID=[notificationDict objectForKey:@"NotificationID"];
                BOOL success=   [db updateEvent:txtTitle.text Date:currentSelectedDate AlarmTone:str_Mp3name Repeat:IsRepeatAlarm SoundTone:0 ID:[strID intValue]];
                
                if (success)
                {
                    
                    NSDictionary *infoDict = [NSDictionary dictionaryWithObjectsAndKeys:strID, @"ID", nil];
                    localNotification.userInfo = infoDict;
                    
                    NSArray *notificationsArray = [[UIApplication sharedApplication] scheduledLocalNotifications];
                    
                    int i;
                    for (i=0; i<notificationsArray.count; i++)
                    {
                        UILocalNotification *localNotification1=[notificationsArray objectAtIndex:i];
                        NSDictionary *notiDict=[localNotification1 userInfo];
                        NSString *notiDictID=[notiDict objectForKey:@"ID"];
                        
                        if ([notiDictID isEqualToString:strID])
                        {
                            NSString*key= [NSString stringWithFormat:@"%d",i];
                            [[UIApplication sharedApplication] cancelLocalNotification:localNotification1];
                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:key];
                            break;
                        }
                    }
                    
                    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
                }
                else
                {
                    [SupportingClass showAlertWithTitle:@"EGA Reminder Alert" Message:@"Sorry Not able to update yet, Please Try Again Later" CancelButtonTitle:@"OK"];
                    
                    return ;
                }
            }
            else
            {
                
                NSInteger rowID= [db insertEvent:txtTitle.text andDetails:txtVwDetails.text  Date:currentSelectedDate AlarmTone:str_Mp3name Repeat:IsRepeatAlarm SoundTone:0];
                
                //                NSInteger rowID= [db insertEvent:txtTitle.text Date:currentSelectedDate AlarmTone:str_Mp3name Repeat:IsRepeatAlarm SoundTone:0];
                
                NSString *str=[[NSString alloc]initWithFormat:@"%ld",(long)rowID];
                
                NSDictionary *infoDict = [NSDictionary dictionaryWithObjectsAndKeys:str, @"ID", nil];
                
                localNotification.userInfo = infoDict;
                
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
                
            }
            db=nil;
        }
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - TextField Navigation Methodsbase
-(void)doneEditingButtonClicked
{
    [txtTitle resignFirstResponder];
    [txtVwDetails resignFirstResponder];
}
-(void)prevButtonClicked
{
    if (activeTextField==txtTitle) {
        [txtVwDetails becomeFirstResponder];
    }
    else{
        [txtTitle becomeFirstResponder];
    }
}
-(void)nextButtonClicked
{
    if (activeTextField==txtTitle) {
        [txtVwDetails becomeFirstResponder];
    }
    else{
        [txtTitle becomeFirstResponder];
    }
}
#pragma mark - TextField
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    activeTextField = textField;
}

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    
    if ([txtVwDetails.text isEqualToString:@"Detail Description...."]) {
        txtVwDetails.text = @"";
    }
    
    txtVwDetails.textColor = [UIColor blackColor];
    return YES;
}

-(void) textViewDidChange:(UITextView *)textView
{
    
    if(txtVwDetails.text.length == 0){
        txtVwDetails.textColor = [UIColor lightGrayColor];
        txtVwDetails.text = @"Detail Description....";
        [txtVwDetails resignFirstResponder];
    }
}

#pragma mark - TextView Delegate
- (void)textViewDidBeginEditing:(UITextView *)textView{
    activeTextField = nil;
}


@end
