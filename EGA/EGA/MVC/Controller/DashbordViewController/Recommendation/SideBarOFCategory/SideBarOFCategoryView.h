//
//  SideBarOFCategoryView.h
//  FashionAndYou
//
//  Created by Chetan on 04/03/15.
//  Copyright (c) 2015 Chetan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideBarOFCategoryView : UIView<UITableViewDataSource,UITableViewDelegate>
{
    NSArray *arrCategory ;
    NSMutableArray*subCategoryCellCount;
    UITableView *tblCategory;

}
@property(nonatomic,retain)UITableView *tblCategory;

@end
