//
//  DashboardViewController.m
//  EGA
//
//  Created by Soniya on 03/10/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "DashboardViewController.h"
#import "DashboardMenuView.h"
#import "LoginEmailViewController.h"
#import "AttemptedDietPlanViewController.h"
#import "MyDietPlanViewController.h"
#import "ReduceWeightViewController.h"
#import "AnalyticQuestionDM.h"
#import "MyDietPlanDM.h"

@interface DashboardViewController ()<DashboardMenuViewDelegate,WebServiceCallerDelegate>{
     WebServiceCaller *webServiceCaller;

}
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *vwContainer;
@property (weak, nonatomic) IBOutlet DashboardMenuView *vwMenu;

@end

@implementation DashboardViewController
#pragma mark - Memory Management
-(void)removeReferences{
    _scrollView=nil;
    _vwContainer=nil;
   
    [webServiceCaller cancelWebserviceCall];
    webServiceCaller.delegate = nil;
    webServiceCaller = nil;
    
    
    [ProgressHUD dismiss];

}
#pragma mark - View Controller Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self screenDesignOfDashboard];
      // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
     webServiceCaller.delegate = self;
    self.navigationController.navigationBarHidden = NO;
}
-(void)viewWillDisappear:(BOOL)animated{
    webServiceCaller.delegate = nil;
    [super viewWillDisappear:YES];
}
-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    [self designDashboardButtons];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - ScreenDesigning Methods
-(void)screenDesignOfDashboard{
    _vwMenu.delegate =self;
    _scrollView.contentSize = CGSizeMake(screenWidth, _vwContainer.frame.size.height);
    webServiceCaller = [[WebServiceCaller alloc]init];
   
    [self SyncData];

    [self setCompnyLogoInNavigationBar];
    [self showListMenu];
    [self listClicked:nil];
 }
-(void)designDashboardButtons{
    for (int i=100; i<=106; i++) {
        UIView *view = [self.view viewWithTag:i];
        view.layer.borderColor = [UIColor colorWithHexString:BORDERCOLOR].CGColor;
        view.layer.cornerRadius =5.0f;
        view.layer.borderWidth =1.0;
        
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - Operational Methods
-(void)SyncData{
    if (appDelegate.isNetAvailable) {
        [ProgressHUD show:@"Loading..." Interaction:NO];
        [webServiceCaller getListOfDietPlan:appDelegate.userData.str_userID];
    }
    else{
        [SupportingClass showErrorMessage:NETWORKERRORMESSAGE];
    }
    
}
-(IBAction)navigateToDietPlan{
    
 
    if (appDelegate.intStatusCode == 402) {
        
        AttemptedDietPlanViewController * objAttempted=[self.storyboard instantiateViewControllerWithIdentifier:@"AttemptedDietPlanViewController"];
        
        [self.navigationController pushViewController:objAttempted animated:YES];
        
    }
    else if(appDelegate.intStatusCode == 403){
        
        
        NSString *StrAlertMessage =[NSString stringWithFormat:@"Welcome %@ %@ !Since You are login first time. Please attempted body analysis question to generate Diet Plan",appDelegate.userData.str_firstname,appDelegate.userData.str_lastname];
        
        [SupportingClass showAlertWithTitle:@"EGA Alert" Message:StrAlertMessage CancelButtonTitle:@"ok"];
        
        ReduceWeightViewController * objAttempted=[self.storyboard instantiateViewControllerWithIdentifier:@"ReduceWeightViewController"];
        
        [self.navigationController pushViewController:objAttempted animated:YES];

    }
    else{
        MyDietPlanViewController * objAttempted=[self.storyboard instantiateViewControllerWithIdentifier:@"MyDietPlanViewController"];
        
        [self.navigationController pushViewController:objAttempted animated:YES];

    }
    
}
-(void)listClicked:(UIBarButtonItem*)sender{
    [_vwMenu showHideSlideMenu];
}
-(void)logOutClicked{
    [SupportingClass showAlertWithTitle:@"" Message:@"Do you really want to logout" CancelButtonTitle:@"No" OtherButton:@"Yes" Delegate:self];
}
#pragma mark -  UIAlertView Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex==1) {
        appDelegate.userData = nil;
        [self removeReferences];
        NSUserDefaults * standardUserDefaults = [NSUserDefaults standardUserDefaults];
        [standardUserDefaults setObject:nil forKey:@"LoggedInUserID"];
        [standardUserDefaults setBool:NO forKey:@"AnalyticQuestionWebServiceCalled"];
         [standardUserDefaults setBool:NO forKey:@"AnalyticMessageDontShow"];
         [standardUserDefaults synchronize];
        [[UIApplication sharedApplication] cancelAllLocalNotifications];
        LoginEmailViewController *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginEmailViewController"];
        NSArray *viewController = [NSArray arrayWithObjects:loginVC, nil];
        [self.navigationController setViewControllers:viewController animated:YES];
    }
    
    
}
#pragma mark -  Webservice delegate

-(void)getListOfDietPlanCompletesSuccessfully:(BOOL)success StatusCode:(NSInteger)statusCode WithList:(NSMutableArray*)arrDiets WithMessage:(NSString*)message{
   
    
    if (statusCode == 401) {
        
        //  diet plan  available
        
        appDelegate.isDietPlanExpire = NO;
        appDelegate.strMessageRegardingDietPlan = message;
        appDelegate.intStatusCode =statusCode;
        [webServiceCaller getListOfAnalyticQuestionsAnswerOfUserID:appDelegate.userData.str_userID];
        
    }
    else if (statusCode == 402 )
    {
         [ProgressHUD dismiss];
        //  diet plan expire with date
        appDelegate.isDietPlanExpire = YES;
        appDelegate.strMessageRegardingDietPlan = message;
        appDelegate.intStatusCode =statusCode;
        MyDietPlanDM *dbDietPlan =[[MyDietPlanDM alloc]init];
        [dbDietPlan deleteDietPlan:appDelegate.userData.str_userID];  
        
    }
    else if (statusCode == 403)
    {
         [ProgressHUD dismiss];
        //  diet plan not available
        appDelegate.isDietPlanExpire = YES;
        appDelegate.strMessageRegardingDietPlan = message;
        appDelegate.intStatusCode =statusCode;
        MyDietPlanDM *dbDietPlan =[[MyDietPlanDM alloc]init];
        [dbDietPlan deleteDietPlan:appDelegate.userData.str_userID];
    }
    else{
         [ProgressHUD dismiss];
        [SupportingClass showErrorMessage:message];
    }
}

-(void)getListOfAnalyticQuestionsWithAnswerCompletesSuccessfully:(BOOL)success WithList:(NSMutableArray*)arrQuestions WithMessage:(NSString*)message
{
    [ProgressHUD dismiss];
     if (success) {
         if (arrQuestions.count>0) {
             AnalyticQuestionDM *analyticQuesDM=[[AnalyticQuestionDM alloc]init];
            [analyticQuesDM insertAnalyticsQuestionAnswers:arrQuestions userid:appDelegate.userData.str_userID];
        }
     }
    else{
        [SupportingClass showErrorMessage:message];
    }
    
    }
@end
