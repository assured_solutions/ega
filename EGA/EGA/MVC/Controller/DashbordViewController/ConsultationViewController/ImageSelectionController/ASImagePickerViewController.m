//
//  MultipleImagePickerViewController.m
//
//  Created by Soniya on 12/10/15.
//  Copyright (c) 2015 Assure Solutions Group. All rights reserved.
//

#import "ASImagePickerViewController.h"
#import "UIPlaceHolderTextView.h"
@interface ASImagePickerViewController ()
@property (weak, nonatomic) IBOutlet UIPlaceHolderTextView *txtVwComments;

@end

@implementation ASImagePickerViewController

- (instancetype)init
{
    self = [super initWithNibName:@"ASImagePickerViewController" bundle:[NSBundle mainBundle]];
    if (self) {
        self.sourceType = UIImagePickerControllerSourceTypePhotoLibrary; // Default
        self.images = [NSMutableArray new];
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // Refresh
    [self setCurrentImage:self.image];
    [self reloadCollectionView];
    [self refreshTitle];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self selectLastImage];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Background
    
    [self.collectionView registerClass:[ASImageCell class] forCellWithReuseIdentifier:@"ASImageCell"];
    
    self.selectedIndex = 0;
    [self selectLastImage];
   
    self.bgView.backgroundColor = [ConfigurationFile getThemeColor];
    
    // Background view for images collection
    self.bgView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.bgView.layer.shadowRadius = 3.0f;
    self.bgView.layer.shadowOpacity = 0.15f;
    
    // Customize default ImageView
    self.imageView.layer.masksToBounds = true;
    self.imageView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.imageView.layer.shadowOpacity = 0.3f;
    self.imageView.layer.shadowRadius = 6.0f;
    
    // Picker Controller Init
    self.pickerController = [[UIImagePickerController alloc] init];
    self.pickerController.delegate = self;
    self.pickerController.sourceType = self.sourceType;
    
    
    // Set buttons to navigation
//    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel", @"Cancel") style:UIBarButtonItemStylePlain target:self action:@selector(cancel)];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(done)];
    
    //[self.navigationItem setLeftBarButtonItem:cancelButton];
    [self.navigationItem setRightBarButtonItem:doneButton];
    _lblMessage.layer.borderWidth = 1.0;
    _txtVwComments.layer.borderWidth = 1.0;
    self.lblMessage.layer.borderColor = [UIColor colorWithHexString:BORDERCOLOR].CGColor;
    self.txtVwComments.layer.borderColor = [UIColor colorWithHexString:BORDERCOLOR].CGColor;
    [self createKeyboardAccessoryToolBar:NO];
    self.txtVwComments.inputAccessoryView = accessoryToolBar;
   _txtVwComments.placeholder = @"Comment....";
    

//    
//    // Set ImageView size
//    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){ // iPhone
//        
//        if([[UIScreen mainScreen] bounds].size.height > 500){
//            self.imageView.bounds = CGRectMake(self.imageView.bounds.origin.x, self.imageView.bounds.origin.y, 240, 350);
//        }else{
//            self.imageView.bounds = CGRectMake(self.imageView.bounds.origin.x, self.imageView.bounds.origin.y, 190, 300);
//        }
//        
//    }else{
//        // TODO: iPad
//    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Util

- (void) addImage:(UIImage *)image
{
    self.image = image;
    [self.images addObject:image];
}

- (void) selectLastImage
{
    if(self.images.count > 0){
        self.imageView.hidden = NO;
        self.btRemover.hidden = NO;
        self.lblMessage.hidden = YES;
        self.selectedIndex = self.images.count;
        [self.collectionView selectItemAtIndexPath:[NSIndexPath indexPathForItem:self.selectedIndex inSection:0] animated:YES scrollPosition:UICollectionViewScrollPositionRight];
    }else{
        self.selectedIndex = 0;
        
        self.imageView.hidden = YES;
        self.btRemover.hidden = YES;
        self.lblMessage.hidden = NO;
        
    }
}

- (void) setCurrentImage:(UIImage *) image {
    if (image!=nil){
    self.imageView.image = image;
//    double heigth = self.imageView.image.size.height * self.imageView.frame.size.width / self.imageView.image.size.width;
//    self.imageView.bounds = CGRectMake(self.imageView.bounds.origin.x, self.imageView.bounds.origin.y, self.imageView.bounds.size.width, heigth);
//    
//    // Positioning button x
//    double btX = (self.imageView.center.x - (self.imageView.frame.size.width/2)) - 15;
//    double btY = (self.imageView.center.y - (self.imageView.frame.size.height/2)) - 15;
//    self.btRemover.frame = CGRectMake(btX, btY, self.btRemover.frame.size.width, self.btRemover.frame.size.height);
    }
    
    
}

- (void) refreshTitle
{
    if(self.images.count == 1){
        self.title = [NSString stringWithFormat:@"1 %@", NSLocalizedString(@"image", @"image")];
    }else if(self.images.count > 1){
        self.title = [NSString stringWithFormat:@"%lu %@", (unsigned long)self.images.count, NSLocalizedString(@"images", @"images")];
    }
}

- (void) done
{
    
    if(self.doneCallback){
        self.doneCallback(self.images);
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) cancel
{
    self.image = nil;
    self.images = nil;
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) reloadCollectionView
{
    [self.collectionView performBatchUpdates:^{
        [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:0]];
    } completion:nil];
}
-(void)openImagePickers:(BOOL)isCamera{
    if (isCamera) {
        self.pickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    else{
         self.pickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    [self presentViewController: self.pickerController animated:YES completion:nil];
}
#pragma mark - Collection view delegate

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.images.count + 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"ASImageCell";
    ASImageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    if(cell == nil){
        cell = [[ASImageCell alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 50.0f, 50.0f)];
    }
    
    if(indexPath.row == 0){
        [cell styleAddButton];
        cell.backgroundImageView.image = nil;
    }else{
        [cell styleImage];
        cell.backgroundImageView.image = [self.images objectAtIndex:(indexPath.row-1)];
    }

    return cell;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(50, 50);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0){
        
        UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Choose Snapshot " message:@"(of prescription, reports etc)" preferredStyle:UIAlertControllerStyleActionSheet];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            // Camera  button tappped.
            [self openImagePickers:YES];
        }]];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Photo Library" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            // PhotoLibrary button tapped.
            [self openImagePickers:NO];
            
        }]];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            
            // Cancel button tapped.
            
            [self dismissViewControllerAnimated:YES completion:^{
            }];
        }]];
        
        // Present action sheet.
        [self presentViewController:actionSheet animated:YES completion:nil];
        
    }else{
        
        [self setCurrentImage:[self.images objectAtIndex:(indexPath.row-1)]];
        self.selectedIndex = indexPath.row;
        
    }
}

#pragma mark - ImagePicker delegate methods

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
    UIImage *originalImage, *editedImage, *imageToUse;
    
    if (CFStringCompare ((CFStringRef) mediaType, kUTTypeImage, 0) == kCFCompareEqualTo) {
        
        if(self.images == nil){
            self.images = [NSMutableArray new];
        }
        
        editedImage = (UIImage *) [info objectForKey: UIImagePickerControllerEditedImage]; // Edited image if available
        originalImage = (UIImage *) [info objectForKey: UIImagePickerControllerOriginalImage];
        imageToUse = editedImage ? editedImage : originalImage;
        
        [self addImage:imageToUse];
        [picker dismissViewControllerAnimated:YES completion:nil];
        
    }
    
}

#pragma mark - UI navigation bar delegate

- (void)navigationController:(UINavigationController *)navigationController
      willShowViewController:(UIViewController *)viewController
                    animated:(BOOL)animated {
    
    navigationController.navigationBar.barStyle = UIBarStyleBlack;
    
}

#pragma mark - IBActions

-(IBAction)remove:(id)sender
{
    [self.images removeObjectAtIndex:(self.selectedIndex-1)];
    [self reloadCollectionView];
    [self selectLastImage];
    
    if(self.images.count > 0){
        [self setCurrentImage:[self.images objectAtIndex:(self.selectedIndex-1)]];
    }else{
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
    [self refreshTitle];
}
#pragma mark - TextField Navigation Methodsbase
-(void)doneEditingButtonClicked
{
    [_txtVwComments resignFirstResponder];
    
}
@end
