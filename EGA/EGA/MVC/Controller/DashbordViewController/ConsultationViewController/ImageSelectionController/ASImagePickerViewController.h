//
//  MultipleImagePickerViewController.h
//
//  Created by Soniya on 12/10/15.
//  Copyright (c) 2015 Assure Solutions Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "ASImageCell.h"

typedef void (^ ASImagePickerDoneCallback)(NSArray *images);

@interface ASImagePickerViewController : BaseViewController<UICollectionViewDataSource, UICollectionViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UINavigationBarDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblMessage;
@property (nonatomic, copy) ASImagePickerDoneCallback doneCallback;
@property (nonatomic, strong) UIImagePickerController *pickerController;
@property (nonatomic, strong) NSMutableArray *images;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) IBOutlet UIButton *btRemover;
@property (nonatomic, strong) IBOutlet UIImageView *imageView;
@property (nonatomic, strong) IBOutlet UICollectionView *collectionView;
@property (nonatomic, weak) IBOutlet UIView *bgView;
@property (nonatomic, readwrite) NSUInteger selectedIndex;
@property (nonatomic, readwrite) UIImagePickerControllerSourceType sourceType;

- (void)addImage:(UIImage*)image;
- (IBAction)remove:(id)sender;

@end
