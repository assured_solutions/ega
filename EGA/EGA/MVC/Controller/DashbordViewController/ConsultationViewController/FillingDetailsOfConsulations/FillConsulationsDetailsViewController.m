//
//  FillConsulationsDetailsViewController.m
//  EGA
//
//  Created by animesh bansal on 9/27/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "FillConsulationsDetailsViewController.h"
#import "FillDetailHeaderView.h"
#import "ASCustomButton.h"
#import "TxtBtnCustomCell.h"


@interface FillConsulationsDetailsViewController ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>{
    NSMutableArray * arrMedicalPrescribed;
    NSMutableArray * arrConsultations;
    NSMutableArray * arrPathologyTest;
    NSString *recordName;
     NSString *txtVwDesc;
     NSString *txtDoctorName;
     NSString *txtConsultationDate;
     NSString *txtNextConsultationDate;
}
@property (weak, nonatomic) IBOutlet UITableView *tblView;

@end

@implementation FillConsulationsDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    arrMedicalPrescribed = [[NSMutableArray alloc]init];
    arrConsultations = [[NSMutableArray alloc]init];
    arrPathologyTest = [[NSMutableArray alloc]init];
    [self.tblView setEditing:YES animated:YES];
    [self registerForKeyboardNotifications];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)reminderButtonClicked:(UIButton*)sender{
    NSLog(@"%ld",(long)sender.tag);
}
#pragma mark - TableView DataSource and Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (section ==0) {
        return 5;
    }
    else if (section ==1){
        return arrMedicalPrescribed.count+1;
    }
    else if (section ==2){
        return arrConsultations.count+1;
    }
    else if (section ==3){
        return arrPathologyTest.count+1;
    }
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
   
    return 45;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 3) {
        return 45;
    }
    return 0;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
   
     FillDetailHeaderView *vwHeader = [[FillDetailHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 45)];
    if (section == 0) {
        vwHeader.lblTitle.text = @"Fill Basic Details";
        vwHeader.btnReminder.hidden = YES;
     }
    else if (section == 1) {
        vwHeader.lblTitle.text = @"Medicine Prescribed";
    }
    else if (section == 2) {
        vwHeader.lblTitle.text = @"Consultations";
    }
    else if (section == 3) {
        vwHeader.lblTitle.text = @"Pathology Test";
    }
    vwHeader.btnReminder.tag = section;
    [vwHeader.btnReminder addTarget:self action:@selector(reminderButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    return vwHeader;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section ==3) {
        ASCustomButton *btnFooter = [[ASCustomButton alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 45)];
        [btnFooter setTitle:@"Submit Record" forState:UIControlStateNormal];
        [btnFooter addTarget:self action:@selector(reminderButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        return btnFooter;

    }
    return nil;
    }

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Create an instance of ItemCell
    
    NSString *identifier = @"TxtBtnCustomCell";
    NSString *rowString;
    BOOL hide = YES;
    if (indexPath.section == 0){
        if (indexPath.row == 0) {
            rowString = @"Record Name";
        }
        else if (indexPath.row == 1) {
            rowString = @"A Brief note will help you identify record";
        }
         else if (indexPath.row == 2) {
          
             rowString = @"Doctor Name";
        }
        else if (indexPath.row == 3) {
            hide = NO;
            rowString = @"Consultation date";
        }
        else if (indexPath.row == 4) {
             rowString = @"Next Consultation date";
        }
           }
    else if (indexPath.section == 1){
        if (indexPath.row == arrMedicalPrescribed.count) {
            identifier = @"Normal";
            rowString = @"Add medicine prescribed";
        }
        
    }
    else if (indexPath.section == 2){
        if (indexPath.row == arrConsultations.count) {
            identifier = @"Normal";
            rowString = @"Add any other consultations";
        }
        
    }
    else if (indexPath.section == 3){
        if (indexPath.row == arrPathologyTest.count) {
            identifier = @"Normal";
            rowString = @"Add pathology test recommended";
        }
    }
    
    if ([identifier isEqualToString:@"TxtBtnCustomCell"]) {
        TxtBtnCustomCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        cell.txtValue.placeholder = rowString;
        cell.txtValue.delegate=self;
        [cell hideReminder:hide];
        return cell;
    }
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (rowString.length>0) {
        cell.textLabel.text = rowString;
    }
    
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    BOOL callEdit = NO;
    if (indexPath.section == 1){
        if (indexPath.row == arrMedicalPrescribed.count) {
            callEdit =YES;
        }
        
    }
    else if (indexPath.section == 2){
        if (indexPath.row == arrConsultations.count) {
            callEdit = YES;
        }
        
    }
    else if (indexPath.section == 3){
        if (indexPath.row == arrPathologyTest.count) {
            callEdit = YES;
        }
    }
    if (callEdit) {
         [self insertRowWithIndexPath:indexPath];
    }

}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section>0) {
        return YES;
    }
    return NO;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section==1) {
        if (indexPath.row == arrMedicalPrescribed.count) {
            return UITableViewCellEditingStyleInsert;
        } else {
            return UITableViewCellEditingStyleDelete;
        }
        
    }
    else if (indexPath.section==2) {
        if (indexPath.row == arrConsultations.count) {
            return UITableViewCellEditingStyleInsert;
        } else {
            return UITableViewCellEditingStyleDelete;
        }
        
    }
    else if (indexPath.section==3) {
        if (indexPath.row == arrPathologyTest.count) {
            return UITableViewCellEditingStyleInsert;
        } else {
            return UITableViewCellEditingStyleDelete;
        }
        
    }
    return UITableViewCellEditingStyleNone;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    // If row is deleted, remove it from the list.
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        if (indexPath.section==1) {
            [arrMedicalPrescribed removeObjectAtIndex:indexPath.row];
            
        }
        else if (indexPath.section==2) {
            [arrConsultations removeObjectAtIndex:indexPath.row];
        }
        else if (indexPath.section==3) {
            [arrPathologyTest removeObjectAtIndex:indexPath.row];
        }
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
    else{
        [self insertRowWithIndexPath:indexPath];
    }
}

-(void)insertRowWithIndexPath:(NSIndexPath*)indexPath{
    if (indexPath.section==1) {
        [arrMedicalPrescribed insertObject:@"" atIndex:arrMedicalPrescribed.count];
        
    }
    else if (indexPath.section==2) {
        [arrConsultations insertObject:@"" atIndex:arrConsultations.count];
    }
    else if (indexPath.section==3) {
        [arrPathologyTest insertObject:@"" atIndex:arrPathologyTest.count];
    }
    
    
    [self.tblView insertRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];

}


// Call this method somewhere in your view controller setup code.
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height+10, 0.0);
    self.tblView.contentInset = contentInsets;
    self.tblView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, activeTextField.frame.origin) ) {
        [self.tblView scrollRectToVisible:activeTextField.frame animated:YES];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.tblView.contentInset = contentInsets;
    self.tblView.scrollIndicatorInsets = contentInsets;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeTextField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    activeTextField = nil;
}

//- (void)keyboardWasShown:(NSNotification*)aNotification {
//    NSDictionary* info = [aNotification userInfo];
//    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
//    CGRect bkgndRect = activeTextField.superview.frame;
//    bkgndRect.size.height += kbSize.height;
//    [activeTextField.superview setFrame:bkgndRect];
//    [self.tblView setContentOffset:CGPointMake(0.0, activeTextField.frame.origin.y-kbSize.height) animated:YES];
//}
@end
