//
//  FillDetailHeaderView.h
//  EGA
//
//  Created by Soniya on 05/10/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FillDetailHeaderView : UIView
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnReminder;

@end
