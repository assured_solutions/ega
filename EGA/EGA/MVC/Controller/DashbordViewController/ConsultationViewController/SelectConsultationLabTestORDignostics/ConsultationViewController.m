//
//  ConsultationViewController.m
//  EGA
//
//  Created by animesh bansal on 9/27/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "ConsultationViewController.h"

@interface ConsultationViewController ()

@end

@implementation ConsultationViewController

@synthesize btnConsultandLabTest;
@synthesize btnSelfDiagnostics;

#pragma mark - View Controller Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self ScreenDesigningOfConsultantView];
    // Do any additional setup after loading the view.
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ScreenDesigning Methods

-(void)ScreenDesigningOfConsultantView{
    btnConsultandLabTest.layer.cornerRadius=10;
    btnConsultandLabTest.layer.borderWidth=2.0f;
    btnConsultandLabTest.layer.borderColor = [ConfigurationFile getThemeColor].CGColor;
    
    btnSelfDiagnostics.layer.cornerRadius=10;
    btnSelfDiagnostics.layer.borderWidth=2.0f;
    btnSelfDiagnostics.layer.borderColor = [ConfigurationFile getThemeColor].CGColor;
 
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
