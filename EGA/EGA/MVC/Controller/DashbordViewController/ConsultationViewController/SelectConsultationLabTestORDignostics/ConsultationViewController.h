//
//  ConsultationViewController.h
//  EGA
//
//  Created by animesh bansal on 9/27/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConsultationViewController : BaseViewController
@property(nonatomic,retain)IBOutlet UIButton *btnConsultandLabTest;
@property(nonatomic,retain)IBOutlet UIButton *btnSelfDiagnostics;

@end
