//
//  FillDetailHeaderView.m
//  EGA
//
//  Created by Soniya on 05/10/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "FillDetailHeaderView.h"

@implementation FillDetailHeaderView
@synthesize lblTitle;
@synthesize btnReminder;
-(id)initWithFrame:(CGRect)frame {
    if ( !(self = [super initWithFrame:frame]) ) return nil;
    [self awakeFromNib];
    return self;
}

- (id)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self awakeFromNib];
    }
    return self;
}
-(void)awakeFromNib {
    UIView *xibView = [[[NSBundle mainBundle] loadNibNamed:@"FillDetailHeaderView" owner:self options:nil] objectAtIndex:0];
    [self addSubview:xibView];
    
    xibView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    [self viewDesigning];
}

-(void)viewDesigning{
    btnReminder.tintColor = [UIColor whiteColor];
    self.backgroundColor = [UIColor blackColor];//[ConfigurationFile getThemeColor];
    self.autoresizingMask =  UIViewAutoresizingFlexibleRightMargin |
    UIViewAutoresizingFlexibleLeftMargin |
    UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    
}

@end
