//
//  BiologicalQuesListViewController.m
//  EGA
//
//  Created by Soniya on 01/11/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "BiologicalQuesListViewController.h"
#import "BiologicalAgeHeaderView.h"
#import "ASCustomButton.h"
#import "BiologicaAgeViewController.h"
@interface BiologicalQuesListViewController ()<WebServiceCallerDelegate>{
    NSMutableArray *arrQuestionsList;
    __weak IBOutlet UITableView *tblQuesList;
    
    __weak IBOutlet UIView *vwFooter;
    WebServiceCaller *webServiceCaller;
}
@end

@implementation BiologicalQuesListViewController
@synthesize age;
#pragma mark - Memory Management

-(void)removeReferences{
    [webServiceCaller cancelWebserviceCall];
    webServiceCaller.delegate = nil;
    webServiceCaller = nil;
    
    
    [ProgressHUD dismiss];
}
#pragma mark - View Controller Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self screenDesignOfBiologicalQues];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma mark - ScreenDesigning Methods
-(void)screenDesignOfBiologicalQues{
    [self setTitle:@"Biological Age" SubTitle:@"Know your biological age"];
    webServiceCaller = [[WebServiceCaller alloc]init];
    webServiceCaller.delegate = self;
    [self showQuestions];
}
#pragma mark - Operational Methods
-(void)showQuestions{
    if (appDelegate.isNetAvailable) {
        [ProgressHUD show:@"Loading..." Interaction:NO];
        [webServiceCaller cancelAllCalls];
        [webServiceCaller getListOfBiologicalAgeQuestions:appDelegate.userData.str_userID];

    }
    else{
        [SupportingClass showErrorMessage:NETWORKERRORMESSAGE];
    }
 }

-(IBAction)submitButtonClicked:(UIButton*)sender{
    if (appDelegate.isNetAvailable) {
        if ([self validateAllFields]) {
            [ProgressHUD show:@"Submitting..." Interaction:NO];
            NSMutableArray *arrItems = [[NSMutableArray alloc]init];
            for (NSDictionary *dictQuestion in arrQuestionsList ) {
                
                NSArray *arrOptions = [dictQuestion objectForKey:@"answers"];
                NSString *optionVal = @"0";
                for (NSDictionary *optionsDetail in arrOptions) {
                    //   optionid = [optionsDetail objectForKey:@"opid"];
                    if( [[optionsDetail objectForKey:@"isSelected"] isEqualToString:@"Yes"]){
                        optionVal= [optionsDetail objectForKey:@"opvalue"];
                    }
                    
                }
                
                NSDictionary *dict = [[NSDictionary alloc]initWithObjectsAndKeys:[dictQuestion objectForKey:@"qid"],@"questionid",optionVal,@"opvalue", nil];
                [arrItems addObject:dict];
                dict=nil;
            }
            //send this as a json
            NSDictionary *dictAnswers = [[NSDictionary alloc]initWithObjectsAndKeys:appDelegate.userData.str_userID,@"userid",arrItems,@"question", nil];
            [webServiceCaller submitBiologicalAgeQuestions:dictAnswers UserID:appDelegate.userData.str_userID];
        }

    }
    else{
        [SupportingClass showErrorMessage:NETWORKERRORMESSAGE];
    }
   }
-(BOOL)validateAllFields{
    
    NSString *message=nil;
    for (NSDictionary *dictQuestion in arrQuestionsList ) {
        NSArray *arrOptions = [dictQuestion objectForKey:@"answers"];
        BOOL notSelected = YES;
        for (NSDictionary *optionsDetail in arrOptions) {
            if( [[optionsDetail objectForKey:@"isSelected"] isEqualToString:@"Yes"]){
                notSelected = NO;
                break;
            }
            
        }
        if (notSelected) {
            message = @"Please answer to all questions";
            break;
        }
    }
    if (message!=nil) {
        [SupportingClass showErrorMessage:message];
        return NO;
    }
    return YES;
}
-(CGFloat)getHeightForQuesText:(NSInteger)section{
    NSDictionary *dictQuestion = [arrQuestionsList objectAtIndex:section];
    NSString *quesText = [dictQuestion objectForKey:@"question"];
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:18 weight:UIFontWeightLight]};
    // NSString class method: boundingRectWithSize:options:attributes:context is
    // available only on ios7.0 sdk.
    CGRect rect = [quesText boundingRectWithSize:CGSizeMake(tblQuesList.frame.size.width, CGFLOAT_MAX)
                                         options:NSStringDrawingUsesLineFragmentOrigin
                                      attributes:attributes
                                         context:nil];
    
    //
    //    CGSize sizeOFString = [[quesText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] sizeWithFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:18] constrainedToSize:CGSizeMake(screenWidth-30, 2000) lineBreakMode:NSLineBreakByWordWrapping];
    
    //CGFloat height = rect.size.height+50;
    
    CGFloat height = rect.size.height+40;
    
    if (height<=80)
        return 80;
    return height;
}


#pragma mark - Table View DataSource And Delegate Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return arrQuestionsList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSDictionary *dictQuestion = [arrQuestionsList objectAtIndex:section];
    NSArray *arrOptions = [dictQuestion objectForKey:@"answers"];
    return arrOptions.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return [self getHeightForQuesText:section];
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == arrQuestionsList.count-1) {
        return vwFooter.frame.size.height;
    }
    return 0;
}
- ( UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    NSDictionary *dictQuestion = [arrQuestionsList objectAtIndex:section];
    BiologicalAgeHeaderView * headerView = [[BiologicalAgeHeaderView alloc]initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, [self getHeightForQuesText:section])];
    headerView.lblQuesText.text = [dictQuestion objectForKey:@"question"];
    [headerView.lblQuesText sizeToFit];
    return headerView;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section ==arrQuestionsList.count-1) {
        return vwFooter;
        
    }
    return nil;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Create an instance of ItemCell
    UITableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"OptionCell"];
    NSDictionary *dictQuestion = [arrQuestionsList objectAtIndex:indexPath.section];
    NSArray *arrOptions = [dictQuestion objectForKey:@"answers"];
    NSDictionary *optionsDetail = [arrOptions objectAtIndex:indexPath.row];
    cell.textLabel.text = [optionsDetail objectForKey:@"optitle"];
    cell.detailTextLabel.text = [optionsDetail objectForKey:@"opsubtitle"];
    UIImage *img = [UIImage imageNamed:@"RadioButton"];
    if ([[optionsDetail objectForKey:@"isSelected"] isEqualToString:@"Yes"]) {
        img = [UIImage imageNamed:@"RadioButtonSelected"];
    }
    cell.imageView.image = img;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dictQuestion = [arrQuestionsList objectAtIndex:indexPath.section];
    NSArray *arrOptions = [dictQuestion objectForKey:@"answers"];
    NSDictionary *optionsDetail = [arrOptions objectAtIndex:indexPath.row];
    NSString *selected = @"No";
    if ([[optionsDetail objectForKey:@"isSelected"] isEqualToString:@"No"]||![optionsDetail objectForKey:@"isSelected"]) {
        selected = @"Yes";
    }
    
    for (NSDictionary *dict in arrOptions) {
        [dict setValue:@"No" forKey:@"isSelected"];
    }
    [optionsDetail setValue:selected forKey:@"isSelected"];
    [tblQuesList reloadData];
}

#pragma mark -  Webservice delegate

-(void)getListOfBiologicalAgeQuestionsCompletesSuccessfully:(BOOL)success WithList:(NSMutableArray*)arrQuestions WithMessage:(NSString*)message{
    [ProgressHUD dismiss];
    vwFooter.hidden = NO;
    if (success) {
        arrQuestionsList = arrQuestions;
        [tblQuesList reloadData];
        
    }
    else{
        [SupportingClass showErrorMessage:message];
    }
    if (arrQuestionsList.count==0) {
        vwFooter.hidden = YES;
    }
}

-(void)submitBiologicalAgeQuestionsCompletesSuccessfully:(BOOL)success WithData:(NSDictionary*)dictResult WithMessage:(NSString*)message{
    [ProgressHUD dismiss];
    if (success) {
        BiologicaAgeViewController*biologicalAgeVC=[self.storyboard instantiateViewControllerWithIdentifier:@"BiologicaAgeViewController"];
        biologicalAgeVC.actualAge = [[dictResult objectForKey:@"currentage"] floatValue];
        biologicalAgeVC.biologicalAge = [[dictResult objectForKey:@"biologicalage"] floatValue];
        [self.navigationController pushViewController:biologicalAgeVC animated:YES];
    }
    else{
        [SupportingClass showErrorMessage:message];
    }
}
@end
