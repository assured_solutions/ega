//
//  BiologicalCalculatorViewController.m
//  EGA
//
//  Created by Soniya on 01/11/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "BiologicalCalculatorViewController.h"
#import "BiologicalQuesListViewController.h"


@interface BiologicalCalculatorViewController ()<WebServiceCallerDelegate>{
    WebServiceCaller *webServiceCaller;
    UIDatePicker *datePicker;
    __weak IBOutlet UIButton *btnEdit;
    float age;
    NSString *oldDOB;
}
@property (weak, nonatomic) IBOutlet UILabel *lblAge;
@property (weak, nonatomic) IBOutlet UITextField *txtBirthDate;

@end

@implementation BiologicalCalculatorViewController

#pragma mark - Memory Management
-(void)removeReferences{

    [webServiceCaller cancelWebserviceCall];
    webServiceCaller.delegate = nil;
    webServiceCaller = nil;
    
    
    [ProgressHUD dismiss];
}

#pragma mark - View Controller Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self screenDesigningForBiologicalCalculator];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - ScreenDesigning Methods
-(void)screenDesigningForBiologicalCalculator{
    self.navigationItem.title = @"Biological Age";
    accessoryToolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    accessoryToolBar.barTintColor = [ConfigurationFile getThemeColor];
    accessoryToolBar.tintColor = [UIColor whiteColor];
    UIBarButtonItem* flexSpace = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
      UIBarButtonItem* doneButton = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneEditingButtonClicked)];
    UIBarButtonItem* cancelButton = [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone target:self action:@selector(cancelEditingButtonClicked)];
      NSMutableArray* items = [[NSMutableArray alloc]init];
    [items addObject:doneButton];
     [items addObject:flexSpace];
    [items addObject:cancelButton];
    accessoryToolBar.items = items;
    [accessoryToolBar sizeToFit];

    _txtBirthDate.inputAccessoryView = accessoryToolBar;
    datePicker = [[UIDatePicker alloc]init];
    datePicker.datePickerMode = UIDatePickerModeDate;
    datePicker.maximumDate = [NSDate date];
    _txtBirthDate.inputView = datePicker;
    
    _txtBirthDate.text = [SupportingClass getStringDate:appDelegate.userData.str_dob From:[SupportingClass dateFormat] to:DisplayDateFormat];
    [self getAgeOfUser];
    _lblAge.text = [NSString stringWithFormat:@"%.0f",age];
    
    webServiceCaller = [[WebServiceCaller alloc]init];
    webServiceCaller.delegate = self;
 }
#pragma mark - TextField Navigation Methodsbase
-(void)doneEditingButtonClicked
{
    [self editDateOfBirth:btnEdit];
}
-(void)cancelEditingButtonClicked
{
    [btnEdit setTitle:@"Edit" forState:UIControlStateNormal];
    _txtBirthDate.enabled = NO;
    [self.txtBirthDate resignFirstResponder];
 }
-(void)updateDOB:(UIDatePicker*)sender{
    NSDateFormatter *dateFomratter = [[NSDateFormatter alloc]init];
    dateFomratter.dateFormat = DisplayDateFormat;
    _txtBirthDate.text = [dateFomratter stringFromDate:sender.date];
    //[self getAgeOfUser];
}
-(void)callUpdateDOBWebService {
    if (appDelegate.isNetAvailable){
            [ProgressHUD show:@"Updating DOB..." Interaction:NO];
        NSString *dob = [SupportingClass getStringDate:_txtBirthDate.text From:DisplayDateFormat to:[SupportingClass dateFormat]];
            [webServiceCaller updateDOBOfUser:appDelegate.userData.str_userID WithDOB:dob];
            
        }
    else{
        [SupportingClass showErrorMessage:NETWORKERRORMESSAGE];
        
    }

}
#pragma mark - Operational Methods
- (IBAction)submitClicked:(id)sender {
    if (appDelegate.isNetAvailable){
        if ([self validateAllFields]) {
            BiologicalQuesListViewController *biologicalVC = [self.storyboard instantiateViewControllerWithIdentifier:@"BiologicalQuesListViewController"];
            biologicalVC.age =age;
            [self.navigationController pushViewController:biologicalVC animated:YES];

        }
        
        }
    
    else{
        [SupportingClass showErrorMessage:NETWORKERRORMESSAGE];
     }
}
-(BOOL)validateAllFields{
   
    NSString* dob = _txtBirthDate.text;
    NSString * errorMessage = nil;
    if(dob.length<=0){
        errorMessage = [SupportingClass blankDOBErrorMessage];
    }
    
    
    [_txtBirthDate resignFirstResponder];
    if (errorMessage!=nil) {
        [SupportingClass showErrorMessage:errorMessage];
        return NO;
    }
    return YES;
}
- (IBAction)editDateOfBirth:(UIButton*)sender {
    if ([sender.titleLabel.text isEqualToString:@"Edit"]) {
        [sender setTitle:@"Done" forState:UIControlStateNormal];
        _txtBirthDate.enabled = YES;
        [_txtBirthDate becomeFirstResponder];
    }
    else{
       
         [sender setTitle:@"Edit" forState:UIControlStateNormal];
          oldDOB= _txtBirthDate.text;
         [self updateDOB:datePicker];
       // _lblAge.text = [NSString stringWithFormat:@"%.0f",age];
        _txtBirthDate.enabled = NO;
         [_txtBirthDate resignFirstResponder];
        //Call Webservice
        [self callUpdateDOBWebService];
    }
}

-(float)getAgeOfUser{
                NSDate *today = [NSDate date];
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:DisplayDateFormat];
                NSDate *birthdate = [dateFormatter dateFromString: _txtBirthDate.text];
                NSDateComponents *ageComponents = [[NSCalendar currentCalendar]
                                                   components:NSCalendarUnitYear
                                                   fromDate:birthdate
                                                   toDate:today
                                                   options:0];
    age = ageComponents.year;
    
            return age;
}


#pragma mark - TextView Delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    NSDateFormatter *dateFomratter = [[NSDateFormatter alloc]init];
    dateFomratter.dateFormat = DisplayDateFormat;//[SupportingClass dateFormat];
    NSDate *date = [dateFomratter dateFromString:_txtBirthDate.text];
   // NSString *strdate
    if (date!=nil) {
        [datePicker setDate:date animated:NO];
    }
    
}

#pragma mark -  Webservice delegate

-(void)updateDOBOfUserCompletesSuccessfully:(BOOL)success WithMessage:(NSString*)message{
    [ProgressHUD dismiss];
    if (success) {
        NSString *dob = [SupportingClass getStringDate:_txtBirthDate.text From:DisplayDateFormat to:[SupportingClass dateFormat]];
        appDelegate.userData.str_dob = dob;
        [appDelegate.userData updateDOBInDB];
        [self getAgeOfUser];
        _lblAge.text = [NSString stringWithFormat:@"%.0f",age];
    }
    else{
        _txtBirthDate.text = oldDOB;
        oldDOB = nil;
        if ([message isEqualToString:NETWORKERRORMESSAGE]) {
            [SupportingClass showErrorMessage:message];
        }
        else {
            [SupportingClass showAlertWithTitle:@"" Message:message CancelButtonTitle:@"OK"];
        }
    }
}

@end
