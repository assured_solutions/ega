//
//  SendReportToMailViewController.h
//  EGA
//
//  Created by Soniya on 04/10/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "BaseViewController.h"

@interface SendReportToMailViewController : BaseViewController
@property(nonatomic,strong)NSString *strEmailID;
@end
