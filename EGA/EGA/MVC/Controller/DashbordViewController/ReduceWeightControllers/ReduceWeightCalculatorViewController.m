//
//  ReduceWeightCalculatorViewController.m
//  EGA
//
//  Created by Soniya on 14/11/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "ReduceWeightCalculatorViewController.h"
#import "BodyAnalyisViewController.h"
#import "AttemptedBodyAnalysisViewController.h"

#define defaultTarget @"Select your Target"
@interface ReduceWeightCalculatorViewController ()<WebServiceCallerDelegate,UIPickerViewDataSource,UIPickerViewDelegate>{
    WebServiceCaller *webServiceCaller;
    NSMutableArray *arrTargetList;
     UIPickerView *dropDownPickerView;
    NSInteger selectedTargetIndex;
    NSString *unit;
    float expectedWeight;
    float _weightExceed;
}
@property (weak, nonatomic) IBOutlet UILabel *lblWeight;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblMetricUnit;
@property (weak, nonatomic) IBOutlet UITextField *txtTarget;

@end

@implementation ReduceWeightCalculatorViewController
#pragma mark - Memory Management

-(void)removeReferences{
    [webServiceCaller cancelWebserviceCall];
    webServiceCaller.delegate = nil;
    webServiceCaller = nil;
    
    
    [ProgressHUD dismiss];
}
#pragma mark - View Controller Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self ScreenDesigningForReduceWeight];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - ScreenDesigning Methods

-(void)ScreenDesigningForReduceWeight{

    [self setTitle:@"Reduce Weight" SubTitle:@"Set your target"];
    float height = [appDelegate.userData.str_height floatValue]/100;
    expectedWeight = BMI * height * height;
    float actualWeight = [appDelegate.userData.str_weight floatValue];
    _weightExceed = actualWeight-expectedWeight;
    
    [self createKeyboardAccessoryToolBar:NO];
    self.txtTarget.inputAccessoryView = accessoryToolBar;
    
    
    dropDownPickerView = [[UIPickerView alloc] init];
    dropDownPickerView.dataSource = self;
    dropDownPickerView.delegate = self;
    dropDownPickerView.showsSelectionIndicator = YES;
    self.txtTarget.inputView = dropDownPickerView;
    
    NSString *strTitle =@"You are of normal weight";
    _weightExceed = [[NSString stringWithFormat:@"%0.1f",_weightExceed] floatValue];
    if (_weightExceed<0) {
        strTitle =@"You are underweight by";
        _weightExceed = (0 - _weightExceed);
    }
    else if (_weightExceed>0){
        strTitle =@"You are overweight by";
    }
    else if(_weightExceed==0){
        strTitle =@"You are having normal weight";
    }
   
    _lblTitle.text = strTitle;
    
    if (_isUnitInKg) {
         _lblWeight.text = [NSString stringWithFormat:@"%0.1f",_weightExceed];
        unit = @"Kg";
    }
    else{
        _lblWeight.text = [NSString stringWithFormat:@"%0.1f",_weightExceed*2.20462];
        unit = @"Lbs";

    }
    _lblMetricUnit.text= unit;
    
    if (_weightExceed==0){
         _lblWeight.text = @"0";
    }
    
    webServiceCaller = [[WebServiceCaller alloc]init];
    webServiceCaller.delegate = self;
    [self getTargets];
}

#pragma mark - Operational Methods
-(void)getTargets{
    if (appDelegate.isNetAvailable) {
        [ProgressHUD show:@"Loading..." Interaction:NO];
        [webServiceCaller cancelAllCalls];
        [webServiceCaller getTargets];
    }
    else{
        [SupportingClass showErrorMessage:NETWORKERRORMESSAGE];
    }
}
-(IBAction)submitTarget:(UIButton*)sender{
    if (selectedTargetIndex==0){
        [SupportingClass showErrorMessage:@"Please select your target"];
        return;
    }
    NSDictionary* dict = [arrTargetList objectAtIndex:selectedTargetIndex-1];
    NSString *target= [dict objectForKey:@"weight"] ;
    NSDictionary *dictResult = [[NSDictionary alloc] initWithObjectsAndKeys:appDelegate.userData.str_userID,@"user_id",appDelegate.userData.str_height,@"height",appDelegate.userData.str_weight,@"weight",[NSString stringWithFormat:@"%0.1f",BMI],@"bmi",[NSString stringWithFormat:@"%0.1f",expectedWeight],@"ideal_weight",[NSString stringWithFormat:@"%0.1f",_weightExceed],@"over_weight",target,@"reduce_weight", nil];
    if (appDelegate.isNetAvailable) {
        [ProgressHUD show:@"Loading..." Interaction:NO];
        [webServiceCaller cancelAllCalls];
        [webServiceCaller submitIdealWeightDetails:dictResult];
    }
    else{
        [SupportingClass showErrorMessage:NETWORKERRORMESSAGE];
    }
    }

-(void)getWeightReduction{
    NSString *target = @"2";
    if (_weightExceed>=0&&_weightExceed<5) {
       target = @"0.5";
    }
    if (_weightExceed>=5&&_weightExceed<10){
        target = @"1.0";
    }
    if (_weightExceed>=10&&_weightExceed<15){
        target = @"1.5";
    }
    
    for (int  i =0; i<arrTargetList.count; i++) {
        NSDictionary* dict = [arrTargetList objectAtIndex:i];
        NSString *weight = [dict objectForKey:@"weight"];
        if ([weight isEqualToString:target]) {
            [dropDownPickerView selectRow:i+1 inComponent:0 animated:YES];
            [dropDownPickerView.delegate pickerView:dropDownPickerView didSelectRow:i+1 inComponent:0];
            break;
            
        }
    }
}
#pragma mark - Picker Delegate
// Number of components.
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

// Total rows in our component.
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return arrTargetList.count+1;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 30.0;
}

// Display each row's data.
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    if (row==0) {
        return defaultTarget;
    }
    NSDictionary* dict = [arrTargetList objectAtIndex:row-1];
    float weight = [[dict objectForKey:@"weight"] floatValue];
    if (!_isUnitInKg) {
        weight = weight*2.20462;
    }
    return [NSString stringWithFormat:@"Loses %0.1f %@/fortnight",weight,unit];
}

// Do something with the selected row.
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    selectedTargetIndex = row;
   NSString *strReferentName = defaultTarget;
    if (row!=0) {
        NSDictionary* dict = [arrTargetList objectAtIndex:row-1];
        float weight = [[dict objectForKey:@"weight"] floatValue];
        if (!_isUnitInKg) {
            weight = weight*2.20462;
        }
        strReferentName = [NSString stringWithFormat:@"Loses %0.1f %@/fortnight",weight,unit];
    }
    
    self.txtTarget.text = strReferentName;
}
#pragma mark - TextField Navigation Methodsbase
-(void)doneEditingButtonClicked
{
    [_txtTarget resignFirstResponder];
    
}
#pragma mark -  Webservice delegate

-(void)getTargetsCompletesSuccessfully:(BOOL)success WithList:(NSMutableArray*)arrTargets WithMessage:(NSString*)message{
    [ProgressHUD dismiss];
     if (success) {
        arrTargetList = arrTargets;
         [dropDownPickerView reloadAllComponents];
         [self getWeightReduction];
     }
    else{
        [SupportingClass showErrorMessage:message];
    }
}

-(void)submitIdealWeightCompletesSuccessfully:(BOOL)success WithData:(NSDictionary*)arrTips WithMessage:(NSString*)message{
    [ProgressHUD dismiss];
    if (success) {
        
        if (appDelegate.intStatusCode == 401) {
            
            //  diet plan  available
            
AttemptedBodyAnalysisViewController *objBodyAnalysisQuestion=[self.storyboard instantiateViewControllerWithIdentifier:@"AttemptedBodyAnalysisViewController"];
    [self.navigationController pushViewController:objBodyAnalysisQuestion animated:YES];
            
            
                  }
        else if (appDelegate.intStatusCode == 402 )
        {
            //  diet plan expire with date
            AttemptedBodyAnalysisViewController *objBodyAnalysisQuestion=[self.storyboard instantiateViewControllerWithIdentifier:@"AttemptedBodyAnalysisViewController"];
            [self.navigationController pushViewController:objBodyAnalysisQuestion animated:YES];
            

            
        }
        else if (appDelegate.intStatusCode == 403)
        {
            //  diet plan not available
            
            
            BodyAnalyisViewController *bodyAnalysis = [self.storyboard instantiateViewControllerWithIdentifier:@"BodyAnalyisViewController"];
            [self.navigationController pushViewController:bodyAnalysis animated:YES];
            

        }
        
        
        
    }
    else{
        [SupportingClass showErrorMessage:message];
    }
   
}
@end
