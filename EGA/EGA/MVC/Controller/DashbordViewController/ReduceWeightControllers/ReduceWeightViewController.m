//
//  ReduceWeightViewController.m
//  EGA
//
//  Created by Soniya on 02/11/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "ReduceWeightViewController.h"
#import "ReduceWeightCalculatorViewController.h"
@interface ReduceWeightViewController ()<WebServiceCallerDelegate>
{
     WebServiceCaller *webServiceCaller;
    NSString *strfeet;
    NSString *strInch;
    NSString *strHeightCm;
    NSString *strWeightKg;
    NSString *strWeightLbs;
    BOOL dataChanged;
    NSString *gender;
}
@property (weak, nonatomic) IBOutlet UISegmentedControl *genderSegmentControl;
@property (weak, nonatomic) IBOutlet UITextField *txtHeightInCm;
@property (weak, nonatomic) IBOutlet UITextField *txtWeight;
@property (weak, nonatomic) IBOutlet UISwitch *metricSwitch;
@property (weak, nonatomic) IBOutlet UITextField *txtHeightInFeet;
@property (weak, nonatomic) IBOutlet UITextField *txtHeightInInches;
@property (weak, nonatomic) IBOutlet UILabel *lblWeightUnit;
@property (weak, nonatomic) IBOutlet UIView *vwUSMetricHeight;
@property (weak, nonatomic) IBOutlet UILabel *lblUnitIndicator;

@end

@implementation ReduceWeightViewController
#pragma mark - Memory Management
-(void)removeReferences{
    
    [webServiceCaller cancelWebserviceCall];
    webServiceCaller.delegate = nil;
    webServiceCaller = nil;
    
    
    [ProgressHUD dismiss];
}
#pragma mark - View Controller Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
     [self screenDesigningForBiologicalCalculator];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - ScreenDesigning Methods
-(void)screenDesigningForBiologicalCalculator{
    self.navigationItem.title = @"Reduce Weight";
    [self createKeyboardAccessoryToolBar:YES];
    
    _txtHeightInCm.inputAccessoryView = accessoryToolBar;
    _txtWeight.inputAccessoryView = accessoryToolBar;
    _txtHeightInFeet.inputAccessoryView = accessoryToolBar;
    _txtHeightInInches.inputAccessoryView = accessoryToolBar;
    
    _txtHeightInCm.text =  appDelegate.userData.str_height;
    _txtWeight.text = appDelegate.userData.str_weight;
    gender = appDelegate.userData.str_gender;
    _genderSegmentControl.selectedSegmentIndex = [gender isEqualToString:@"m"]?0:1;
    if(_goToHome){
        // Create a UIBarButtonItem
        UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Home" style:UIBarButtonItemStylePlain target:self action:@selector(goBackToHome:)];
        
        // Associate the barButtonItem to the previous view
        [self.navigationItem setLeftBarButtonItem:barButtonItem];
    }
    [self calculateHeightWeightOnBasisOfCm:YES];
    webServiceCaller = [[WebServiceCaller alloc]init];
    webServiceCaller.delegate = self;
    
}
#pragma mark - Operational Methods
- (IBAction)saveAndSubmitClicked:(id)sender{
    if (appDelegate.isNetAvailable){
        if ([self validateAllFields]){
            if (dataChanged){
                [SupportingClass showAlertWithTitle:@"Alert" Message:@"Do you want to submit?" CancelButtonTitle:@"NO" OtherButton:@"YES" Delegate:self];
            }
            else{
                [self navigateToReduceWeight];
            }
          
            
          //  [SupportingClass showErrorMessage:@"Under Development Process"];
        }
    }
    else{
        [SupportingClass showErrorMessage:NETWORKERRORMESSAGE];
        
    }
}
-(BOOL)validateAllFields{
    
    _txtWeight.text=[_txtWeight.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    _txtHeightInCm.text=[_txtHeightInCm.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    _txtHeightInFeet.text=[_txtHeightInFeet.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    _txtHeightInInches.text=[_txtHeightInInches.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
  
    NSString* weight = _txtWeight.text;
 
        //Check whether textField are left empty or not
    NSString *errorMessage = nil;
    
    if (_metricSwitch.on) {
        if (weight.length<=0) {
            errorMessage = @"Please provide your weight";
        }
        else if (_txtHeightInFeet.text.length<=0) {
            errorMessage = @"Please provide your height";
        }
        if (_txtHeightInInches.text.length==0) {
            _txtHeightInInches.text=@"0";
        }

    }
    else{
        if (weight.length<=0) {
            errorMessage = @"Please provide your weight";
        }
        else if (_txtHeightInCm.text.length<=0) {
            errorMessage = @"Please provide your height";
        }
    }
    [self doneEditingButtonClicked];
    if (errorMessage!=nil) {
        [SupportingClass showErrorMessage:errorMessage];
        return NO;
    }
    return YES;
}


- (IBAction)metricChanged:(UISwitch *)sender {
    _txtWeight.text=[_txtWeight.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    _txtHeightInCm.text=[_txtHeightInCm.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    _txtHeightInFeet.text=[_txtHeightInFeet.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    _txtHeightInInches.text=[_txtHeightInInches.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (sender.on) {
        _txtHeightInCm.hidden = YES;
        _vwUSMetricHeight.hidden = NO;
        _txtWeight.placeholder = @"in Lbs";
        _lblWeightUnit.text = @"lbs";
        _lblUnitIndicator.text = @"US Units";
        _txtHeightInFeet.text=strfeet;
        _txtHeightInInches.text=strInch;
        _txtWeight.text = strWeightLbs;
    }
    else{
         _lblUnitIndicator.text = @"Metrics Units";
        _txtHeightInCm.hidden = NO;
        _vwUSMetricHeight.hidden = YES;
        _txtWeight.placeholder = @"in Kg";
        _lblWeightUnit.text = @"kg";
         _txtHeightInCm.text = strHeightCm;
        _txtWeight.text = strWeightKg;
    }
}


-(void)calculateHeightWeightOnBasisOfCm:(BOOL)onBasisOfCm{
    if (onBasisOfCm) {
        if (_txtHeightInCm.text.length!=0) {
            
            // Convert Height
            
            strHeightCm = _txtHeightInCm.text;
            float heightCm = [strHeightCm floatValue];
            float weight = [[NSString stringWithFormat:@"%.1f",heightCm * 0.3937008]floatValue];;
            float weightConversion = weight/12.0;
            NSString *str = [NSString stringWithFormat:@"%0.2f", weightConversion];
            NSArray *arr = [str componentsSeparatedByString:@"."];
            NSInteger integerPart=0.0;
            if (arr.count>0) {
                integerPart = [[arr objectAtIndex:0] integerValue];
            }
            float decimalpart = weightConversion-integerPart;
            decimalpart = decimalpart * 12;
            
           strfeet = [NSString stringWithFormat:@"%ld",lroundf(integerPart)];
            strInch= [NSString stringWithFormat:@"%0.0f",round ( decimalpart * 100) / 100.0];
            
            // Convert Weight
            
            strWeightKg = _txtWeight.text;
            strWeightLbs = [NSString stringWithFormat:@"%.1f", [strWeightKg floatValue]*2.20462];
        }

    }
    else{
        if (_txtHeightInFeet.text.length!=0) {
        if (_txtHeightInInches.text.length==0) {
            _txtHeightInInches.text=@"0";
        }
            
            strfeet = _txtHeightInFeet.text;
            strInch = _txtHeightInInches.text;
        float cmValue = (([strfeet integerValue]*12)+[strInch integerValue])* 2.54;
        
       strHeightCm = [NSString stringWithFormat:@"%0.2f",round(cmValue * 100) / 100];
    }
        
        
        strWeightLbs = _txtWeight.text;
        strWeightKg = [NSString stringWithFormat:@"%.1f", [strWeightLbs floatValue]/2.20462];
    }
    
}
-(void)navigateToReduceWeight{
    ReduceWeightCalculatorViewController *reduceWeightVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ReduceWeightCalculatorViewController"];
    reduceWeightVC.isUnitInKg = !_metricSwitch.on;
    [self.navigationController pushViewController:reduceWeightVC animated:YES];
   }
- (IBAction)genderValueChanged:(UISegmentedControl *)sender {
    dataChanged = YES;
    gender = _genderSegmentControl.selectedSegmentIndex==0?@"m":@"f";
}
-(void)goBackToHome:(id)sender{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
#pragma mark - UIAlert Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==1) {
        [ProgressHUD show:@"Saving your changes..."];
        [webServiceCaller updateHeight:strHeightCm Weight:strWeightKg Gender:gender OfUser:appDelegate.userData.str_userID];
        
    }
}
#pragma mark - TextField Navigation Methodsbase
-(void)doneEditingButtonClicked
{
    [self.txtWeight resignFirstResponder];
    [self.txtHeightInCm resignFirstResponder];
    [self.txtHeightInFeet resignFirstResponder];
    [self.txtHeightInInches resignFirstResponder];
    
}
-(void)prevButtonClicked
{
    if (_metricSwitch.on) {
        if (activeTextField==self.txtWeight) {
            [self.txtHeightInInches becomeFirstResponder];
        }
        else if (activeTextField==self.txtHeightInInches) {
            [self.txtHeightInFeet becomeFirstResponder];
        }
        else if (activeTextField==self.txtHeightInFeet) {
            [self.txtWeight becomeFirstResponder];
        }
    }
    else{
        if (activeTextField==self.txtWeight) {
            [self.txtHeightInCm becomeFirstResponder];
        }
        else if (activeTextField==self.txtHeightInCm) {
            [self.txtWeight becomeFirstResponder];
        }
       
    }
   
}
-(void)nextButtonClicked
{
    if (_metricSwitch.on) {
        if (activeTextField==self.txtHeightInFeet) {
            [self.txtHeightInInches becomeFirstResponder];
        }
        else if (activeTextField==self.txtHeightInInches) {
            [self.txtWeight becomeFirstResponder];
        }
        else if (activeTextField==self.txtWeight) {
            [self.txtHeightInFeet becomeFirstResponder];
        }
    }
    else{
        if (activeTextField==self.txtHeightInCm) {
            [self.txtWeight becomeFirstResponder];
        }
        else if (activeTextField==self.txtWeight) {
            [self.txtHeightInCm becomeFirstResponder];
        }
        
    }}

#pragma mark - TextField Delegates

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    activeTextField = textField;
    dataChanged=YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    if (textField==_txtHeightInInches) {
        if (_txtHeightInInches.text.length==0){
            _txtHeightInInches.text = @"0";
        }
        else if(_txtHeightInInches.text.integerValue>=12){
            _txtHeightInInches.text = @"11";
        }
    }
    
     if (textField==_txtHeightInFeet) {
         if(_txtHeightInFeet.text.integerValue>9){
             _txtHeightInFeet.text = @"9";
         }
     }
    
    if (textField==_txtHeightInCm) {
        if(_txtHeightInCm.text.integerValue>303){
            _txtHeightInCm.text = @"302.26";
        }
      _txtHeightInCm.text =  [NSString stringWithFormat:@"%0.2f",[ _txtHeightInCm.text floatValue]];
    }

    
    if (_metricSwitch.on) {
        [self calculateHeightWeightOnBasisOfCm:NO];
    }
    else{
        [self calculateHeightWeightOnBasisOfCm:YES];
    }
    
}

#pragma mark -  Webservice delegate

-(void)updateHeightWeightGenderOfUserCompletesSuccessfully:(BOOL)success WithMessage:(NSString*)message{
    [ProgressHUD dismiss];
    if (success) {
         appDelegate.userData.str_gender = gender;
        appDelegate.userData.str_weight =  strWeightKg;
        appDelegate.userData.str_height = strHeightCm;
        [appDelegate.userData updateHeightWeightGenderInDB];
        [self navigateToReduceWeight];
    }
    else{
        if ([message isEqualToString:NETWORKERRORMESSAGE]) {
            [SupportingClass showErrorMessage:message];
        }
        else {
            [SupportingClass showAlertWithTitle:@"" Message:message CancelButtonTitle:@"OK"];
        }
    }
}

@end
