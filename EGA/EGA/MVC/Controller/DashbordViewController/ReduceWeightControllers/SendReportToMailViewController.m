//
//  SendReportToMailViewController.m
//  EGA
//
//  Created by Soniya on 04/10/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "SendReportToMailViewController.h"

@interface SendReportToMailViewController ()
@property (weak, nonatomic) IBOutlet UILabel *lblEmailID;

@end

@implementation SendReportToMailViewController

#pragma mark - Memory Management
-(void)removeReferences{
  
}

#pragma mark - View Controller Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self screenDesignOfSendReportToMail];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ScreenDesigning Methods
-(void)screenDesignOfSendReportToMail{
    _lblEmailID.text = [NSString stringWithFormat:@"ID : %@",_strEmailID];
    self.navigationItem.hidesBackButton = YES;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - Operational Methods
-(IBAction)goToHome:(id)sender{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
