//
//  BodyAnalyisViewController.m
//  EGA
//
//  Created by Soniya on 01/10/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "BodyAnalyisViewController.h"
#import "BodyAnalysisTableViewCell.h"
#import "ANProgressStepper.h"
#import "BodyAnalysisResultViewController.h"
#import "AnalyticQuestionDM.h"

#define numberOfRecordsPerPage 15

@interface BodyAnalyisViewController ()<UITableViewDataSource,UITableViewDelegate,WebServiceCallerDelegate>
{
    NSInteger currentIndex;
    NSDictionary *dictQuesList;
    NSInteger totalPages;
    NSMutableArray *arrQuestionList;
    __weak IBOutlet ANProgressStepper *stepper;
    __weak IBOutlet UIView *vwFooter;
     WebServiceCaller *webServiceCaller;
    AnalyticQuestionDM *analyticQuesDM;
    __weak IBOutlet UIView *vwMessage;
    
    __weak IBOutlet UISwitch *messageSwitch;
    __weak IBOutlet UIView *vwSubMessage;
}
@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewStepper;
@property (weak, nonatomic) IBOutlet UITableView *tblView;

@end

@implementation BodyAnalyisViewController

#pragma mark - Memory Management

-(void)removeReferences{
    
    _tblView.delegate = nil;
    _tblView.dataSource = nil;
    _tblView=nil;
    _scrollViewStepper=nil;
    [webServiceCaller cancelWebserviceCall];
    webServiceCaller.delegate = nil;
    webServiceCaller = nil;
    
    
    [ProgressHUD dismiss];
 }

#pragma mark - View Controller Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self screenDesignOfBodyAnalysis];
    // Do any additional setup after loading the view.
}
-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    [_tblView reloadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ScreenDesigning Methods
-(void)screenDesignOfBodyAnalysis{
self.navigationItem.title = @"Know your body type";
    webServiceCaller = [[WebServiceCaller alloc]init];
    webServiceCaller.delegate = self;

    analyticQuesDM  = [[AnalyticQuestionDM alloc]init];
    stepper.currentStep = 1;
    stepper.lineThickness = 10.0f;
    stepper.incompleteColor = stepper.completeNumberColor = stepper.activeColor = stepper.borderColor = [UIColor whiteColor];
    stepper.completeColor = [UIColor colorWithHexString:@"#37B4EB"];
    stepper.activeColor = [ConfigurationFile getThemeColor];
   // stepper.incompleteNumberColor
   stepper.incompleteNumberColor = [UIColor blackColor];
     stepper.activeNumberColor = [UIColor whiteColor];
    stepper.numberSize = 18;
    stepper.showLinesBetweenSteps = YES;
    
    vwSubMessage.layer.cornerRadius = 3.0f;
    
    // Create a UIBarButtonItem
    //UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Home" style:UIBarButtonItemStylePlain target:self action:@selector(goBackToHome:)];
    
    // Associate the barButtonItem to the previous view
    
    if(_goToHome){
        // Create a UIBarButtonItem
        UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Home" style:UIBarButtonItemStylePlain target:self action:@selector(goBackToHome:)];
        
        // Associate the barButtonItem to the previous view
        [self.navigationItem setLeftBarButtonItem:barButtonItem];
    }
    else{
        self.navigationItem.hidesBackButton = YES;
    }
    [self showQuestions];
     }

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
-(void)goBackToHome:(id)sender{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)submitButtonClicked:(UIButton *)sender {
    currentIndex = currentIndex+1;
    
    if (currentIndex>=totalPages) {
        currentIndex = totalPages-1;
        NSInteger nonZeroQuesCount = [analyticQuesDM selectCountOfNonZeroAnswerValueForUser:appDelegate.userData.str_userID];
        if (nonZeroQuesCount>0) {
             [ProgressHUD show:@"Submitting..." Interaction:NO];
            //Call Webservice]
            NSMutableArray *arrItems = [analyticQuesDM selectAnswerValueForUser:appDelegate.userData.str_userID];
            
              ;
                //send this as a json
                NSDictionary *dictAnswers = [[NSDictionary alloc]initWithObjectsAndKeys:appDelegate.userData.str_userID,@"userid",arrItems,@"question", nil];
                [webServiceCaller submitAnalyticalQuestions:dictAnswers UserID:appDelegate.userData.str_userID];
            
            
                  }
        else{
            [SupportingClass showAlertWithTitle:@"Alert" Message:@"Seems you didn’t answer properly. Please Try Again." CancelButtonTitle:@"OK"];
        }
        
    }else{
        
       arrQuestionList = [analyticQuesDM selectQuestionsOfEachQuestionTypeWithPageNo:currentIndex*5 RecordCount:5 UserID:appDelegate.userData.str_userID];
        [SupportingClass animateViewWithType:kCATransitionFade SubType:kCATransitionFromRight OnView:_tblView Duration:0.5];
        [_tblView setContentOffset:CGPointMake(0, 0)];
        [_tblView reloadData];
        
    }
    stepper.currentStep=(int)currentIndex+1;
    [stepper reloadStepper];
    [ProgressHUD dismiss];
}

#pragma mark - Operational Methods
-(void)showQuestions{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    BOOL webserviceCalled =[standardUserDefaults boolForKey:@"AnalyticQuestionWebServiceCalled"];
    
    if (webserviceCalled) {
        NSInteger count = [analyticQuesDM selectCountOfQuestions];
        [self InitializeAnalyseQuestion:count];
        [self submitButtonClicked:nil];

    }
    else{
    [ProgressHUD show:@"Loading..." Interaction:NO];
    [webServiceCaller cancelAllCalls];
    [webServiceCaller getListOfAnalyticQuestions];
    }

}
-(void)InitializeAnalyseQuestion:(NSInteger)totalRecords{
    currentIndex = -1;
    totalPages = totalRecords/numberOfRecordsPerPage;
    if (totalRecords%numberOfRecordsPerPage!=0) {
        totalPages++;
    }
    stepper.numberOfSteps = totalPages;
    
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    BOOL dontShowMessage = [standardUserDefaults boolForKey:@"AnalyticMessageDontShow"];
    if (dontShowMessage) {
         [SupportingClass animateViewWithType:kCATransitionFade SubType:kCATransitionFromRight OnView:vwMessage Duration:0.5];
        vwMessage.hidden = YES;
    }
    else{
        vwMessage.hidden = NO;
    }
}
- (IBAction)dismissMessagePopUp:(id)sender {
    if (messageSwitch.isOn) {
        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [standardUserDefaults setBool:YES forKey:@"AnalyticMessageDontShow"];
        
    }
     [SupportingClass animateViewWithType:kCATransitionFade SubType:kCATransitionFromRight OnView:vwMessage Duration:0.5];
   vwMessage.hidden = YES;
}

- (IBAction)infoButtonClicked:(UIButton *)sender {
     [SupportingClass animateViewWithType:kCATransitionFade SubType:kCATransitionFromRight OnView:vwMessage Duration:0.5];
   vwMessage.hidden = NO;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
     return arrQuestionList.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSMutableDictionary *dict = [arrQuestionList objectAtIndex:indexPath.row];
   NSString *quesText = [dict objectForKey:@"question"];
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:18 weight:UIFontWeightLight]};
    // NSString class method: boundingRectWithSize:options:attributes:context is
    // available only on ios7.0 sdk.
     CGRect rect = [quesText boundingRectWithSize:CGSizeMake(screenWidth, CGFLOAT_MAX)
                                              options:NSStringDrawingUsesLineFragmentOrigin
                                           attributes:attributes
                                              context:nil];
     CGFloat height = rect.size.height+70;
    
    if (height<80)
        return 80;
    return height;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == arrQuestionList.count-1) {
        return vwFooter.frame.size.height;
    }
    return 0;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{

        return vwFooter;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    BodyAnalysisTableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"BodyAnalysisTableViewCell"];
    
   
    NSInteger tag = indexPath.row;
    cell.stepper.tag = tag;
    cell.tag = tag+1000;
   
   
    cell.stepper.valueChangedCallback = ^(PKYStepper *objStepper, float count) {
        NSInteger tag= objStepper.tag;
        NSString *value = [NSString stringWithFormat:@"%@", @(count)];
        objStepper.countLabel.text = value;
        NSMutableDictionary *dict = [arrQuestionList objectAtIndex:tag];
        [dict setValue:[NSString stringWithFormat:@"%ld",(long)count] forKey:@"que_ans"];
        [analyticQuesDM insertAnalyticsQuestionAnswerWithQuesID:[dict objectForKey:@"question_id"] Option:value UserID:appDelegate.userData.str_userID];
     };
    return cell;
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    cell.backgroundColor=[UIColor clearColor];
    NSMutableDictionary *dict = [arrQuestionList objectAtIndex:indexPath.row];
    if (![dict objectForKey:@"que_ans"]) {
        [dict setObject:@"3" forKey:@"que_ans"];
       
    }
    BodyAnalysisTableViewCell *cell1 =(BodyAnalysisTableViewCell*)cell;
    [cell1 setValueOfCell:dict];

}
//#pragma mark -  Alert delegate
//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
//    if (buttonIndex==1) {
//        BodyAnalysisResultViewController *bodyAnalysisResultVC = [self.storyboard instantiateViewControllerWithIdentifier:@"BodyAnalysisResultViewController"];
//        [self.navigationController pushViewController:bodyAnalysisResultVC animated:YES];
//    }
//}

#pragma mark -  Webservice delegate

-(void)getListOfAnalyticQuestionsCompletesSuccessfully:(BOOL)success WithList:(NSMutableArray*)arrQuestions WithMessage:(NSString*)message{
    [ProgressHUD dismiss];
    vwFooter.hidden = NO;
    if (success) {
        [analyticQuesDM insertAnalyticsQuestion:arrQuestions];
        [self InitializeAnalyseQuestion:arrQuestions.count];
        [self submitButtonClicked:nil];
          NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
         [standardUserDefaults setBool:YES forKey:@"AnalyticQuestionWebServiceCalled"];
    }
    else{
        [SupportingClass showErrorMessage:message];
    }
    if (arrQuestionList.count==0) {
        vwFooter.hidden = YES;
    }
}
-(void)submitAnalyticalQuestionsCompletesSuccessfully:(BOOL)success WithData:(NSDictionary*)dictResult WithMessage:(NSString*)message{
    [ProgressHUD dismiss];
    if (success) {
        BodyAnalysisResultViewController *bodyAnalysisResultVC = [self.storyboard instantiateViewControllerWithIdentifier:@"BodyAnalysisResultViewController"];
        bodyAnalysisResultVC.bodyType = [dictResult objectForKey:@"body_type"];
        [self.navigationController pushViewController:bodyAnalysisResultVC animated:YES];
     }
    else{
        [SupportingClass showErrorMessage:message];
    }
}
@end
