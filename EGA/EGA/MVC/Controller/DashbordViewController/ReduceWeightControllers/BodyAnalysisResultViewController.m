//
//  BodyAnalysisResultViewController.m
//  EGA
//
//  Created by Soniya on 01/10/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "BodyAnalysisResultViewController.h"
#import "SendReportToMailViewController.h"
#import "MyDietPlanViewController.h"

#define Water @"Water"
#define Fire @"Fire"
#define Air @"Air"

@interface BodyAnalysisResultViewController() <WebServiceCallerDelegate>{
    WebServiceCaller *webServiceCaller;
}
@property (weak, nonatomic) IBOutlet UIImageView *imgBodyType;
@property (weak, nonatomic) IBOutlet UILabel *lblResult;

@end

@implementation BodyAnalysisResultViewController

#pragma mark - Memory Management
-(void)removeReferences{
    
    [webServiceCaller cancelWebserviceCall];
    webServiceCaller.delegate = nil;
    webServiceCaller = nil;
    
    
    [ProgressHUD dismiss];
}

#pragma mark - View Controller Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
     // Do any additional setup after loading the view.
    [self screenDesignOfBodyAnalysisResult];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ScreenDesigning Methods
-(void)screenDesignOfBodyAnalysisResult{
    self.navigationItem.title = @"Your body type";
    webServiceCaller = [[WebServiceCaller alloc]init];
    webServiceCaller.delegate = self;

    _lblResult.text = [[NSString stringWithFormat:@"YOUR BODY TYPE IS \"%@\"",_bodyType] uppercaseString];
    
    NSString *imageName = @"BodyTypeAir";
    if ([_bodyType isEqualToString:Water]) {
        
        imageName = @"BodyTypeWater";
    }
    else if([_bodyType isEqualToString:Fire]){
        imageName = @"BodyTypeFire";
    }
    _imgBodyType.image = [UIImage imageNamed:imageName];
    
    // Create a UIBarButtonItem
//    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Home" style:UIBarButtonItemStylePlain target:self action:@selector(goBackToHome:)];
//    
//    // Associate the barButtonItem to the previous view
//    [self.navigationItem setLeftBarButtonItem:barButtonItem];

    self.navigationItem.hidesBackButton = YES;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - Operational Methods

-(void)goBackToHome:(id)sender{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
//- (IBAction)sendReportClicked:(UIButton *)sender {
//    [ProgressHUD show:@"Sending Report..." Interaction:NO];
//    if (appDelegate.isNetAvailable){
//        
//        [webServiceCaller analyticsSendMailToUser:appDelegate.userData.str_userID];
//     }
//else{
//    [ProgressHUD dismiss];
//    [SupportingClass showErrorMessage:NETWORKERRORMESSAGE];
//    
//}
//}

- (IBAction)showDietPlanClicked:(UIButton *)sender {
    [ProgressHUD show:@"Loading..." Interaction:NO];
    if (appDelegate.isNetAvailable){
        [webServiceCaller analyticsSendMailToUser:appDelegate.userData.str_userID];
         [webServiceCaller getDietPlanStatus:appDelegate.userData.str_userID];
    }
    else{
        [ProgressHUD dismiss];
        [SupportingClass showErrorMessage:NETWORKERRORMESSAGE];
        
    }
}
#pragma mark -  Webservice delegate

//-(void)analyticalResultEmailCompletesSuccessfully:(BOOL)success WithMessage:(NSString*)message{
//    [ProgressHUD dismiss];
//    if (success) {
//        [SupportingClass showAlertWithTitle:@"" Message:message CancelButtonTitle:@"OK"];
//    }
//    else{
//        if ([message isEqualToString:NETWORKERRORMESSAGE]) {
//            [SupportingClass showErrorMessage:message];
//        }
//        else {
//            [SupportingClass showAlertWithTitle:@"" Message:message CancelButtonTitle:@"OK"];
//        }
//    }
//}
-(void)getDietPlanStatusCompletesSuccessfully:(BOOL)success StatusCode:(NSInteger)statusCode WithMessage:(NSString*)message{
        [ProgressHUD dismiss];
        if (success) {
            MyDietPlanViewController *myDietPlan = [self.storyboard instantiateViewControllerWithIdentifier:@"MyDietPlanViewController"];
            myDietPlan.goToHome = YES;
            [self.navigationController pushViewController:myDietPlan animated:YES];
        }
        else{
            if ([message isEqualToString:NETWORKERRORMESSAGE]) {
                [SupportingClass showErrorMessage:message];
            }
            else {
                [SupportingClass showAlertWithTitle:@"" Message:message CancelButtonTitle:@"OK"];
            }
        }
}
@end
