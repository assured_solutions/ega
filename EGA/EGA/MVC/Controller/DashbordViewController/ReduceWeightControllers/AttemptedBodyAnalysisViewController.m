//
//  AttemptedBodyAnalysisViewController.m
//  EGA
//
//  Created by Soniya on 15/12/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "AttemptedBodyAnalysisViewController.h"
#import "BodyAnalyisViewController.h"
@interface AttemptedBodyAnalysisViewController ()

@end

@implementation AttemptedBodyAnalysisViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"BodyAnalyisViewController"])
    {
        BodyAnalyisViewController *bodyAnalysis = [segue destinationViewController];
        bodyAnalysis.goToHome = YES;
        
    }}


@end
