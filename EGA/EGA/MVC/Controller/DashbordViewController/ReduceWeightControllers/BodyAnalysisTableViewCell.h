//
//  BodyAnalysisTableViewCell.h
//  EGA
//
//  Created by Soniya on 17/11/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKYStepper.h"
@interface BodyAnalysisTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblQuesText;
@property (strong, nonatomic) IBOutlet PKYStepper *stepper;
-(void)setValueOfCell:(NSDictionary*)dictCellDetail;
@end
