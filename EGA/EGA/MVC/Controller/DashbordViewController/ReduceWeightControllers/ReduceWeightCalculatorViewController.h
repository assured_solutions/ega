//
//  ReduceWeightCalculatorViewController.h
//  EGA
//
//  Created by Soniya on 14/11/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "BaseViewController.h"

@interface ReduceWeightCalculatorViewController : BaseViewController
@property (nonatomic) BOOL isUnitInKg;
@end
