//
//  DashboardMenuView.m
//  EGA
//
//  Created by Soniya on 18/12/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "DashboardMenuView.h"
#import "ASShare.h"
@interface DashboardMenuView()<UITableViewDataSource,UITableViewDelegate,ASShareDelegate>{
    ASShare *share;
    __weak IBOutlet UIImageView *imgProfilePic;
    BOOL isVisible;
    __weak IBOutlet UIView *menuView;
    __weak IBOutlet UILabel *lblCopyRight;
    __weak IBOutlet UILabel *lblGreet;
}
@end

@implementation DashboardMenuView
@synthesize delegate;

-(id)initWithFrame:(CGRect)frame {
    if ( !(self = [super initWithFrame:frame]) ) return nil;
    [self awakeFromNib];
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self awakeFromNib];
    }
    return self;
}
-(void)awakeFromNib {
    UIView *xibView = [[[NSBundle mainBundle] loadNibNamed:@"DashboardMenuView" owner:self options:nil] objectAtIndex:0];
    [self addSubview:xibView];
     isVisible = YES;
  
}
-(void)viewDesigning{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (share == nil){
        UIViewController *viewController = [SupportingClass currentTopViewController];
        share = [[ASShare alloc]init];
        share.callingController = viewController;
        share.delegate = self;
    }
    lblCopyRight.text=COPYRIGHT;
    lblGreet.text = [NSString stringWithFormat:@"Welcome %@",appDelegate.userData.str_firstname];
    [self setProfileImage];
 }
- (void)layoutSubviews{
      [self viewDesigning];
}
// Getting Profile image if exist
-(void)setProfileImage{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSString *strProfilePicName = [NSString stringWithFormat:@"%@ProfilePic.png",appDelegate.userData.str_userID];
    NSArray  *paths	= NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docuPath = [paths objectAtIndex:0];
    NSString *newDocumentPath=[NSString stringWithFormat:@"%@/ProfilePics/%@",docuPath,strProfilePicName];
    UIImage *img = [UIImage imageWithContentsOfFile:newDocumentPath];
    if (img==nil) {
        img=[UIImage imageNamed:@"ProfilePicDefault"];
    }
    CGRect frame = imgProfilePic.frame;
    CGFloat width = frame.size.width;
    if (frame.size.height>width) {
        width = frame.size.height;
    }
    
    frame.size.height = frame.size.width = width;
    imgProfilePic.frame=frame;
    imgProfilePic.image = img;
  
    imgProfilePic.layer.cornerRadius = imgProfilePic.frame.size.width/2;//half of the width
    
    imgProfilePic.layer.borderColor = [UIColor colorWithHexString:BORDERCOLOR].CGColor;
    imgProfilePic.layer.borderWidth=1.0f;
    
    imgProfilePic.layer.masksToBounds = YES;
   
}
-(void)showHideSlideMenu{
    isVisible = !isVisible;
    CGRect frame = self.frame;
    frame.origin.x = 0;
    if (!isVisible) {
        frame.origin.x = self.frame.size.width;
    }
    [UIView animateWithDuration:0.5 animations:^{
            self.frame = frame;
    } completion:nil];
    
}

-(void)showWebURL:(NSString*)urlString hideHistoryButton:(BOOL)hideHistory{
    UIViewController *viewController = [SupportingClass currentTopViewController];
    WebViewController * webViewController = [[WebViewController alloc]initWithNibName:@"WebViewController" bundle:nil];
    webViewController.urlAddress = urlString;
    webViewController.hideHistorySegment = hideHistory;
    [viewController presentViewController:webViewController animated:YES completion:nil];
}
#pragma mark - Table View DataSource And Delegate Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (section == 4) {
        return 1;
    }
    else if (section==3){
        return 3;
    }
    return 2;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (section==0) {
        return @"   About";
    }
    else if(section==1){
        return @"   Need help?";
    }
    else if(section==2){
        return @"   Like this app?";
    }
    else if(section==3){
        return @"   Setting";
    }
    else if(section==4){
        return @"   Account";
    }
    return @"";
}

 
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.textColor = [UIColor whiteColor];
}
- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    // Background color
    view.tintColor = [UIColor whiteColor];
    
    // Text Color
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setTextColor:[UIColor colorWithHexString:BORDERCOLOR]];
    
    // Another way to set the background color
    // Note: does not preserve gradient effect of original header
    // header.contentView.backgroundColor = [UIColor blackColor];
}
- (void)tableView:(UITableView *)tableView willDisplayFooterView:(UIView *)view forSection:(NSInteger)section{
    // Background color
    view.tintColor = [UIColor whiteColor];
    
    // Text Color
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setTextColor:[UIColor lightGrayColor]];

}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* cellIdentifier = @"CellIdentifier";
    UITableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"HealthTipsCellIdentifier"];
    
    if (cell==nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    NSString *value = @"";
    
    if (indexPath.section == 0) {
        if (indexPath.row==0) {
            value = @"About EGA";
        }
        else if (indexPath.row == 1) {
            value = @"FAQ";
        }

    }
    else  if (indexPath.section == 1) {
        if (indexPath.row==0) {
            value = @"Help";
        }
        else if (indexPath.row == 1) {
            value = @"Email Us";
        }
        
    }
    else  if (indexPath.section == 2) {
        if (indexPath.row==0) {
            value = @"Share with friends";
        }
        else if (indexPath.row == 1) {
            value = @"Rate it";
        }
        
    }
    else  if (indexPath.section == 3) {
        if (indexPath.row==0) {
            value = @"Disclaimer";
        }
        else if (indexPath.row == 1) {
            value = @"Terms and conditions";
        }
        else if (indexPath.row == 2) {
            value = @"Privacy";
        }

    }
    else  if (indexPath.section == 4) {
        if (indexPath.row==0) {
            value = @"Log out";
        }
       
    }
    cell.textLabel.text = value;

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 2) {
        if (indexPath.row==0) {
            [share shareWithActivityIndicator:shareText];
        }
    }
    else if (indexPath.section==3) {
        if (indexPath.row==0) {
            [SupportingClass showAlertWithTitle:@"Disclaimer" Message:DISCLAIMER CancelButtonTitle:@"OK"];
        }
        else if (indexPath.row == 1) {
            [self showWebURL:TERMSANDCONDITION hideHistoryButton:YES];
        }
        else if (indexPath.row == 2) {
            [self showWebURL:PRIVACY hideHistoryButton:YES];
        }
    }
    else if (indexPath.section==4) {
        if (indexPath.row==0) {
            if(self.delegate!=nil && [(id)[self delegate] respondsToSelector:@selector(logOutClicked)])
            {
                [(id)[self delegate] logOutClicked];
            }
        }
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    UITouch *touch = [touches anyObject];
    
    CGPoint touchPoint = [touch locationInView:self];
    CGFloat width = self.frame.size.width-menuView.frame.size.width;
    if ((touchPoint.x < width)) {
        [self showHideSlideMenu];
    }
}
@end
