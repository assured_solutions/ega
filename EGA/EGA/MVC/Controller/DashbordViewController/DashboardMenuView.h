//
//  DashboardMenuView.h
//  EGA
//
//  Created by Soniya on 18/12/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DashboardMenuViewDelegate;
@interface DashboardMenuView : UIView
@property(nonatomic, assign) id <DashboardMenuViewDelegate> delegate;
-(void)showHideSlideMenu;
@end
@protocol DashboardMenuViewDelegate <NSObject>

@optional
//Calls when Registration is Done Succesfully
-(void)logOutClicked;

@end