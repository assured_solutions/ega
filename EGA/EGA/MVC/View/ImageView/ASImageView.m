//
//  ASImageView.m
//  LaunchApp
//
//  Created by Soniya on 07/09/15.
//  Copyright (c) 2015 Assure Solutions Group. All rights reserved.
//

#import "ASImageView.h"

@implementation ASImageView

#pragma mark - UIImageView Overrides
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self designImageView];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)coder
{
    // Call the parent implementation of initWithCoder
    self = [super initWithCoder:coder];
    
    // Custom drawing methods
    if (self)
    {
        [self designImageView];
    }
    
    return self;
}
#pragma mark - Operational setters

- (void)designImageView
{
    // Get the root layer (any UIView subclass comes with one)
    self.image = [self.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self setTintColor:[ConfigurationFile getThemeColor]];
}

@end
