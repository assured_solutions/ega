//
//  ASCustomButtonWithImage.m
//  LaunchApp
//
//  Created by Soniya on 07/09/15.
//  Copyright (c) 2015 Assure Solutions Group. All rights reserved.
//

#import "ASCustomButtonWithImage.h"


@implementation ASCustomButtonWithImage

#pragma mark - UIButton Overrides
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self designButton];
      }
    return self;
}

- (id)initWithCoder:(NSCoder *)coder
{
    // Call the parent implementation of initWithCoder
    self = [super initWithCoder:coder];
    
    // Custom drawing methods
    if (self)
    {
        [self designButton];
     }
    
    return self;
}

+ (ASCustomButtonWithImage *)buttonWithType:(UIButtonType)type
{
    return [super buttonWithType:UIButtonTypeSystem];
}

#pragma mark - Operational setters

- (void)designButton
{
    // Get the root layer (any UIView subclass comes with one)
    
    self.tintColor = [ConfigurationFile getThemeColor];
    [self.imageView setContentMode: UIViewContentModeScaleAspectFit];
}
- (void)setHighlighted:(BOOL)highlighted
{
    //[super setHighlighted:highlighted];
    // Disable implicit animation
    [CATransaction begin];
    [CATransaction setDisableActions:YES];
    
    // Hide/show inverted gradient
    if (highlighted) {
        self.tintColor =  [ConfigurationFile getHighlightedThemeColor];
    }
    else{
         self.tintColor =  [ConfigurationFile getThemeColor];
    }
    [self setNeedsDisplay];
     [CATransaction commit];
    
    //
}


@end
