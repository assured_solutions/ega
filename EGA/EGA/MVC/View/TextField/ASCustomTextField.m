//
//  ASCustomTextField.m
//  EGA
//
//  Created by Soniya on 03/10/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "ASCustomTextField.h"

@implementation ASCustomTextField

#pragma mark - UITextField Overrides
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self designTextField];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)coder
{
    // Call the parent implementation of initWithCoder
    self = [super initWithCoder:coder];
    
    // Custom drawing methods
    if (self)
    {
        [self designTextField];
    }
    
    return self;
}
#pragma mark - Operational setters

- (void)designTextField
{
    
    if (_leftAccessoryImage!=nil) {
        UIImageView *someImageView = [[UIImageView alloc] init];
        someImageView.contentMode = UIViewContentModeLeft;
        // add 5 points to each side of the frame while keeping the same center point
        someImageView.frame = CGRectInset(someImageView.frame, -15, 0);
        someImageView.image = _leftAccessoryImage;
        [self setLeftViewMode:UITextFieldViewModeAlways];
        self.leftView= someImageView;
    }
    if (_rightAccessoryImage!=nil) {
        UIImageView *someImageView = [[UIImageView alloc] init];
        someImageView.contentMode = UIViewContentModeRight;
       
        // add 5 points to each side of the frame while keeping the same center point
        someImageView.frame = CGRectInset(someImageView.frame, -15, 0);
         someImageView.image = _rightAccessoryImage;
        [self setRightViewMode:UITextFieldViewModeAlways];
        self.rightView= someImageView;
    }

    self.font = [UIFont systemFontOfSize:18 weight:UIFontWeightLight];
    self.layer.borderWidth = 1.0f;
    self.layer.borderColor = [UIColor colorWithHexString:BORDERCOLOR].CGColor;
    self.layer.cornerRadius = 2.0f;
    self.backgroundColor = [UIColor whiteColor];
    self.textColor = [UIColor colorWithHexString:DARKGRAYTEXTCOLOR];
}

- (void)drawRect:(CGRect)rect {
    [self designTextField];
}

- (void)setNeedsLayout {
    [super setNeedsLayout];
    [self setNeedsDisplay];
}


- (void)prepareForInterfaceBuilder {
    
    [self designTextField];
}

- (CGRect) rightViewRectForBounds:(CGRect)bounds {
    
    CGRect textRect = [super rightViewRectForBounds:bounds];
    textRect.origin.x -= 5;
    return textRect;
}

- (CGRect) leftViewRectForBounds:(CGRect)bounds {
    
    CGRect textRect = [super leftViewRectForBounds:bounds];
    textRect.origin.x += 5;
    return textRect;
}
//- (CGRect)textRectForBounds:(CGRect)bounds{
//   return [self getPaddingForTextField:bounds];
//}
//- (CGRect)placeholderRectForBounds:(CGRect)bounds{
//    return [self getPaddingForTextField:bounds];
//}
//- (CGRect)editingRectForBounds:(CGRect)bounds{
//    return [self getPaddingForTextField:bounds];
//}
//
//-(CGRect)getPaddingForTextField:(CGRect)bounds{
//    
//  return CGRectInset( bounds , 10 , 3 );
//}
@end
