//
//  ASTextField.h
//  EGA
//
//  Created by Soniya on 02/11/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import <UIKit/UIKit.h>
IB_DESIGNABLE
@interface ASTextField : UITextField

@end
