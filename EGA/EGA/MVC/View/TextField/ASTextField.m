//
//  ASTextField.m
//  EGA
//
//  Created by Soniya on 02/11/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "ASTextField.h"

@implementation ASTextField
#pragma mark - UITextField Overrides
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self designTextField];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)coder
{
    // Call the parent implementation of initWithCoder
    self = [super initWithCoder:coder];
    
    // Custom drawing methods
    if (self)
    {
        [self designTextField];
    }
    
    return self;
}
#pragma mark - Operational setters

- (void)designTextField
{
    
    self.font = [UIFont systemFontOfSize:18 weight:UIFontWeightLight];
    self.layer.borderWidth = 1.0f;
    self.layer.borderColor = [UIColor colorWithHexString:BORDERCOLOR].CGColor;
    self.layer.cornerRadius = 2.0f;
    self.backgroundColor = [UIColor whiteColor];
    self.textColor = [UIColor colorWithHexString:DARKGRAYTEXTCOLOR];
    
}
- (void)drawRect:(CGRect)rect {
    [self designTextField];
}

- (void)setNeedsLayout {
    [super setNeedsLayout];
    [self setNeedsDisplay];
}


- (void)prepareForInterfaceBuilder {
    
    [self designTextField];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (CGRect)textRectForBounds:(CGRect)bounds{
   return [self getPaddingForTextField:bounds];
}
- (CGRect)placeholderRectForBounds:(CGRect)bounds{
    return [self getPaddingForTextField:bounds];
}
- (CGRect)editingRectForBounds:(CGRect)bounds{
    return [self getPaddingForTextField:bounds];
}

-(CGRect)getPaddingForTextField:(CGRect)bounds{

  return CGRectInset( bounds , 10 , 3 );
}
@end
