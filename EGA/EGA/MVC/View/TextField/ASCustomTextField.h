//
//  ASCustomTextField.h
//  EGA
//
//  Created by Soniya on 03/10/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import <UIKit/UIKit.h>
IB_DESIGNABLE
@interface ASCustomTextField : UITextField
@property (nonatomic) IBInspectable UIImage *leftAccessoryImage;
@property (nonatomic) IBInspectable UIImage *rightAccessoryImage;
@end
