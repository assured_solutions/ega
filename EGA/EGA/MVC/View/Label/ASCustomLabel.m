//
//  ASCustomLabel.m
//  LaunchApp
//
//  Created by Soniya on 07/09/15.
//  Copyright (c) 2015 Assure Solutions Group. All rights reserved.
//

#import "ASCustomLabel.h"

@implementation ASCustomLabel
#pragma mark - UILabel Overrides
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self designLabel];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)coder
{
    // Call the parent implementation of initWithCoder
    self = [super initWithCoder:coder];
    
    // Custom drawing methods
    if (self)
    {
        [self designLabel];
    }
    
    return self;
}
#pragma mark - Operational setters

- (void)designLabel
{
     // Get the root layer (any UIView subclass comes with one)
     self.textColor = [ConfigurationFile getThemeColor];
}


@end
