//
//  ASCustomTextLabel.m
//  EGA
//
//  Created by Soniya on 16/12/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "ASCustomTextLabel.h"

@implementation ASCustomTextLabel
#pragma mark - UILabel Overrides
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self designLabel];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)coder
{
    // Call the parent implementation of initWithCoder
    self = [super initWithCoder:coder];
    
    // Custom drawing methods
    if (self)
    {
        [self designLabel];
    }
    
    return self;
}
#pragma mark - Operational setters

- (void)designLabel
{
    // Get the root layer (any UIView subclass comes with one)
    self.textColor = [UIColor colorWithHexString:DARKGRAYTEXTCOLOR];
}


@end
