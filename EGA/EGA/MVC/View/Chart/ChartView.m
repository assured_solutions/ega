//
//  ChartView.m
//  LaunchApp
//
//  Created by Soniya on 08/09/15.
//  Copyright (c) 2015 Assure Solutions Group. All rights reserved.
//

#import "ChartView.h"
#import <Charts/Charts.h>


@interface ChartView ()<ChartViewDelegate>
{
    PieChartView *chartView;
}
@end

@implementation ChartView

-(id)initWithFrame:(CGRect)frame {
    if ( !(self = [super initWithFrame:frame]) ) return nil;
    // [self awakeFromNib];
    [self  viewDesigning];
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        // [self awakeFromNib];
        [self  viewDesigning];
    }
    return self;
}

-(void)viewDesigning{
    
    chartView = [[PieChartView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    [self addSubview:chartView];
    chartView.delegate = self;
    
    chartView.usePercentValuesEnabled = NO;
    chartView.holeTransparent = YES;
    chartView.centerTextFont = [UIFont fontWithName:@"HelveticaNeue-Light" size:12.f];
    chartView.holeRadiusPercent = 0.58;
    chartView.transparentCircleRadiusPercent = 0.61;
    chartView.descriptionText = @"";
    chartView.drawCenterTextEnabled = YES;
    chartView.drawHoleEnabled = YES;
    chartView.rotationAngle = 0.0;
    chartView.rotationEnabled = YES;
    chartView.centerText = @"Broker App";
    chartView.drawSliceTextEnabled = NO;
    
    ChartLegend *l = chartView.legend;
    l.position = ChartLegendPositionRightOfChart;
    l.xEntrySpace = 7.0;
    l.yEntrySpace = 0.0;
    l.yOffset = 0.0;
    
    
}
// Dictionary with key shown as legends and value will be the value
- (void)drawPieChartWithData:(NSDictionary*)dictDetails Title:(NSString*)titleOfLegend CenterText:(NSString*)centerText
{
    chartView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    chartView.centerText =centerText;
    if (dictDetails!=nil) {
        NSArray *arrAllKeys = [dictDetails allKeys];
        NSMutableArray *yVals = [[NSMutableArray alloc] init];
        NSMutableArray *xVals = [[NSMutableArray alloc] init];
        for (int i=0;i<arrAllKeys.count;i++) {
            NSString *key = [arrAllKeys objectAtIndex:i];
            [xVals addObject:key];
            [yVals addObject:[[BarChartDataEntry alloc] initWithValue:[[dictDetails objectForKey:key] doubleValue] xIndex:i]];
            
        }
        PieChartDataSet *dataSet = [[PieChartDataSet alloc] initWithYVals:yVals label:titleOfLegend];
        dataSet.sliceSpace = 3.0;
        
        // add a lot of colors
        
        NSMutableArray *colors = [[NSMutableArray alloc] init];
        [colors addObject:[ConfigurationFile getThemeColor]];
        [colors addObject:GRAYCOLOR];
        [colors addObject:[UIColor colorWithRed:164/255.f green:164/255.f blue:164/255.f alpha:1.0f]];
        [colors addObjectsFromArray:ChartColorTemplates.joyful];
        [colors addObjectsFromArray:ChartColorTemplates.colorful];
        [colors addObjectsFromArray:ChartColorTemplates.liberty];
        [colors addObjectsFromArray:ChartColorTemplates.pastel];
        [colors addObject:[UIColor colorWithRed:51/255.f green:181/255.f blue:229/255.f alpha:1.0f]];
        
        dataSet.colors = colors;
        
        PieChartData *data = [[PieChartData alloc] initWithXVals:xVals dataSet:dataSet];
        
        NSNumberFormatter *pFormatter = [[NSNumberFormatter alloc] init];
        pFormatter.numberStyle = NSNumberFormatterPercentStyle;
        pFormatter.maximumFractionDigits = 0;
        pFormatter.multiplier = @1.f;
        pFormatter.percentSymbol = @"";
        [data setValueFormatter:pFormatter];
        [data setValueFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:11.f]];
        [data setValueTextColor:UIColor.blackColor];
        
        chartView.data = data;
        [chartView highlightValues:nil];
        [chartView animateWithXAxisDuration:1.5 yAxisDuration:1.5 easingOption:ChartEasingOptionEaseOutBack];
    }
    
    
}

#pragma mark - ChartViewDelegate

- (void)chartValueSelected:(ChartViewBase * __nonnull)chartView entry:(ChartDataEntry * __nonnull)entry dataSetIndex:(NSInteger)dataSetIndex highlight:(ChartHighlight * __nonnull)highlight
{
    NSLog(@"chartValueSelected");
}

- (void)chartValueNothingSelected:(ChartViewBase * __nonnull)chartView
{
    NSLog(@"chartValueNothingSelected");
}
@end
