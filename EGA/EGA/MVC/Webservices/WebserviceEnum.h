//
//  WebserviceEnum.h
//  UFSApp
//
//  Created by Soniya on 14/09/15.
//  Copyright (c) 2015 Assure Solutions Group. All rights reserved.
//

#ifndef UFSApp_WebserviceEnum_h
#define UFSApp_WebserviceEnum_h

typedef enum : NSInteger {
    BiologicalQuestion = 0,
    BiologicalQuestionSubmission,
    
    Registration,
    Login,
    ForgotPwd,
    ProfileUpdate,
    ProfileDOB,
    ProfileHeightWeightGender,
    ProfilePicUpdate,
    
    AnalyticQuestions,
    AnalyticQuestionsAnswers,
    AnalyticalQuestionSubmission,
    AnalyticalResultEmail,
    AnalyticalUserDetails,
    
    ReduceWeightTarget,
    IdealWeightSubmission,
    
    HealthTips,
    
    GetDietPlanList,
    GetDietPlanStatus,
    AutoDietPlanStatus,
    ConsumeBreakfast,
    ConsumeLunch,
    ConsumeDinner,
    
} WebServiceCallerType;

typedef enum : NSInteger {
    VideoMediaType = 0,
    ImageMediaType,
    AudioMediaType,
} MediaType;


#endif
