//
//  WebServiceCallerObject.m
//  UFSApp
//
//  Created by Soniya on 14/09/15.
//  Copyright (c) 2015 Assure Solutions Group. All rights reserved.
//

#import "WebServiceCallerObject.h"

@implementation WebServiceCallerObject

@synthesize requestURL;
@synthesize isPost;
@synthesize jsonStringToPost;
@synthesize webserviceType;
@synthesize attachedImage;
@synthesize attachMediaType;

@synthesize success;
@synthesize parseData;
@synthesize comment;

@end
