
//FileName : WebServiceCaller
//Version : 1.0
//DateOfCreation : 30/08/2015
//Author : Soniya Vishwakarma
//Dependencies :
//Description : This is caller for the JSON webservice which will used in application
/******* Webservice
 @URL :
 
 ****/

#import <Foundation/Foundation.h>
#import "WebserviceEnum.h"

@protocol WebServiceCallerDelegate;

@interface WebServiceCaller : NSObject
{
    __unsafe_unretained id <WebServiceCallerDelegate> delegate;
    NSOperationQueue * operationQueue;
   
}
@property(nonatomic, assign) id <WebServiceCallerDelegate> delegate;

-(void)cancelWebserviceCall;

-(void)cancelAllCalls;

#pragma mark - Class Methods

-(void)loginUserWithData:(NSDictionary*)userDetail;

-(void)registerUserWithData:(NSDictionary*)userDetail;

-(void)forgotPasswordWithEmailID:(NSString*)emailID;

#pragma mark -

-(void)updateDOBOfUser:(NSString*)userID WithDOB:(NSString*)dob;

-(void)updateHeight:(NSString*)height Weight:(NSString*)weight Gender:(NSString*)gender OfUser:(NSString*)userID;

-(void)updateProfileImage:(UIImage*)profileImage OfUser:(NSString*)userID;

-(void)updateProfile:(NSDictionary*)dictuserDetail OfUser:(NSString*)userID;

#pragma mark -

-(void)getHealthTipsForPageNo:(NSInteger)pageno PageCount:(NSInteger)pagecount;

#pragma mark - 

-(void)getTargets;

-(void)submitIdealWeightDetails:(NSDictionary*)weightalcData;

#pragma mark -

-(void)getListOfBiologicalAgeQuestions:(NSString*)userID;

-(void)submitBiologicalAgeQuestions:(NSDictionary*)ageCalcData UserID:(NSString*)userID;

#pragma mark -

-(void)getListOfAnalyticQuestions;

-(void)getListOfAnalyticQuestionsAnswerOfUserID:(NSString*)userid;

-(void)submitAnalyticalQuestions:(NSDictionary*)analyticData UserID:(NSString*)userID;

-(void)getAnalyticalUserDetailsForUserID:(NSString*)userID;

-(void)analyticsSendMailToUser:(NSString*)userID;

#pragma mark -

-(void)getListOfDietPlan:(NSString*)userID;

-(void)getDietPlanStatus:(NSString*)userID;

-(void)autoDietPlanInsertStatus:(NSString*)userID;

-(void)breakfastConsumedOfUser:(NSString*)userid StartDate:(NSString*)startDate isConsumed:(NSInteger)consumed;

-(void)lunchConsumedOfUser:(NSString*)userid StartDate:(NSString*)startDate isConsumed:(NSInteger)consumed;

-(void)dinnerConsumedOfUser:(NSString*)userid StartDate:(NSString*)startDate isConsumed:(NSInteger)consumed;

@end

#pragma mark - Protocol Methods

@protocol WebServiceCallerDelegate <NSObject>

@optional

#pragma mark - 

-(void)loginUserCompletesSuccessfully:(BOOL)success WithData:(NSMutableDictionary*)dictUserData WithMessage:(NSString*)message;

-(void)registerUserCompletesSuccessfully:(BOOL)success WithData:(NSMutableDictionary*)dictUserData WithMessage:(NSString*)message;

-(void)forgotPasswordCompletesSuccessfully:(BOOL)success WithMessage:(NSString*)message;

#pragma mark -

-(void)updateDOBOfUserCompletesSuccessfully:(BOOL)success WithMessage:(NSString*)message;

-(void)updateHeightWeightGenderOfUserCompletesSuccessfully:(BOOL)success WithMessage:(NSString*)message;

-(void)updateProfileCompletesSuccessfully:(BOOL)success WithData:(NSMutableDictionary*)dictUserData WithMessage:(NSString*)message;

-(void)updateProfileImageCompletesSuccessfully:(BOOL)success WithData:(UIImage*)img WithMessage:(NSString*)message;

#pragma mark -

-(void)getHealthTipsCompletesSuccessfully:(BOOL)success WithList:(NSMutableArray*)arrTips WithMessage:(NSString*)message;

#pragma mark -

-(void)getTargetsCompletesSuccessfully:(BOOL)success WithList:(NSMutableArray*)arrTargets WithMessage:(NSString*)message;

-(void)submitIdealWeightCompletesSuccessfully:(BOOL)success WithData:(NSDictionary*)arrTips WithMessage:(NSString*)message;

#pragma mark -

-(void)getListOfBiologicalAgeQuestionsCompletesSuccessfully:(BOOL)success WithList:(NSMutableArray*)arrQuestions WithMessage:(NSString*)message;

-(void)submitBiologicalAgeQuestionsCompletesSuccessfully:(BOOL)success WithData:(NSDictionary*)dictResult WithMessage:(NSString*)message;

#pragma mark -

-(void)getListOfAnalyticQuestionsCompletesSuccessfully:(BOOL)success WithList:(NSMutableArray*)arrQuestions WithMessage:(NSString*)message;

-(void)getListOfAnalyticQuestionsWithAnswerCompletesSuccessfully:(BOOL)success WithList:(NSMutableArray*)arrQuestions WithMessage:(NSString*)message;


-(void)submitAnalyticalQuestionsCompletesSuccessfully:(BOOL)success WithData:(NSDictionary*)dictResult WithMessage:(NSString*)message;


-(void)analyticalResultEmailCompletesSuccessfully:(BOOL)success WithMessage:(NSString*)message;

-(void)getAnalyticalUserDetailsCompletesSuccessfully:(BOOL)success WithData:(NSDictionary*)dictResult WithMessage:(NSString*)message;

#pragma mark -

-(void)getListOfDietPlanCompletesSuccessfully:(BOOL)success StatusCode:(NSInteger)statusCode WithList:(NSMutableArray*)arrDiets WithMessage:(NSString*)message;

-(void)getDietPlanStatusCompletesSuccessfully:(BOOL)success StatusCode:(NSInteger)statusCode WithMessage:(NSString*)message;

-(void)autoDietPlanInsertStatusCompletesSuccessfully:(BOOL)success StatusCode:(NSInteger)statusCode WithMessage:(NSString*)message;

-(void)breakfastConsumedCompletesSuccessfully:(BOOL)success WithMessage:(NSString*)message;

-(void)lunchConsumedCompletesSuccessfully:(BOOL)success WithMessage:(NSString*)message;

-(void)dinnerConsumedCompletesSuccessfully:(BOOL)success WithMessage:(NSString*)message;

@end
