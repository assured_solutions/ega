//
//  WebServiceConnectionCaller.h
//  UFSApp
//
//  Created by Soniya on 14/09/15.
//  Copyright (c) 2015 Assure Solutions Group. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebServiceCallerObject.h"

@interface WebServiceConnectionCaller : NSObject
{
     NSURLConnection *connection;
     NSMutableData *responseData;
    WebServiceCallerObject *serviceResponse;
}
-(void)callWebserviceWithRequest:(WebServiceCallerObject*)request;
-(void)callWebserviceWithMultiPartRequest:(WebServiceCallerObject*)multiPartRequest;
@end
