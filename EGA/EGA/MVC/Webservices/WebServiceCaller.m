//
//  WebServiceCaller.m
//  UFSApp
//
//  Created by Soniya on 31/08/15.
//  Copyright (c) 2015 Assure Solutions Group. All rights reserved.
//

#import "WebServiceCaller.h"
#import "WebServiceCallerObject.h"
#import "WebServiceConnectionCaller.h"

#define SuccessMessage @"Successfully Called"


@interface WebServiceCaller ()
@end

@implementation WebServiceCaller
@synthesize delegate;

typedef enum : NSInteger {
    BaseURL = 0,
    
} BaseURLType;

- (id)init
{
    self = [super init];
    if (self) {
        operationQueue = [[NSOperationQueue alloc] init];
        [operationQueue setMaxConcurrentOperationCount:10];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(webserviceCallerFinished:) name:webserviceCallerFinishedKey object:nil];
        // [self checkForCacheClear];
        [[NSURLCache sharedURLCache] removeAllCachedResponses];
    }
    return self;
}

-(void)cancelWebserviceCall{
    [operationQueue cancelAllOperations];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void)cancelAllCalls{
    [operationQueue cancelAllOperations];
}



#pragma mark - Login

-(void)loginUserWithData:(NSDictionary*)userDetail{
    
    WebServiceCallerObject *objWebserviceCaller = [[WebServiceCallerObject alloc]init];
    objWebserviceCaller.requestURL = [self getURLWithBase:BaseURL relativeURL:@"login.php"];
    objWebserviceCaller.webserviceType = Login;
    objWebserviceCaller.isPost  = YES;
    objWebserviceCaller.jsonStringToPost = [self getJSONStringForData:userDetail];
    
    WebServiceConnectionCaller * webserviceCaller = [[WebServiceConnectionCaller alloc]init];
    NSInvocationOperation *invocationOperation = [[NSInvocationOperation alloc]initWithTarget:webserviceCaller selector:@selector(callWebserviceWithRequest:) object:objWebserviceCaller];
    [operationQueue addOperation:invocationOperation];
}
-(void)loginUserCalledSuccessfully:(BOOL)success WithData:(id)Data WithMessage:(NSString*)errorMessage{
    NSDictionary *result = (NSDictionary*)Data;
    NSString *message = errorMessage;
    BOOL succes = NO;
    NSMutableDictionary *dictUserData = nil;
    if (success) {
        if (Data) {
            
            succes = [[result objectForKey:@"success"] boolValue];
            message = [result objectForKey:@"message"];
            NSArray *arrUserProfile = [result objectForKey:@"userprofiledata"];
            if (arrUserProfile.count>0) {
                dictUserData = [arrUserProfile objectAtIndex:0];
            }
            
        }
        else
            message = @"Not able to register, Please try later";
    }
    else{
        if (message.length==0) {
            message = NETWORKERRORMESSAGE;
        }
    }
    
    if(self.delegate!=nil && [(id)[self delegate] respondsToSelector:@selector(loginUserCompletesSuccessfully:WithData:WithMessage:)])
    {
        [(id)[self delegate] loginUserCompletesSuccessfully:succes WithData:dictUserData WithMessage:message];
    }
}

#pragma mark - Registration

-(void)registerUserWithData:(NSDictionary*)userDetail{
    
    WebServiceCallerObject *objWebserviceCaller = [[WebServiceCallerObject alloc]init];
    objWebserviceCaller.requestURL = [self getURLWithBase:BaseURL relativeURL:@"registration.php"];
    objWebserviceCaller.webserviceType = Registration;
    objWebserviceCaller.isPost  = YES;
    objWebserviceCaller.jsonStringToPost = [self getJSONStringForData:userDetail];
    
    WebServiceConnectionCaller * webserviceCaller = [[WebServiceConnectionCaller alloc]init];
    NSInvocationOperation *invocationOperation = [[NSInvocationOperation alloc]initWithTarget:webserviceCaller selector:@selector(callWebserviceWithRequest:) object:objWebserviceCaller];
    [operationQueue addOperation:invocationOperation];
}
-(void)registerUserDataCalledSuccessfully:(BOOL)success WithData:(id)Data WithMessage:(NSString*)errorMessage{
    
    NSString *message = errorMessage;
    BOOL succes = NO;
     NSMutableDictionary *result = (NSMutableDictionary*)Data;
    if (success) {
        if (Data) {
           
            succes = [[result objectForKey:@"status"] boolValue];
            message = [result objectForKey:@"message"];
            
        }
        else
            message = @"Not able to register, Please try later";
    }
    else{
        if (message.length==0) {
            message = NETWORKERRORMESSAGE;
        }
    }
    
    if(self.delegate!=nil && [(id)[self delegate] respondsToSelector:@selector(registerUserCompletesSuccessfully:WithData:WithMessage:)])
    {
        [(id)[self delegate] registerUserCompletesSuccessfully:succes WithData:result WithMessage:message];
    }
}

#pragma mark - Forgot Password
//http://ega.web1demos.com/webservices/forgot_password.php?emailid=dom@gmail.com

-(void)forgotPasswordWithEmailID:(NSString*)emailID{
    NSString *strUrl = [NSString stringWithFormat:@"forgot_password.php?emailid=%@",emailID];
    WebServiceCallerObject *objWebserviceCaller = [[WebServiceCallerObject alloc]init];
    objWebserviceCaller.requestURL = [self getURLWithBase:BaseURL relativeURL:strUrl];
    objWebserviceCaller.webserviceType = ForgotPwd;
    objWebserviceCaller.isPost  = NO;
    objWebserviceCaller.jsonStringToPost = nil;
    
    WebServiceConnectionCaller * webserviceCaller = [[WebServiceConnectionCaller alloc]init];
    NSInvocationOperation *invocationOperation = [[NSInvocationOperation alloc]initWithTarget:webserviceCaller selector:@selector(callWebserviceWithRequest:) object:objWebserviceCaller];
    [operationQueue addOperation:invocationOperation];
}
-(void)forgotPasswordCalledSuccessfully:(BOOL)success WithData:(id)Data WithMessage:(NSString*)errorMessage{
    
    NSString *message = errorMessage;
    BOOL succes = NO;
    if (success) {
        if (Data) {
            NSDictionary *result = (NSDictionary*)Data;
            succes = [[result objectForKey:@"success"] boolValue];
            message = [result objectForKey:@"message"];
        }
        else
            message = @"Not able to fulfill your request. Please try again later..!";
    }
    else{
        if (message.length==0) {
            message = NETWORKERRORMESSAGE;
        }
    }
    
    if(self.delegate!=nil && [(id)[self delegate] respondsToSelector:@selector(forgotPasswordCompletesSuccessfully:WithMessage:)])
    {
        [(id)[self delegate] forgotPasswordCompletesSuccessfully:succes WithMessage:message];
    }
}
#pragma mark - Profile

#pragma mark - Update DOB
//http://ega.web1demos.com/webservices/update_dob.php?user_id=3&&dob=1990-07-18

-(void)updateDOBOfUser:(NSString*)userID WithDOB:(NSString*)dob{
    NSString *strUrl = [NSString stringWithFormat:@"update_dob.php?user_id=%@&&dob=%@",userID,dob];
    WebServiceCallerObject *objWebserviceCaller = [[WebServiceCallerObject alloc]init];
    objWebserviceCaller.requestURL = [self getURLWithBase:BaseURL relativeURL:strUrl];
    objWebserviceCaller.webserviceType = ProfileDOB;
    objWebserviceCaller.isPost  = NO;
    objWebserviceCaller.jsonStringToPost = nil;
    
    WebServiceConnectionCaller * webserviceCaller = [[WebServiceConnectionCaller alloc]init];
    NSInvocationOperation *invocationOperation = [[NSInvocationOperation alloc]initWithTarget:webserviceCaller selector:@selector(callWebserviceWithRequest:) object:objWebserviceCaller];
    [operationQueue addOperation:invocationOperation];
}
-(void)updateDOBOfUserCalledSuccessfully:(BOOL)success WithData:(id)Data WithMessage:(NSString*)errorMessage{
    
    NSString *message = errorMessage;
    BOOL succes = NO;
    if (success) {
        if (Data) {
            NSDictionary *result = (NSDictionary*)Data;
            succes = [[result objectForKey:@"status"] boolValue];
            message = [result objectForKey:@"message"];
        }
        else
            message = @"Not able to fulfill your request. Please try again later..!";
    }
    else{
        if (message.length==0) {
            message = NETWORKERRORMESSAGE;
        }
    }
    
    if(self.delegate!=nil && [(id)[self delegate] respondsToSelector:@selector(updateDOBOfUserCompletesSuccessfully:WithMessage:)])
    {
        [(id)[self delegate] updateDOBOfUserCompletesSuccessfully:succes WithMessage:message];
    }
}

#pragma mark - Update Weight_Height_Gender
//http://ega.web1demos.com/webservices/update_weight_height.php?user_id=3&height=167&weight=76&gender=m

-(void)updateHeight:(NSString*)height Weight:(NSString*)weight Gender:(NSString*)gender OfUser:(NSString*)userID{
    NSString *strUrl = [NSString stringWithFormat:@"update_weight_height.php?user_id=%@&height=%@&weight=%@&gender=%@",userID,height,weight,gender];
    WebServiceCallerObject *objWebserviceCaller = [[WebServiceCallerObject alloc]init];
    objWebserviceCaller.requestURL = [self getURLWithBase:BaseURL relativeURL:strUrl];
    objWebserviceCaller.webserviceType = ProfileHeightWeightGender;
    objWebserviceCaller.isPost  = NO;
    objWebserviceCaller.jsonStringToPost = nil;
    
    WebServiceConnectionCaller * webserviceCaller = [[WebServiceConnectionCaller alloc]init];
    NSInvocationOperation *invocationOperation = [[NSInvocationOperation alloc]initWithTarget:webserviceCaller selector:@selector(callWebserviceWithRequest:) object:objWebserviceCaller];
    [operationQueue addOperation:invocationOperation];
}
-(void)updateHeightWeightGenderOfUserCalledSuccessfully:(BOOL)success WithData:(id)Data WithMessage:(NSString*)errorMessage{
    
    NSString *message = errorMessage;
    BOOL succes = NO;
    if (success) {
        if (Data) {
            NSDictionary *result = (NSDictionary*)Data;
            succes = [[result objectForKey:@"status"] boolValue];
            message = [result objectForKey:@"message"];
        }
        else
            message = @"Not able to fulfill your request. Please try again later..!";
    }
    else{
        if (message.length==0) {
            message = NETWORKERRORMESSAGE;
        }
    }
    
    if(self.delegate!=nil && [(id)[self delegate] respondsToSelector:@selector(updateHeightWeightGenderOfUserCompletesSuccessfully:WithMessage:)])
    {
        [(id)[self delegate] updateHeightWeightGenderOfUserCompletesSuccessfully:succes WithMessage:message];
    }
}


#pragma mark - Update Profile
//http://ega.web1demos.com/webservices/profile_update.php?user_id=1

-(void)updateProfile:(NSDictionary*)dictuserDetail OfUser:(NSString*)userID{
    NSString *strUrl = [NSString stringWithFormat:@"profile_update.php?user_id=%@",userID];
    WebServiceCallerObject *objWebserviceCaller = [[WebServiceCallerObject alloc]init];
    objWebserviceCaller.requestURL = [self getURLWithBase:BaseURL relativeURL:strUrl];
    objWebserviceCaller.webserviceType = ProfileUpdate;
    objWebserviceCaller.isPost  = YES;
    objWebserviceCaller.jsonStringToPost = [self getJSONStringForData:dictuserDetail];
    
    WebServiceConnectionCaller * webserviceCaller = [[WebServiceConnectionCaller alloc]init];
    NSInvocationOperation *invocationOperation = [[NSInvocationOperation alloc]initWithTarget:webserviceCaller selector:@selector(callWebserviceWithRequest:) object:objWebserviceCaller];
    [operationQueue addOperation:invocationOperation];
}
-(void)updateProfileCalledSuccessfully:(BOOL)success WithData:(id)Data WithMessage:(NSString*)errorMessage{
    
    NSString *message = errorMessage;
    BOOL succes = NO;
    NSMutableDictionary *result = (NSMutableDictionary*)Data;
    if (success) {
        if (Data) {
            
            succes = [[result objectForKey:@"status"] boolValue];
            message = [result objectForKey:@"message"];
        }
        else
            message = @"Not able to fulfill your request. Please try again later..!";
    }
    else{
        if (message.length==0) {
            message = NETWORKERRORMESSAGE;
        }
    }
    
    if(self.delegate!=nil && [(id)[self delegate] respondsToSelector:@selector(updateProfileCompletesSuccessfully:WithData:WithMessage:)])
    {
        [(id)[self delegate] updateProfileCompletesSuccessfully:succes WithData:result WithMessage:message];
    }
}
#pragma mark - Update Profile Image
//http://ega.web1demos.com/webservices/update_image.php?user_id=1
//http://ega.web1demos.com/webservices/user-image.php?user_id=3

-(void)updateProfileImage:(UIImage*)profileImage OfUser:(NSString*)userID{
    NSString *strUrl = [NSString stringWithFormat:@"update_image.php?user_id=%@",userID];
    WebServiceCallerObject *objWebserviceCaller = [[WebServiceCallerObject alloc]init];
    objWebserviceCaller.requestURL = [self getURLWithBase:BaseURL relativeURL:strUrl];
    objWebserviceCaller.webserviceType = ProfilePicUpdate;
    objWebserviceCaller.attachedImage = profileImage;
    objWebserviceCaller.isPost  = YES;
    objWebserviceCaller.jsonStringToPost = nil;
    
    WebServiceConnectionCaller * webserviceCaller = [[WebServiceConnectionCaller alloc]init];
    NSInvocationOperation *invocationOperation = [[NSInvocationOperation alloc]initWithTarget:webserviceCaller selector:@selector(callWebserviceWithMultiPartRequest:) object:objWebserviceCaller];
    [operationQueue addOperation:invocationOperation];
}
-(void)updateProfileImageCalledSuccessfully:(BOOL)success WithData:(id)Data Image:(UIImage*)img WithMessage:(NSString*)errorMessage{
    
    NSString *message = errorMessage;
    BOOL succes = NO;
    
    if (success) {
        if (Data) {
            NSDictionary *result = (NSDictionary*)Data;
            succes = [[result objectForKey:@"status"] boolValue];
            message = [result objectForKey:@"message"];
        }
        else
            message = @"Not able to fulfill your request. Please try again later..!";
    }
    else{
        if (message.length==0) {
            message = NETWORKERRORMESSAGE;
        }
    }
    
    if(self.delegate!=nil && [(id)[self delegate] respondsToSelector:@selector(updateProfileImageCompletesSuccessfully:WithData:WithMessage:)])
    {
        [(id)[self delegate] updateProfileImageCompletesSuccessfully:succes WithData:img WithMessage:message];
    }
}
#pragma mark - Health Tips
//http://ega.web1demos.com/webservices/health_tips.php?pageno=1&&pagecount=5

-(void)getHealthTipsForPageNo:(NSInteger)pageno PageCount:(NSInteger)pagecount{
    
    WebServiceCallerObject *objWebserviceCaller = [[WebServiceCallerObject alloc]init];
    NSString *url = [NSString stringWithFormat:@"%@?pageno=%ld&pagecount=%ld",[self getURLWithBase:BaseURL relativeURL:@"health_tips.php"],(long)pageno,(long)pagecount];
    
    objWebserviceCaller.requestURL = url;
    objWebserviceCaller.webserviceType = HealthTips;
    objWebserviceCaller.isPost  = NO;
    objWebserviceCaller.jsonStringToPost = nil;
    
    WebServiceConnectionCaller * webserviceCaller = [[WebServiceConnectionCaller alloc]init];
    NSInvocationOperation *invocationOperation = [[NSInvocationOperation alloc]initWithTarget:webserviceCaller selector:@selector(callWebserviceWithRequest:) object:objWebserviceCaller];
    [operationQueue addOperation:invocationOperation];
}
-(void)getHealthTipsCalledSuccessfully:(BOOL)success WithData:(id)Data WithMessage:(NSString*)errorMessage{
    
    NSMutableArray *arrHealthTips = (NSMutableArray*)Data;
    NSString *message = errorMessage;
    if (success) {
        message = SuccessMessage;
    }
    else{
        if (message.length==0) {
            message = NETWORKERRORMESSAGE;
        }
    }
    if(self.delegate!=nil && [(id)[self delegate] respondsToSelector:@selector(getHealthTipsCompletesSuccessfully:WithList:WithMessage:)])
    {
        [(id)[self delegate] getHealthTipsCompletesSuccessfully:success WithList:arrHealthTips WithMessage:message];
    }
    
}

#pragma mark - Reduce Weight

#pragma mark - Reduce Weight Submission

//​http://ega.web1demos.com/webservices/ideal_weight_insert.php

-(void)submitIdealWeightDetails:(NSDictionary*)weightalcData{
    
    WebServiceCallerObject *objWebserviceCaller = [[WebServiceCallerObject alloc]init];
    objWebserviceCaller.requestURL =[self getURLWithBase:BaseURL relativeURL:@"ideal_weight_insert.php"];;
    objWebserviceCaller.webserviceType = IdealWeightSubmission;
    objWebserviceCaller.isPost  = YES;
    objWebserviceCaller.jsonStringToPost = [self getJSONStringForData:weightalcData];
    
    WebServiceConnectionCaller * webserviceCaller = [[WebServiceConnectionCaller alloc]init];
    NSInvocationOperation *invocationOperation = [[NSInvocationOperation alloc]initWithTarget:webserviceCaller selector:@selector(callWebserviceWithRequest:) object:objWebserviceCaller];
    [operationQueue addOperation:invocationOperation];
}
-(void)submitIdealWeightCalledSuccessfully:(BOOL)success WithData:(id)Data WithMessage:(NSString*)errorMessage{
    
    NSString *message = errorMessage;
    NSDictionary *dictResult = (NSDictionary*)Data;
    BOOL succes = NO;
    if (success) {
        if (dictResult) {
            
            succes = [[dictResult objectForKey:@"status"] boolValue];
            message = [dictResult objectForKey:@"message"];
        }
        else
            message = @"Not able to fulfill your request. Please try again later..!";
    }
    else{
        if (message.length==0) {
            message = NETWORKERRORMESSAGE;
        }
    }
    
    if(self.delegate!=nil && [(id)[self delegate] respondsToSelector:@selector(submitIdealWeightCompletesSuccessfully:WithData:WithMessage:)])
    {
        [(id)[self delegate] submitIdealWeightCompletesSuccessfully:success WithData:dictResult WithMessage:message];
    }
}

#pragma mark - Target to reduce Weight
//http://ega.web1demos.com/webservices/reduce_weight.php

-(void)getTargets{
    
    WebServiceCallerObject *objWebserviceCaller = [[WebServiceCallerObject alloc]init];
    objWebserviceCaller.requestURL = [self getURLWithBase:BaseURL relativeURL:@"reduce_weight.php"];
    objWebserviceCaller.webserviceType = ReduceWeightTarget;
    objWebserviceCaller.isPost  = NO;
    objWebserviceCaller.jsonStringToPost = nil;
    
    WebServiceConnectionCaller * webserviceCaller = [[WebServiceConnectionCaller alloc]init];
    NSInvocationOperation *invocationOperation = [[NSInvocationOperation alloc]initWithTarget:webserviceCaller selector:@selector(callWebserviceWithRequest:) object:objWebserviceCaller];
    [operationQueue addOperation:invocationOperation];
}
-(void)getTargetsCalledSuccessfully:(BOOL)success WithData:(id)Data WithMessage:(NSString*)errorMessage{
    
    NSMutableArray *arrTargets = (NSMutableArray*)Data;    NSString *message = errorMessage;
    if (success) {
        message = SuccessMessage;
    }
    else{
        if (message.length==0) {
            message = NETWORKERRORMESSAGE;
        }
    }
    if(self.delegate!=nil && [(id)[self delegate] respondsToSelector:@selector(getTargetsCompletesSuccessfully:WithList:WithMessage:)])
    {
        [(id)[self delegate] getTargetsCompletesSuccessfully:success WithList:arrTargets WithMessage:message];
    }
    
}

#pragma mark - Biological Age

#pragma mark - Biological Age Questions

-(void)getListOfBiologicalAgeQuestions:(NSString*)userID{
    
    WebServiceCallerObject *objWebserviceCaller = [[WebServiceCallerObject alloc]init];
    NSString *url = [NSString stringWithFormat:@"%@?user_id=%@",[self getURLWithBase:BaseURL relativeURL:@"biological_question.php"],userID];
    objWebserviceCaller.requestURL =url;
    objWebserviceCaller.webserviceType = BiologicalQuestion;
    objWebserviceCaller.isPost  = NO;
    objWebserviceCaller.jsonStringToPost = nil;
    
    WebServiceConnectionCaller * webserviceCaller = [[WebServiceConnectionCaller alloc]init];
    NSInvocationOperation *invocationOperation = [[NSInvocationOperation alloc]initWithTarget:webserviceCaller selector:@selector(callWebserviceWithRequest:) object:objWebserviceCaller];
    [operationQueue addOperation:invocationOperation];
}
-(void)getListOfBiologicalAgeQuestionsCalledSuccessfully:(BOOL)success WithData:(id)Data WithMessage:(NSString*)errorMessage{
    
    NSMutableArray *arrBiologicalAge = (NSMutableArray*)Data;
    NSString *message = errorMessage;
    if (success) {
        message = SuccessMessage;
    }
    else{
        if (message.length==0) {
            message = NETWORKERRORMESSAGE;
        }
    }
    
    if(self.delegate!=nil && [(id)[self delegate] respondsToSelector:@selector(getListOfBiologicalAgeQuestionsCompletesSuccessfully:WithList:WithMessage:)])
    {
        [(id)[self delegate] getListOfBiologicalAgeQuestionsCompletesSuccessfully:success WithList:arrBiologicalAge WithMessage:message];
    }
}



#pragma mark - Biological Age Question Submission

//http://ega.web1demos.com/webservices/biological_question_answer.php?user_id=1

-(void)submitBiologicalAgeQuestions:(NSDictionary*)ageCalcData UserID:(NSString*)userID{
    
    WebServiceCallerObject *objWebserviceCaller = [[WebServiceCallerObject alloc]init];
    NSString *url = [NSString stringWithFormat:@"%@?user_id=%@",[self getURLWithBase:BaseURL relativeURL:@"biological_question_answer.php"],userID];
    objWebserviceCaller.requestURL =url;
    objWebserviceCaller.webserviceType = BiologicalQuestionSubmission;
    objWebserviceCaller.isPost  = YES;
    objWebserviceCaller.jsonStringToPost = [self getJSONStringForData:[NSArray arrayWithObject:ageCalcData]];
    
    WebServiceConnectionCaller * webserviceCaller = [[WebServiceConnectionCaller alloc]init];
    NSInvocationOperation *invocationOperation = [[NSInvocationOperation alloc]initWithTarget:webserviceCaller selector:@selector(callWebserviceWithRequest:) object:objWebserviceCaller];
    [operationQueue addOperation:invocationOperation];
}
-(void)submitBiologicalAgeQuestionsCalledSuccessfully:(BOOL)success WithData:(id)Data WithMessage:(NSString*)errorMessage{
    
    NSString *message = errorMessage;
    NSDictionary *dictResult = (NSDictionary*)Data;
    BOOL succes = NO;
    if (success) {
        if (dictResult) {
            
            succes = [[dictResult objectForKey:@"status"] boolValue];
            message = [dictResult objectForKey:@"message"];
        }
        else
            message = @"Not able to fulfill your request. Please try again later..!";
    }
    else{
        if (message.length==0) {
            message = NETWORKERRORMESSAGE;
        }
    }
    
    if(self.delegate!=nil && [(id)[self delegate] respondsToSelector:@selector(submitBiologicalAgeQuestionsCompletesSuccessfully:WithData:WithMessage:)])
    {
        [(id)[self delegate] submitBiologicalAgeQuestionsCompletesSuccessfully:success WithData:dictResult WithMessage:message];
    }
}

#pragma mark - Analytical

#pragma mark - Analytical Questions

//http://ega.web1demos.com/webservices/wt_question.php

-(void)getListOfAnalyticQuestions{
    
    WebServiceCallerObject *objWebserviceCaller = [[WebServiceCallerObject alloc]init];
    objWebserviceCaller.requestURL = [self getURLWithBase:BaseURL relativeURL:@"wt_question.php"];
    objWebserviceCaller.webserviceType = AnalyticQuestions;
    objWebserviceCaller.isPost  = NO;
    objWebserviceCaller.jsonStringToPost = nil;
    
    WebServiceConnectionCaller * webserviceCaller = [[WebServiceConnectionCaller alloc]init];
    NSInvocationOperation *invocationOperation = [[NSInvocationOperation alloc]initWithTarget:webserviceCaller selector:@selector(callWebserviceWithRequest:) object:objWebserviceCaller];
    [operationQueue addOperation:invocationOperation];
}
-(void)getListOfAnalyticQuestionsCalledSuccessfully:(BOOL)success WithData:(id)Data WithMessage:(NSString*)errorMessage{
    
    NSDictionary *dictResult = (NSDictionary*)Data;
    NSMutableArray *arrAnalytic = nil;
    NSString *message = errorMessage;
    if (success) {
        arrAnalytic = [dictResult objectForKey:@"items"];
        if ( ![arrAnalytic isKindOfClass:[NSArray class]]) {
            arrAnalytic = nil;
        }

        message = SuccessMessage;
    }
    else{
        if (message.length==0) {
            message = NETWORKERRORMESSAGE;
        }
    }
    if(self.delegate!=nil && [(id)[self delegate] respondsToSelector:@selector(getListOfAnalyticQuestionsCompletesSuccessfully:WithList:WithMessage:)])
    {
        [(id)[self delegate] getListOfAnalyticQuestionsCompletesSuccessfully:success WithList:arrAnalytic WithMessage:message];
    }
    
}

#pragma mark - Analytical Questions With Answers

//http://ega.web1demos.com/webservices/wt_question.php?user_id=8

-(void)getListOfAnalyticQuestionsAnswerOfUserID:(NSString*)userid{
    
    NSString *url = [NSString stringWithFormat:@"%@?user_id=%@",[self getURLWithBase:BaseURL relativeURL:@"wt_question.php"],userid];
    
    WebServiceCallerObject *objWebserviceCaller = [[WebServiceCallerObject alloc]init];
    objWebserviceCaller.requestURL = url;
    objWebserviceCaller.webserviceType = AnalyticQuestionsAnswers;
    objWebserviceCaller.isPost  = NO;
    objWebserviceCaller.jsonStringToPost = nil;
    
    WebServiceConnectionCaller * webserviceCaller = [[WebServiceConnectionCaller alloc]init];
    NSInvocationOperation *invocationOperation = [[NSInvocationOperation alloc]initWithTarget:webserviceCaller selector:@selector(callWebserviceWithRequest:) object:objWebserviceCaller];
    [operationQueue addOperation:invocationOperation];
}
-(void)getListOfAnalyticQuestionsWithAnswerCalledSuccessfully:(BOOL)success WithData:(id)Data WithMessage:(NSString*)errorMessage{
    
    NSDictionary *dictResult = (NSDictionary*)Data;
    NSMutableArray *arrAnalytic = nil;
    NSString *message = errorMessage;
    if (success) {
        arrAnalytic = [dictResult objectForKey:@"items"];
        if ( ![arrAnalytic isKindOfClass:[NSArray class]]) {
            arrAnalytic = nil;
        }
        message = SuccessMessage;
    }
    else{
        if (message.length==0) {
            message = NETWORKERRORMESSAGE;
        }
    }
    if(self.delegate!=nil && [(id)[self delegate] respondsToSelector:@selector(getListOfAnalyticQuestionsWithAnswerCompletesSuccessfully:WithList:WithMessage:)])
    {
        [(id)[self delegate] getListOfAnalyticQuestionsWithAnswerCompletesSuccessfully:success WithList:arrAnalytic WithMessage:message];
    }
    
}

#pragma mark - Analytical Question Submission

//http://ega.web1demos.com/webservices/wt_que_answer.php?user_id=1

-(void)submitAnalyticalQuestions:(NSDictionary*)analyticData UserID:(NSString*)userID{
    
    WebServiceCallerObject *objWebserviceCaller = [[WebServiceCallerObject alloc]init];
    NSString *url = [NSString stringWithFormat:@"%@?user_id=%@",[self getURLWithBase:BaseURL relativeURL:@"wt_que_answer.php"],userID];
    objWebserviceCaller.requestURL =url;
    objWebserviceCaller.webserviceType = AnalyticalQuestionSubmission;
    objWebserviceCaller.isPost  = YES;
    objWebserviceCaller.jsonStringToPost = [self getJSONStringForData:[NSArray arrayWithObject:analyticData]];
    
    WebServiceConnectionCaller * webserviceCaller = [[WebServiceConnectionCaller alloc]init];
    NSInvocationOperation *invocationOperation = [[NSInvocationOperation alloc]initWithTarget:webserviceCaller selector:@selector(callWebserviceWithRequest:) object:objWebserviceCaller];
    [operationQueue addOperation:invocationOperation];
}
-(void)submitAnalyticalQuestionsCalledSuccessfully:(BOOL)success WithData:(id)Data WithMessage:(NSString*)errorMessage{
    
    NSString *message = errorMessage;
    NSDictionary *dictResult = (NSDictionary*)Data;
    BOOL succes = NO;
    if (success) {
        if (dictResult) {
            
            succes = [[dictResult objectForKey:@"status"] boolValue];
            message = [dictResult objectForKey:@"message"];
        }
        else
            message = @"Not able to fulfill your request. Please try again later..!";
    }
    else{
        if (message.length==0) {
            message = NETWORKERRORMESSAGE;
        }
    }
    
    if(self.delegate!=nil && [(id)[self delegate] respondsToSelector:@selector(submitAnalyticalQuestionsCompletesSuccessfully:WithData:WithMessage:)])
    {
        [(id)[self delegate] submitAnalyticalQuestionsCompletesSuccessfully:success WithData:dictResult WithMessage:message];
    }
}

#pragma mark - Analytic Mail Report
//http://ega.web1demos.com/webservices/body_analize_mail_report.php?user_id=5

-(void)analyticsSendMailToUser:(NSString*)userID{
    NSString *strUrl = [NSString stringWithFormat:@"body_analize_mail_report.php?user_id=%@",userID];
    WebServiceCallerObject *objWebserviceCaller = [[WebServiceCallerObject alloc]init];
    objWebserviceCaller.requestURL = [self getURLWithBase:BaseURL relativeURL:strUrl];
    objWebserviceCaller.webserviceType = AnalyticalResultEmail;
    objWebserviceCaller.isPost  = NO;
    objWebserviceCaller.jsonStringToPost = nil;
    
    WebServiceConnectionCaller * webserviceCaller = [[WebServiceConnectionCaller alloc]init];
    NSInvocationOperation *invocationOperation = [[NSInvocationOperation alloc]initWithTarget:webserviceCaller selector:@selector(callWebserviceWithRequest:) object:objWebserviceCaller];
    [operationQueue addOperation:invocationOperation];
}
-(void) analyticalResultEmailCalledSuccessfully:(BOOL)success WithData:(id)Data WithMessage:(NSString*)errorMessage{
    
    NSString *message = errorMessage;
    BOOL succes = NO;
    if (success) {
        if (Data) {
            NSDictionary *result = (NSDictionary*)Data;
            succes = [[result objectForKey:@"success"] boolValue];
            message = [result objectForKey:@"message"];
        }
        else
            message = @"Not able to fulfill your request. Please try again later..!";
    }
    else{
        if (message.length==0) {
            message = NETWORKERRORMESSAGE;
        }
    }
    
    if(self.delegate!=nil && [(id)[self delegate] respondsToSelector:@selector(analyticalResultEmailCompletesSuccessfully:WithMessage:)])
    {
        [(id)[self delegate] analyticalResultEmailCompletesSuccessfully:succes WithMessage:message];
    }
}

#pragma mark - Analytical User Details

//http://ega.web1demos.com/webservices/show_body_analize.php?user_id=3

-(void)getAnalyticalUserDetailsForUserID:(NSString*)userID{

    WebServiceCallerObject *objWebserviceCaller = [[WebServiceCallerObject alloc]init];
    NSString *url = [NSString stringWithFormat:@"%@?user_id=%@",[self getURLWithBase:BaseURL relativeURL:@"show_body_analize.php"],userID];
    objWebserviceCaller.requestURL =url;
    objWebserviceCaller.webserviceType = AnalyticalUserDetails;
    objWebserviceCaller.isPost  = NO;
    objWebserviceCaller.jsonStringToPost =nil;
    
    WebServiceConnectionCaller * webserviceCaller = [[WebServiceConnectionCaller alloc]init];
    NSInvocationOperation *invocationOperation = [[NSInvocationOperation alloc]initWithTarget:webserviceCaller selector:@selector(callWebserviceWithRequest:) object:objWebserviceCaller];
    [operationQueue addOperation:invocationOperation];
}
-(void)getAnalyticalUserDetailsCalledSuccessfully:(BOOL)success WithData:(id)Data WithMessage:(NSString*)errorMessage{
    
    NSString *message = errorMessage;
    NSDictionary *dictResult = (NSDictionary*)Data;
    BOOL succes = NO;
    if (success) {
        if (dictResult) {
            
            succes = [[dictResult objectForKey:@"status"] boolValue];
            message = [dictResult objectForKey:@"message"];
        }
        else
            message = @"Not able to fulfill your request. Please try again later..!";
    }
    else{
        if (message.length==0) {
            message = NETWORKERRORMESSAGE;
        }
    }
    
    if(self.delegate!=nil && [(id)[self delegate] respondsToSelector:@selector(getAnalyticalUserDetailsCompletesSuccessfully:WithData:WithMessage:)])
    {
        [(id)[self delegate] getAnalyticalUserDetailsCompletesSuccessfully:success WithData:dictResult WithMessage:message];
    }
}


#pragma mark - Diet Plan

#pragma mark - Diet Plan List

//​http://ega.web1demos.com/webservices/show_my_diet_plan.php?user_id=1

-(void)getListOfDietPlan:(NSString*)userID{
    
    WebServiceCallerObject *objWebserviceCaller = [[WebServiceCallerObject alloc]init];
    NSString *strUrl = [NSString stringWithFormat:@"show_my_diet_plan.php?user_id=%@",userID];
    objWebserviceCaller.requestURL = [self getURLWithBase:BaseURL relativeURL:strUrl];
    objWebserviceCaller.webserviceType = GetDietPlanList;
    objWebserviceCaller.isPost  = NO;
    objWebserviceCaller.jsonStringToPost = nil;
    
    WebServiceConnectionCaller * webserviceCaller = [[WebServiceConnectionCaller alloc]init];
    NSInvocationOperation *invocationOperation = [[NSInvocationOperation alloc]initWithTarget:webserviceCaller selector:@selector(callWebserviceWithRequest:) object:objWebserviceCaller];
    [operationQueue addOperation:invocationOperation];
}
-(void)getListOfDietPlanCalledSuccessfully:(BOOL)success WithData:(id)Data WithMessage:(NSString*)errorMessage{
    
    NSArray *arrResult = (NSArray*)Data;
    NSMutableArray *arrDietPlan = nil;
    NSString *message = errorMessage;
    NSInteger statusCode = 0;
    if (success) {
        if (arrResult.count>0) {
            NSDictionary *dictResult = (NSDictionary*)[arrResult objectAtIndex:0];
            arrDietPlan = [dictResult objectForKey:@"DietPlans"];
            message = [dictResult objectForKey:@"message"];
            statusCode = [[dictResult objectForKey:@"status"] integerValue];
        }
           }
    else{
        if (message.length==0) {
            message = NETWORKERRORMESSAGE;
        }
    }
    if(self.delegate!=nil && [(id)[self delegate] respondsToSelector:@selector(getListOfDietPlanCompletesSuccessfully:StatusCode:WithList:WithMessage:)])
    {
        [(id)[self delegate] getListOfDietPlanCompletesSuccessfully:success StatusCode:statusCode WithList:arrDietPlan WithMessage:message];
    }
    
}


#pragma mark - Diet Plan Status

//http://ega.web1demos.com/webservices/diet_plan_type.php?user_id=8


-(void)getDietPlanStatus:(NSString*)userID{
    
    WebServiceCallerObject *objWebserviceCaller = [[WebServiceCallerObject alloc]init];
    NSString *strUrl = [NSString stringWithFormat:@"diet_plan_type.php?user_id=%@",userID];
    objWebserviceCaller.requestURL = [self getURLWithBase:BaseURL relativeURL:strUrl];
    objWebserviceCaller.webserviceType = GetDietPlanStatus;
    objWebserviceCaller.isPost  = NO;
    objWebserviceCaller.jsonStringToPost = nil;
    
    WebServiceConnectionCaller * webserviceCaller = [[WebServiceConnectionCaller alloc]init];
    NSInvocationOperation *invocationOperation = [[NSInvocationOperation alloc]initWithTarget:webserviceCaller selector:@selector(callWebserviceWithRequest:) object:objWebserviceCaller];
    [operationQueue addOperation:invocationOperation];
}
-(void)getDietPlanStatusCalledSuccessfully:(BOOL)success WithData:(id)Data WithMessage:(NSString*)errorMessage{
    
    NSString *message = errorMessage;
    NSInteger statusCode = 0;
    if (success) {
          NSDictionary *dictResult = (NSDictionary*)Data;
        statusCode = [[dictResult objectForKey:@"status"] integerValue];
        message = [dictResult objectForKey:@"message"];
    }
    else{
        if (message.length==0) {
            message = NETWORKERRORMESSAGE;
        }
    }
    if(self.delegate!=nil && [(id)[self delegate] respondsToSelector:@selector(getDietPlanStatusCompletesSuccessfully:StatusCode:WithMessage:)])
    {
        [(id)[self delegate] getDietPlanStatusCompletesSuccessfully:success StatusCode:statusCode WithMessage:message];
    }
    
}
#pragma mark - Diet Plan Insert Status

//http://ega.web1demos.com/webservices/auto_diet_plan_suggestion.php?user_id=8


-(void)autoDietPlanInsertStatus:(NSString*)userID{
    
    WebServiceCallerObject *objWebserviceCaller = [[WebServiceCallerObject alloc]init];
    NSString *strUrl = [NSString stringWithFormat:@"auto_diet_plan_suggestion.php?user_id=%@",userID];
    objWebserviceCaller.requestURL = [self getURLWithBase:BaseURL relativeURL:strUrl];
    objWebserviceCaller.webserviceType = AutoDietPlanStatus;
    objWebserviceCaller.isPost  = NO;
    objWebserviceCaller.jsonStringToPost = nil;
    
    WebServiceConnectionCaller * webserviceCaller = [[WebServiceConnectionCaller alloc]init];
    NSInvocationOperation *invocationOperation = [[NSInvocationOperation alloc]initWithTarget:webserviceCaller selector:@selector(callWebserviceWithRequest:) object:objWebserviceCaller];
    [operationQueue addOperation:invocationOperation];
}
-(void)autoDietPlanInsertStatusCalledSuccessfully:(BOOL)success WithData:(id)Data WithMessage:(NSString*)errorMessage{
    
    NSString *message = errorMessage;
    NSInteger statusCode = 0;
    if (success) {
        NSDictionary *dictResult = (NSDictionary*)Data;
        statusCode = [[dictResult objectForKey:@"status"] integerValue];
        message = [dictResult objectForKey:@"message"];
    }
    else{
        if (message.length==0) {
            message = NETWORKERRORMESSAGE;
        }
    }
    if(self.delegate!=nil && [(id)[self delegate] respondsToSelector:@selector(autoDietPlanInsertStatusCompletesSuccessfully:StatusCode:WithMessage:)])
    {
        [(id)[self delegate] autoDietPlanInsertStatusCompletesSuccessfully:success StatusCode:statusCode WithMessage:message];
    }
    
}
#pragma mark - Breakfast Consume

//​​http://ega.web1demos.com/webservices/​consume_my_breakfast.php?user_id=1&Startdate=2015-12-12&iSConsumedBreakFast=1


-(void)breakfastConsumedOfUser:(NSString*)userid StartDate:(NSString*)startDate isConsumed:(NSInteger)consumed{
    
    WebServiceCallerObject *objWebserviceCaller = [[WebServiceCallerObject alloc]init];
    NSString *strUrl = [NSString stringWithFormat:@"consume_my_breakfast.php?user_id=%@&Startdate=%@&iSConsumedBreakFast=%ld",userid,startDate,(long)consumed];
    objWebserviceCaller.requestURL = [self getURLWithBase:BaseURL relativeURL:strUrl];
    objWebserviceCaller.webserviceType = ConsumeBreakfast;
    objWebserviceCaller.isPost  = NO;
    objWebserviceCaller.jsonStringToPost = nil;
    
    WebServiceConnectionCaller * webserviceCaller = [[WebServiceConnectionCaller alloc]init];
    NSInvocationOperation *invocationOperation = [[NSInvocationOperation alloc]initWithTarget:webserviceCaller selector:@selector(callWebserviceWithRequest:) object:objWebserviceCaller];
    [operationQueue addOperation:invocationOperation];
}
-(void)breakfastConsumedCalledSuccessfully:(BOOL)success WithData:(id)Data WithMessage:(NSString*)errorMessage{
    
    NSString *message = errorMessage;
   BOOL succes = NO;
    if (success) {
        NSDictionary *dictResult = (NSDictionary*)Data;
        succes = [[dictResult objectForKey:@"status"] boolValue];
        message = [dictResult objectForKey:@"message"];
    }
    else{
        if (message.length==0) {
            message = NETWORKERRORMESSAGE;
        }
    }
    if(self.delegate!=nil && [(id)[self delegate] respondsToSelector:@selector(breakfastConsumedCompletesSuccessfully:WithMessage:)])
    {
        [(id)[self delegate] breakfastConsumedCompletesSuccessfully:succes WithMessage:message];
    }
    
}
#pragma mark - Lunch Consume

//​​​http://ega.web1demos.com/webservices/​consume_my_lunch.php?user_id=1&Startdate=2015-12-12&iSConsumedLunch=1


-(void)lunchConsumedOfUser:(NSString*)userid StartDate:(NSString*)startDate isConsumed:(NSInteger)consumed{
    
    WebServiceCallerObject *objWebserviceCaller = [[WebServiceCallerObject alloc]init];
    NSString *strUrl = [NSString stringWithFormat:@"consume_my_lunch.php?user_id=%@&Startdate=%@&iSConsumedLunch=%ld",userid,startDate,(long)consumed];
    objWebserviceCaller.requestURL = [self getURLWithBase:BaseURL relativeURL:strUrl];
    objWebserviceCaller.webserviceType = ConsumeLunch;
    objWebserviceCaller.isPost  = NO;
    objWebserviceCaller.jsonStringToPost = nil;
    
    WebServiceConnectionCaller * webserviceCaller = [[WebServiceConnectionCaller alloc]init];
    NSInvocationOperation *invocationOperation = [[NSInvocationOperation alloc]initWithTarget:webserviceCaller selector:@selector(callWebserviceWithRequest:) object:objWebserviceCaller];
    [operationQueue addOperation:invocationOperation];
}
-(void)lunchConsumedCalledSuccessfully:(BOOL)success WithData:(id)Data WithMessage:(NSString*)errorMessage{
    
    NSString *message = errorMessage;
    BOOL succes = NO;
    if (success) {
        NSDictionary *dictResult = (NSDictionary*)Data;
        succes = [[dictResult objectForKey:@"status"] boolValue];
        message = [dictResult objectForKey:@"message"];
    }
    else{
        if (message.length==0) {
            message = NETWORKERRORMESSAGE;
        }
    }
    if(self.delegate!=nil && [(id)[self delegate] respondsToSelector:@selector(lunchConsumedCompletesSuccessfully:WithMessage:)])
    {
        [(id)[self delegate] lunchConsumedCompletesSuccessfully:succes WithMessage:message];
    }
    
}


#pragma mark - Dinner Consume

//​​​​​http://ega.web1demos.com/webservices/​consume_my_dinner.php?user_id=1&Startdate=2015-12-12&iSConsumedDinner=1


-(void)dinnerConsumedOfUser:(NSString*)userid StartDate:(NSString*)startDate isConsumed:(NSInteger)consumed{
    
    WebServiceCallerObject *objWebserviceCaller = [[WebServiceCallerObject alloc]init];
    NSString *strUrl = [NSString stringWithFormat:@"consume_my_dinner.php?user_id=%@&Startdate=%@&iSConsumedDinner=%ld",userid,startDate,(long)consumed];
    objWebserviceCaller.requestURL = [self getURLWithBase:BaseURL relativeURL:strUrl];
    objWebserviceCaller.webserviceType = ConsumeDinner;
    objWebserviceCaller.isPost  = NO;
    objWebserviceCaller.jsonStringToPost = nil;
    
    WebServiceConnectionCaller * webserviceCaller = [[WebServiceConnectionCaller alloc]init];
    NSInvocationOperation *invocationOperation = [[NSInvocationOperation alloc]initWithTarget:webserviceCaller selector:@selector(callWebserviceWithRequest:) object:objWebserviceCaller];
    [operationQueue addOperation:invocationOperation];
}
-(void)dinnerConsumedCalledSuccessfully:(BOOL)success WithData:(id)Data WithMessage:(NSString*)errorMessage{
    
    NSString *message = errorMessage;
   BOOL succes = NO;
    if (success) {
        NSDictionary *dictResult = (NSDictionary*)Data;
        succes = [[dictResult objectForKey:@"status"] boolValue];
        message = [dictResult objectForKey:@"message"];
    }
    else{
        if (message.length==0) {
            message = NETWORKERRORMESSAGE;
        }
    }
    if(self.delegate!=nil && [(id)[self delegate] respondsToSelector:@selector(dinnerConsumedCompletesSuccessfully:WithMessage:)])
    {
        [(id)[self delegate] dinnerConsumedCompletesSuccessfully:succes WithMessage:message];
    }
    
}
#pragma mark - URL creator

-(NSString*)getURLWithBase:(BaseURLType)urlType relativeURL:(NSString*)relativeURL{
    return [NSString stringWithFormat:BASEURL,relativeURL];
}

#pragma mark - JSON creator

-(NSString*)getJSONStringForData:(id)dataToConvert{
    NSError *error;
    NSString *jsonString = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dataToConvert
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    return jsonString;
}

-(void)webserviceCallerFinished:(NSNotification *)notification{
    WebServiceCallerObject * webserviceCallerObject = notification.object;
    
    switch (webserviceCallerObject.webserviceType) {
            
            /******************/
            
        case BiologicalQuestion:
            [self getListOfBiologicalAgeQuestionsCalledSuccessfully:webserviceCallerObject.success WithData:webserviceCallerObject.parseData WithMessage:webserviceCallerObject.comment];
            break;
            
            /******************/
            
        case Registration:
            [self registerUserDataCalledSuccessfully:webserviceCallerObject.success WithData:webserviceCallerObject.parseData WithMessage:webserviceCallerObject.comment];
            break;
            
            /******************/
            
        case Login:
            [self loginUserCalledSuccessfully:webserviceCallerObject.success WithData:webserviceCallerObject.parseData WithMessage:webserviceCallerObject.comment];
            break;
            
            /******************/
            
        case ForgotPwd:
            [self forgotPasswordCalledSuccessfully:webserviceCallerObject.success WithData:webserviceCallerObject.parseData WithMessage:webserviceCallerObject.comment];
            break;
            
            /******************/
            
        case AnalyticQuestions:
            [self getListOfAnalyticQuestionsCalledSuccessfully:webserviceCallerObject.success WithData:webserviceCallerObject.parseData WithMessage:webserviceCallerObject.comment];
            break;
            
            /******************/
            
        case BiologicalQuestionSubmission:
            [self submitBiologicalAgeQuestionsCalledSuccessfully:webserviceCallerObject.success WithData:webserviceCallerObject.parseData WithMessage:webserviceCallerObject.comment];
            break;
            
            /******************/
            
        case ProfileDOB:
            [self updateDOBOfUserCalledSuccessfully:webserviceCallerObject.success WithData:webserviceCallerObject.parseData WithMessage:webserviceCallerObject.comment];
            break;
            
            /******************/
            
        case ProfileHeightWeightGender:
            [self updateHeightWeightGenderOfUserCalledSuccessfully:webserviceCallerObject.success WithData:webserviceCallerObject.parseData WithMessage:webserviceCallerObject.comment];
            break;
            
            /******************/
            
        case ProfileUpdate:
            [self updateProfileCalledSuccessfully:webserviceCallerObject.success WithData:webserviceCallerObject.parseData WithMessage:webserviceCallerObject.comment];
            break;
            
            /******************/
            
        case ProfilePicUpdate:
            [self updateProfileImageCalledSuccessfully:webserviceCallerObject.success WithData:webserviceCallerObject.parseData Image:webserviceCallerObject.attachedImage WithMessage:webserviceCallerObject.comment];
            break;
            
            /******************/
            
        case ReduceWeightTarget:
            [self getTargetsCalledSuccessfully:webserviceCallerObject.success WithData:webserviceCallerObject.parseData WithMessage:webserviceCallerObject.comment];
            break;
            
            /******************/
            
        case AnalyticalQuestionSubmission:
            [self submitAnalyticalQuestionsCalledSuccessfully:webserviceCallerObject.success WithData:webserviceCallerObject.parseData WithMessage:webserviceCallerObject.comment];
            break;
            
            /******************/
            
        case AnalyticalResultEmail:
            [self analyticalResultEmailCalledSuccessfully:webserviceCallerObject.success WithData:webserviceCallerObject.parseData WithMessage:webserviceCallerObject.comment];
            break;
            
            /******************/
            
        case HealthTips:
            [self getHealthTipsCalledSuccessfully:webserviceCallerObject.success WithData:webserviceCallerObject.parseData WithMessage:webserviceCallerObject.comment];
            break;
            
            /******************/
            
        case IdealWeightSubmission:
            [self submitIdealWeightCalledSuccessfully:webserviceCallerObject.success WithData:webserviceCallerObject.parseData WithMessage:webserviceCallerObject.comment];
            break;
            
            /******************/
            
        case GetDietPlanList:
            [self getListOfDietPlanCalledSuccessfully:webserviceCallerObject.success WithData:webserviceCallerObject.parseData WithMessage:webserviceCallerObject.comment];
            break;
            
            /******************/
           
        case GetDietPlanStatus:
            [self getDietPlanStatusCalledSuccessfully:webserviceCallerObject.success WithData:webserviceCallerObject.parseData WithMessage:webserviceCallerObject.comment];
            break;
            
            /******************/
            
        case  AnalyticalUserDetails:
            [self getAnalyticalUserDetailsCalledSuccessfully:webserviceCallerObject.success WithData:webserviceCallerObject.parseData WithMessage:webserviceCallerObject.comment];
            break;
            
            /******************/
            
            /******************/
            
        case ConsumeBreakfast:
            [self breakfastConsumedCalledSuccessfully:webserviceCallerObject.success WithData:webserviceCallerObject.parseData WithMessage:webserviceCallerObject.comment];
            break;
            
            /******************/
            
        case ConsumeLunch:
            [self lunchConsumedCalledSuccessfully:webserviceCallerObject.success WithData:webserviceCallerObject.parseData WithMessage:webserviceCallerObject.comment];
            break;
            
            /******************/
            
        case ConsumeDinner:
            [self dinnerConsumedCalledSuccessfully:webserviceCallerObject.success WithData:webserviceCallerObject.parseData WithMessage:webserviceCallerObject.comment];
            break;
           
            /******************/
            
        case AutoDietPlanStatus:
            [self autoDietPlanInsertStatusCalledSuccessfully:webserviceCallerObject.success WithData:webserviceCallerObject.parseData WithMessage:webserviceCallerObject.comment];
            break;
            
            /******************/
            
        case AnalyticQuestionsAnswers:
            [self getListOfAnalyticQuestionsWithAnswerCalledSuccessfully:webserviceCallerObject.success WithData:webserviceCallerObject.parseData WithMessage:webserviceCallerObject.comment];
            break;
            
            /******************/

         default:
            break;
    }
}
#pragma mark -  Check Expiration

-(void)checkForCacheClear{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *expires = [defaults objectForKey:@"CleanCache"];
    NSDate * date = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"yyyy-MM-dd hh:mm a"];
    //dateFormat.dateFormat = NSDateFormatterShortStyle;
    NSString *todaysDate  = [dateFormat stringFromDate:date];
    if (expires==nil) {
        [defaults setObject:todaysDate forKey:@"CleanCache"];
        [defaults synchronize];
        [[NSURLCache sharedURLCache] removeAllCachedResponses];
        
    }
    else{
        if ([todaysDate compare:expires] == NSOrderedDescending) {
            [defaults setObject:todaysDate forKey:@"CleanCache"];
            [defaults synchronize];
            [[NSURLCache sharedURLCache] removeAllCachedResponses];
            
        }
    }
    
}
@end
