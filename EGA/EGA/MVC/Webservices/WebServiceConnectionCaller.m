 //
//  WebServiceConnectionCaller.m
//  UFSApp
//
//  Created by Soniya on 14/09/15.
//  Copyright (c) 2015 Assure Solutions Group. All rights reserved.
//

#import "WebServiceConnectionCaller.h"

@implementation WebServiceConnectionCaller
#pragma mark - Webservice Caller

-(void)callWebserviceWithRequest:(WebServiceCallerObject*)request
{
    if (request == nil) {
        return;
    }
    
    serviceResponse = request;
    
    NSString *urlString = [serviceResponse.requestURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    responseData = [NSMutableData data];
    
    NSLog(@"\n%@",urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:60];
    
    if (serviceResponse.isPost) {
        [urlRequest setHTTPMethod:@"POST"];
        if (serviceResponse.jsonStringToPost.length!=0) {
            NSData *postData = [serviceResponse.jsonStringToPost dataUsingEncoding:NSUTF8StringEncoding];
            NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
            [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            [urlRequest setValue:postLength forHTTPHeaderField:@"Content-Length"];
            [urlRequest setHTTPBody:postData];
            
        }
        
        
    }
    connection = [[NSURLConnection alloc]initWithRequest:urlRequest delegate:self startImmediately:NO];
    [connection scheduleInRunLoop:[NSRunLoop mainRunLoop] forMode:NSRunLoopCommonModes];
    [connection start];
    
}

-(void)callWebserviceWithMultiPartRequest:(WebServiceCallerObject*)multiPartRequest
{
    if (multiPartRequest == nil) {
        return;
    }
    
    serviceResponse = multiPartRequest;
    NSData *attachData = UIImagePNGRepresentation(multiPartRequest.attachedImage);
    
  NSString *urlString = [serviceResponse.requestURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSURL *url = [NSURL URLWithString: urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    [request setValue:@"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8" forHTTPHeaderField:@"Accept"];
    [request setValue:@"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/536.26.14 (KHTML, like Gecko) Version/6.0.1 Safari/536.26.14" forHTTPHeaderField:@"User-Agent"];
    
    NSMutableData *body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"uploadedfile\"; filename=\"%@.png\"\r\n", @"Uploaded_file"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[NSData dataWithData:attachData]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [request setHTTPBody:body];
    [request addValue:[NSString stringWithFormat:@"%lu", (unsigned long)[body length]] forHTTPHeaderField:@"Content-Length"];

    
    responseData = [NSMutableData data];
    
    
     connection = [[NSURLConnection alloc]initWithRequest:request delegate:self startImmediately:NO];
    [connection scheduleInRunLoop:[NSRunLoop mainRunLoop] forMode:NSRunLoopCommonModes];
    [connection start];
    
}


- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    [responseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSLog(@"User Login Error during connection: %@", [error description]);
    
    serviceResponse.parseData = nil;
    serviceResponse.success = NO;
    serviceResponse.comment = [error localizedDescription];
//    NSInteger errCode = error.code;
//    if (errCode == -1009) {
//        serviceResponse.comment = NETWORKERRORMESSAGE;
//    }
    [[NSNotificationCenter defaultCenter] postNotificationName:webserviceCallerFinishedKey object:serviceResponse userInfo:nil];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    //  NSLog(@"%@",responseString);
    
    NSError *error = nil;
    
    id result=nil;
    if (responseData!=nil) {
        result = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:&error];
    }
    
    BOOL success = YES;
    if (error) {
        success = NO;
        serviceResponse.comment = [error localizedDescription];
    }
    
    
    serviceResponse.parseData = result;
//    if (serviceResponse.webserviceType == RegisterTrader) {
//        serviceResponse.parseData = responseString;
//    }
    serviceResponse.success = success;
    [[NSNotificationCenter defaultCenter] postNotificationName:webserviceCallerFinishedKey object:serviceResponse userInfo:nil];
    
}

- (void) connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    if([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
    {
        [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust]
             forAuthenticationChallenge:challenge];
    }
    
    [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}

- (void) connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace
{
    if([[protectionSpace authenticationMethod] isEqualToString:NSURLAuthenticationMethodServerTrust])
    {
        // return YES;
    }
}
//- (NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse
//{
//    NSURLResponse *response = cachedResponse.response;
//    if ([response isKindOfClass:NSHTTPURLResponse.class]) return cachedResponse;
//
//    NSHTTPURLResponse *HTTPResponse = (NSHTTPURLResponse*)response;
//    NSDictionary *headers = HTTPResponse.allHeaderFields;
//    if (headers[@"Cache-Control"]) return cachedResponse;
//
//    NSMutableDictionary *modifiedHeaders = headers.mutableCopy;
//    modifiedHeaders[@"Cache-Control"] = @"max-age=60";
//    NSHTTPURLResponse *modifiedResponse = [[NSHTTPURLResponse alloc]
//                                           initWithURL:HTTPResponse.URL
//                                           statusCode:HTTPResponse.statusCode
//                                           HTTPVersion:@"HTTP/1.1"
//                                           headerFields:modifiedHeaders];
//
//    cachedResponse = [[NSCachedURLResponse alloc]
//                      initWithResponse:modifiedResponse
//                      data:cachedResponse.data
//                      userInfo:cachedResponse.userInfo
//                      storagePolicy:cachedResponse.storagePolicy];
//    return cachedResponse;
//}
@end
