 //
//  ConfigurationFile.m
//  LaunchApp
//
//  Created by Soniya on 04/09/15.
//  Copyright (c) 2015 Assure Solutions Group. All rights reserved.
//

#import "ConfigurationFile.h"



@implementation ConfigurationFile

static NSString * clientID;
static NSString * address;
static NSString * companyName;
static NSString * contactNumber;
static NSString * clientName;
static UIColor  * primaryColor;
static UIColor  * secondaryColor;
static UIColor  * backGroundColor;
static NSString * facebookLink;
static NSString * linkedInLink;
static NSString * twitterLink;
static NSString * compnayWebsiteLink;
static NSString * contactUsLink;
static NSString * newsLetterLink;
static NSString * contactMailID;
static NSString * youTubeLink;
static NSString * youTubeID;
static NSString * copyRight;
static UIImage * logoImage;
static bool isActiveSubscription;


#pragma mark - Getter Methods

+(NSString*)getClientID{
    return clientID;
}

+(NSString*)getAddress{
    return address;
}

+(NSString*)getCompanyName{
    return companyName;
}

+(NSString*)getContactNumber{
    return contactNumber;
}

+(NSString*)getClientName{
    return clientName;
}

+(UIColor*)getThemeColor{
    return primaryColor;
}
+(UIColor*)getHighlightedThemeColor{
    return secondaryColor;
}
+(UIColor*)getBackGroundColor{
    return backGroundColor;
}
+(NSString*)getCompanyFacebookLink{
    return facebookLink;
}
+(NSString*)getCompanyLinkedInLink{
    return linkedInLink;
}
+(NSString*)getCompanyTwitterLink{
    return twitterLink;
}
+(NSString*)getCompanyLink{
    return compnayWebsiteLink;
}
+(NSString*)getContactUsLink{
    return contactUsLink;
}
+(NSString*)getCompanyNewsLetterLink{
    return newsLetterLink;
}
+(NSString*)getContactUsMailID{
    return contactMailID;
}

+(NSString*)getYouTubeLink{
    return youTubeLink;
}

+(NSString*)getCopyRight{
    return copyRight;
}

+(NSString*)getYouTubeID{
    return youTubeID;
}

+(UIImage*)getLogoImage{
    return logoImage;
}

+(BOOL)isSubscriptionActive{
    return isActiveSubscription;
}

#pragma mark - Setter Methods

+(void)setClientID:(NSString*)ID{
    if (clientID != ID)
    {
        clientID = [ID copy];
    }
}
+(void)setAddress:(NSString*)addr{
    if (address != addr)
    {
        address = [addr copy];
    }
}
+(void)setCompanyName:(NSString*)company{
    if (companyName != company)
    {
        companyName = [company copy];
    }
}
+(void)setClientName:(NSString*)name{
    if (clientName != name)
    {
        clientName = [name copy];
    }
}
+(void)setContactNumber:(NSString*)number{
    if (contactNumber != number)
    {
        contactNumber = [number copy];
    }
}

+(void)setThemeColor:(UIColor*)color{
    if (primaryColor != color)
    {
        primaryColor = [color copy];
    }
}
+(void)setHighlightedThemeColor:(UIColor*)color{
    if (secondaryColor != color)
    {
        secondaryColor = [color copy];
    }
}
+(void)setBackGroundColor:(UIColor*)color{
    if (backGroundColor != color)
    {
        backGroundColor = [color copy];
    }
}
+(void)setCompanyFacebookLink:(NSString*)fbLink{
    if (facebookLink != fbLink)
    {
        facebookLink = [fbLink copy];
    }
}
+(void)setCompanyLinkedInLink:(NSString*)lnLink{
    if (linkedInLink != lnLink)
    {
        linkedInLink = [lnLink copy];
    }
}
+(void)setCompanyTwitterLink:(NSString*)twLink{
    if (twitterLink != twLink)
    {
        twitterLink = [twLink copy];
    }
}
+(void)setCompanyLink:(NSString*)companyLink{
    if (compnayWebsiteLink != companyLink)
    {
        compnayWebsiteLink = [companyLink copy];
    }
}
+(void)setContactUsLink:(NSString*)contactPageLink{
    if (contactUsLink != contactPageLink)
    {
        contactUsLink = [contactPageLink copy];
    }
}
+(void)setCompanyNewsLetterLink:(NSString*)newsLetterPageLink{
    if (newsLetterLink != newsLetterPageLink)
    {
        newsLetterLink = [newsLetterPageLink copy];
    }
}
+(void)setContactUsMailID:(NSString*)contactUsMailID{
    if (contactMailID != contactUsMailID)
    {
        contactMailID = [contactUsMailID copy];
    }
}

+(void)setYouTubeLink:(NSString*)youTubePageLink{
    if (youTubeLink != youTubePageLink)
    {
        youTubeLink = [youTubePageLink copy];
    }
}

+(void)setCopyRight:(NSString*)copyRightText{
    if (copyRight != copyRightText)
    {
        copyRight = [copyRightText copy];
    }
}

+(void)setYouTubeID:(NSString*)youTubeVideoID{
    if (youTubeID != youTubeVideoID)
    {
        youTubeID = [youTubeVideoID copy];
    }
}

+(void)setLogoImage:(UIImage*)imgLogo{
    if (logoImage != imgLogo)
    {
        logoImage = [imgLogo copy];
    }
}

+(void)setSubscriptionStatus:(BOOL)isActive{
    if (isActiveSubscription != isActive)
    {
        isActiveSubscription = isActive;
    }
}

@end
