//
//  UserDM.h
//  EGA
//
//  Created by Soniya on 28/11/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//


/*
CREATE TABLE "tbl_User" (user_id VARCHAR, email VARCHAR, pwd VARCHAR, firstname VARCHAR, lastname VARCHAR, phone VARCHAR, dob VARCHAR, gender VARCHAR, bloodgroup VARCHAR, weight VARCHAR, height VARCHAR, current_illness VARCHAR, current_medication VARCHAR, surgical_procedure VARCHAR, drinking_habits VARCHAR, drugs_intake VARCHAR, city_id VARCHAR, state_id VARCHAR, country_id VARCHAR, photopath VARCHAR, user_isactive VARCHAR)
 */

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "UserOM.h"

@interface UserDM : NSObject

{
    NSString *dataBasepath;
    AppDelegate *appDelegate;
    
}

-(BOOL)insertUser:(NSDictionary *)dictUser;

- (BOOL)updateProfile:(NSDictionary*)dictUser OfUser:(NSString*)userid;

- (BOOL)updateDOB:(NSString*)dob OfUser:(NSString*)userid;

- (BOOL)updateHeight:(NSString*)height Weight:(NSString *)weight Gender:(NSString *)gender OfUser:(NSString *)userID;

-(BOOL)deleteUser:(NSString*)userID;

-(void)deleteAllUserAndTheirData;

- (UserOM *)selectUserOfID:(NSString*)userID;

-(BOOL)isUserExistWithUserID:(NSString*)userid;

@end
