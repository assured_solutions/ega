//
//  MyDietPlanDM.h
//  EGA
//
//  Created by animesh on 12/7/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface MyDietPlanDM : NSObject

{
    NSString *dataBasepath;
    AppDelegate *appDelegate;
    
}

@property(nonatomic,retain)NSString *dataBasepath;

-(NSMutableArray*)GetMyDietPlans;
- (BOOL)insertMyDietPlans:(NSArray *)arrMyDietPlans;
-(BOOL)updateTableOFMyDietPlanBreakFastDetails:(NSString *)strStartDate andbreakFastID:(NSString *)strBreakFastID anduserID:(NSString *)strUSerID;
-(BOOL)updateTableOFMyDietPlanLunchDetails:(NSString *)strStartDate andLunchID:(NSString *)strLunchID anduserID:(NSString *)strUSerID;
-(BOOL)updateTableOFMyDietPlanDinnerDetails:(NSString *)strStartDate andDinnerID:(NSString *)strDinnerID anduserID:(NSString *)strUSerID;
-(BOOL)deleteDietPlan:(NSString*)userID;

@end
