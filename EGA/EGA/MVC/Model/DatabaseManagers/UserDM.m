//
//  UserDM.m
//  EGA
//
//  Created by Soniya on 28/11/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "UserDM.h"


@implementation UserDM
- (id)init
{
    self = [super init];
    if (self != nil)
    {
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        dataBasepath = [appDelegate getDBPath];
        dataBasepath = [SupportingClass getDocumentPathForComponent:DBNAME];
    }
    return self;
}


-(BOOL)insertUser:(NSDictionary *)dictUser{
    BOOL returnValue = YES;
    sqlite3_stmt *selectStmt = nil;
    sqlite3_stmt *insertStmt = nil;
    sqlite3_stmt *updateStmt = nil;
    sqlite3 *database = nil;
    
    if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK)
    {
        returnValue = NO;
    }
    
    // to enable the cache memory
    int ret = SQLITE_OPEN_SHAREDCACHE;
    
    if(ret != SQLITE_OK)
    {
        
    }
    
    const char *filePath=[dataBasepath UTF8String];
    int SqlCode=sqlite3_open(filePath, &database);
    if (SqlCode == SQLITE_OK)
    {
        //Preapare Select Statment
        if(selectStmt == nil)
        {
            NSString *strValue = [NSString stringWithFormat:@"SELECT rowid FROM tbl_User where user_id = ?"];
            const char *SELECTsql = [strValue UTF8String];
            
            if(sqlite3_prepare_v2(database, SELECTsql, -1, &selectStmt, NULL) != SQLITE_OK)
            {
                NSLog(@"Error while creating Select statement for '%s' on table tbl_User.", sqlite3_errmsg(database));
                returnValue = NO;
            }
        }
        //Preapare Update Statment
        if(updateStmt == nil)
        {
            NSString *strValue = [NSString stringWithFormat:@"UPDATE tbl_User SET email=?,pwd=?,firstname=?,lastname=?,phone=?,dob=?,gender=?,bloodgroup=?,weight=?,height=?,current_illness=?,current_medication=?,surgical_procedure=?,drinking_habits=?,drugs_intake=?,city_id=?,state_id=?,country_id=?,photopath=?,user_isactive=? where user_id=?"];
            
            const char *UPDATEsql = [strValue UTF8String];
            
            if(sqlite3_prepare_v2(database, UPDATEsql, -1, &updateStmt, NULL) != SQLITE_OK)
            {
                NSLog(@"Error while UPDATE '%s' prepare statement. table tbl_User", sqlite3_errmsg(database));
                returnValue = NO;
            }
        }
        
        //Preapare Insert Statment
        if(insertStmt == nil)
        {
            NSString *aString =[NSString stringWithFormat:@"INSERT INTO tbl_User ('user_id','email','pwd','firstname','lastname','phone','dob','gender','bloodgroup','weight','height','current_illness','current_medication','surgical_procedure','drinking_habits','drugs_intake','city_id','state_id','country_id','photopath','user_isactive') Values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"];
            const char *sql = [aString UTF8String];
            
            if(sqlite3_prepare_v2(database, sql, -1, &insertStmt, NULL) != SQLITE_OK)
            {
                NSLog(@"Error while creating insert '%s' add statement. table tbl_User", sqlite3_errmsg(database));
                returnValue = NO;
            }
        }
        
        NSInteger rowidentifier = -1;
        
        
        if(sqlite3_bind_text(selectStmt, 1, [[dictUser objectForKey:@"user_id"] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
            returnValue= NO;
        }
        
        rowidentifier = -1;
        
        // Check whether particular id exists or not
        
        while(sqlite3_step(selectStmt) == SQLITE_ROW)
        {
            rowidentifier = sqlite3_column_int(selectStmt, 0);
        }
        sqlite3_reset(selectStmt);
        
        
               if(rowidentifier == -1)
        {
            if(sqlite3_bind_text(insertStmt, 1,[[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"user_id"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            if(sqlite3_bind_text(insertStmt, 2, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"emailid"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            if(sqlite3_bind_text(insertStmt, 3, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"password"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            if(sqlite3_bind_text(insertStmt, 4, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"firstname"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            if(sqlite3_bind_text(insertStmt, 5,[[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"lastname"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            if(sqlite3_bind_text(insertStmt, 6, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"phone"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            if(sqlite3_bind_text(insertStmt, 7, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"dob"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            if(sqlite3_bind_text(insertStmt, 8, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"gender"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            if(sqlite3_bind_text(insertStmt, 9, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"bloodgroup"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            if(sqlite3_bind_text(insertStmt, 10, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"weight"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            if(sqlite3_bind_text(insertStmt, 11, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"height"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            if(sqlite3_bind_text(insertStmt, 12,[[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"current_illness"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            if(sqlite3_bind_text(insertStmt, 13, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"current_medication"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            if(sqlite3_bind_text(insertStmt, 14, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"surgical_procedure"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            if(sqlite3_bind_text(insertStmt, 15, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"drinking_habits"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            if(sqlite3_bind_text(insertStmt, 16, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"drugs_intake"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            if(sqlite3_bind_text(insertStmt, 17, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"city_id"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            if(sqlite3_bind_text(insertStmt, 18, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"state_id"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            if(sqlite3_bind_text(insertStmt, 19, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"country_id"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            if(sqlite3_bind_text(insertStmt, 20, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"photoid"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            if(sqlite3_bind_text(insertStmt, 21, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"user_isactive"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }

            if(SQLITE_DONE != sqlite3_step(insertStmt))
            {
                NSLog(@"Error while inserting into table tbl_User : '%s'", sqlite3_errmsg(database));
                returnValue = NO;
            }
            sqlite3_reset(insertStmt);
        }
        // If id exists then update the record.
        else
        {
            
            if(sqlite3_bind_text(updateStmt, 1, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"emailid"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            if(sqlite3_bind_text(updateStmt, 2, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"password"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            if(sqlite3_bind_text(updateStmt, 3, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"firstname"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            if(sqlite3_bind_text(updateStmt, 4,[[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"lastname"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            if(sqlite3_bind_text(updateStmt, 5, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"phone"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            if(sqlite3_bind_text(updateStmt, 6, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"dob"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            if(sqlite3_bind_text(updateStmt, 7, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"gender"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            if(sqlite3_bind_text(updateStmt, 8, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"bloodgroup"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            if(sqlite3_bind_text(updateStmt, 9, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"weight"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            if(sqlite3_bind_text(updateStmt, 10, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"height"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            if(sqlite3_bind_text(updateStmt, 11,[[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"current_illness"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            if(sqlite3_bind_text(updateStmt, 12, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"current_medication"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            if(sqlite3_bind_text(updateStmt, 13, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"surgical_procedure"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            if(sqlite3_bind_text(updateStmt, 14, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"drinking_habits"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            if(sqlite3_bind_text(updateStmt, 15, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"drugs_intake"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            if(sqlite3_bind_text(updateStmt, 16, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"city_id"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            if(sqlite3_bind_text(updateStmt, 17, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"state_id"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            if(sqlite3_bind_text(updateStmt, 18, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"country_id"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            if(sqlite3_bind_text(updateStmt, 19, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"photoid"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            if(sqlite3_bind_text(updateStmt, 20, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"user_isactive"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            if(sqlite3_bind_text(updateStmt, 21,[[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"user_id"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            if(SQLITE_DONE != sqlite3_step(updateStmt))
            {
                NSLog(@"Error while updating into table tbl_User : '%s'", sqlite3_errmsg(database));
                returnValue = NO;
            }
            sqlite3_reset(updateStmt);
        }
        //       }
      
        
    }
    if ([dictUser objectForKey:@"photoid"]) {
        NSString *imgUrl = [dictUser objectForKey:@"photoid"];
        NSData *imgData = [NSData dataWithContentsOfURL:[NSURL URLWithString:imgUrl]];
        if (imgData!=nil) {
            UIImage *img = [UIImage imageWithData:imgData];
            NSString *strProfilePicName = [NSString stringWithFormat:@"%@ProfilePic.png",[dictUser objectForKey:@"user_id"]];
            [SupportingClass saveImageInDocumentFolder:img FolderName:@"ProfilePics" FileName:strProfilePicName];
             img =nil;
        }
     
       
        imgData =nil;
        imgUrl =nil;
        filePath=nil;

    }
      if (insertStmt)
    {
        sqlite3_finalize(insertStmt);
        insertStmt = nil;
    }
    
    if (updateStmt)
    {
        sqlite3_finalize(updateStmt);
        updateStmt = nil;
    }
    
    if (selectStmt)
    {
        sqlite3_finalize(selectStmt);
        selectStmt = nil;
    }
    
    sqlite3_close(database);
    database = nil;
    
    return returnValue;
}

#pragma mark - ====Update====
/*
 @Description : This method update dob of User
 @author : Soniya Vishwakarma
 Parameter: User ID and DOB
 @return: BOOL signify success of the query
 */

- (BOOL)updateProfile:(NSDictionary*)dictUser OfUser:(NSString*)userid
{
    BOOL returnValue = YES;
    sqlite3_stmt *updateStmt = nil;
    sqlite3 *database = nil;
    
    if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK)
    {
        returnValue = NO;
    }
    
    // to enable the cache memory
    int ret = SQLITE_OPEN_SHAREDCACHE;
    
    if(ret != SQLITE_OK)
    {
        
    }
    
    const char *filePath=[dataBasepath UTF8String];
    if (sqlite3_open(filePath, &database) == SQLITE_OK)
    {
        
        //Preapare Update Statment
        if(updateStmt == nil)
        {
            NSString *strValue = [NSString stringWithFormat:@"UPDATE tbl_User SET email=?,pwd=?,firstname=?,lastname=?,phone=?,dob=?,gender=?,bloodgroup=?,weight=?,height=?,current_illness=?,current_medication=?,surgical_procedure=?,drinking_habits=?,drugs_intake=?,city_id=?,state_id=?,country_id=?,photopath=?,user_isactive=? where user_id=?"];
            
            const char *UPDATEsql = [strValue UTF8String];
            
            if(sqlite3_prepare_v2(database, UPDATEsql, -1, &updateStmt, NULL) != SQLITE_OK)
            {
                NSLog(@"Error while UPDATE '%s' prepare statement. table tbl_User", sqlite3_errmsg(database));
                returnValue = NO;
            }
        }
        
        
        if(sqlite3_bind_text(updateStmt, 1, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"emailid"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
            returnValue = NO;
        }
        if(sqlite3_bind_text(updateStmt, 2, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"password"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
            returnValue = NO;
        }
        if(sqlite3_bind_text(updateStmt, 3, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"firstname"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
            returnValue = NO;
        }
        if(sqlite3_bind_text(updateStmt, 4,[[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"lastname"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
            returnValue = NO;
        }
        if(sqlite3_bind_text(updateStmt, 5, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"phone"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
            returnValue = NO;
        }
        if(sqlite3_bind_text(updateStmt, 6, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"dob"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
            returnValue = NO;
        }
        if(sqlite3_bind_text(updateStmt, 7, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"gender"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
            returnValue = NO;
        }
        if(sqlite3_bind_text(updateStmt, 8, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"bloodgroup"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
            returnValue = NO;
        }
        if(sqlite3_bind_text(updateStmt, 9, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"weight"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
            returnValue = NO;
        }
        if(sqlite3_bind_text(updateStmt, 10, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"height"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
            returnValue = NO;
        }
        if(sqlite3_bind_text(updateStmt, 11,[[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"current_illness"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
            returnValue = NO;
        }
        if(sqlite3_bind_text(updateStmt, 12, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"current_medication"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
            returnValue = NO;
        }
        if(sqlite3_bind_text(updateStmt, 13, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"surgical_procedure"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
            returnValue = NO;
        }
        if(sqlite3_bind_text(updateStmt, 14, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"drinking_habits"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
            returnValue = NO;
        }
        if(sqlite3_bind_text(updateStmt, 15, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"drugs_intake"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
            returnValue = NO;
        }
        if(sqlite3_bind_text(updateStmt, 16, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"city_id"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
            returnValue = NO;
        }
        if(sqlite3_bind_text(updateStmt, 17, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"state_id"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
            returnValue = NO;
        }
        if(sqlite3_bind_text(updateStmt, 18, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"country_id"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
            returnValue = NO;
        }
        if(sqlite3_bind_text(updateStmt, 19, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"photoid"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
            returnValue = NO;
        }
        if(sqlite3_bind_text(updateStmt, 20, [[NSString stringWithFormat:@"%@",[dictUser objectForKey:@"user_isactive"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
            returnValue = NO;
        }
        if(sqlite3_bind_text(updateStmt, 21,[userid UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
            returnValue = NO;
        }
        if(SQLITE_DONE != sqlite3_step(updateStmt))
        {
            NSLog(@"Error while updating into table tbl_User : '%s'", sqlite3_errmsg(database));
            returnValue = NO;
        }
        sqlite3_reset(updateStmt);
    }
    filePath=nil;
    if (updateStmt)
    {
        sqlite3_finalize(updateStmt);
        updateStmt = nil;
    }
    
    
    sqlite3_close(database);
    database = nil;
    
    return returnValue;
    
    
}

/*
 @Description : This method update dob of User
 @author : Soniya Vishwakarma
 Parameter: User ID and DOB
 @return: BOOL signify success of the query
 */

- (BOOL)updateDOB:(NSString*)dob OfUser:(NSString*)userid
{
    BOOL returnValue = YES;
    sqlite3_stmt *updateStmt = nil;
    sqlite3 *database = nil;
    
    if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK)
    {
        returnValue = NO;
    }
    
    // to enable the cache memory
    int ret = SQLITE_OPEN_SHAREDCACHE;
    
    if(ret != SQLITE_OK)
    {
        
    }
    
    const char *filePath=[dataBasepath UTF8String];
    if (sqlite3_open(filePath, &database) == SQLITE_OK)
    {
        
        //Preapare Update Statment
        if(updateStmt == nil)
        {
            NSString *strValue = [NSString stringWithFormat:@"UPDATE tbl_User SET dob=? where user_id=?"];
            
            const char *UPDATEsql = [strValue UTF8String];
            
            if(sqlite3_prepare_v2(database, UPDATEsql, -1, &updateStmt, NULL) != SQLITE_OK)
            {
                NSLog(@"Error while UPDATE '%s' prepare statement. table tbl_User", sqlite3_errmsg(database));
                returnValue = NO;
            }
        }
        
        
        if(sqlite3_bind_text(updateStmt, 1, [dob UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
            returnValue = NO;
        }
        if(sqlite3_bind_text(updateStmt, 2, [userid UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
            returnValue = NO;
        }
        if(SQLITE_DONE != sqlite3_step(updateStmt))
        {
            NSLog(@"Error while updating into table tbl_User : '%s'", sqlite3_errmsg(database));
            returnValue = NO;
        }
        sqlite3_reset(updateStmt);
    }
    filePath=nil;
    if (updateStmt)
    {
        sqlite3_finalize(updateStmt);
        updateStmt = nil;
    }
    
    
    sqlite3_close(database);
    database = nil;
    
    return returnValue;
    
    
}
/*
 @Description : This method update dob of User
 @author : Soniya Vishwakarma
 Parameter: User ID and DOB
 @return: BOOL signify success of the query
 */

- (BOOL)updateHeight:(NSString*)height Weight:(NSString *)weight Gender:(NSString *)gender OfUser:(NSString *)userID
{
    BOOL returnValue = YES;
    sqlite3_stmt *updateStmt = nil;
    sqlite3 *database = nil;
    
    if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK)
    {
        returnValue = NO;
    }
    
    // to enable the cache memory
    int ret = SQLITE_OPEN_SHAREDCACHE;
    
    if(ret != SQLITE_OK)
    {
        
    }
   
    
    const char *filePath=[dataBasepath UTF8String];
    if (sqlite3_open(filePath, &database) == SQLITE_OK)
    {
        
        //Preapare Update Statment
        if(updateStmt == nil)
        {
            NSString *strValue = [NSString stringWithFormat:@"UPDATE tbl_User SET height=?,weight=?,gender=? where user_id=?"];
            
            const char *UPDATEsql = [strValue UTF8String];
            
            if(sqlite3_prepare_v2(database, UPDATEsql, -1, &updateStmt, NULL) != SQLITE_OK)
            {
                NSLog(@"Error while UPDATE '%s' prepare statement. table tbl_User", sqlite3_errmsg(database));
                returnValue = NO;
            }
        }
        
        
        if(sqlite3_bind_text(updateStmt, 1, [height UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
            returnValue = NO;
        }
        if(sqlite3_bind_text(updateStmt, 2, [weight UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
            returnValue = NO;
        } if(sqlite3_bind_text(updateStmt, 3, [gender UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
            returnValue = NO;
        }
        if(sqlite3_bind_text(updateStmt, 4, [userID UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
            returnValue = NO;
        }
        if(SQLITE_DONE != sqlite3_step(updateStmt))
        {
            NSLog(@"Error while updating into table tbl_User : '%s'", sqlite3_errmsg(database));
            returnValue = NO;
        }
        sqlite3_reset(updateStmt);
    }
    filePath=nil;
    if (updateStmt)
    {
        sqlite3_finalize(updateStmt);
        updateStmt = nil;
    }
    
    
    sqlite3_close(database);
    database = nil;
    
    return returnValue;
    
    
}


#pragma mark - === Delete ===
/*
 @Description : This method is used to delete Data of particular user.
 @author :  Soniya Vishwakarma
 @param :
 Parameter1 user ID
 @return: Bool value to signify the success of the Query
 */

-(BOOL)deleteUser:(NSString*)userID{
    BOOL returnValue = YES;
    sqlite3_stmt *deleteStmt = nil;
    sqlite3 *database = nil;
    
    if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK)
    {
        returnValue = NO;
    }
    
    // to enable the cache memory
    int ret = SQLITE_OPEN_SHAREDCACHE;
    
    if(ret != SQLITE_OK)
    {
        
    }
    
    const char *filePath=[dataBasepath UTF8String];
    int SqlCode=sqlite3_open(filePath, &database);
    if (SqlCode == SQLITE_OK)
    {
        
        //Preapare Update Statment
        if(deleteStmt == nil)
        {
            NSString *strValue = @"Delete from tbl_User where user_id = ?";
            
            const char *DELETEsql = [strValue UTF8String];
            if(sqlite3_prepare_v2(database, DELETEsql, -1, &deleteStmt, NULL) != SQLITE_OK)
            {
                NSLog(@"Error while Delete '%s' prepare statement. table tbl_User", sqlite3_errmsg(database));
                returnValue = NO;
            }
        }
        if(sqlite3_bind_text(deleteStmt, 1, [userID UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
            returnValue = NO;
        }
        
        if(SQLITE_DONE != sqlite3_step(deleteStmt))
        {
            NSLog(@"Error while deleting into table tbl_User : '%s'", sqlite3_errmsg(database));
            returnValue = NO;
        }
        sqlite3_reset(deleteStmt);
    }
    
    if (deleteStmt)
    {
        sqlite3_finalize(deleteStmt);
        deleteStmt = nil;
    }
    sqlite3_close(database);
    database = nil;
    
    return returnValue;
}

/*
 @Description : This method is used to delete Data all user.
 @author :  Soniya Vishwakarma
 @return: Bool value to signify the success of the Query
 */

-(void)deleteAllUserAndTheirData{
  
    sqlite3_stmt *selectStmt = nil;
    sqlite3 *database = nil;
    NSMutableArray *arrTableName = nil;
    
    if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK) {
        NSLog(@"Can now use sqlite on multiple threads, using the same connection");
    }
    
    // to enable the cache memory
    int ret = SQLITE_OPEN_SHAREDCACHE;
    
    if(ret != SQLITE_OK)
    {
        
    }
    const char *filePath=[dataBasepath UTF8String];
    int SqlCode=sqlite3_open(filePath, &database);
    if (SqlCode == SQLITE_OK)
    {
        if(selectStmt == nil)
        {
            NSString *strquery;
            
            strquery = @"SELECT name FROM sqlite_master WHERE type='table'";
            
            const char *SELECTsql = [strquery UTF8String];
            
            if(sqlite3_prepare_v2(database, SELECTsql, -1, &selectStmt, NULL) != SQLITE_OK)
            {
                NSLog(@"Error while creating Select statement '%s' .", sqlite3_errmsg(database));
            }
            else  {
                arrTableName=[[NSMutableArray alloc]init];
                while(sqlite3_step(selectStmt) == SQLITE_ROW){
                    
                    
                    NSString *tableName=@"";
                    if(sqlite3_column_text(selectStmt, 0)!=nil)
                        tableName= [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStmt, 0)];
                    
                    [arrTableName addObject:tableName];
                }
                
                [self deleteFromAllTable:arrTableName];
            }
        }
    }
    sqlite3_reset(selectStmt);
    filePath=nil;
    if (selectStmt) {
        sqlite3_finalize(selectStmt);
        selectStmt = nil;
    }    sqlite3_close(database);
    database = nil;
}
-(void)deleteFromAllTable:(NSArray *)arrTableName{
    BOOL returnValue = YES;
    sqlite3_stmt *deleteStmt = nil;
    sqlite3 *database = nil;
    
    if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK)
    {
        returnValue = NO;
    }
    
    // to enable the cache memory
    int ret = SQLITE_OPEN_SHAREDCACHE;
    
    if(ret != SQLITE_OK)
    {
        
    }
    
    const char *filePath=[dataBasepath UTF8String];
    int SqlCode=sqlite3_open(filePath, &database);
    if (SqlCode == SQLITE_OK)
    {
          for (NSString *tblName in arrTableName) {
        //Preapare Update Statment
        if(deleteStmt == nil)
        {
            NSString *strValue = [NSString stringWithFormat:@"Delete from %@",tblName];
            
            const char *DELETEsql = [strValue UTF8String];
            if(sqlite3_prepare_v2(database, DELETEsql, -1, &deleteStmt, NULL) != SQLITE_OK)
            {
                NSLog(@"Error while Delete '%s' prepare statement.", sqlite3_errmsg(database));
                returnValue = NO;
            }
        }
            
            if(SQLITE_DONE != sqlite3_step(deleteStmt))
            {
                NSLog(@"Error while deleting into table : '%s'", sqlite3_errmsg(database));
                returnValue = NO;
            }
            sqlite3_reset(deleteStmt);
              if (deleteStmt)
              {
                  sqlite3_finalize(deleteStmt);
                  deleteStmt = nil;
              }

        }
            }
    
   
    sqlite3_close(database);
    database = nil;
  
}
#pragma mark - === Select ===

/*
 @Description : This method is used to select records from the table for the particular user.
 @author :  Soniya Vishwakarma
 @parameter : user ID whose data to be retrieve
 @return: Object with user Profile
 */

- (UserOM *)selectUserOfID:(NSString*)userID
{
    sqlite3_stmt *selectStmt = nil;
    sqlite3 *database = nil;
    UserOM *objUserProfile = nil;
    
    if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK) {
        NSLog(@"Can now use sqlite on multiple threads, using the same connection");
    }
    
    // to enable the cache memory
    int ret = SQLITE_OPEN_SHAREDCACHE;
    
    if(ret != SQLITE_OK)
    {
        
    }
    const char *filePath=[dataBasepath UTF8String];
    int SqlCode=sqlite3_open(filePath, &database);
    if (SqlCode == SQLITE_OK)
    {
        if(selectStmt == nil)
        {
            NSString *strquery;
            
            strquery = [NSString stringWithFormat:@"SELECT user_id,email,pwd,firstname,lastname,phone,dob,gender,bloodgroup,weight,height,current_illness,current_medication,surgical_procedure,drinking_habits,drugs_intake,city_id,state_id,country_id,photopath,user_isactive FROM tbl_User where user_id=?"];
            
            const char *SELECTsql = [strquery UTF8String];
            
            if(sqlite3_prepare_v2(database, SELECTsql, -1, &selectStmt, NULL) != SQLITE_OK)
            {
                NSLog(@"Error while creating Select statement for tbl_User '%s' .", sqlite3_errmsg(database));
            }
            else  {
                
                sqlite3_bind_text(selectStmt, 1, [userID UTF8String], -1, SQLITE_TRANSIENT);
                while(sqlite3_step(selectStmt) == SQLITE_ROW){
                    
                    objUserProfile=[[UserOM alloc]init];
                    
                    if(sqlite3_column_text(selectStmt, 0)!=nil)
                        objUserProfile.str_userID= [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStmt, 0)];
                    
                    if(sqlite3_column_text(selectStmt, 1)!=nil)
                        objUserProfile.str_email= [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStmt, 1)];
                    
                    if(sqlite3_column_text(selectStmt, 2)!=nil)
                        objUserProfile.str_pwd= [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStmt, 2)];
                    
                    if(sqlite3_column_text(selectStmt, 3)!=nil)
                        objUserProfile.str_firstname= [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStmt, 3)];
                    
                    if(sqlite3_column_text(selectStmt, 4)!=nil)
                        objUserProfile.str_lastname= [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStmt, 4)];
                    
                    if(sqlite3_column_text(selectStmt, 5)!=nil)
                        objUserProfile.str_phone= [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStmt, 5)];
                    
                    if(sqlite3_column_text(selectStmt, 6)!=nil)
                        objUserProfile.str_dob= [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStmt, 6)];
                    
                    if(sqlite3_column_text(selectStmt, 7)!=nil)
                        objUserProfile.str_gender= [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStmt, 7)];
                    
                    if(sqlite3_column_text(selectStmt, 8)!=nil)
                        objUserProfile.str_bloodgroup= [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStmt, 8)];
                    
                    if(sqlite3_column_text(selectStmt, 9)!=nil)
                        objUserProfile.str_weight= [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStmt, 9)];
                    
                    if(sqlite3_column_text(selectStmt, 10)!=nil)
                        objUserProfile.str_height= [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStmt, 10)];
                    
                    
                    if(sqlite3_column_text(selectStmt, 11)!=nil)
                        objUserProfile.str_current_illness= [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStmt, 11)];
                    
                    if(sqlite3_column_text(selectStmt, 12)!=nil)
                        objUserProfile.str_current_medication= [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStmt, 12)];
                    
                    if(sqlite3_column_text(selectStmt, 13)!=nil)
                        objUserProfile.str_surgical_procedure= [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStmt, 13)];
                    
                    if(sqlite3_column_text(selectStmt, 14)!=nil)
                        objUserProfile.str_drinking_habits= [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStmt, 14)];
                    
                    if(sqlite3_column_text(selectStmt, 15)!=nil)
                        objUserProfile.str_drugs_intake= [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStmt, 15)];
                    
                    if(sqlite3_column_text(selectStmt, 16)!=nil)
                        objUserProfile.str_city_id= [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStmt, 16)];
                  
                    if(sqlite3_column_text(selectStmt, 17)!=nil)
                        objUserProfile.str_state_id= [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStmt, 17)];
                    
                    if(sqlite3_column_text(selectStmt, 18)!=nil)
                        objUserProfile.str_country_id= [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStmt, 14)];
                    
                    if(sqlite3_column_text(selectStmt, 19)!=nil)
                        objUserProfile.str_photo= [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStmt, 15)];
                    
                    if(sqlite3_column_text(selectStmt, 20)!=nil)
                        objUserProfile.str_isActive= [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStmt, 15)];
                }
            }
        }
    }
    sqlite3_reset(selectStmt);
    filePath=nil;
    if (selectStmt) {
        sqlite3_finalize(selectStmt);
        selectStmt = nil;
    }    sqlite3_close(database);
    database = nil;
    return objUserProfile;
    
}
/*
 @Description : This method is used to check existence of the user
 @author :  Soniya Vishwakarma
 @parameter : Login ID corresponding to which data will be authenticate
 @return: userID
 */


-(BOOL)isUserExistWithUserID:(NSString*)userid
{
    BOOL returnValue = YES;
    sqlite3_stmt *selectStmt = nil;
    sqlite3 *database = nil;
    
    if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK)
    {
        returnValue = NO;
    }
    
    // to enable the cache memory
    int ret = SQLITE_OPEN_SHAREDCACHE;
    
    if(ret != SQLITE_OK)
    {
        
    }
    
    const char *filePath=[dataBasepath UTF8String];
    int SqlCode=sqlite3_open(filePath, &database);
    if (SqlCode == SQLITE_OK)
    {
        //Preapare Select Statment
        if(selectStmt == nil)
        {
            NSString *strValue = [NSString stringWithFormat:@"SELECT rowid FROM tbl_User where user_id = ?"];
            const char *SELECTsql = [strValue UTF8String];
            
            if(sqlite3_prepare_v2(database, SELECTsql, -1, &selectStmt, NULL) != SQLITE_OK)
            {
                NSLog(@"Error while creating Select statement for '%s' on table tbl_User.", sqlite3_errmsg(database));
                returnValue = NO;
            }
        }
        
        NSInteger rowidentifier = -1;
        
        if(sqlite3_bind_text(selectStmt, 1, [userid UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
            returnValue= NO;
        }
        
        // Check whether Option of particular id exists or not
        
        while(sqlite3_step(selectStmt) == SQLITE_ROW)
        {
            rowidentifier = sqlite3_column_int(selectStmt, 0);
        }
        sqlite3_reset(selectStmt);
        
        
        
        if(rowidentifier == -1)
        {
            returnValue=NO;
        }
        // If Option id exists
        else
        {
            returnValue=YES;
        }
        
    }
    filePath=nil;
    if (selectStmt)
    {
        sqlite3_finalize(selectStmt);
        selectStmt = nil;
    }
    
    sqlite3_close(database);
    database = nil;
    
    return returnValue;
   }

@end
