//
//  MyDietPlanDM.m
//  EGA
//
//  Created by animesh on 12/7/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "MyDietPlanDM.h"

@implementation MyDietPlanDM

@synthesize dataBasepath;

- (id)init
{
    self = [super init];
    if (self != nil)
    {
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        self.dataBasepath = [appDelegate getDBPath];
        dataBasepath = [SupportingClass getDocumentPathForComponent:DBNAME];
    }
    return self;
}



- (BOOL)insertMyDietPlans:(NSArray *)arrMyDietPlans
{
    BOOL returnValue = YES;
    sqlite3_stmt *selectStmt = nil;
    sqlite3_stmt *insertStmt = nil;
    sqlite3 *database = nil;
    
    if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK)
    {
        returnValue = NO;
    }
    
    // to enable the cache memory
    int ret = SQLITE_OPEN_SHAREDCACHE;
    
    if(ret != SQLITE_OK)
    {
        
    }
    const char *filePath=[dataBasepath UTF8String];
    int SqlCode=sqlite3_open(filePath, &database);
    if (SqlCode == SQLITE_OK)
    {
        //Preapare Select Statment
        if(selectStmt == nil)
        {
            NSString *strValue = [NSString stringWithFormat:@"SELECT rowid FROM MyDietPlan where Startdate = ?"];
            const char *SELECTsql = [strValue UTF8String];
            int sql_code = sqlite3_prepare_v2(database, SELECTsql, -1, &selectStmt, NULL);
            SELECTsql=nil;
            if(sql_code != SQLITE_OK)
            {
                NSLog(@"Error while creating Select statement for '%s' on table MyDietPlan.", sqlite3_errmsg(database));
                returnValue = NO;
            }
            strValue=nil;
            SELECTsql=nil;
        }

        
       // SELECT validityDate,textBreakFast,isConsumedBreakFast,textLunch,isConsumedLunch,txtDinner,isConsumedDinner,Startdate,Enddate from MyDietPlan
        
        //Preapare Insert Statment
        if(insertStmt == nil)
        {
            NSString *strValue =[NSString stringWithFormat:@"INSERT INTO MyDietPlan ('validityDate','textBreakFast','isConsumedBreakFast','idBreakfast','textLunch','isConsumedLunch','idLunch','txtDinner','isConsumedDinner','idDinner','Startdate','Enddate','userid') Values(?,?,?,?,?,?,?,?,?,?,?,?,?)"];
            const char *insertSql = [strValue UTF8String];
            int sql_code =sqlite3_prepare_v2(database, insertSql, -1, &insertStmt, NULL);
            insertSql=nil;
            
            if(sql_code != SQLITE_OK)
            {
                NSLog(@"Error while creating insert '%s' add statement. table MyDietPlan", sqlite3_errmsg(database));
                returnValue = NO;
            }
        }
        
        
       
        for (NSDictionary *dictQuestion in arrMyDietPlans) {
             NSInteger rowidentifier = -1;
            if(sqlite3_bind_text(selectStmt, 1, [[dictQuestion objectForKey:@"Startdate"] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue= NO;
            }
            
            
            
//            if(sqlite3_bind_text(selectStmt, 3, [[dictQuestion objectForKey:@"idBreakfast"] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
//                returnValue= NO;
//            }
//            
//            if(sqlite3_bind_text(selectStmt, 4, [[dictQuestion objectForKey:@"idLunch"] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
//                returnValue= NO;
//            }
//            if(sqlite3_bind_text(selectStmt, 5, [[dictQuestion objectForKey:@"idDinner"] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
//                returnValue= NO;
//            }
            NSLog(@"%d",sqlite3_column_int(selectStmt, 0));
            
            // Check whether Record particular id exists or not
            
            while(sqlite3_step(selectStmt) == SQLITE_ROW)
            {
                rowidentifier = sqlite3_column_int(selectStmt, 0);
            }
            sqlite3_reset(selectStmt);
            sqlite3_clear_bindings(selectStmt);
            
            if(rowidentifier == -1)
            {
                
                
                
                if(sqlite3_bind_text(insertStmt, 1, [[NSString stringWithFormat:@"%@",[dictQuestion objectForKey:@"Enddate"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                    returnValue = NO;
                }
                if(sqlite3_bind_text(insertStmt, 2, [[NSString stringWithFormat:@"%@",[dictQuestion objectForKey:@"BreakfastText"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                    returnValue = NO;
                }
                if(sqlite3_bind_text(insertStmt, 3, [[NSString stringWithFormat:@"%@",[dictQuestion objectForKey:@"iSConsumedBreakFast"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                    returnValue = NO;
                }
                
                if(sqlite3_bind_text(insertStmt, 4, [[NSString stringWithFormat:@"%@",[dictQuestion objectForKey:@"idBreakfast"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                    returnValue = NO;
                }
                
                
                if(sqlite3_bind_text(insertStmt, 5, [[NSString stringWithFormat:@"%@",[dictQuestion objectForKey:@"LunchText"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                    returnValue = NO;
                }
                if(sqlite3_bind_text(insertStmt, 6, [[NSString stringWithFormat:@"%@",[dictQuestion objectForKey:@"iSConsumedLunch"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                    returnValue = NO;
                }
                
                if(sqlite3_bind_text(insertStmt, 7, [[NSString stringWithFormat:@"%@",[dictQuestion objectForKey:@"idLunch"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                    returnValue = NO;
                }
                
                
                
                if(sqlite3_bind_text(insertStmt, 8, [[NSString stringWithFormat:@"%@",[dictQuestion objectForKey:@"DinnerText"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                    returnValue = NO;
                }
                if(sqlite3_bind_text(insertStmt, 9, [[NSString stringWithFormat:@"%@",[dictQuestion objectForKey:@"iSConsumedDinner"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                    returnValue = NO;
                }
                
                if(sqlite3_bind_text(insertStmt, 10, [[NSString stringWithFormat:@"%@",[dictQuestion objectForKey:@"idDinner"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                    returnValue = NO;
                }
                
                if(sqlite3_bind_text(insertStmt, 11, [[NSString stringWithFormat:@"%@",[dictQuestion objectForKey:@"Startdate"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                    returnValue = NO;
                }

                if(sqlite3_bind_text(insertStmt, 12, [[NSString stringWithFormat:@"%@",[dictQuestion objectForKey:@"Enddate"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                    returnValue = NO;
                }
                if(sqlite3_bind_text(insertStmt, 13, [[NSString stringWithFormat:@"%@",appDelegate.userData.str_userID] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                    returnValue = NO;
                }

                
                if(SQLITE_DONE != sqlite3_step(insertStmt))
                {
                    NSLog(@"Error while inserting into table MyDietPlan : '%s'", sqlite3_errmsg(database));
                    returnValue = NO;
                }
                sqlite3_reset(insertStmt);
                sqlite3_clear_bindings(insertStmt);
            }

        }
    }
    filePath=nil;
    if (insertStmt)
    {
        sqlite3_finalize(insertStmt);
        insertStmt = nil;
    }
    
    
    if (selectStmt)
    {
        sqlite3_finalize(selectStmt);
        selectStmt = nil;
    }
    
    sqlite3_close(database);
    database = nil;
    
    return returnValue;
    
}




//
//- (BOOL)insertINDatabase:(NSArray *)arrDietPlans{
//    BOOL returnValue = YES;
//    sqlite3_stmt *insertStmt = nil;
//    sqlite3_stmt *selectStmt = nil;
//    sqlite3 *database = nil;
//    
//    if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK)
//    {
//        returnValue = NO;
//    }
//    
//    // to enable the cache memory
//    int ret = SQLITE_OPEN_SHAREDCACHE;
//    
//    if(ret != SQLITE_OK)
//    {
//        
//    }
//    
//    const char *filePath=[self.dataBasepath UTF8String];
//    int SqlCode=sqlite3_open(filePath, &database);
//    if (SqlCode == SQLITE_OK)
//    {
//        //Preapare Select Statment
//        if(selectStmt == nil)
//        {
//            
//            
//              NSString *strValue = [NSString stringWithFormat:@"SELECT rowid FROM MyDietPlan where Startdate = ? and userid=?"];
//            
////            NSString *strValue = [NSString stringWithFormat:@"SELECT rowid FROM tbl_Student_Test where Student_id = ? and Test_id=?"];
//            const char *SELECTsql = [strValue UTF8String];
//            
//            if(sqlite3_prepare_v2(database, SELECTsql, -1, &selectStmt, NULL) != SQLITE_OK)
//            {
//                NSLog(@"Error while creating Select statement for '%s' on table tbl_Student_Test.", sqlite3_errmsg(database));
//                returnValue = NO;
//            }
//        }
//        
//        
//        //Preapare Insert Statment
//        if(insertStmt == nil)
//        {
//            NSString *aString =[NSString stringWithFormat:@"INSERT INTO tbl_Student_Test ('Student_id','Test_id','isAttempted') Values(?,?,?)"];
//            const char *sql = [aString UTF8String];
//            
//            if(sqlite3_prepare_v2(database, sql, -1, &insertStmt, NULL) != SQLITE_OK)
//            {
//                NSLog(@"Error while creating insert '%s' add statement. table tbl_Student_Test", sqlite3_errmsg(database));
//                returnValue = NO;
//            }
//        }
//        
//        NSInteger rowidentifier = -1;
//        char *sqliteError=nil;
//        
//        sqlite3_exec(database, "BEGIN TRANSACTION", NULL, NULL, &sqliteError);
//        
//        for (int recordCount=0;recordCount<arrStudentTest.count;recordCount++) {
//            NSString *strTestID=@"";
//            if (sync==0) {
//                iCompetePackageTestObject *objPackageTest=[arrStudentTest objectAtIndex:recordCount];
//                strTestID=objPackageTest.str_Test_Id;
//            }
//            else
//            {
//                NSDictionary *dictPackageTest= [arrStudentTest objectAtIndex:recordCount];
//                
//                if (isMock) {
//                    strTestID=[dictPackageTest objectForKey:@"Mock_Test_id"];
//                }
//                else
//                {
//                    strTestID=[dictPackageTest objectForKey:@"Test_id"];
//                }
//            }
//            
//            if(sqlite3_bind_text(selectStmt, 1, [strStudentID UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
//                returnValue= NO;
//            }
//            
//            if(sqlite3_bind_text(selectStmt, 2, [strTestID UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
//                returnValue= NO;
//            }
//            
//            rowidentifier = -1;
//            
//            // Check whether Student Test of particular id exists or not
//            
//            while(sqlite3_step(selectStmt) == SQLITE_ROW)
//            {
//                rowidentifier = sqlite3_column_int(selectStmt, 0);
//            }
//            sqlite3_reset(selectStmt);
//            sqlite3_clear_bindings(selectStmt);
//            // If Student Test not exists then insert the record.
//            
//            if(rowidentifier == -1)
//            {
//                if(sqlite3_bind_text(insertStmt, 1,[strStudentID UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
//                    returnValue = NO;
//                }
//                if(sqlite3_bind_text(insertStmt, 2, [strTestID UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
//                    returnValue = NO;
//                }
//                if(sqlite3_bind_int(insertStmt, 3, sync) != SQLITE_OK) {
//                    returnValue= NO;
//                }
//                if(SQLITE_DONE != sqlite3_step(insertStmt))
//                {
//                    NSLog(@"Error while inserting into table tbl_Student_Test : '%s'", sqlite3_errmsg(database));
//                    returnValue = NO;
//                }
//                sqlite3_reset(insertStmt);
//                sqlite3_clear_bindings(insertStmt);
//                
//            }
//            
//            
//        }
//        sqlite3_exec(database, "END TRANSACTION", NULL, NULL, &sqliteError);
//    }
//    filePath=nil;
//    if (insertStmt)
//    {
//        sqlite3_finalize(insertStmt);
//        insertStmt = nil;
//    }
//    
//    
//    if (selectStmt)
//    {
//        sqlite3_finalize(selectStmt);
//        selectStmt = nil;
//    }
//    
//    sqlite3_close(database);
//    database = nil;
//    return returnValue;
//}


















-(BOOL)updateTableOFMyDietPlanBreakFastDetails:(NSString *)strStartDate andbreakFastID:(NSString *)strBreakFastID anduserID:(NSString *)strUSerID
{
    BOOL returnValue = YES;
    sqlite3_stmt *updateStmt = nil;
    sqlite3 *database = nil;
    
    if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK)
    {
        returnValue = NO;
    }
    
    // to enable the cache memory
    int ret = SQLITE_OPEN_SHAREDCACHE;
    
    if(ret != SQLITE_OK)
    {
        
    }
    
    const char *filePath=[self.dataBasepath UTF8String];
    int SqlCode=sqlite3_open(filePath, &database);
    if (SqlCode == SQLITE_OK)
    {
        //Preapare Update Statment
        if(updateStmt == nil)
        {
            NSString *strValue = [NSString stringWithFormat:@"UPDATE MyDietPlan SET isConsumedBreakFast=? where StartDate=? and idBreakfast=? and userid=?"];
            //
            const char *UPDATEsql = [strValue UTF8String];
            
            if(sqlite3_prepare_v2(database, UPDATEsql, -1, &updateStmt, NULL) != SQLITE_OK)
            {
                NSLog(@"Error while UPDATE '%s' prepare statement. table MyDietPlan", sqlite3_errmsg(database));
                returnValue = NO;
            }
        }
        
        char *sqliteError=nil;
        sqlite3_exec(database, "BEGIN TRANSACTION", NULL, NULL, &sqliteError);
        
        if(sqlite3_bind_text(updateStmt, 1, [@"1" UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            if(sqlite3_bind_text(updateStmt, 2, [strStartDate UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            if(sqlite3_bind_text(updateStmt, 3, [strBreakFastID UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            if(sqlite3_bind_text(updateStmt, 4, [strUSerID UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
        
        
            
            if(SQLITE_DONE != sqlite3_step(updateStmt))
            {
                NSLog(@"Error while updating into table MyDietPlan : '%s'", sqlite3_errmsg(database));
                returnValue = NO;
            }
            sqlite3_reset(updateStmt);
            sqlite3_clear_bindings(updateStmt);
            
       
        sqlite3_exec(database, "END TRANSACTION", NULL, NULL, &sqliteError);
    }
    filePath=nil;
    if (updateStmt)
    {
        sqlite3_finalize(updateStmt);
        updateStmt = nil;
    }
    
   	sqlite3_close(database);
    database = nil;
    
    return returnValue;
    
}






-(BOOL)updateTableOFMyDietPlanLunchDetails:(NSString *)strStartDate andLunchID:(NSString *)strLunchID anduserID:(NSString *)strUSerID
{
    BOOL returnValue = YES;
    sqlite3_stmt *updateStmt = nil;
    sqlite3 *database = nil;
    
    if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK)
    {
        returnValue = NO;
    }
    
    // to enable the cache memory
    int ret = SQLITE_OPEN_SHAREDCACHE;
    
    if(ret != SQLITE_OK)
    {
        
    }
    
    const char *filePath=[self.dataBasepath UTF8String];
    int SqlCode=sqlite3_open(filePath, &database);
    if (SqlCode == SQLITE_OK)
    {
        //Preapare Update Statment
        if(updateStmt == nil)
        {
            
         
            
            NSString *strValue = [NSString stringWithFormat:@"UPDATE MyDietPlan SET isConsumedLunch=? where StartDate=? and idLunch=? and userid=? "];
            
            const char *UPDATEsql = [strValue UTF8String];
            
            if(sqlite3_prepare_v2(database, UPDATEsql, -1, &updateStmt, NULL) != SQLITE_OK)
            {
                NSLog(@"Error while UPDATE '%s' prepare statement. table MyDietPlan", sqlite3_errmsg(database));
                returnValue = NO;
            }
        }
        
        char *sqliteError=nil;
        sqlite3_exec(database, "BEGIN TRANSACTION", NULL, NULL, &sqliteError);
        
        if(sqlite3_bind_text(updateStmt, 1, [@"1" UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
            returnValue = NO;
        }
        if(sqlite3_bind_text(updateStmt, 2, [strStartDate UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
            returnValue = NO;
        }
        if(sqlite3_bind_text(updateStmt, 3, [strLunchID UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
            returnValue = NO;
        }
        if(sqlite3_bind_text(updateStmt, 4, [strUSerID UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
            returnValue = NO;
        }
        
        
        
        if(SQLITE_DONE != sqlite3_step(updateStmt))
        {
            NSLog(@"Error while updating into table MyDietPlan : '%s'", sqlite3_errmsg(database));
            returnValue = NO;
        }
        sqlite3_reset(updateStmt);
        sqlite3_clear_bindings(updateStmt);
        
        
        sqlite3_exec(database, "END TRANSACTION", NULL, NULL, &sqliteError);
    }
    filePath=nil;
    if (updateStmt)
    {
        sqlite3_finalize(updateStmt);
        updateStmt = nil;
    }
    
   	sqlite3_close(database);
    database = nil;
    
    return returnValue;
    
}



-(BOOL)updateTableOFMyDietPlanDinnerDetails:(NSString *)strStartDate andDinnerID:(NSString *)strDinnerID anduserID:(NSString *)strUSerID
{
    BOOL returnValue = YES;
    sqlite3_stmt *updateStmt = nil;
    sqlite3 *database = nil;
    
    if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK)
    {
        returnValue = NO;
    }
    
    // to enable the cache memory
    int ret = SQLITE_OPEN_SHAREDCACHE;
    
    if(ret != SQLITE_OK)
    {
        
    }
    
    const char *filePath=[self.dataBasepath UTF8String];
    int SqlCode=sqlite3_open(filePath, &database);
    if (SqlCode == SQLITE_OK)
    {
        //Preapare Update Statment
        if(updateStmt == nil)
        {
            NSString *strValue = [NSString stringWithFormat:@"UPDATE MyDietPlan SET isConsumedDinner=? where StartDate=? and idDinner=? and userid=? "];
            
            const char *UPDATEsql = [strValue UTF8String];
            
            if(sqlite3_prepare_v2(database, UPDATEsql, -1, &updateStmt, NULL) != SQLITE_OK)
            {
                NSLog(@"Error while UPDATE '%s' prepare statement. table MyDietPlan", sqlite3_errmsg(database));
                returnValue = NO;
            }
        }
        
        char *sqliteError=nil;
        sqlite3_exec(database, "BEGIN TRANSACTION", NULL, NULL, &sqliteError);
        
        if(sqlite3_bind_text(updateStmt, 1, [@"1" UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
            returnValue = NO;
        }
        if(sqlite3_bind_text(updateStmt, 2, [strStartDate UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
            returnValue = NO;
        }
        if(sqlite3_bind_text(updateStmt, 3, [strDinnerID UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
            returnValue = NO;
        }
        if(sqlite3_bind_text(updateStmt, 4, [strUSerID UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
            returnValue = NO;
        }
        
        
        
        if(SQLITE_DONE != sqlite3_step(updateStmt))
        {
            NSLog(@"Error while updating into table MyDietPlan : '%s'", sqlite3_errmsg(database));
            returnValue = NO;
        }
        sqlite3_reset(updateStmt);
        sqlite3_clear_bindings(updateStmt);
        
        
        sqlite3_exec(database, "END TRANSACTION", NULL, NULL, &sqliteError);
    }
    filePath=nil;
    if (updateStmt)
    {
        sqlite3_finalize(updateStmt);
        updateStmt = nil;
    }
    
   	sqlite3_close(database);
    database = nil;
    
    return returnValue;
    
}









-(NSMutableArray*)GetMyDietPlans
{
    sqlite3_stmt *selectStmt = nil;
    sqlite3 *database = nil;
    
    NSMutableArray *arrNotification = nil;
    
    arrNotification = [[NSMutableArray alloc] init];
    
    if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK) {
        NSLog(@"Can now use sqlite on multiple threads, using the same connection");
    }
    
    // to enable the cache memory
    int ret = SQLITE_OPEN_SHAREDCACHE;
    
    if(ret != SQLITE_OK)
    {
        
    }
    
    const char *filePath=[self.dataBasepath UTF8String];
    int SqlCode=sqlite3_open(filePath, &database);
    if (SqlCode == SQLITE_OK)
    {
        if(selectStmt == nil)
        {
            
            
           
            
            NSString *strValue;
            strValue = [NSString stringWithFormat:@"SELECT validityDate,textBreakFast,isConsumedBreakFast,textLunch,isConsumedLunch,txtDinner,isConsumedDinner,Startdate,Enddate,idBreakfast,idLunch,idDinner from MyDietPlan"];
            
            
            const char *SELECTsql = [strValue UTF8String];
            
            if(sqlite3_prepare_v2(database, SELECTsql, -1, &selectStmt, NULL) != SQLITE_OK)
            {
                NSLog(@"Error while creating Select statement for MyDietPlan '%s' .", sqlite3_errmsg(database));
            }
            else  {
                
                
                while(sqlite3_step(selectStmt) == SQLITE_ROW){
                    
        NSString *strvalidityDate=[NSString stringWithUTF8String:(char*)sqlite3_column_text(selectStmt, 0)];
        NSString *strtextBreakFast=[NSString stringWithUTF8String:(char*)sqlite3_column_text(selectStmt, 1)];
        NSString *strisConsumedBreakFast=[NSString stringWithUTF8String:(char*)sqlite3_column_text(selectStmt, 2)];
        NSString *strtextLunch=[NSString stringWithUTF8String:(char*)sqlite3_column_text(selectStmt, 3)];
        NSString *strisConsumedLunch=[NSString stringWithUTF8String:(char*)sqlite3_column_text(selectStmt, 4)];
        NSString *strtxtDinner=[NSString stringWithUTF8String:(char*)sqlite3_column_text(selectStmt, 5)];
        NSString *strisConsumedDinner=[NSString stringWithUTF8String:(char*)sqlite3_column_text(selectStmt, 6)];
        NSString *strStartdate=[NSString stringWithUTF8String:(char*)sqlite3_column_text(selectStmt, 7)];
        NSString *strEnddate=[NSString stringWithUTF8String:(char*)sqlite3_column_text(selectStmt, 8)];
                    
                    
                    NSString *stridBreakfast=[NSString stringWithUTF8String:(char*)sqlite3_column_text(selectStmt, 9)];
                    NSString *stridLunch=[NSString stringWithUTF8String:(char*)sqlite3_column_text(selectStmt, 10)];
                    NSString *stridDinner=[NSString stringWithUTF8String:(char*)sqlite3_column_text(selectStmt, 11)];
                    
                    
                    //   idBreakfast
                    
                    //idLunch
                    //idDinner
                    


        NSMutableDictionary *noterecord=[[NSMutableDictionary alloc] initWithObjectsAndKeys:strvalidityDate ,@"validityDate",strtextBreakFast,@"textBreakFast",strisConsumedBreakFast,@"isConsumedBreakFast", strtextLunch,@"textLunch",strisConsumedLunch,@"isConsumedLunch",strtxtDinner,@"txtDinner",strisConsumedDinner,@"isConsumedDinner",strStartdate,@"Startdate",strEnddate,@"Enddate",stridBreakfast,@"idBreakfast",stridLunch,@"idLunch",stridDinner,@"idDinner",nil];
            [arrNotification addObject:noterecord];
                    
                }
            }
        }
    }
    sqlite3_reset(selectStmt);
    filePath=nil;
    if (selectStmt) {
        sqlite3_finalize(selectStmt);
        selectStmt = nil;
    }    sqlite3_close(database);
    return arrNotification;
    
}


#pragma mark - === Delete ===
/*
 @Description : This method is used to delete Diet Plan of user.
 @author :  Animesh Bansal
 @param :
 Parameter1 user ID
 @return: Bool value to signify the success of the Query
 */

-(BOOL)deleteDietPlan:(NSString*)userID{
    BOOL returnValue = YES;
    sqlite3_stmt *deleteStmt = nil;
    sqlite3 *database = nil;
    
    if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK)
    {
        returnValue = NO;
    }
    
    // to enable the cache memory
    int ret = SQLITE_OPEN_SHAREDCACHE;
    
    if(ret != SQLITE_OK)
    {
        
    }
    
    const char *filePath=[dataBasepath UTF8String];
    int SqlCode=sqlite3_open(filePath, &database);
    if (SqlCode == SQLITE_OK)
    {
        
        //Preapare Update Statment
        if(deleteStmt == nil)
        {
            NSString *strValue = @"Delete from MyDietPlan where userid = ?";
            
            const char *DELETEsql = [strValue UTF8String];
            if(sqlite3_prepare_v2(database, DELETEsql, -1, &deleteStmt, NULL) != SQLITE_OK)
            {
                NSLog(@"Error while Delete '%s' prepare statement. table MyDietPlan", sqlite3_errmsg(database));
                returnValue = NO;
            }
        }
        if(sqlite3_bind_text(deleteStmt, 1, [userID UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
            returnValue = NO;
        }
        
        if(SQLITE_DONE != sqlite3_step(deleteStmt))
        {
            NSLog(@"Error while deleting into table MyDietPlan : '%s'", sqlite3_errmsg(database));
            returnValue = NO;
        }
        sqlite3_reset(deleteStmt);
    }
    
    if (deleteStmt)
    {
        sqlite3_finalize(deleteStmt);
        deleteStmt = nil;
    }
    sqlite3_close(database);
    database = nil;
    
    return returnValue;
}




@end
