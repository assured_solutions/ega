//
//  AnalyticQuestionDM.h
//  EGA
//
//  Created by Soniya on 24/11/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

/*
CREATE TABLE "tbl_AnalysisQuestion" (question_id VARCHAR, question VARCHAR, que_wt VARCHAR, que_type VARCHAR)
 */

#import <Foundation/Foundation.h>
#import <sqlite3.h>
@interface AnalyticQuestionDM : NSObject

{
    NSString *dataBasepath;
    AppDelegate *appDelegate;
    
}


- (BOOL)insertAnalyticsQuestion:(NSArray *)arrAnalyticsQues;

- (BOOL)insertAnalyticsQuestionAnswerWithQuesID:(NSString*)qid Option:(NSString*)optValue UserID:(NSString*)userid;

- (BOOL)insertAnalyticsQuestionAnswers:(NSArray *)arrAnalyticsQuesAnswer userid:(NSString*)userid;

- (BOOL)updateAnswer:(NSString*)ans ofQuestionID:(NSString*)questionID;

- (NSInteger)selectCountOfQuestions;

- (NSMutableArray *)selectQuestionsOfEachQuestionTypeWithPageNo:(NSInteger)pageNo RecordCount:(NSInteger)recordCount UserID:(NSString*)userid;

- (NSInteger)selectCountOfNonZeroAnswerValueForUser:(NSString*)userID;

- (NSMutableArray*)selectAnswerValueForUser:(NSString*)userID;

@end
