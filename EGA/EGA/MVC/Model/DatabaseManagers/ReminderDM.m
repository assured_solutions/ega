//
//  ReminderDM.m
//  EGA
//
//  Created by Soniya on 28/10/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "ReminderDM.h"

@implementation ReminderDM

@synthesize dataBasepath;

- (id)init
{
    self = [super init];
    if (self != nil)
    {
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        self.dataBasepath = [appDelegate getDBPath];
        dataBasepath = [SupportingClass getDocumentPathForComponent:DBNAME];
    }
    return self;
}


//<**************************************************************************>

#pragma mark - Book Notes Details

//<**************************************************************************>


// Methods to retrive, delete, update and insert records in Book Notes Table

// Used to insert Book Notes Details in a Book Notes Table


-(NSInteger)insertEvent:(NSString*)title andDetails:(NSString *)strDetails Date:(NSString*)date AlarmTone:(NSString*)alarmTone Repeat:(NSInteger)repeat SoundTone:(NSInteger)soundTone {
    
    
    BOOL returnValue = YES;
    sqlite3_stmt *insertStmt = nil;
    sqlite3 *database = nil;
    
    if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK)
    {
        returnValue = NO;
    }
    
    // to enable the cache memory
    int ret = SQLITE_OPEN_SHAREDCACHE;
    
    if(ret != SQLITE_OK)
    {
        
    }
    
    const char *filePath=[self.dataBasepath UTF8String];
    int SqlCode=sqlite3_open(filePath, &database);
    if (SqlCode == SQLITE_OK)
    {
        
        //Preapare Insert Statment
        if(insertStmt == nil)
        {
            NSString *aString =[NSString stringWithFormat:@"INSERT INTO Tbl_AlarmEvent (Title,AlarmDetails,DateTime,Repeat,Sound,AlarmTone) VALUES (?,?,?,?,?,?)"];
            const char *sql = [aString UTF8String];
            
            if(sqlite3_prepare_v2(database, sql, -1, &insertStmt, NULL) != SQLITE_OK)
            {
                NSLog(@"Error while creating insert '%s' add statement. table Tbl_AlarmEvent", sqlite3_errmsg(database));
                returnValue = NO;
            }
        }
        if(sqlite3_bind_text(insertStmt, 1,[title UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
            returnValue = NO;
        }
        
        if(sqlite3_bind_text(insertStmt, 2,[strDetails UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
            returnValue = NO;
        }
        if(sqlite3_bind_text(insertStmt, 3, [date UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
            returnValue = NO;
        }
        
        sqlite3_bind_int(insertStmt, 4, repeat);
        
        sqlite3_bind_int(insertStmt, 5, soundTone);
        
        if(sqlite3_bind_text(insertStmt, 6, [alarmTone UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
            returnValue = NO;
        }
        
        
        //
        //        if(SQLITE_DONE != sqlite3_step(insertStmt))
        //        {
        //            NSLog(@"Error while inserting into table Tbl_AlarmEvent : '%s'", sqlite3_errmsg(database));
        //            returnValue = NO;
        //        }
        //        sqlite3_reset(insertStmt);
    }
    
    
    
    //// returning row id from sqlite statment.
    
    
    
    int success = sqlite3_step(insertStmt);
    
    if (success != SQLITE_ERROR)
    {
        
        NSInteger rowID = sqlite3_last_insert_rowid(database);
        
        return rowID;
        
        //////NSLog(@"Highlight with note INSERTED !");
        
    }else
    {
        return -1;
    }
    
    
    
    filePath=nil;
    if (insertStmt)
    {
        sqlite3_finalize(insertStmt);
        insertStmt = nil;
    }
   	
    sqlite3_close(database);
    database = nil;
    
    return returnValue;
    
    
    
}



//-(NSInteger)insertEvent:(NSString*)title Date:(NSString*)date AlarmTone:(NSString*)alarmTone Repeat:(NSInteger)repeat SoundTone:(NSInteger)soundTone {
//    @try
//    {
//        BOOL returnValue = YES;
//        sqlite3_stmt *insertstatement = nil;
//        sqlite3 *database = nil;
//
//
//
//
//        if (alarmTone==nil) {
//            alarmTone=@"";
//        }
//
//              NSString *query= [NSString stringWithFormat:@"INSERT INTO Tbl_AlarmEvent (Title,DateTime,Repeat,Sound,AlarmTone) VALUES (?,?,?,?,?)"];
//
//
//        const char *sql = [query UTF8String];
//
//        if(sqlite3_prepare_v2(database, sql, -1, &insertstatement, NULL) != SQLITE_OK)
//        {
//            NSLog(@"Error while creating insert '%s' add statement. table Tbl_AlarmEvent", sqlite3_errmsg(database));
//            returnValue = NO;
//        }
//        else {
//
//            sqlite3_bind_text(insertstatement, 1,[title UTF8String],-1,SQLITE_STATIC);
//            sqlite3_bind_text(insertstatement, 2,[date UTF8String],-1,SQLITE_STATIC);
//            sqlite3_bind_int(insertstatement, 3, repeat);
//            sqlite3_bind_int(insertstatement, 4, soundTone);
//            sqlite3_bind_text(insertstatement, 5,[alarmTone UTF8String],-1,SQLITE_STATIC);
//
//            int success = sqlite3_step(insertstatement);
//
//            if (success != SQLITE_ERROR)
//            {
//
//                NSInteger rowID = sqlite3_last_insert_rowid(database);
//
//                return rowID;
//
//                //////NSLog(@"Highlight with note INSERTED !");
//
//            }else
//            {
//                return -1;
//            }
//
//        }
//
//
//
//        sqlite3_finalize(insertstatement);
//    }
//    @catch (NSException *exception) {
//        NSLog(@"Error in Inserting Book Note %@",exception.name);
//    }
//    @finally {
//
//    }
//}
//












-(BOOL)updateEvent:(NSString*)title Date:(NSString*)date AlarmTone:(NSString*)alarmTone Repeat:(NSInteger)repeat SoundTone:(NSInteger)soundTone ID:(NSInteger)notificationID{
    BOOL returnValue = YES;
    
    sqlite3_stmt *updateStmt = nil;
    sqlite3 *database = nil;
    
    if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK)
    {
        returnValue = NO;
    }
    const char *filePath=[self.dataBasepath UTF8String];
    int SqlCode=sqlite3_open(filePath, &database);
    if (SqlCode == SQLITE_OK)
    {
        if(updateStmt == nil)
        {
            NSString *strquery = [NSString stringWithFormat:@"Update Tbl_AlarmEvent set Title=?,DateTime=?,Repeat=?,Sound=?,AlarmTone=? where NotificationID=?"];
            
            const char *UPDATEsql = [strquery UTF8String];
            
            if(sqlite3_prepare_v2(database, UPDATEsql, -1, &updateStmt, NULL) != SQLITE_OK)
            {
                NSLog(@"Error while UPDATE '%s' prepare statement. table Tbl_AlarmEvent", sqlite3_errmsg(database));
                returnValue = NO;
            }
            
            if(sqlite3_bind_text(updateStmt, 1, [title UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            
            
            if(sqlite3_bind_text(updateStmt, 2, [date UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            
            
            sqlite3_bind_int(updateStmt, 3,repeat);
            sqlite3_bind_int(updateStmt, 4,soundTone);
            
            
            
            
            if(sqlite3_bind_text(updateStmt, 5, [alarmTone UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            
            sqlite3_bind_int(updateStmt, 6,notificationID);
            
            
            
            
            
        }
        
        if(SQLITE_DONE != sqlite3_step(updateStmt))
        {
            
            NSLog(@"Error while updating into table Tbl_AlarmEvent : '%s'", sqlite3_errmsg(database));
            returnValue = NO;
        }
        sqlite3_clear_bindings(updateStmt);
        sqlite3_reset(updateStmt);
    }
    filePath=nil;
    if (updateStmt) {
        sqlite3_finalize(updateStmt);
        updateStmt = nil;
    }
    sqlite3_close(database);
    database = nil;
    
    return returnValue;
    
}





-(BOOL)deleteEvent:(NSInteger)notificationid
{
    BOOL returnValue = YES;
    sqlite3_stmt *deleteStmt = nil;
    sqlite3 *database = nil;
    
    if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK)
    {
        returnValue = NO;
    }
    
    // to enable the cache memory
    int ret = SQLITE_OPEN_SHAREDCACHE;
    
    if(ret != SQLITE_OK)
    {
        
    }
    
    const char *filePath=[self.dataBasepath UTF8String];
    int SqlCode=sqlite3_open(filePath, &database);
    if (SqlCode == SQLITE_OK)
    {
        
        //Preapare Update Statment
        if(deleteStmt == nil)
        {
            NSString *strValue = @"DELETE FROM Tbl_AlarmEvent where NotificationID=?";
            
            const char *DELETEsql = [strValue UTF8String];
            if(sqlite3_prepare_v2(database, DELETEsql, -1, &deleteStmt, NULL) != SQLITE_OK)
            {
                NSLog(@"Error while Delete '%s' prepare statement. table Tbl_AlarmEvent", sqlite3_errmsg(database));
                returnValue = NO;
            }
        }
        
        sqlite3_bind_int(deleteStmt, 1,notificationid);
        
        if(SQLITE_DONE != sqlite3_step(deleteStmt))
        {
            //NSLog(@"Error while deleting into table tbl_Student : '%s'", sqlite3_errmsg(database));
            returnValue = NO;
        }
        sqlite3_reset(deleteStmt);
    }
    
    if (deleteStmt)
    {
        sqlite3_finalize(deleteStmt);
        deleteStmt = nil;
    }
    sqlite3_close(database);
    database = nil;
    
    return returnValue;
}



-(BOOL)deletePreviousEvent:(NSString*)date
{
    BOOL returnValue = YES;
    sqlite3_stmt *deleteStmt = nil;
    sqlite3 *database = nil;
    
    if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK)
    {
        returnValue = NO;
    }
    
    // to enable the cache memory
    int ret = SQLITE_OPEN_SHAREDCACHE;
    
    if(ret != SQLITE_OK)
    {
        
    }
    
    const char *filePath=[self.dataBasepath UTF8String];
    int SqlCode=sqlite3_open(filePath, &database);
    if (SqlCode == SQLITE_OK)
    {
        
        //Preapare Update Statment
        if(deleteStmt == nil)
        {
            NSString *strValue = @"DELETE from Tbl_AlarmEvent where DateTime<? and Repeat=0";
            
            const char *DELETEsql = [strValue UTF8String];
            if(sqlite3_prepare_v2(database, DELETEsql, -1, &deleteStmt, NULL) != SQLITE_OK)
            {
                NSLog(@"Error while Delete '%s' prepare statement. table Tbl_AlarmEvent", sqlite3_errmsg(database));
                returnValue = NO;
            }
        }
        
        sqlite3_bind_text(deleteStmt, 1,[date UTF8String],-1,SQLITE_STATIC);
        
        int success =  sqlite3_step(deleteStmt);
        
        
        
        if(SQLITE_DONE != sqlite3_step(deleteStmt))
        {
            NSLog(@"Error while deleting into table Tbl_AlarmEvent : '%s'", sqlite3_errmsg(database));
            returnValue = NO;
        }
        sqlite3_reset(deleteStmt);
    }
    
    if (deleteStmt)
    {
        sqlite3_finalize(deleteStmt);
        deleteStmt = nil;
    }
    sqlite3_close(database);
    database = nil;
    
    return returnValue;
}

-(NSMutableArray*)getNotification
{
    sqlite3_stmt *selectStmt = nil;
    sqlite3 *database = nil;
    
    NSMutableArray *arrNotification = nil;
    
    arrNotification = [[NSMutableArray alloc] init];
    
    if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK) {
        NSLog(@"Can now use sqlite on multiple threads, using the same connection");
    }
    
    // to enable the cache memory
    int ret = SQLITE_OPEN_SHAREDCACHE;
    
    if(ret != SQLITE_OK)
    {
        
    }
    
    const char *filePath=[self.dataBasepath UTF8String];
    int SqlCode=sqlite3_open(filePath, &database);
    if (SqlCode == SQLITE_OK)
    {
        if(selectStmt == nil)
        {
            NSString *strValue;
            //  strValue = [NSString stringWithFormat:@"SELECT * FROM tbl_Question_options where Question_id='%@'",objQuestionBankQuestionID.str_Question_id];
            
            strValue = [NSString stringWithFormat:@"SELECT NotificationID,Title,AlarmDetails,DateTime,Repeat,Sound,AlarmTone from Tbl_AlarmEvent"];
            
            
            const char *SELECTsql = [strValue UTF8String];
            
            if(sqlite3_prepare_v2(database, SELECTsql, -1, &selectStmt, NULL) != SQLITE_OK)
            {
                NSLog(@"Error while creating Select statement for Tbl_AlarmEvent '%s' .", sqlite3_errmsg(database));
            }
            else  {
                
                
                while(sqlite3_step(selectStmt) == SQLITE_ROW){
                    NSString *NotificationID=[NSString stringWithUTF8String:(char*)sqlite3_column_text(selectStmt, 0)];
                    NSString *Title=[NSString stringWithUTF8String:(char*)sqlite3_column_text(selectStmt, 1)];
                    NSString *AlarmDetails=[NSString stringWithUTF8String:(char*)sqlite3_column_text(selectStmt, 2)];
                    NSString *DateTime=[NSString stringWithUTF8String:(char*)sqlite3_column_text(selectStmt, 3)];
                    NSString *RepeatTime=[NSString stringWithUTF8String:(char*)sqlite3_column_text(selectStmt, 4)];
                    NSString *SoundTone=[NSString stringWithUTF8String:(char*)sqlite3_column_text(selectStmt, 5)];
                    
                    
                    NSString *AlarmTone=[NSString stringWithUTF8String:(char*)sqlite3_column_text(selectStmt, 6)];
                    NSMutableDictionary *noterecord=[[NSMutableDictionary alloc] initWithObjectsAndKeys:NotificationID,@"NotificationID",Title,@"Title",AlarmDetails,@"AlarmDetail",DateTime,@"DateTime",SoundTone,@"SoundTone",RepeatTime,@"RepeatTime",AlarmTone,@"AlarmTone",nil];
                    
                    [arrNotification addObject:noterecord];
                    
                }
            }
        }
    }
    sqlite3_reset(selectStmt);
    filePath=nil;
    if (selectStmt) {
        sqlite3_finalize(selectStmt);
        selectStmt = nil;
    }    sqlite3_close(database);
    return arrNotification;
    
}

-(NSMutableArray*)getAllNotificationOfAllDates
{
    sqlite3_stmt *selectStmt = nil;
    sqlite3 *database = nil;
    
    NSMutableArray *arrNotification = nil;
    
    arrNotification = [[NSMutableArray alloc] init];
    
    if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK) {
        NSLog(@"Can now use sqlite on multiple threads, using the same connection");
    }
    
    // to enable the cache memory
    int ret = SQLITE_OPEN_SHAREDCACHE;
    
    if(ret != SQLITE_OK)
    {
        
    }
    
    const char *filePath=[self.dataBasepath UTF8String];
    int SqlCode=sqlite3_open(filePath, &database);
    if (SqlCode == SQLITE_OK)
    {
        if(selectStmt == nil)
        {
            NSString *strValue;
            //  strValue = [NSString stringWithFormat:@"SELECT * FROM tbl_Question_options where Question_id='%@'",objQuestionBankQuestionID.str_Question_id];
            
            strValue = [NSString stringWithFormat:@"SELECT distinct date(DateTime) from Tbl_AlarmEvent"];
            
            
            const char *SELECTsql = [strValue UTF8String];
            
            if(sqlite3_prepare_v2(database, SELECTsql, -1, &selectStmt, NULL) != SQLITE_OK)
            {
                NSLog(@"Error while creating Select statement for Tbl_AlarmEvent '%s' .", sqlite3_errmsg(database));
            }
            else  {
                
                
                while(sqlite3_step(selectStmt) == SQLITE_ROW){
                    NSString *dateTime=[NSString stringWithUTF8String:(char*)sqlite3_column_text(selectStmt, 0)];
                    [arrNotification addObject:dateTime];
                    
                }
            }
        }
    }
    sqlite3_reset(selectStmt);
    filePath=nil;
    if (selectStmt) {
        sqlite3_finalize(selectStmt);
        selectStmt = nil;
    }    sqlite3_close(database);
    return arrNotification;
    
}
-(NSMutableArray*)getNotificationOfDate:(NSString *)date
{
    sqlite3_stmt *selectStmt = nil;
    sqlite3 *database = nil;
    
    NSMutableArray *arrNotification = nil;
    
    arrNotification = [[NSMutableArray alloc] init];
    
    if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK) {
        NSLog(@"Can now use sqlite on multiple threads, using the same connection");
    }
    
    // to enable the cache memory
    int ret = SQLITE_OPEN_SHAREDCACHE;
    
    if(ret != SQLITE_OK)
    {
        
    }
    
    const char *filePath=[self.dataBasepath UTF8String];
    int SqlCode=sqlite3_open(filePath, &database);
    if (SqlCode == SQLITE_OK)
    {
        if(selectStmt == nil)
        {
            NSString *strValue;
            //  strValue = [NSString stringWithFormat:@"SELECT * FROM tbl_Question_options where Question_id='%@'",objQuestionBankQuestionID.str_Question_id];
            
            strValue = [NSString stringWithFormat:@"SELECT NotificationID,Title,AlarmDetails,DateTime,Repeat,Sound,AlarmTone from Tbl_AlarmEvent where date(DateTime)='%@'",date];
            
            
            const char *SELECTsql = [strValue UTF8String];
            
            if(sqlite3_prepare_v2(database, SELECTsql, -1, &selectStmt, NULL) != SQLITE_OK)
            {
                NSLog(@"Error while creating Select statement for Tbl_AlarmEvent '%s' .", sqlite3_errmsg(database));
            }
            else  {
                
                
                while(sqlite3_step(selectStmt) == SQLITE_ROW){
                    NSString *NotificationID=[NSString stringWithUTF8String:(char*)sqlite3_column_text(selectStmt, 0)];
                    NSString *Title=[NSString stringWithUTF8String:(char*)sqlite3_column_text(selectStmt, 1)];
                    
                    NSString *AlarmDetails=[NSString stringWithUTF8String:(char*)sqlite3_column_text(selectStmt, 2)];
                    
                    
                    
                    
                    NSString *DateTime=[NSString stringWithUTF8String:(char*)sqlite3_column_text(selectStmt, 3)];
                    NSString *RepeatTime=[NSString stringWithUTF8String:(char*)sqlite3_column_text(selectStmt, 4)];
                    NSString *SoundTone=[NSString stringWithUTF8String:(char*)sqlite3_column_text(selectStmt, 5)];
                    
                    
                    NSString *AlarmTone=[NSString stringWithUTF8String:(char*)sqlite3_column_text(selectStmt, 6)];
                    NSMutableDictionary *noterecord=[[NSMutableDictionary alloc] initWithObjectsAndKeys:NotificationID,@"NotificationID",Title,@"Title",AlarmDetails,@"AlarmDetail", DateTime,@"DateTime",SoundTone,@"SoundTone",RepeatTime,@"RepeatTime",AlarmTone,@"AlarmTone",nil];
                    
                    [arrNotification addObject:noterecord];
                    
                }
            }
        }
    }
    sqlite3_reset(selectStmt);
    filePath=nil;
    if (selectStmt) {
        sqlite3_finalize(selectStmt);
        selectStmt = nil;
    }    sqlite3_close(database);
    return arrNotification;
    
}

@end
