//
//  AnalyticQuestionDM.m
//  EGA
//
//  Created by Soniya on 24/11/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "AnalyticQuestionDM.h"

@implementation AnalyticQuestionDM

- (id)init
{
    self = [super init];
    if (self != nil)
    {
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        dataBasepath = [appDelegate getDBPath];
        dataBasepath = [SupportingClass getDocumentPathForComponent:DBNAME];
    }
    return self;
}


#pragma mark - === Insert ===

/*
 @Description : This method is used to insert  Analytic Question records if not exist and update if exists
 @author :  Soniya Vishwakarma
 @param :
 Parameter1 arrAnalyticsQues : Array of Anlytics Questions which need to be inserted or updated.
 @return: Bool value to signify the success of the Query
  */
- (BOOL)insertAnalyticsQuestion:(NSArray *)arrAnalyticsQues
{
    BOOL returnValue = YES;
    sqlite3_stmt *selectStmt = nil;
    sqlite3_stmt *insertStmt = nil;
    sqlite3_stmt *updateStmt = nil;
    sqlite3 *database = nil;
    
    if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK)
    {
        returnValue = NO;
    }
    
    // to enable the cache memory
    int ret = SQLITE_OPEN_SHAREDCACHE;
    
    if(ret != SQLITE_OK)
    {
        
    }
    const char *filePath=[dataBasepath UTF8String];
    int SqlCode=sqlite3_open(filePath, &database);
    if (SqlCode == SQLITE_OK)
    {
        //Preapare Select Statment
        if(selectStmt == nil)
        {
            NSString *strValue = [NSString stringWithFormat:@"SELECT rowid FROM tbl_AnalysisQuestion where question_id = ?"];
            const char *SELECTsql = [strValue UTF8String];
            
            if(sqlite3_prepare_v2(database, SELECTsql, -1, &selectStmt, NULL) != SQLITE_OK)
            {
                NSLog(@"Error while creating Select statement for '%s' on table tbl_AnalysisQuestion.", sqlite3_errmsg(database));
                returnValue = NO;
            }
        }
        //Preapare Update Statment
        if(updateStmt == nil)
        {
            NSString *strValue = [NSString stringWithFormat:@"UPDATE tbl_AnalysisQuestion SET question=?,que_wt=?,que_type=? where question_id=?"];
            
            const char *UPDATEsql = [strValue UTF8String];
            
            if(sqlite3_prepare_v2(database, UPDATEsql, -1, &updateStmt, NULL) != SQLITE_OK)
            {
                NSLog(@"Error while UPDATE '%s' prepare statement. table tbl_AnalysisQuestion", sqlite3_errmsg(database));
                returnValue = NO;
            }
        }
        
        //Preapare Insert Statment
        if(insertStmt == nil)
        {
            NSString *strValue =[NSString stringWithFormat:@"INSERT INTO tbl_AnalysisQuestion ('question_id','question','que_wt','que_type') Values(?,?,?,?)"];
            const char *insertSql = [strValue UTF8String];
            
            if(sqlite3_prepare_v2(database, insertSql, -1, &insertStmt, NULL) != SQLITE_OK)
            {
                NSLog(@"Error while creating insert '%s' add statement. table tbl_AnalysisQuestion", sqlite3_errmsg(database));
                returnValue = NO;
            }
        }
        
        NSInteger rowidentifier = -1;
          char *sqliteError=nil;
        sqlite3_exec(database, "BEGIN TRANSACTION", NULL, NULL, &sqliteError);
        for (NSDictionary *dictQuestion in arrAnalyticsQues) {
            
                if(sqlite3_bind_text(selectStmt, 1, [[dictQuestion objectForKey:@"questionid"] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue= NO;
            }
            
            rowidentifier = -1;
            
            // Check whether Record particular id exists or not
            while(sqlite3_step(selectStmt) == SQLITE_ROW)
            {
                rowidentifier = sqlite3_column_int(selectStmt, 0);
            }
            sqlite3_reset(selectStmt);
           // sqlite3_clear_bindings(selectStmt);
            // If Package id not exists then insert the record.
            
            if(rowidentifier == -1)
            {
                if(sqlite3_bind_text(insertStmt, 1, [[NSString stringWithFormat:@"%@",[dictQuestion objectForKey:@"questionid"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                    returnValue = NO;
                }
                if(sqlite3_bind_text(insertStmt, 2, [[NSString stringWithFormat:@"%@",[dictQuestion objectForKey:@"question"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                    returnValue = NO;
                }
                if(sqlite3_bind_text(insertStmt, 3, [[NSString stringWithFormat:@"%@",[dictQuestion objectForKey:@"que_wt"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                    returnValue = NO;
                }
         
                if(sqlite3_bind_text(insertStmt, 4, [[NSString stringWithFormat:@"%@",[dictQuestion objectForKey:@"que_type"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                    returnValue = NO;
                }
                
                               if(SQLITE_DONE != sqlite3_step(insertStmt))
                {
                    NSLog(@"Error while inserting into table tbl_AnalysisQuestion : '%s'", sqlite3_errmsg(database));
                    returnValue = NO;
                }
                sqlite3_reset(insertStmt);
                sqlite3_clear_bindings(insertStmt);
            }
            // If Question id exists then update the record.
            else
            {
                if(sqlite3_bind_text(updateStmt, 1, [[NSString stringWithFormat:@"%@",[dictQuestion objectForKey:@"question"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                    returnValue = NO;
                }
                if(sqlite3_bind_text(updateStmt, 2, [[NSString stringWithFormat:@"%@",[dictQuestion objectForKey:@"que_wt"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                    returnValue = NO;
                }
               
                if(sqlite3_bind_text(updateStmt, 3, [[NSString stringWithFormat:@"%@",[dictQuestion objectForKey:@"que_type"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                    returnValue = NO;
                }
                if(SQLITE_DONE != sqlite3_step(updateStmt))
                {
                    NSLog(@"Error while updating into table tbl_AnalysisQuestion : '%s'", sqlite3_errmsg(database));
                    returnValue = NO;
                }
                sqlite3_reset(updateStmt);
                sqlite3_clear_bindings(updateStmt);
            }
        }
        sqlite3_exec(database, "END TRANSACTION", NULL, NULL, &sqliteError);
    }
    filePath=nil;
    if (insertStmt)
    {
        sqlite3_finalize(insertStmt);
        insertStmt = nil;
    }
    
    if (updateStmt)
    {
        sqlite3_finalize(updateStmt);
        updateStmt = nil;
    }
    
    if (selectStmt)
    {
        sqlite3_finalize(selectStmt);
        selectStmt = nil;
    }
    
    sqlite3_close(database);
    database = nil;
    
    return returnValue;
    
}
/*
 @Description : This method is used to insert  Analytic Question answer records of a user if not exist and update if exists
 @author :  Soniya Vishwakarma
 @param :
 Parameter1 arrAnalyticsQues : Array of Anlytics Questions which need to be inserted or updated.
 @return: Bool value to signify the success of the Query
 */
- (BOOL)insertAnalyticsQuestionAnswerWithQuesID:(NSString*)qid Option:(NSString*)optValue UserID:(NSString*)userid
{
    BOOL returnValue = YES;
    sqlite3_stmt *selectStmt = nil;
    sqlite3_stmt *insertStmt = nil;
    sqlite3_stmt *updateStmt = nil;
    sqlite3 *database = nil;
    
    if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK)
    {
        returnValue = NO;
    }
    
    // to enable the cache memory
    int ret = SQLITE_OPEN_SHAREDCACHE;
    
    if(ret != SQLITE_OK)
    {
        
    }
    const char *filePath=[dataBasepath UTF8String];
    int SqlCode=sqlite3_open(filePath, &database);
    if (SqlCode == SQLITE_OK)
    {
        //Preapare Select Statment
        if(selectStmt == nil)
        {
            NSString *strValue = [NSString stringWithFormat:@"SELECT rowid FROM tbl_AnalysisQuestion_Answers where question_id = ? and user_id=?"];
            const char *SELECTsql = [strValue UTF8String];
            int sql_code = sqlite3_prepare_v2(database, SELECTsql, -1, &selectStmt, NULL);
            SELECTsql=nil;
            if(sql_code != SQLITE_OK)
            {
                NSLog(@"Error while creating Select statement for '%s' on table tbl_AnalysisQuestion_Answers.", sqlite3_errmsg(database));
                returnValue = NO;
            }
            strValue=nil;
            SELECTsql=nil;
        }
        //Preapare Update Statment
        if(updateStmt == nil)
        {
            NSString *strValue = [NSString stringWithFormat:@"UPDATE tbl_AnalysisQuestion_Answers SET Value=?,user_id=? where question_id=?"];
            
            const char *UPDATEsql = [strValue UTF8String];
            int sql_code =sqlite3_prepare_v2(database, UPDATEsql, -1, &updateStmt, NULL);
            UPDATEsql=nil;
            
            if(sql_code != SQLITE_OK)
            {
                NSLog(@"Error while UPDATE '%s' prepare statement. table tbl_AnalysisQuestion_Answers", sqlite3_errmsg(database));
                returnValue = NO;
            }
        }
        
        //Preapare Insert Statment
        if(insertStmt == nil)
        {
            NSString *strValue =[NSString stringWithFormat:@"INSERT INTO tbl_AnalysisQuestion_Answers ('question_id','Value','user_id') Values(?,?,?)"];
            const char *insertSql = [strValue UTF8String];
            int sql_code =sqlite3_prepare_v2(database, insertSql, -1, &insertStmt, NULL);
            insertSql=nil;
            
            if(sql_code != SQLITE_OK)
            {
                NSLog(@"Error while creating insert '%s' add statement. table tbl_AnalysisQuestion_Answers", sqlite3_errmsg(database));
                returnValue = NO;
            }
        }
        
        NSInteger rowidentifier = -1;
        char *sqliteError=nil;
        sqlite3_exec(database, "BEGIN TRANSACTION", NULL, NULL, &sqliteError);
        
            if(sqlite3_bind_text(selectStmt, 1, [qid UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue= NO;
            }
        if(sqlite3_bind_text(selectStmt, 2, [userid UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
            returnValue= NO;
        }
            rowidentifier = -1;
            
            // Check whether Record particular id exists or not
            
            while(sqlite3_step(selectStmt) == SQLITE_ROW)
            {
                rowidentifier = sqlite3_column_int(selectStmt, 0);
            }
            sqlite3_reset(selectStmt);
            sqlite3_clear_bindings(selectStmt);
            // If Package id not exists then insert the record.
            
            if(rowidentifier == -1)
            {
                if(sqlite3_bind_text(insertStmt, 1, [qid UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                    returnValue = NO;
                }
                if(sqlite3_bind_text(insertStmt, 2, [optValue UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                    returnValue = NO;
                }
                if(sqlite3_bind_text(insertStmt, 3, [userid UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                    returnValue = NO;
                }
                
                if(SQLITE_DONE != sqlite3_step(insertStmt))
                {
                    NSLog(@"Error while inserting into table tbl_AnalysisQuestion_Answers : '%s'", sqlite3_errmsg(database));
                    returnValue = NO;
                }
                sqlite3_reset(insertStmt);
                sqlite3_clear_bindings(insertStmt);
            }
            // If Question id exists then update the record.
            else
            {
                if(sqlite3_bind_text(updateStmt, 1, [optValue UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                    returnValue = NO;
                }
                if(sqlite3_bind_text(updateStmt, 2, [userid UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                    returnValue = NO;
                }
                
                if(sqlite3_bind_text(updateStmt, 3, [qid UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                    returnValue = NO;
                }
                if(SQLITE_DONE != sqlite3_step(updateStmt))
                {
                    NSLog(@"Error while updating into table tbl_AnalysisQuestion_Answers : '%s'", sqlite3_errmsg(database));
                    returnValue = NO;
                }
                sqlite3_reset(updateStmt);
                sqlite3_clear_bindings(updateStmt);
            }
        
        sqlite3_exec(database, "END TRANSACTION", NULL, NULL, &sqliteError);
    }
    filePath=nil;
    if (insertStmt)
    {
        sqlite3_finalize(insertStmt);
        insertStmt = nil;
    }
    
    if (updateStmt)
    {
        sqlite3_finalize(updateStmt);
        updateStmt = nil;
    }
    
    if (selectStmt)
    {
        sqlite3_finalize(selectStmt);
        selectStmt = nil;
    }
    
    sqlite3_close(database);
    database = nil;
    
    return returnValue;
    
}

/*
 @Description : This method is used to insert  Analytic Question Answers records if not exist and update if exists
 @author :  Soniya Vishwakarma
 @param :
 Parameter1 arrAnalyticsQues : Array of Anlytics Questions which need to be inserted or updated.
 @return: Bool value to signify the success of the Query
 */
- (BOOL)insertAnalyticsQuestionAnswers:(NSArray *)arrAnalyticsQuesAnswer userid:(NSString*)userid
{
    BOOL returnValue = YES;
    sqlite3_stmt *selectStmt = nil;
    sqlite3_stmt *insertStmt = nil;
    sqlite3_stmt *updateStmt = nil;
    sqlite3 *database = nil;
    
    if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK)
    {
        returnValue = NO;
    }
    
    // to enable the cache memory
    int ret = SQLITE_OPEN_SHAREDCACHE;
    
    if(ret != SQLITE_OK)
    {
        
    }
    const char *filePath=[dataBasepath UTF8String];
    int SqlCode=sqlite3_open(filePath, &database);
    if (SqlCode == SQLITE_OK)
    {
        //Preapare Select Statment
        if(selectStmt == nil)
        {
            NSString *strValue = [NSString stringWithFormat:@"SELECT rowid FROM tbl_AnalysisQuestion_Answers where question_id = ? and user_id=?"];
            const char *SELECTsql = [strValue UTF8String];
            int sql_code = sqlite3_prepare_v2(database, SELECTsql, -1, &selectStmt, NULL);
            SELECTsql=nil;
            if(sql_code != SQLITE_OK)
            {
                NSLog(@"Error while creating Select statement for '%s' on table tbl_AnalysisQuestion_Answers.", sqlite3_errmsg(database));
                returnValue = NO;
            }
            strValue=nil;
            SELECTsql=nil;
        }
        //Preapare Update Statment
        if(updateStmt == nil)
        {
            NSString *strValue = [NSString stringWithFormat:@"UPDATE tbl_AnalysisQuestion_Answers SET Value=?,user_id=? where question_id=?"];
            
            const char *UPDATEsql = [strValue UTF8String];
            int sql_code =sqlite3_prepare_v2(database, UPDATEsql, -1, &updateStmt, NULL);
            UPDATEsql=nil;
            
            if(sql_code != SQLITE_OK)
            {
                NSLog(@"Error while UPDATE '%s' prepare statement. table tbl_AnalysisQuestion_Answers", sqlite3_errmsg(database));
                returnValue = NO;
            }
        }
        
        //Preapare Insert Statment
        if(insertStmt == nil)
        {
            NSString *strValue =[NSString stringWithFormat:@"INSERT INTO tbl_AnalysisQuestion_Answers ('question_id','Value','user_id') Values(?,?,?)"];
            const char *insertSql = [strValue UTF8String];
            int sql_code =sqlite3_prepare_v2(database, insertSql, -1, &insertStmt, NULL);
            insertSql=nil;
            
            if(sql_code != SQLITE_OK)
            {
                NSLog(@"Error while creating insert '%s' add statement. table tbl_AnalysisQuestion_Answers", sqlite3_errmsg(database));
                returnValue = NO;
            }
        }
        
        NSInteger rowidentifier = -1;
        char *sqliteError=nil;
        sqlite3_exec(database, "BEGIN TRANSACTION", NULL, NULL, &sqliteError);
        for (NSDictionary *dictQuestion in arrAnalyticsQuesAnswer) {
        
            if(sqlite3_bind_text(selectStmt, 1, [[dictQuestion objectForKey:@"questionid"] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue= NO;
            }

            if(sqlite3_bind_text(selectStmt, 2, [userid UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue= NO;
            }

            rowidentifier = -1;
        
        // Check whether Record particular id exists or not
        
        while(sqlite3_step(selectStmt) == SQLITE_ROW)
        {
            rowidentifier = sqlite3_column_int(selectStmt, 0);
        }
        sqlite3_reset(selectStmt);
        sqlite3_clear_bindings(selectStmt);
        // If Package id not exists then insert the record.
        
        if(rowidentifier == -1)
        {
            if(sqlite3_bind_text(insertStmt, 1, [[NSString stringWithFormat:@"%@",[dictQuestion objectForKey:@"questionid"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            if(sqlite3_bind_text(insertStmt, 2, [[NSString stringWithFormat:@"%@",[dictQuestion objectForKey:@"answer"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            if(sqlite3_bind_text(insertStmt, 3, [userid UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            
            if(SQLITE_DONE != sqlite3_step(insertStmt))
            {
                NSLog(@"Error while inserting into table tbl_AnalysisQuestion_Answers : '%s'", sqlite3_errmsg(database));
                returnValue = NO;
            }
            sqlite3_reset(insertStmt);
            sqlite3_clear_bindings(insertStmt);
        }
        // If Question id exists then update the record.
        else
        {
            if(sqlite3_bind_text(updateStmt, 1, [[NSString stringWithFormat:@"%@",[dictQuestion objectForKey:@"answer"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            if(sqlite3_bind_text(updateStmt, 2, [userid UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            
            if(sqlite3_bind_text(updateStmt, 3, [[NSString stringWithFormat:@"%@",[dictQuestion objectForKey:@"questionid"]] UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
                returnValue = NO;
            }
            if(SQLITE_DONE != sqlite3_step(updateStmt))
            {
                NSLog(@"Error while updating into table tbl_AnalysisQuestion_Answers : '%s'", sqlite3_errmsg(database));
                returnValue = NO;
            }
            sqlite3_reset(updateStmt);
            sqlite3_clear_bindings(updateStmt);
        }
        }
        sqlite3_exec(database, "END TRANSACTION", NULL, NULL, &sqliteError);
    }
    filePath=nil;
    if (insertStmt)
    {
        sqlite3_finalize(insertStmt);
        insertStmt = nil;
    }
    
    if (updateStmt)
    {
        sqlite3_finalize(updateStmt);
        updateStmt = nil;
    }
    
    if (selectStmt)
    {
        sqlite3_finalize(selectStmt);
        selectStmt = nil;
    }
    
    sqlite3_close(database);
    database = nil;
    
    return returnValue;
    
}

#pragma mark - ====Update====
/*
 @Description : This method update question's Answer
 @author : Soniya Vishwakarma
 Parameter: question id and Answer
 @return: BOOL signify success of the query
 */

- (BOOL)updateAnswer:(NSString*)ans ofQuestionID:(NSString*)questionID
{
    BOOL returnValue = YES;
    sqlite3_stmt *updateStmt = nil;
    sqlite3 *database = nil;
    
    if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK)
    {
        returnValue = NO;
    }
    
    // to enable the cache memory
    int ret = SQLITE_OPEN_SHAREDCACHE;
    
    if(ret != SQLITE_OK)
    {
        
    }
    
    const char *filePath=[dataBasepath UTF8String];
    if (sqlite3_open(filePath, &database) == SQLITE_OK)
    {
        
        //Preapare Update Statment
        if(updateStmt == nil)
        {
            NSString *strValue = [NSString stringWithFormat:@"UPDATE tbl_AnalysisQuestion SET que_ans=? where question_id=?"];
            
            const char *UPDATEsql = [strValue UTF8String];
            
            if(sqlite3_prepare_v2(database, UPDATEsql, -1, &updateStmt, NULL) != SQLITE_OK)
            {
                NSLog(@"Error while UPDATE '%s' prepare statement. table tbl_AnalysisQuestion", sqlite3_errmsg(database));
                returnValue = NO;
            }
        }
        
        if(sqlite3_bind_text(updateStmt, 1, [ans UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
            returnValue = NO;
        }
        if(sqlite3_bind_text(updateStmt, 2, [questionID UTF8String], -1, SQLITE_TRANSIENT) != SQLITE_OK) {
            returnValue = NO;
        }
        if(SQLITE_DONE != sqlite3_step(updateStmt))
        {
            NSLog(@"Error while updating into table tbl_AnalysisQuestion : '%s'", sqlite3_errmsg(database));
            returnValue = NO;
        }
        sqlite3_reset(updateStmt);
    }
    filePath=nil;
    if (updateStmt)
    {
        sqlite3_finalize(updateStmt);
        updateStmt = nil;
    }
    
    
    sqlite3_close(database);
    database = nil;
    
    return returnValue;
    
    
}

#pragma mark - === Select ===
/*
 @Description : This method is used to select count of Question
 @author :  Soniya Vishwakarma
 @return: count of Question
 */
- (NSInteger)selectCountOfQuestions{
    sqlite3_stmt *selectStmt = nil;
    sqlite3 *database = nil;
    NSInteger count=0;
    
    if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK) {
        NSLog(@"Can now use sqlite on multiple threads, using the same connection");
    }
    
    // to enable the cache memory
    int ret = SQLITE_OPEN_SHAREDCACHE;
    
    if(ret != SQLITE_OK)
    {
        
    }
    
    const char *filePath=[dataBasepath UTF8String];
    int SqlCode=sqlite3_open(filePath, &database);
    if (SqlCode == SQLITE_OK)
    {
        if(selectStmt == nil)
        {
            NSString *strquery = @"Select count(*) from tbl_AnalysisQuestion";
            
            const char *SELECTsql = [strquery UTF8String];
            
            if(sqlite3_prepare_v2(database, SELECTsql, -1, &selectStmt, NULL) != SQLITE_OK)
            {
                NSLog(@"Error while creating Select statement for tbl_AnalysisQuestion '%s' .", sqlite3_errmsg(database));
            }
            else  {
                
                while(sqlite3_step(selectStmt) == SQLITE_ROW){
                    
                    count= sqlite3_column_int(selectStmt, 0);
                }
            }
        }
    }
    sqlite3_reset(selectStmt);
    filePath=nil;
    if (selectStmt) {
        sqlite3_finalize(selectStmt);
        selectStmt = nil;
    }    sqlite3_close(database);
    database = nil;
    return count;
    
}
/*
 @Description : This method is used to select all Question Type
 @author :  Soniya Vishwakarma
 @return: array of Question Type
 */
- (NSMutableArray*)selectTypeOfQuestions{
    sqlite3_stmt *selectStmt = nil;
    sqlite3 *database = nil;
    NSMutableArray* arrQuesType = nil;
    
    if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK) {
        NSLog(@"Can now use sqlite on multiple threads, using the same connection");
    }
    
    // to enable the cache memory
    int ret = SQLITE_OPEN_SHAREDCACHE;
    
    if(ret != SQLITE_OK)
    {
        
    }
    
    const char *filePath=[dataBasepath UTF8String];
    int SqlCode=sqlite3_open(filePath, &database);
    if (SqlCode == SQLITE_OK)
    {
        if(selectStmt == nil)
        {
            NSString *strquery = @"Select distinct que_type from tbl_AnalysisQuestion";
            
            const char *SELECTsql = [strquery UTF8String];
            
            if(sqlite3_prepare_v2(database, SELECTsql, -1, &selectStmt, NULL) != SQLITE_OK)
            {
                NSLog(@"Error while creating Select statement for tbl_AnalysisQuestion '%s' .", sqlite3_errmsg(database));
            }
            else  {
                arrQuesType = [[NSMutableArray alloc] init];
                while(sqlite3_step(selectStmt) == SQLITE_ROW){
                    NSString * quesType = @"";
                    if(sqlite3_column_text(selectStmt, 0)!=nil)
                        quesType = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStmt, 0)];
                    [arrQuesType addObject:quesType];
                }
            }
        }
    }
    sqlite3_reset(selectStmt);
    filePath=nil;
    if (selectStmt) {
        sqlite3_finalize(selectStmt);
        selectStmt = nil;
    }    sqlite3_close(database);
    database = nil;
    return arrQuesType;
    
}
/*
 @Description : This method is used to select all records based on pagination
 @author :  Soniya Vishwakarma
 @return: Array of question
 */

- (NSMutableArray *)selectQuestionsOfEachQuestionTypeWithPageNo:(NSInteger)pageNo RecordCount:(NSInteger)recordCount UserID:(NSString*)userid
{
    sqlite3_stmt *selectStmt = nil;
    sqlite3 *database = nil;
    NSMutableArray *arrQuestions = [[NSMutableArray alloc]init];
    NSArray *arrQuesType = [self selectTypeOfQuestions];
    if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK) {
        NSLog(@"Can now use sqlite on multiple threads, using the same connection");
    }
    
    // to enable the cache memory
    int ret = SQLITE_OPEN_SHAREDCACHE;
    
    if(ret != SQLITE_OK)
    {
        
    }
    const char *filePath=[dataBasepath UTF8String];
    if (sqlite3_open(filePath, &database) == SQLITE_OK)
    {
        //Preapare Select Statment
        if(selectStmt == nil)
        {
            NSString *strquery = @"SELECT question_id, question, que_wt,que_type FROM tbl_AnalysisQuestion where  que_type=? limit ?, ?";
            
            const char *SELECTsql = [strquery UTF8String];
            
            if(sqlite3_prepare_v2(database, SELECTsql, -1, &selectStmt, NULL) != SQLITE_OK)
            {
                NSLog(@"Error while creating Select statement for '%s' on table tbl_AnalysisQuestion.", sqlite3_errmsg(database));
            }
        }
        for (NSString *quesType in arrQuesType) {
            
            sqlite3_bind_text(selectStmt, 1, [quesType UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_int(selectStmt, 2, pageNo);
            sqlite3_bind_int(selectStmt, 3, recordCount);
            
            while(sqlite3_step(selectStmt) == SQLITE_ROW){
                    
                NSString * question_id = @"";
                NSString * question = @"";
                NSString * que_wt = @"";
                NSString * que_type = @"";
                NSString * value = @"0";
              
                if(sqlite3_column_text(selectStmt, 0)!=nil)
                    question_id = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStmt, 0)];
                if(sqlite3_column_text(selectStmt, 1)!=nil)
                    question = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStmt, 1)];
                if(sqlite3_column_text(selectStmt, 2)!=nil)
                    que_wt = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStmt, 2)];

                if(sqlite3_column_text(selectStmt, 3)!=nil)
                    que_type = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStmt, 3)];
               
                value = [self selectAnswerValueOfQuestion:question_id ForUser:userid];
                
                NSMutableDictionary *dictQuestion = [[NSMutableDictionary alloc] initWithObjectsAndKeys:question_id,@"question_id",question,@"question",que_wt,@"que_wt",que_type,@"que_type",value,@"que_ans", nil];
                [arrQuestions addObject:dictQuestion];
                    dictQuestion = nil;
                }
            sqlite3_reset(selectStmt);
            }

        }

    filePath=nil;
    if (selectStmt) {
        sqlite3_finalize(selectStmt);
        selectStmt = nil;
    }    sqlite3_close(database);
    database = nil;
    return arrQuestions;
    
}

/*
 @Description : This method is used to select all Question Type
 @author :  Soniya Vishwakarma
 @return: array of Question Type
 */
- (NSString*)selectAnswerValueOfQuestion:(NSString*)qid ForUser:(NSString*)userID{
    sqlite3_stmt *selectStmt = nil;
    sqlite3 *database = nil;
    NSString* value = @"0";
    
    if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK) {
        NSLog(@"Can now use sqlite on multiple threads, using the same connection");
    }
    
    // to enable the cache memory
    int ret = SQLITE_OPEN_SHAREDCACHE;
    
    if(ret != SQLITE_OK)
    {
        
    }
    
    const char *filePath=[dataBasepath UTF8String];
    int SqlCode=sqlite3_open(filePath, &database);
    if (SqlCode == SQLITE_OK)
    {
        if(selectStmt == nil)
        {
            NSString *strquery = @"Select Value from tbl_AnalysisQuestion_Answers where question_id=? and user_id=?";
            
            const char *SELECTsql = [strquery UTF8String];
            
            if(sqlite3_prepare_v2(database, SELECTsql, -1, &selectStmt, NULL) != SQLITE_OK)
            {
                NSLog(@"Error while creating Select statement for tbl_AnalysisQuestion_Answers '%s' .", sqlite3_errmsg(database));
            }
            else  {
                sqlite3_bind_text(selectStmt, 1, [qid UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(selectStmt, 2, [userID UTF8String], -1, SQLITE_TRANSIENT);
                while(sqlite3_step(selectStmt) == SQLITE_ROW){
                    if(sqlite3_column_text(selectStmt, 0)!=nil)
                        value = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStmt, 0)];
                    
                }
            }
        }
    }
    sqlite3_reset(selectStmt);
    filePath=nil;
    if (selectStmt) {
        sqlite3_finalize(selectStmt);
        selectStmt = nil;
    }    sqlite3_close(database);
    database = nil;
    return value;
    
}

/*
 @Description : This method is used to select all Question Answer
 @author :  Soniya Vishwakarma
 @return: array of Question Type
 */
- (NSMutableArray*)selectAnswerValueForUser:(NSString*)userID{
    sqlite3_stmt *selectStmt = nil;
    sqlite3 *database = nil;
    NSMutableArray *arrQuestions = [[NSMutableArray alloc]init];
    
    if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK) {
        NSLog(@"Can now use sqlite on multiple threads, using the same connection");
    }
    
    // to enable the cache memory
    int ret = SQLITE_OPEN_SHAREDCACHE;
    
    if(ret != SQLITE_OK)
    {
        
    }
    
    const char *filePath=[dataBasepath UTF8String];
    int SqlCode=sqlite3_open(filePath, &database);
    if (SqlCode == SQLITE_OK)
    {
        if(selectStmt == nil)
        {
            NSString *strquery = @"Select question_id,Value from tbl_AnalysisQuestion_Answers where user_id=?";
            
            const char *SELECTsql = [strquery UTF8String];
            
            if(sqlite3_prepare_v2(database, SELECTsql, -1, &selectStmt, NULL) != SQLITE_OK)
            {
                NSLog(@"Error while creating Select statement for tbl_AnalysisQuestion_Answers '%s' .", sqlite3_errmsg(database));
            }
            else  {
                sqlite3_bind_text(selectStmt, 1, [userID UTF8String], -1, SQLITE_TRANSIENT);
                while(sqlite3_step(selectStmt) == SQLITE_ROW){
                    NSString * question_id = @"";
                    NSString * value = @"0";
                    
                    if(sqlite3_column_text(selectStmt, 0)!=nil)
                        question_id = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStmt, 0)];
                    if(sqlite3_column_text(selectStmt, 1)!=nil)
                        value = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStmt, 1)];
                    
                    NSMutableDictionary *dictQuestion = [[NSMutableDictionary alloc] initWithObjectsAndKeys:question_id,@"questionid",value,@"wtanswer", nil];
                    [arrQuestions addObject:dictQuestion];
                    dictQuestion = nil;
                    
                }
            }
        }
    }
    sqlite3_reset(selectStmt);
    filePath=nil;
    if (selectStmt) {
        sqlite3_finalize(selectStmt);
        selectStmt = nil;
    }    sqlite3_close(database);
    database = nil;
    return arrQuestions;
    
}
/*
 @Description : This method is used to select answer with non-zero value
 @author :  Soniya Vishwakarma
 @return: array of Question Type
 */
- (NSInteger)selectCountOfNonZeroAnswerValueForUser:(NSString*)userID{
    sqlite3_stmt *selectStmt = nil;
    sqlite3 *database = nil;
     NSInteger count=0;
    
    if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK) {
        NSLog(@"Can now use sqlite on multiple threads, using the same connection");
    }
    
    // to enable the cache memory
    int ret = SQLITE_OPEN_SHAREDCACHE;
    
    if(ret != SQLITE_OK)
    {
        
    }
    
    const char *filePath=[dataBasepath UTF8String];
    int SqlCode=sqlite3_open(filePath, &database);
    if (SqlCode == SQLITE_OK)
    {
        if(selectStmt == nil)
        {
            NSString *strquery = @"Select count(*) from tbl_AnalysisQuestion_Answers where user_id=? and Value!=0";
            
            const char *SELECTsql = [strquery UTF8String];
            
            if(sqlite3_prepare_v2(database, SELECTsql, -1, &selectStmt, NULL) != SQLITE_OK)
            {
                NSLog(@"Error while creating Select statement for tbl_AnalysisQuestion_Answers '%s' .", sqlite3_errmsg(database));
            }
            else  {
                sqlite3_bind_text(selectStmt, 1, [userID UTF8String], -1, SQLITE_TRANSIENT);
                while(sqlite3_step(selectStmt) == SQLITE_ROW){
                     count= sqlite3_column_int(selectStmt, 0);
                    
                }
            }
        }
    }
    sqlite3_reset(selectStmt);
    filePath=nil;
    if (selectStmt) {
        sqlite3_finalize(selectStmt);
        selectStmt = nil;
    }    sqlite3_close(database);
    database = nil;
    return count;
    
}

@end
