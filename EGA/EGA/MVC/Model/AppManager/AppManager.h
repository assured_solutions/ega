//
//  AppManager.h
//  LaunchApp
//
//  Created by Soniya on 09/09/15.
//  Copyright (c) 2015 Assure Solutions Group. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppManager : NSObject

-(void)setAppConfiguration;

@end
