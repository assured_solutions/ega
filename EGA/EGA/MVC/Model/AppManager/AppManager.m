//
//  AppManager.m
//  LaunchApp
//
//  Created by Soniya on 09/09/15.
//  Copyright (c) 2015 Assure Solutions Group. All rights reserved.
//

#import "AppManager.h"


#define ConfigurationFolder @"ConfigurationDetails"
#define ConfigurationFileName @"ConfigurationFile.plist"
#define LogoName @"CompanyLogo.png"
//Keys



@interface AppManager(){
    NSFileManager *fileManager;
}
@end

@implementation AppManager

-(id)init{
    self =[super init];
    if (self) {
        fileManager = [NSFileManager defaultManager];
    }
    return self;
}


-(void)setAppConfiguration{
  
            //Retrive Primary Color
            
            UIColor *color = [UIColor colorWithHexString:THEMECOLOR];
            [ConfigurationFile setThemeColor:color];
            color = nil;
            
            //Retrive Secondary Color
            
            color = [UIColor colorWithHexString:SUBTHEMECOLOR] ;
            [ConfigurationFile setHighlightedThemeColor:color];
             color = nil;
            
            
            //Retrive Background Color
            
            color = [UIColor colorWithHexString:BACKGROUNDCOLOR] ;
            [ConfigurationFile setBackGroundColor:color];
             color = nil;
            
            //Retrive Facebook Link
            
//            value = [dictConfigurationDetails objectForKey:@"facebookLink"];
//            if (value==nil || value.length<=0) {
//                value = FACEBOOK;
//            }
//            [ConfigurationFile setCompanyFacebookLink:value];
//            
//            value = nil;
//            
//            //Retrive LinkedIn Link
//            
//            value = [dictConfigurationDetails objectForKey:@"linkedinLink"];
//            if (value==nil || value.length<=0) {
//                value = LINKEDIN;
//            }
//            [ConfigurationFile setCompanyLinkedInLink:value];
//            
//            value = nil;
//            
//            //Retrive Twitter Link
//            
//            value = [dictConfigurationDetails objectForKey:@"twitterLink"];
//            if (value==nil || value.length<=0) {
//                value = TWITTER;
//            }
//            [ConfigurationFile setCompanyTwitterLink:value];
//            
//            value = nil;
//            
//            //Retrive Website Link
//            
//            value = [dictConfigurationDetails objectForKey:@"website"];
//            if (value==nil || value.length<=0) {
//                value = WEBSITE;
//            }
//            [ConfigurationFile setCompanyLink:value];
//            
//            value = nil;
//            
//            //Retrive Contact Page Link
//            
//            value = [dictConfigurationDetails objectForKey:@"ContactUsPage"];
//            if (value==nil || value.length<=0) {
//                value = CONTACTUSLINK;
//            }
//            [ConfigurationFile setContactUsLink:value];
//            
//            value = nil;
    
    
            //Retrive Contact Mail ID
            
            [ConfigurationFile setContactUsMailID:CONTACTUSEMAIL];
            
    
    
            [ConfigurationFile setCopyRight:COPYRIGHT];
            
    
            //Check user active
            
     }


@end
