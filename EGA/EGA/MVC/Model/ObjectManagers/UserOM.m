//
//  UserOM.m
//  EGA
//
//  Created by Soniya on 29/11/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "UserOM.h"
#import "UserDM.h"

@interface UserOM(){
    AppDelegate *appDelegate;
}
@end

@implementation UserOM

- (id)init
{
    self = [super init];
    if (self != nil)
    {
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    }
    return self;
}

-(void)updateDOBInDB{
    UserDM *objDM = [[UserDM alloc]init];
    [objDM updateDOB:self.str_dob OfUser:appDelegate.userData.str_userID];
    objDM=nil;
}

-(void)updateHeightWeightGenderInDB{
    UserDM *objDM = [[UserDM alloc]init];
    [objDM updateHeight:self.str_height Weight:self.str_weight Gender:self.str_gender OfUser:appDelegate.userData.str_userID];
    objDM=nil;
}
@end
