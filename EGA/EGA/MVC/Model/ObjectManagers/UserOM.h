//
//  UserOM.h
//  EGA
//
//  Created by Soniya on 29/11/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import <Foundation/Foundation.h>
/*
 CREATE TABLE "tbl_User" (user_id VARCHAR, email VARCHAR, pwd VARCHAR, firstname VARCHAR, lastname VARCHAR, phone VARCHAR, dob VARCHAR, gender VARCHAR, bloodgroup VARCHAR, weight VARCHAR, height VARCHAR, current_illness VARCHAR, current_medication VARCHAR, surgical_procedure VARCHAR, drinking_habits VARCHAR, drugs_intake VARCHAR, city_id VARCHAR, state_id VARCHAR, country_id VARCHAR, photopath VARCHAR, user_isactive VARCHAR)
 */


@interface UserOM : NSObject
@property(nonatomic,strong) NSString * str_userID;
@property(nonatomic,strong) NSString * str_email;
@property(nonatomic,strong) NSString * str_pwd;
@property(nonatomic,strong) NSString * str_firstname;
@property(nonatomic,strong) NSString * str_lastname;
@property(nonatomic,strong) NSString * str_phone;
@property(nonatomic,strong) NSString * str_dob;
@property(nonatomic,strong) NSString * str_gender;
@property(nonatomic,strong) NSString * str_bloodgroup;
@property(nonatomic,strong) NSString * str_weight;
@property(nonatomic,strong) NSString * str_height;
@property(nonatomic,strong) NSString * str_current_illness;
@property(nonatomic,strong) NSString * str_current_medication;
@property(nonatomic,strong) NSString * str_surgical_procedure;
@property(nonatomic,strong) NSString * str_drinking_habits;
@property(nonatomic,strong) NSString * str_drugs_intake;
@property(nonatomic,strong) NSString * str_city_id;
@property(nonatomic,strong) NSString * str_state_id;
@property(nonatomic,strong) NSString * str_country_id;
@property(nonatomic,strong) NSString * str_photo;
@property(nonatomic,strong) NSString * str_isActive;

-(void)updateDOBInDB;

-(void)updateHeightWeightGenderInDB;
@end
