//
//  ASShare.h
//  LaunchApp
//
//  Created by Soniya on 26/08/15.
//  Copyright (c) 2015 Assure Solutions Group. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SKPSMTPMessage.h"
#import <MessageUI/MessageUI.h>

@protocol ASShareDelegate;
@interface ASShare : NSObject <SKPSMTPMessageDelegate,MFMailComposeViewControllerDelegate>
{
    SKPSMTPMessage *smtpMessage;
    __unsafe_unretained id <ASShareDelegate> delegate;
    UIActivityViewController *activityViewController;
}
@property(nonatomic, assign) id <ASShareDelegate> delegate;
@property(nonatomic, weak) UIViewController *callingController;

# pragma mark - Send mail

-(void)shareWithActivityIndicator:(NSString*)stringToShare;
-(void)sendMailUsingSMTPWithSubject:(NSString*)subject From:(NSString*)sender To:(NSString*)reciever BCC:(NSString*)bccreciever CC:(NSString*)ccreciver Body:(NSString*)body isHTML:(BOOL)isHtml;
-(void)sendMailWithSubject:(NSString*)mailSubject Body:(NSString*)body IsHtml:(BOOL)isHtml ToRecipients:(NSArray*)toRecipients Attachment:(NSData*)data MimeType:(NSString*)mime FileName:(NSString*)fileName;

@end
@protocol ASShareDelegate <NSObject>

@optional
//SMTP Mail Delegate Methods
-(void)mailSentUsingSMTPSuccessfully:(BOOL)success;
-(void)mailSentWithSuccessfully:(BOOL)success Message:(NSString*)message;
@end
