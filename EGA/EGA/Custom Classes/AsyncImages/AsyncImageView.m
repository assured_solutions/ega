//
//  AsyncImageView.m
//  TechMee
//
//  Created by D Gupta on 7/23/11.
//  Copyright 2011 Vincent Info Solution Pvt Ltd. All rights reserved.
//

#import "AsyncImageView.h"
#import <QuartzCore/QuartzCore.h>
@implementation AsyncImageView
@synthesize myimage;

NSInteger flag;



- (void)loadImageFromURL:(NSString*)strUrl imageview:(UIImageView *)myimageview{
    activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    CGPoint center = myimageview.center;
    activityIndicator.center = center;
    [myimageview addSubview:activityIndicator];
    [activityIndicator startAnimating];
	urlString=strUrl;
   NSURL* url =[[NSURL alloc]initWithString:urlString];
    self.myimage=myimageview;
   
	if (connection!=nil) {
        connection=nil;
    } //in case we are downloading a 2nd image
	if (data!=nil) {
        data=nil;
    }
	
	NSURLRequest* request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
	connection = [[NSURLConnection alloc] initWithRequest:request delegate:self]; //notice how delegate set to self object
	//TODO error handling, what if connection is nil?
}


//the URL connection calls this repeatedly as data arrives
- (void)connection:(NSURLConnection *)theConnection didReceiveData:(NSData *)incrementalData {
	if (data==nil) { data = [[NSMutableData alloc] initWithCapacity:2048]; } 
	[data appendData:incrementalData];
}
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSLog(@"User Login Error during connection: %@", [error description]);
    
    [activityIndicator stopAnimating];
}
//the URL connection calls this once all the data has downloaded
- (void)connectionDidFinishLoading:(NSURLConnection*)theConnection {
	//so self data now has the complete image 
	connection=nil;
	if ([[self subviews] count]>0) {
		//then this must be another image, the old one is still in subviews
		[[[self subviews] objectAtIndex:0] removeFromSuperview]; //so remove it (releases it also)
	}
     UIImage *img=[UIImage imageWithData:data];
    myimage.image=img;
    
    [activityIndicator stopAnimating];
	img=nil;
	data=nil;
}

//just in case you want to get the image directly, here it is in subviews
- (UIImage*) image {
	UIImageView* iv = [[self subviews] objectAtIndex:0];
	return [iv image];
}

@end
