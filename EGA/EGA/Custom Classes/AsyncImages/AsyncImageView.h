//
//  AsyncImageView.h
//  TechMee
//
//  Created by D Gupta on 7/23/11.
//  Copyright 2011 Vincent Info Solution Pvt Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface AsyncImageView : UIView {
	//could instead be a subclass of UIImageView instead of UIView, depending on what other features you want to 
	// to build into this class?
	UIImageView *myimage;
	NSURLConnection* connection; //keep a reference to the connection so we can cancel download in dealloc
	NSMutableData* data; //keep reference to the data so we can collect it as it downloads
	//but where is the UIImage reference? We keep it in self.subviews - no need to re-code what we have in the parent class
    NSString *urlString;
    UIActivityIndicatorView *activityIndicator;

}
@property (nonatomic,retain)UIImageView *myimage;

- (void)loadImageFromURL:(NSString*)strUrl imageview:(UIImageView *)myimageview;

@end