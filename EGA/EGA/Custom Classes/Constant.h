//
//  Constant.h
//  LoanCalculator
//
//  Created by Somya Bhatnagar on 22/05/2015.
//  Copyright (c) 2015 Somya Bhatnagar. All rights reserved.
//

#ifndef LoanCalculator_Constant_h
#define LoanCalculator_Constant_h


//----- WEBSITE URLS -----//

#define PRIVACY @"http://ega.web1demos.com/PrivacyPolicy.html"
#define TERMSANDCONDITION @"http://ega.web1demos.com/TermsofService.html"
#define CONTACTUSEMAIL @""
//--------- DISCLAIMER ----------//
#define DISCLAIMER @"This application is provided only as general self-help Planning Tools. Results depend on many factors, including the assumptions you provide. We do not guarantee their accuracy, or applicability to your circumstances.\n\n These Planning Tools are not offers, representations or warranties by us, and do not describe any particular products or services they offer."


#define COPYRIGHT @"Copyright (c) 2015 EGA. All rights reserved. (Version 1.0)"

//--------- BASEURL ----------//

# define BASEURL @"http://ega.web1demos.com/webservices/%@"
# define EGAURL @"http://ega.web1demos.com/"

//----- Colors -----//

#define THEMECOLOR @"#fe7800"
#define SUBTHEMECOLOR @"#ff6000"
#define BACKGROUNDCOLOR @"#f0f1f5"
#define DARKGRAYTEXTCOLOR @"#3b3e4a"
#define GRAYTEXTCOLOR @"#444444"
#define SEPARATORCOLOR @"#d2d3d8"
#define BORDERCOLOR @"#d9d9df"
#define GRAYCOLOR  [UIColor colorWithRed:236/255.0f green:236/255.0f blue:236/255.0f alpha:1.0]
#define GREENCOLOR @"#6DC58A"
//----Error Message---//
#define NETWORKERRORMESSAGE @"Network is either slow or not Connected, Please check network to proceed."
#define SERVERERRORMESSAGE @"Sorry, due to some problem,we can't fulfill your current request, Please try later."

#define ExpireDate @"2015-09-25"

#define shareText @"EGA, a health application is mainly for the people who are obese and wish to get filter. EGA provides a platform for consultation, indian recipes for healthy cooking and a host of other features that puts it miles ahead of competition. One of the Best Diet App! Give it a Try!"

#define webserviceCallerFinishedKey @"webServiceCallerFinished"

//-----Constants Value----//

#define DBNAME @"EGADB.sqlite"
#define BMI 24.9

//-----App Constant----//
#define KG @"Kg"
#define LBS @"lbs"


#define DisplayDateFormat @"dd MMM yyyy"

#endif
