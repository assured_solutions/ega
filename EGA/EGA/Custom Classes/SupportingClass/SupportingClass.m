//
//  SupportingClass.m
//  LaunchApp
//
//  Created by Soniya on 25/08/15.
//  Copyright (c) 2015 Assure Solutions Group. All rights reserved.
//

#import "SupportingClass.h"

@implementation SupportingClass

#pragma mark - Error Messages

+ (NSString*)invalidEmaildIDFormatErrorMessage {
    return @"Please provide valid email id";
}
+ (NSString*)blankEmailIDErrorMessage{
    return @"Please provide email id";
}
+ (NSString*)blankPasswordErrorMessage{
    return @"Please provide password";
}
+ (NSString*)invalidDigitsInPasswordErrorMessage{
    return @"Password should be of minimum 6 character";
}
+ (NSString*)blankFnameErrorMessage{
    return @"Please provide your first name";
}
+ (NSString*)blankLnameErrorMessage{
    return @"Please provide your last name";
}
+ (NSString*)blankPhoneNumberErrorMessage{
    return @"Please provide your mobile number";
}
+ (NSString*)invalidPhoneNumberErrorMessage{
    return @"Please provide 10 digits mobile number";
}
+ (NSString*)blankConfirmPasswordErrorMessage{
    return @"Please confirm password";
}
+ (NSString*)passwordMissMatchErrorMessage{
    return @"Password miss match";
}
+ (NSString*)blankDOBErrorMessage{
    return @"Please provide your date of birth";
}
+ (NSString*)networkErrorMessage{
    return @"Network is either slow or not Connected";
}
+ (NSString*)invalidLoginErrorMessage{
    return @"Invalid username or password";
}
+ (NSString*)blankAddressErrorMessage{
    return @"Please provide your address";
}
+ (NSString*)blankStateErrorMessage{
    return @"Please provide your state";
}
+ (NSString*)blankZipCodeErrorMessage{
    return @"Please provide your zipcode";
}
+ (NSString*)blankCountryErrorMessage{
    return @"Please provide your country";
}
+ (NSString*)blankCityErrorMessage{
    return @"Please provide your city";
}
+ (NSString*)specialCharacterErrorMessage{
    return @"Special Characters not allowed";
}

#pragma mark - Date Formattor

+ (NSString*)dateStandardFormat{
    return @"yyyy-MM-dd HH:mm:ss";
}
+ (NSString*)date120Format{
    return @"MM/dd/yyyy hh:mm:ss a";
}

+ (NSString*)dateFormat{
    return @"yyyy-MM-dd";
}

+ (NSDateFormatter *)dateFormatter:(NSString *)dateFormat
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    dateFormatter.dateFormat = dateFormat;
    return dateFormatter;
}

+ (NSString *)getStringDate:(NSString*)strDate From:(NSString *)fromDateFormat to:(NSString*)toDateFormat{

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    dateFormatter.dateFormat = fromDateFormat;
    NSDate *date = [dateFormatter dateFromString:strDate];
    dateFormatter.dateFormat = toDateFormat;
    return  [dateFormatter stringFromDate:date];
}

// Alert Supporting Methods

+ (void)showAlertWithTitle:(NSString*)title Message:(NSString*)message CancelButtonTitle:(NSString*)cancelTitle{
   
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:cancelTitle otherButtonTitles:nil];
    [alertView show];
    alertView = nil;
}

+ (void)showAlertWithTitle:(NSString*)title Message:(NSString*)message CancelButtonTitle:(NSString*)cancelTitle OtherButton:(NSString*)otherButtonTitle Delegate:(id/*<UIAlertViewDelegate>*/)delegate{
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:delegate cancelButtonTitle:cancelTitle otherButtonTitles:otherButtonTitle,nil];
    [alertView show];
}

+(void)showErrorMessage:(NSString*)errorMessage{
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [ProgressHUD showError:errorMessage Interaction:NO];
    });
    
}
+ (UIViewController *)currentTopViewController
{
    UIViewController *topVC = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    while (topVC.presentedViewController)
    {
        topVC = topVC.presentedViewController;
    }
    return topVC;
}
// MARK: - Validate

+(BOOL) isNSStringValidEmail:(NSString *)emailString
{
    BOOL stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailString];
}


//MARK : - Special character checking in string

+(BOOL) isHavingSpecialCharacter:(NSString *)string{
    NSCharacterSet *set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"] invertedSet];
    if ([string rangeOfCharacterFromSet:set].location == NSNotFound) {
        //No special character exists
        return NO;
    }
    //special character exists
    return YES;
}

+(BOOL)isValidUrlString:(NSString*)url{
    
//    NSURLRequest *req = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
//    bool valid = [NSURLConnection canHandleRequest:req];
//    return valid;
    
//    NSURL *URL = [NSURL URLWithString:url];
//    NSLog(@"%@", [URL host]);
//    if (URL==nil) {
//        return NO;
//    }
//    return YES;
    

    NSString *urlRegEx =
    @"((https|http)://)((\\w|-)+)(([.]|[/])((\\w|-)+))+";
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    return [urlTest evaluateWithObject:url];
}

+(NSString*)getValidUrl:(NSString*)url{
    url = [url stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (url.length>0) {
        if (![url hasPrefix:@"http://"] && ![url hasPrefix:@"https://"])
        {
            url = [NSString stringWithFormat:@"http://%@",url];
        }
        url = [url stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    }
    return url;
}
#pragma mark -  Formattor

+(NSString*)formatNumber:(NSString*)mobileNumber
{
    
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    NSLog(@"%@", mobileNumber);
    
    int length = (int)[mobileNumber length];
    if(length > 10)
    {
        mobileNumber = [mobileNumber substringFromIndex: length-10];
        NSLog(@"%@", mobileNumber);
        
    }
    
    
    return mobileNumber;
}


+(int)getLength:(NSString*)mobileNumber
{
    
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    int length =  (int)[mobileNumber length];
    
    return length;
    
    
}

+(NSString*)getFormattedPhoneNumber:(NSString*)mobileNumber{
    static NSCharacterSet* set = nil;
    if (set == nil){
        set = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    }
    NSString* phoneString = [[mobileNumber componentsSeparatedByCharactersInSet:set] componentsJoinedByString:@""];
    switch (phoneString.length) {
        case 7: return [NSString stringWithFormat:@"%@-%@", [phoneString substringToIndex:3], [phoneString substringFromIndex:3]];
        case 10: return [NSString stringWithFormat:@"(%@) %@-%@", [phoneString substringToIndex:3], [phoneString substringWithRange:NSMakeRange(3, 3)],[phoneString substringFromIndex:6]];
        case 11: return [NSString stringWithFormat:@"%@ (%@) %@-%@", [phoneString substringToIndex:1], [phoneString substringWithRange:NSMakeRange(1, 3)], [phoneString substringWithRange:NSMakeRange(4, 3)], [phoneString substringFromIndex:7]];
        case 12: return [NSString stringWithFormat:@"+%@ (%@) %@-%@", [phoneString substringToIndex:2], [phoneString substringWithRange:NSMakeRange(2, 3)], [phoneString substringWithRange:NSMakeRange(5, 3)], [phoneString substringFromIndex:8]];
        default: return nil;
    }
}

+(NSString*)getNonFormattedPhoneNumber:(NSString*)mobileNumber
{
    
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    return mobileNumber;
    
}


+(NSString*)changeFormatOfPhoneNumber:(NSString*)phoneNumber Range:(NSRange)range{
    int length = [SupportingClass getLength:phoneNumber];
    NSString *strPhoneNumber = phoneNumber;
    //NSLog(@"Length  =  %d ",length);
    
    // Write this body in - (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
//    if(length == 10)
//    {
//        if(range.length == 0)
//            return NO;
//    }
    
    if(length == 3)
    {
        NSString *num = [SupportingClass formatNumber:phoneNumber];
        strPhoneNumber = [NSString stringWithFormat:@"(%@) ",num];
        if(range.length > 0)
            strPhoneNumber = [NSString stringWithFormat:@"%@",[num substringToIndex:3]];
    }
    else if(length == 6)
    {
        NSString *num = [SupportingClass formatNumber:phoneNumber];
        //NSLog(@"%@",[num  substringToIndex:3]);
        //NSLog(@"%@",[num substringFromIndex:3]);
        strPhoneNumber = [NSString stringWithFormat:@"(%@) %@-",[num  substringToIndex:3],[num substringFromIndex:3]];
        if(range.length > 0)
            strPhoneNumber = [NSString stringWithFormat:@"(%@) %@",[num substringToIndex:3],[num substringFromIndex:3]];
    }
    return strPhoneNumber;
}


+(NSString*)getThousandSeparatorNumberFormat:(NSString*)numberToFormat{
    numberToFormat = [SupportingClass getNonSeparatorNumberFormat:numberToFormat];
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setGroupingSeparator:@","];
    [numberFormatter setGroupingSize:3];
    [numberFormatter setUsesGroupingSeparator:YES];
    [numberFormatter setDecimalSeparator:@"."];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [numberFormatter setMaximumFractionDigits:2];
    return [numberFormatter stringFromNumber:[NSNumber numberWithDouble:[numberToFormat doubleValue]]];
}
+(NSString*)getNonSeparatorNumberFormat:(NSString*)numberToFormat{
   numberToFormat = [numberToFormat stringByReplacingOccurrencesOfString:@"," withString:@""];

    return numberToFormat;
}
#pragma mark - Animation

+(void) animateViewWithType:(NSString *)type SubType:(NSString*)subType OnView:(UIView*)view Duration:(CFTimeInterval)duration
{
    CATransition *animation = [CATransition animation];
    animation.delegate = self;
    [animation  setDuration:duration];
    animation.type=type;
    animation.subtype=subType;
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
    [view.layer addAnimation:animation forKey:nil];
    
    
}

#pragma mark - Get Path

+(NSString*)getDocumentPathForComponent:(NSString*)component
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:component];
    paths=nil;
    documentsDirectory=nil;
    return path;
}

+(BOOL)saveImageInDocumentFolder:(UIImage*)img FolderName:(NSString*)folderName FileName:(NSString*)fileName{
    BOOL success = NO;
    
    if (img!=nil) {
        NSData *imgData = UIImagePNGRepresentation(img);
        if (imgData!=nil&&imgData.length>0) {
            NSString *dirToCreate = [SupportingClass getDocumentPathForComponent:folderName];
            
            NSError *error = nil;
            if (![[NSFileManager defaultManager] fileExistsAtPath:dirToCreate])
                [[NSFileManager defaultManager] createDirectoryAtPath:dirToCreate withIntermediateDirectories:NO attributes:nil error:&error];
            
            NSString *savedFileName = [dirToCreate stringByAppendingFormat:@"/%@",fileName];
            BOOL successs = [imgData writeToFile:savedFileName atomically:YES];
            
            if (!successs)
            {
                NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
            }
            else
            {
                success = YES;
                AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
                [appDelegate addSkipBackupAttributeToItemAtURL:savedFileName];
            }
            
        }
    }
    
    return success;
    
}

@end
