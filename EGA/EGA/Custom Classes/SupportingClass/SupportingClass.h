/*
 Class Name : SupportingClass
 Author : Soniya
 Dated : 24/08/15
 Description : This class is basically used to support the application and will contain code which is repeatedly used in an app.
 Copyright (c) 2015 Assure Solutions Group. All rights reserved.
 */

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface SupportingClass : NSObject

// Error Messages

+ (NSString*)invalidEmaildIDFormatErrorMessage ;

+ (NSString*)blankEmailIDErrorMessage;

+ (NSString*)blankPasswordErrorMessage;

+ (NSString*)invalidDigitsInPasswordErrorMessage;

+ (NSString*)blankFnameErrorMessage;

+ (NSString*)blankLnameErrorMessage;

+ (NSString*)blankPhoneNumberErrorMessage;

+ (NSString*)invalidPhoneNumberErrorMessage;

+ (NSString*)blankConfirmPasswordErrorMessage;

+ (NSString*)passwordMissMatchErrorMessage;

+ (NSString*)blankDOBErrorMessage;

+ (NSString*)networkErrorMessage;

+ (NSString*)invalidLoginErrorMessage;

+ (NSString*)blankAddressErrorMessage;

+ (NSString*)blankStateErrorMessage;

+ (NSString*)blankZipCodeErrorMessage;

+ (NSString*)blankCountryErrorMessage;

+ (NSString*)blankCityErrorMessage;

+ (NSString*)specialCharacterErrorMessage;

// Formats

+ (NSString*)dateStandardFormat;

+ (NSString*)date120Format;

+ (NSString*)dateFormat;

+ (NSDateFormatter *)dateFormatter:(NSString *)dateFormat;

+ (NSString *)getStringDate:(NSString*)strDate From:(NSString *)fromDateFormat to:(NSString*)toDateFormat;

// Alert Supporting Methods

+ (void)showAlertWithTitle:(NSString*)title Message:(NSString*)message CancelButtonTitle:(NSString*)cancelTitle;

+ (void)showAlertWithTitle:(NSString*)title Message:(NSString*)message CancelButtonTitle:(NSString*)cancelTitle OtherButton:(NSString*)otherButtonTitle Delegate:(id/*<UIAlertViewDelegate>*/)delegate;

+(void)showErrorMessage:(NSString*)errorMessage;

+ (UIViewController *)currentTopViewController;

// Validate

+(BOOL) isNSStringValidEmail:(NSString *)emailString;

// Special character checking in string

+(BOOL) isHavingSpecialCharacter:(NSString *)string;

+(BOOL)isValidUrlString:(NSString*)url;

+(NSString*)getValidUrl:(NSString*)url;

//  Formatting

+(NSString*)formatNumber:(NSString*)mobileNumber;
+(int)getLength:(NSString*)mobileNumber;
+(NSString*)changeFormatOfPhoneNumber:(NSString*)phoneNumber Range:(NSRange)range;
+(NSString*)getNonFormattedPhoneNumber:(NSString*)mobileNumber;
+(NSString*)getFormattedPhoneNumber:(NSString*)mobileNumber;
+(NSString*)getThousandSeparatorNumberFormat:(NSString*)numberToFormat;
+(NSString*)getNonSeparatorNumberFormat:(NSString*)numberToFormat;

// View Animation

+(void) animateViewWithType:(NSString *)type SubType:(NSString*)subType OnView:(UIView*)view Duration:(CFTimeInterval)duration
;

// Get Path

+(NSString*)getDocumentPathForComponent:(NSString*)component;
+(BOOL)saveImageInDocumentFolder:(UIImage*)img FolderName:(NSString*)folderName FileName:(NSString*)fileName;

@end
