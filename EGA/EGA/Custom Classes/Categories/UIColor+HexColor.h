//
//  UIColor+HexColor.h
//  LaunchApp
//
//  Created by Soniya on 09/09/15.
//  Copyright (c) 2015 Assure Solutions Group. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (HexColor)
+ (UIColor *)colorWithHex:(UInt32)col;
+ (UIColor *)colorWithHexString:(NSString *)str;
@end
