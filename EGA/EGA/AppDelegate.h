//
//  AppDelegate.h
//  EGA
//
//  Created by animesh bansal on 9/26/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import "UserOM.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    Reachability *internetReach;
}

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic,readwrite) BOOL isNetAvailable;
@property (nonatomic,strong) UserOM * userData;

//Syncing results with messages
@property (nonatomic,readwrite) BOOL isDietPlanExpire;
@property (nonatomic,strong) NSString *strMessageRegardingDietPlan;
@property (readwrite,nonatomic)NSInteger intStatusCode;


-(void)copyDatabaseIfNeeded;
-(NSString*)getDBPath;
-(BOOL)addSkipBackupAttributeToItemAtURL:(NSString *)URLString;
@end

