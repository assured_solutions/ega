//
//  AppDelegate.h
//  EGA
//
//  Created by animesh bansal on 9/26/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    Reachability *internetReach;
}

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic,readwrite) BOOL isNetAvailable;


-(void)copyDatabaseIfNeeded;
-(NSString*)getDBPath;
@end

