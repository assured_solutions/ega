//
//  ASShare.m
//  LaunchApp
//
//  Created by Soniya on 26/08/15.
//  Copyright (c) 2015 Assure Solutions Group. All rights reserved.
//

#import "ASShare.h"

#import "NSData+Base64Additions.h"
@implementation ASShare
@synthesize delegate;
@synthesize callingController;

# pragma mark - Send mail using SMTP


-(void)sendMailUsingSMTPWithSubject:(NSString*)subject From:(NSString*)sender To:(NSString*)reciever BCC:(NSString*)bccreciever CC:(NSString*)ccreciver Body:(NSString*)body isHTML:(BOOL)isHtml
{
smtpMessage = [[SKPSMTPMessage alloc] init];
    smtpMessage.fromEmail = @"soniya.vishwakarma@Assure Solutions Group.com";
    smtpMessage.toEmail = reciever;

    if (ccreciver.length!=0){
        smtpMessage.ccEmail = ccreciver;
    }
    if (bccreciever.length!=0){
        smtpMessage.bccEmail = bccreciever;
    }

    //Set Your mail server setting here
    smtpMessage.relayHost = @"smtp.gmail.com";
    smtpMessage.requiresAuth = YES;

if (smtpMessage.requiresAuth) {
    smtpMessage.login = @"soniya.vishwakarma@Assure Solutions Group.com";
    smtpMessage.pass = @"@Soni321!";
}

    smtpMessage.wantsSecure = YES; // smtp.gmail.com doesn't work without TLS!


smtpMessage.subject = subject;

// Only do this for self-signed certs!
// smtpMessage.validateSSLChain = NO;
smtpMessage.delegate = self;
    
    NSString *mimeType = @"text/plain";
    if (isHtml){
        mimeType = @"text/html";
    }
    
NSDictionary *htmlPart = [NSDictionary dictionaryWithObjectsAndKeys:mimeType,kSKPSMTPPartContentTypeKey,body,kSKPSMTPPartMessageKey,@"8bit",kSKPSMTPPartContentTransferEncodingKey, nil];
    smtpMessage.parts = [NSArray arrayWithObjects:htmlPart,nil];
  
    /*
    // FOR Sending Attachments
//NSDictionary *plainPart = [NSDictionary dictionaryWithObjectsAndKeys:@"text/plain; charset=UTF-8",kSKPSMTPPartContentTypeKey,
//                           @"This is a tést messåge.",kSKPSMTPPartMessageKey,@"8bit",kSKPSMTPPartContentTransferEncodingKey,nil];
//
//NSString *vcfPath = [[NSBundle mainBundle] pathForResource:@"test" ofType:@"vcf"];
//NSData *vcfData = [NSData dataWithContentsOfFile:vcfPath];
//
//NSDictionary *vcfPart = [NSDictionary dictionaryWithObjectsAndKeys:@"text/directory;\r\n\tx-unix-mode=0644;\r\n\tname=\"test.vcf\"",kSKPSMTPPartContentTypeKey,
//                         @"attachment;\r\n\tfilename=\"test.vcf\"",kSKPSMTPPartContentDispositionKey,[vcfData encodeBase64ForData],kSKPSMTPPartMessageKey,@"base64",kSKPSMTPPartContentTransferEncodingKey,nil];
//
//testMsg.parts = [NSArray arrayWithObjects:plainPart,vcfPart,nil];
    */
dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    [smtpMessage send];
});
}
- (void)messageSent:(SKPSMTPMessage *)message
{
    NSLog(@"delegate - message sent");
    if(self.delegate!=nil && [(id)[self delegate] respondsToSelector:@selector(mailSentUsingSMTPSuccessfully:)])
    {
        [(id)[self delegate] mailSentUsingSMTPSuccessfully:YES];
    }
}

- (void)messageFailed:(SKPSMTPMessage *)message error:(NSError *)error
{
    if(self.delegate!=nil && [(id)[self delegate] respondsToSelector:@selector(mailSentUsingSMTPSuccessfully:)])
    {
        [(id)[self delegate] mailSentUsingSMTPSuccessfully:NO];
    }
   NSLog(@"delegate - error(%ld): %@", (long)[error code], [error localizedDescription]);
}

# pragma mark - Send mail using Mail

-(void)sendMailWithSubject:(NSString*)mailSubject Body:(NSString*)body IsHtml:(BOOL)isHtml ToRecipients:(NSArray*)toRecipients Attachment:(NSData*)data MimeType:(NSString*)mime FileName:(NSString*)fileName{
    if ([MFMailComposeViewController canSendMail]){
       MFMailComposeViewController* mfMailComposerVC = [[MFMailComposeViewController alloc] init];
        mfMailComposerVC.mailComposeDelegate = self;
        
        [mfMailComposerVC setSubject:mailSubject];
        [mfMailComposerVC setMessageBody:body isHTML:isHtml];
        if (data.length!=0) {
            [mfMailComposerVC addAttachmentData:data mimeType:mime fileName:fileName];
            
        }
        if (toRecipients.count!=0) {
            [mfMailComposerVC setToRecipients:toRecipients];
        }
        [callingController presentViewController:mfMailComposerVC animated:YES completion:nil];
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your account is not set up for sending email" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        alert=nil;
        return;
    }
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
NSString *strMessageResult = nil;
    BOOL success = NO;
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            success = YES;
            strMessageResult = @"Mail saved";
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            success = YES;
            strMessageResult = @"Mail sent";
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            success = NO;
            strMessageResult = @"Mail Failed";
            break;
        default:
            break;
    }
    if(self.delegate!=nil && [(id)[self delegate] respondsToSelector:@selector(mailSentWithSuccessfully:Message:)])
    {
        [(id)[self delegate] mailSentWithSuccessfully:success Message:strMessageResult];
    }

    // Close the Mail Interface
    [callingController dismissViewControllerAnimated:YES completion:NULL];
}



@end
