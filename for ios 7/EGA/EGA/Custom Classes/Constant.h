//
//  Constant.h
//  LoanCalculator
//
//  Created by Somya Bhatnagar on 22/05/2015.
//  Copyright (c) 2015 Somya Bhatnagar. All rights reserved.
//

#ifndef LoanCalculator_Constant_h
#define LoanCalculator_Constant_h


//----- WEBSITE URLS -----//

#define WEBSITE @"http://www.Assure Solutions Group.com"

#define FACEBOOK @"https://www.facebook.com/Assure Solutions Group"

#define TWITTER @"https://twitter.com/Assure Solutions Group"

#define LINKEDIN @"https://www.linkedin.com/company/Assure Solutions Group"

#define NEWSLETTER @"http://www.Assure Solutions Group.com/index.php/blog/"

#define YOUTUBELINK @"https://www.youtube.com/channel/UCW1hOKrfd9UsCzR7l3XVsEg"

#define CONTACTUSEMAIL @"info@Assure Solutions Group.com"

#define CONTACTUSLINK @"http://www.Assure Solutions Group.com/index.php/contact/"

#define YOUTUBEID @"ElfBCCV7DWY"


//------- LOCATIONS ---------//

#define ADDRESS @"1/17 Castlereagh St, Sydney NSW 2000"

#define REDIATM @"http://www.rediatm.com.au/atm-locator"


//--------- DISCLAIMER ----------//
#define DISCLAIMER @"These calculators are provided only as general self-help Planning Tools. Results depend on many factors, including the assumptions you provide. We do not guarantee their accuracy, or applicability to your circumstances.\n\n These Planning Tools are not offers, representations or warranties by us, and do not describe any particular products or services they offer. The specific values shown in these calculations may not be identical to the values of the insurance or financial products described in them and offered by any financial institution."


#define COPYRIGHT @"Copyright (c) 2015 Assure Solutions Group. All rights reserved."



//----- Colors -----//

#define THEMECOLOR @"#fe7800"
#define SUBTHEMECOLOR @"#ff6000"
#define BACKGROUNDCOLOR @"#f0f1f5"
#define DARKGRAYTEXTCOLOR @"#3b3e4a"
#define GRAYTEXTCOLOR @"#444444"
#define SEPARATORCOLOR @"#d2d3d8"
#define GRAYCOLOR  [UIColor colorWithRed:236/255.0f green:236/255.0f blue:236/255.0f alpha:1.0]
#define GREENCOLORE @"#"
//----Error Message---//
#define NETWORKERRORMESSAGE @"Network is either slow or not Connected, Please check network to proceed."
#define SERVERERRORMESSAGE @"Sorry, due to some problem,we can't fulfill your current request, Please try later."

#define ExpireDate @"2015-09-25"

//-----Default Logo----//

#define Assure Solutions GroupLOGO [UIImage imageNamed:@"Assure Solutions GroupLogo.png"];


#define webserviceCallerFinishedKey @"webServiceCallerFinished"

//-----Constants Value----//

#define DBNAME @"EGADB.sqlite"

//-----App Constant----//
#define KG @"Kg"
#define LBS @"lbs"

#endif
