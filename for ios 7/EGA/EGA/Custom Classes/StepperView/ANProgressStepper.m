//
//  ANProgressStepper.m
//  ANProgressStepper
//
//  Created by Ayush Newatia on 15/02/2015.
//  Copyright (c) 2015 Spectrum. All rights reserved.
//

#import "ANProgressStepper.h"

@interface ANProgressStepper()

@property (nonatomic, strong) NSParagraphStyle *paragraphStyle;

@end

@implementation ANProgressStepper

- (void)drawRect:(CGRect)rect
{
    CGFloat borderThickness = self.lineThickness;
    
    CGFloat x = rect.origin.x+borderThickness;
    CGFloat y = rect.origin.y+borderThickness;
    CGFloat height = rect.size.height-(borderThickness*2);
    CGFloat width = rect.size.width-(borderThickness*2);
    
    CGFloat step = [self stepSize:CGRectMake(x, y, width, height)];

    for (int i = 0; i < self.numberOfSteps; i++)
    {
        UIBezierPath *circlePath = [UIBezierPath bezierPathWithOvalInRect: CGRectMake(x+(step*i), y, height, height)];
        circlePath.lineWidth = self.lineThickness;
        [self.borderColor setStroke];
        UIColor *numberColor = nil;
        
        if (i < self.currentStep)
        {
            [self.completeColor setFill];
            numberColor = self.completeNumberColor;
        }
        else if (i == self.currentStep)
        {
            [self.activeColor setFill];
            numberColor = self.activeNumberColor;
        }
        else
        {
            [self.incompleteColor setFill];
            numberColor = self.incompleteNumberColor;
        }
        
        [circlePath stroke];
        [circlePath fill];
        
        NSString *textContent = [NSString stringWithFormat:@"%d",(i+1)];
        
        NSDictionary *attrs = @{NSFontAttributeName:self.font,
                                NSForegroundColorAttributeName:numberColor,
                                NSParagraphStyleAttributeName:self.paragraphStyle};
        
        [textContent drawInRect:CGRectMake(x+(step*i), ceil((self.font.pointSize)/2+self.offset)+4, height, self.font.pointSize)
                 withAttributes:attrs];
        
        
        if (self.showLinesBetweenSteps)
        {
            if (i < self.numberOfSteps-1)
            {
                UIBezierPath *bezierPath = UIBezierPath.bezierPath;
                bezierPath.lineWidth = self.lineThickness;
                [bezierPath moveToPoint:CGPointMake(x+(step*i)+height, y+(height/2))];
                [bezierPath addLineToPoint:CGPointMake(x+(step*(i+1)), y+(height/2))];
                [self.borderColor setStroke];
                [bezierPath stroke];
            }
        }
    }
}


- (CGFloat)stepSize:(CGRect)rect
{
    CGFloat height = rect.size.height;
    CGFloat width = rect.size.width;
    
    return (width-height)/(self.numberOfSteps-1);
}


- (void)setCurrentStep:(NSUInteger)currentStep
{
    _currentStep = currentStep - 1;
}


- (UIColor *)defaultNumberColor
{
    return [UIColor blackColor];
}

# pragma mark - Lazy Getters

- (NSParagraphStyle *)paragraphStyle
{
    if (!_paragraphStyle)
    {
        NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
        [style setAlignment:NSTextAlignmentCenter];
        _paragraphStyle = style;
    }
    return _paragraphStyle;
}

- (UIFont *)font
{
    if (!_font)
    {

         if (_numberSize)
        {
            _font = [UIFont systemFontOfSize:self.numberSize];
        }
        else
        {
            _font = [UIFont systemFontOfSize:self.bounds.size.height/2];
        }
    }
    return _font;
}



- (UIColor *)incompleteNumberColor
{
    if (!_incompleteNumberColor)
    {
        if (!_incompleteNumberColor){
            _incompleteNumberColor = [self defaultNumberColor];
        }
    }
    return _incompleteNumberColor;
}


- (UIColor *)completeNumberColor
{
    if (!_completeNumberColor)
    {
            _completeNumberColor = [self defaultNumberColor];
    }
    return _completeNumberColor;
}


- (UIColor *)activeNumberColor
{
    if (!_activeNumberColor)
    {
        _activeNumberColor = [self defaultNumberColor];
    }
    return _activeNumberColor;
}

- (CGFloat)lineThickness
{
   
        if (_lineThickness<=0)
        {
            _lineThickness = 2.0f;
        }
    
    return _lineThickness;
}

- (CGFloat)offset
{
    if (_offset<=0)
    {
       
  _offset = 0.0f;
        
    }
    return _offset;
}

-(void)reloadStepper{
    [self setNeedsDisplay];
}
@end
