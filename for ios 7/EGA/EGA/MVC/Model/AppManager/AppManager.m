//
//  AppManager.m
//  LaunchApp
//
//  Created by Soniya on 09/09/15.
//  Copyright (c) 2015 Assure Solutions Group. All rights reserved.
//

#import "AppManager.h"


#define ConfigurationFolder @"ConfigurationDetails"
#define ConfigurationFileName @"ConfigurationFile.plist"
#define LogoName @"CompanyLogo.png"
//Keys



@interface AppManager(){
    NSFileManager *fileManager;
}
@end

@implementation AppManager

-(id)init{
    self =[super init];
    if (self) {
        fileManager = [NSFileManager defaultManager];
    }
    return self;
}


-(void)setAppConfiguration{
  
            //Retrive Primary Color
            
            UIColor *color = [UIColor colorWithHexString:THEMECOLOR];
            [ConfigurationFile setThemeColor:color];
            color = nil;
            
            //Retrive Secondary Color
            
            color = [UIColor colorWithHexString:SUBTHEMECOLOR] ;
            [ConfigurationFile setHighlightedThemeColor:color];
             color = nil;
            
            
            //Retrive Background Color
            
            color = [UIColor colorWithHexString:BACKGROUNDCOLOR] ;
            [ConfigurationFile setBackGroundColor:color];
             color = nil;
            
            //Retrive Facebook Link
            
//            value = [dictConfigurationDetails objectForKey:@"facebookLink"];
//            if (value==nil || value.length<=0) {
//                value = FACEBOOK;
//            }
//            [ConfigurationFile setCompanyFacebookLink:value];
//            
//            value = nil;
//            
//            //Retrive LinkedIn Link
//            
//            value = [dictConfigurationDetails objectForKey:@"linkedinLink"];
//            if (value==nil || value.length<=0) {
//                value = LINKEDIN;
//            }
//            [ConfigurationFile setCompanyLinkedInLink:value];
//            
//            value = nil;
//            
//            //Retrive Twitter Link
//            
//            value = [dictConfigurationDetails objectForKey:@"twitterLink"];
//            if (value==nil || value.length<=0) {
//                value = TWITTER;
//            }
//            [ConfigurationFile setCompanyTwitterLink:value];
//            
//            value = nil;
//            
//            //Retrive Website Link
//            
//            value = [dictConfigurationDetails objectForKey:@"website"];
//            if (value==nil || value.length<=0) {
//                value = WEBSITE;
//            }
//            [ConfigurationFile setCompanyLink:value];
//            
//            value = nil;
//            
//            //Retrive Contact Page Link
//            
//            value = [dictConfigurationDetails objectForKey:@"ContactUsPage"];
//            if (value==nil || value.length<=0) {
//                value = CONTACTUSLINK;
//            }
//            [ConfigurationFile setContactUsLink:value];
//            
//            value = nil;
    
    
            //Retrive Contact Mail ID
            
            [ConfigurationFile setContactUsMailID:CONTACTUSEMAIL];
            
    
    
            [ConfigurationFile setCopyRight:COPYRIGHT];
            
    
            //Check user active
            
     }
-(void)saveConfigurationDetails:(NSDictionary*)dictDetails{
    if (dictDetails!=nil&&dictDetails.allKeys>0) {
        NSString *dirToCreate = [self getPath];
        
        NSString *pathForFile = [dirToCreate stringByAppendingPathComponent:ConfigurationFileName];
        
        BOOL success = [dictDetails writeToFile:pathForFile atomically: YES];
        
        if(success){
            NSDictionary *dictConfigurationDetails = [[NSDictionary alloc]initWithContentsOfFile:pathForFile];
            NSString * imgLogoURL = [dictConfigurationDetails objectForKey:@"clientLogo_x"];
            [self saveImageInDocumentFolder:imgLogoURL];
            
        }
    }
}
-(BOOL)saveImageInDocumentFolder:(NSString*)urlString{
    BOOL success = NO;
    if (urlString!=nil && urlString.length>0) {
        NSURL *imgURL = [NSURL URLWithString:urlString];
        if (imgURL!=nil) {
            NSData *imgData = [NSData dataWithContentsOfURL:imgURL];
            if (imgData!=nil&&imgData.length>0) {
                NSString *dirToCreate = [self getPath];
                UIImage *imgLogo = [UIImage imageWithData:imgData];
                NSData *dataOfImage = UIImagePNGRepresentation(imgLogo);
                NSString *pathForFile = [dirToCreate stringByAppendingPathComponent:LogoName];
                
              success= [fileManager createFileAtPath:pathForFile contents:dataOfImage attributes:nil];
                
            }
        }
        
    }
    return success;
    
}

-(NSString*)getPath{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *dirToCreate = [NSString stringWithFormat:@"%@/%@",documentsDirectory,ConfigurationFolder];
   
    BOOL isDir=YES;
    
    if(![fileManager fileExistsAtPath:dirToCreate isDirectory:&isDir])
        if(![fileManager createDirectoryAtPath:dirToCreate withIntermediateDirectories:YES attributes:nil error:nil])
            NSLog(@"Error: Create folder failed");
    
    [self addSkipBackupAttributeToItemAtURL:dirToCreate];
    NSLog(@"Directory : %@",dirToCreate);
    return dirToCreate;
}
- (BOOL)addSkipBackupAttributeToItemAtURL:(NSString *)URLString
{
    
    NSURL *URL=[[NSURL alloc]initFileURLWithPath:URLString];
    
    
    
   // assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    return success;
}
@end
