//
//  ConfigurationFile.h
//  LaunchApp
//
//  Created by Soniya on 04/09/15.
//  Copyright (c) 2015 Assure Solutions Group. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConfigurationFile : NSObject

// Getter Methods
+(NSString*)getClientID;

+(NSString*)getAddress;

+(NSString*)getCompanyName;

+(NSString*)getContactNumber;

+(NSString*)getClientName;

+(UIColor*)getThemeColor;

+(UIColor*)getHighlightedThemeColor;

+(UIColor*)getBackGroundColor;

+(NSString*)getCompanyFacebookLink;

+(NSString*)getCompanyLinkedInLink;

+(NSString*)getCompanyTwitterLink;

+(NSString*)getCompanyLink;

+(NSString*)getContactUsLink;

+(NSString*)getCompanyNewsLetterLink;

+(NSString*)getContactUsMailID;

+(NSString*)getYouTubeLink;

+(NSString*)getCopyRight;

+(NSString*)getYouTubeID;

+(UIImage*)getLogoImage;

+(BOOL)isSubscriptionActive;


// Setter Methods

+(void)setClientID:(NSString*)ID;

+(void)setAddress:(NSString*)addr;

+(void)setCompanyName:(NSString*)company;

+(void)setClientName:(NSString*)name;

+(void)setContactNumber:(NSString*)number;

+(void)setThemeColor:(UIColor*)color;

+(void)setHighlightedThemeColor:(UIColor*)color;

+(void)setBackGroundColor:(UIColor*)color;

+(void)setCompanyFacebookLink:(NSString*)fbLink;

+(void)setCompanyLinkedInLink:(NSString*)lnLink;

+(void)setCompanyTwitterLink:(NSString*)twLink;

+(void)setCompanyLink:(NSString*)companyLink;

+(void)setContactUsLink:(NSString*)contactPageLink;

+(void)setCompanyNewsLetterLink:(NSString*)newsLetterPageLink;

+(void)setContactUsMailID:(NSString*)contactUsMailID;

+(void)setYouTubeLink:(NSString*)youTubePageLink;

+(void)setCopyRight:(NSString*)copyRightText;

+(void)setYouTubeID:(NSString*)youTubeVideoID;

+(void)setLogoImage:(UIImage*)imgLogo;

+(void)setSubscriptionStatus:(BOOL)isActive;

@end
