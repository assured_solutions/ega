//
//  ReminderDM.h
//  EGA
//
//  Created by Soniya on 28/10/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
@interface ReminderDM : NSObject

{
    NSString *dataBasepath;
    AppDelegate *appDelegate;
    
}



@property(nonatomic,retain)NSString *dataBasepath;


//<**************************************************************************>


// Methods for Connectivity with the Database


- (BOOL) open;

- (void) close;


//<**************************************************************************>

#pragma mark - Book Notes Details

//<**************************************************************************>

// Methods for Notes in Books

-(BOOL)updateEvent:(NSString*)title Date:(NSString*)date AlarmTone:(NSString*)alarmTone Repeat:(NSInteger)repeat SoundTone:(NSInteger)soundTone ID:(NSInteger)notificationID;
-(NSInteger)insertEvent:(NSString*)title Date:(NSString*)date AlarmTone:(NSString*)alarmTone Repeat:(NSInteger)repeat SoundTone:(NSInteger)soundTone;
-(BOOL)deleteEvent:(NSInteger)notificationid;
-(NSMutableArray*)getNotification;
-(BOOL)deletePreviousEvent:(NSString*)date;
@end
