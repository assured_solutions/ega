//
//  BaseViewController.h
//  
//
//  Created by Soniya on 16/08/15.
//  Copyright (c) 2015 iBeam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface BaseViewController : UIViewController
{
    AppDelegate *appDelegate;
    CGFloat screenWidth;
    CGFloat screenHeight;
    UIToolbar *accessoryToolBar;
    UITextField *activeTextField;
}
-(void) createKeyboardAccessoryToolBar:(BOOL)havingNextPrevButton;
-(void)hidePrevNext:(BOOL)hide;
-(void)showListMenu;
-(void)setCompnyLogoInNavigationBar;
@end
