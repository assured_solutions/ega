//
//  ForgotViewController.m
//  EGA
//
//  Created by Soniya on 28/09/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "ForgotViewController.h"

@interface ForgotViewController ()
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblEmailErrorMessage;
@end

@implementation ForgotViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - Operational Methods
- (IBAction)forgotPasswordClicked:(id)sender {
    if (appDelegate.isNetAvailable){
        if ([self validateAllFields]){
            [SupportingClass showAlertWithTitle:@"Alert" Message:@"Under Process" CancelButtonTitle:@"OK"];
            
        }
    }
    else{
        [SupportingClass showErrorMessage:NETWORKERRORMESSAGE];
        
    }
}
-(BOOL)validateAllFields{
    
     _lblEmailErrorMessage.hidden = YES;
    _txtEmail.text=[_txtEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
     NSString* loginID = _txtEmail.text;
    
    //Check whether textField are left empty or not
    BOOL success = YES;
    if (loginID.length<=0) {
        success = NO;
        _lblEmailErrorMessage.hidden = NO;
        _lblEmailErrorMessage.text = [SupportingClass blankEmailIDErrorMessage];
    }
    else if(![SupportingClass isNSStringValidEmail:loginID]){
        success = NO;
        _lblEmailErrorMessage.hidden = NO;
        _lblEmailErrorMessage.text = [SupportingClass invalidEmaildIDFormatErrorMessage];
    }
   
    
    [_txtEmail resignFirstResponder];
    
    return success;
}
@end
