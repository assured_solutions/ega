//
//  LoginEmailViewController.m
//  EGA
//
//  Created by Soniya on 26/09/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "LoginEmailViewController.h"
#import "DDPageControl.h"

@interface LoginEmailViewController ()
{
    DDPageControl *pageControl;
    NSArray *arrImages;
    NSArray *arrText;
    NSInteger selectedIndex;
}
@property (weak, nonatomic) IBOutlet UIView *vwSliderContainer;
@property (weak, nonatomic) IBOutlet UILabel *lblSliderTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgSlider;

@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@end

@implementation LoginEmailViewController

#pragma mark - Memory Management
-(void)removeReferences{
    _txtEmail=nil;
    _txtPassword=nil;
    
    _vwSliderContainer =nil;
    _lblSliderTitle=nil;
    _imgSlider=nil;
}

#pragma mark - View Controller Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self ScreenDesigningForSplashOfLogin];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = NO;
}
#pragma mark - ScreenDesigning Methods

-(void)ScreenDesigningForSplashOfLogin{
    
    arrImages = [[NSArray alloc]initWithObjects:[UIImage imageNamed:@"BiologicalAgeLogin"],[UIImage imageNamed:@"ReverseYourAgeLogin"],[UIImage imageNamed:@"StayHealthyLogin"], nil];
    arrText = [[NSArray alloc]initWithObjects:@"Know your Biological age",@"Reverse Your Age",@"Stay Healthy", nil];
  
    selectedIndex = 0;
    
    _imgSlider.image = [arrImages objectAtIndex:selectedIndex];
    _lblSliderTitle.text = [[arrText objectAtIndex:selectedIndex] uppercaseString];

    pageControl = [[DDPageControl alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 37)] ;
    [pageControl setCenter: CGPointMake(self.view.center.x, _vwSliderContainer.frame.origin.y+_vwSliderContainer.frame.size.height+10.0f)] ;
    [pageControl setNumberOfPages: arrImages.count] ;
    [pageControl setCurrentPage: 0] ;
    [pageControl setDefersCurrentPageDisplay: YES] ;
    [pageControl setType: DDPageControlTypeOnEmptyOffEmpty] ;
    [pageControl setOnColor: [ConfigurationFile getThemeColor]];
    [pageControl setOffColor: [UIColor colorWithHexString:@"#d9d9df"]] ;
    [pageControl setIndicatorDiameter: 15.0f] ;
    [pageControl setIndicatorSpace: 15.0f] ;
    [self.view addSubview: pageControl] ;

    _txtEmail.text = @"test@ega.com";
    
     [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(scrollingTimer) userInfo:nil repeats:YES];
}

#pragma mark - Operational Methods
- (void)scrollingTimer {
    
    selectedIndex++;
    if (selectedIndex==3) {
        selectedIndex = 0;
    }
    
    _imgSlider.image = [arrImages objectAtIndex:selectedIndex];
    _lblSliderTitle.text = [[arrText objectAtIndex:selectedIndex] uppercaseString];
    [SupportingClass animateViewWithType:kCATransitionFade SubType:kCATransitionFromRight OnView:_vwSliderContainer Duration:0.8];
    pageControl.currentPage=selectedIndex;
    [pageControl updateCurrentPageDisplay] ;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */



- (IBAction)signInClicked:(id)sender {
    if (appDelegate.isNetAvailable){
        if ([self validateAllFields]){
            UIViewController *dashboardVC = [self.storyboard instantiateViewControllerWithIdentifier:@"DashboardViewController"];
            NSArray *viewController = [NSArray arrayWithObjects:dashboardVC, nil];
            [self.navigationController setViewControllers:viewController animated:YES];
            
        }
    }
    else{
        [SupportingClass showErrorMessage:NETWORKERRORMESSAGE];
        
    }
}
-(BOOL)validateAllFields{
     _txtEmail.text=[_txtEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    _txtPassword.text=[_txtPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSString* loginID = _txtEmail.text;
    NSString* password = _txtPassword.text;
    
    //Check whether textField are left empty or not
    NSString *message=nil;
    if (loginID.length<=0) {
         message = [SupportingClass blankEmailIDErrorMessage];
    }
    else if(![SupportingClass isNSStringValidEmail:loginID]){
         message = [SupportingClass invalidEmaildIDFormatErrorMessage];
    }
    else if(password.length<=0){
         message = [SupportingClass blankPasswordErrorMessage];
    }
    else if (![loginID isEqualToString:@"test@ega.com"] ||![password isEqualToString:@"123456"]) {
         message = @"Wrong username or password.";
    }
  
    [_txtEmail resignFirstResponder];
    [_txtPassword resignFirstResponder];
    
    if (message!=nil) {
        [SupportingClass showErrorMessage:message];
        return NO;
    }
    return YES;
}
-(IBAction)sendFocusToPassword
{
    [_txtEmail resignFirstResponder];
    [_txtPassword becomeFirstResponder];
}

@end
