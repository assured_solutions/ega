//
//  SideBarOFCategoryView.m
//  FashionAndYou
//
//  Created by Chetan on 04/03/15.
//  Copyright (c) 2015 Chetan. All rights reserved.
//

#import "SideBarOFCategoryView.h"

@implementation SideBarOFCategoryView

@synthesize tblCategory;

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (id)initWithFrame:(CGRect)frame{
   
    self = [super initWithFrame:frame];
    if (self) {
     [self screenDesign];
    }
    return self;
    
     
}
- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    
    // Custom drawing methods
    if (self)
    {
        [self screenDesign];
    }
    
    return self;
}

-(void)screenDesign{
    NSString *strPath=[[NSBundle mainBundle]pathForResource:@"AccordianData" ofType:@"plist"];
    arrCategory=[NSArray arrayWithContentsOfFile:strPath];
    subCategoryCellCount=[[NSMutableArray alloc]init];
    for (int i=0;i<arrCategory.count;i++)
    {
        [subCategoryCellCount addObject:[NSNumber numberWithInt:0]];
    }
    
    
    tblCategory=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    tblCategory.delegate=self;
    tblCategory.dataSource=self;
    [self addSubview:tblCategory];
}
-(IBAction)buttonClicked:(id)sender
{
    UIButton *button=(UIButton *)sender;
    NSInteger _index=[sender tag]-1000;
    if(![button isSelected])
        [subCategoryCellCount replaceObjectAtIndex:_index withObject:[NSNumber numberWithInt:0]];
    else
    {
        NSInteger count= [[[arrCategory objectAtIndex:_index]objectForKey:@"SubCategory"] count];
        [subCategoryCellCount replaceObjectAtIndex:_index withObject:[NSNumber numberWithInt:count]];
    }
    [tblCategory reloadData];
}

#pragma mark - Table View
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSString *stringCategory=[[arrCategory objectAtIndex:section] objectForKey:@"CategoryName"];
    
    UIView *headerView=[[UIView alloc]init ];
    headerView.backgroundColor = [UIColor redColor];
    [headerView setFrame:CGRectMake(0, 0, tblCategory.frame.size.width ,0)];
    UIButton *button=[UIButton buttonWithType:UIButtonTypeCustom];
    UIImageView *img9= [[UIImageView alloc] init ];//WithImage:[UIImage
    img9.frame = CGRectMake(5, 10 , 20, 20);
    button.frame = CGRectMake(5, 10 , 20, 20);
    [headerView addSubview:img9];
    button.tag = section+1000;
    [button addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    if([[subCategoryCellCount objectAtIndex:section] intValue] == 0)
    {
        button.selected=YES;
        img9.image = [UIImage imageNamed:@"bullet.png"];
    }
    else
    {
        button.selected = NO;
        img9.image = [UIImage imageNamed:@"bullet1.png"];
    }
    button.backgroundColor=GRAYCOLOR;
    [headerView addSubview:button];
    UILabel *headerTitle = [[UILabel alloc] init];
    headerTitle.frame = CGRectMake(35, 5, tblCategory.frame.size.width-30, 44);
    button.frame = CGRectMake(0, 0, tblCategory.frame.size.width ,50);
    headerTitle.numberOfLines=0;
    headerTitle.lineBreakMode = NSLineBreakByWordWrapping;
    [headerTitle setBackgroundColor:[UIColor clearColor]];
    headerTitle.userInteractionEnabled = NO;
    headerTitle.textColor = [UIColor blackColor];
    [headerTitle setText:stringCategory];
    [headerTitle sizeThatFits:CGSizeMake(tblCategory.frame.size.width-30, 44)];
    [headerTitle clipsToBounds];
    [headerView addSubview:headerTitle];
    headerView.clipsToBounds = YES;
    return  headerView;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [arrCategory count];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[subCategoryCellCount objectAtIndex:section] integerValue];
}
#pragma mark tabledatasource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dictCategory=[arrCategory objectAtIndex:indexPath.section];
    NSArray *arrSubCategory=[dictCategory objectForKey:@"SubCategory"];
    NSString *strSubCategory=[[arrSubCategory objectAtIndex:indexPath.row] objectForKey:@"SubCategoryName"];
    NSUInteger row = [indexPath section];
    NSString *cellIdentifier = [ NSString stringWithFormat:@"CategoryCustomCell%d",row];
    UITableViewCell *cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        
        cell =[[UITableViewCell alloc]  initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    cell.textLabel.numberOfLines=0;
    cell.textLabel.lineBreakMode=NSLineBreakByWordWrapping;
    [cell.textLabel setText:strSubCategory];
    [cell.textLabel sizeThatFits:cell.textLabel.frame.size];
    return  cell;
    
}

@end
