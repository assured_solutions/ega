//
//  RecommendationViewController.m
//  EGA
//
//  Created by animesh bansal on 9/30/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "RecommendationViewController.h"

@interface RecommendationViewController ()

@end

@implementation RecommendationViewController
@synthesize navigatetoviewSideBarOfCategory;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self screenDesigning];
    // Do any additional setup after loading the view.
}





-(void)screenDesigning{
    navigatetoviewSideBarOfCategory=[[SideBarOFCategoryView alloc]init];
    navigatetoviewSideBarOfCategory.frame=CGRectMake(0, 50, self.view.frame.size.width, self.view.frame.size.height-100);
    navigatetoviewSideBarOfCategory.tblCategory.frame=CGRectMake(0, 0,  navigatetoviewSideBarOfCategory.frame.size.width, navigatetoviewSideBarOfCategory.frame.size.height);
    navigatetoviewSideBarOfCategory.backgroundColor=[UIColor redColor];
    //navigatetoviewSideBarOfCategory.alpha=0.5;
    [self.view addSubview:navigatetoviewSideBarOfCategory];
    [UIView commitAnimations];



}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
