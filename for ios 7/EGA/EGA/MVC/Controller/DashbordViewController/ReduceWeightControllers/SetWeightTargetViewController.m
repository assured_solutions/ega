//
//  SetWeightTargetViewController.m
//  EGA
//
//  Created by Soniya on 29/09/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "SetWeightTargetViewController.h"
#import "ReduceWeightCalculatorViewController.h"
@interface SetWeightTargetViewController ()

@property (weak, nonatomic) IBOutlet UITextField *txtTarget;
@property (weak, nonatomic) IBOutlet UIButton *btnStart;

@end

@implementation SetWeightTargetViewController

#pragma mark - View Controller Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self screenDesigningOfSetWeightTargetView];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - ScreenDesigning Methods
-(void)screenDesigningOfSetWeightTargetView{
    self.navigationItem.title = @"Reduce Weight";
    _txtTarget.text = [self getWeightReduction];
 }

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - Operational Methods
- (IBAction)startClicked:(UIButton *)sender {
    ReduceWeightCalculatorViewController *reduceWeightVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ReduceWeightCalculatorViewController"];
    reduceWeightVC.weightExceed = _weightExceed;
        [self.navigationController pushViewController:reduceWeightVC animated:YES];
 }

-(NSString*)getWeightReduction{
    if (_weightExceed>=0&&_weightExceed<5) {
        return @"0.5Kg/fortnight";
    }
    if (_weightExceed>=5&&_weightExceed<10){
        return @"1.0Kg/ fortnight";
    }
    if (_weightExceed>=10&&_weightExceed<15){
        return @"1.5Kg/ fortnight";
    }
    return @"2Kg/ fortnight";
   
}
@end
