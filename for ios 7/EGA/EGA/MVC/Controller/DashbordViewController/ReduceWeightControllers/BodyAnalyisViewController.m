//
//  BodyAnalyisViewController.m
//  EGA
//
//  Created by Soniya on 01/10/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "BodyAnalyisViewController.h"
#import "BodyAnalysisTableViewCell.h"
#import "ANProgressStepper.h"
#import "BodyAnalysisResultViewController.h"

@interface BodyAnalyisViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    NSInteger currentIndex;
    NSDictionary *dictQuesList;
    NSArray *arrSections;
    NSMutableArray *arrRows;
    __weak IBOutlet ANProgressStepper *stepper;
    __weak IBOutlet UIView *vwFooter;
}
@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewStepper;
@property (weak, nonatomic) IBOutlet UITableView *tblView;

@end

@implementation BodyAnalyisViewController

#pragma mark - Memory Management
-(void)removeReferences{
    _tblView=nil;
    _scrollViewStepper=nil;
 }

#pragma mark - View Controller Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self screenDesignOfBodyAnalysis];
    // Do any additional setup after loading the view.
}
-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    [_tblView reloadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ScreenDesigning Methods
-(void)screenDesignOfBodyAnalysis{
    //[ProgressHUD show:@"Loading..." Interaction:NO];
    
  //  [_tblView registerNib:[UINib nibWithNibName:@"BodyAnalysisTableViewCell"bundle:nil] forCellReuseIdentifier:@"BodyAnalysisTableViewCell"];

   [self showQuestions];
   
  int  _totalStep = (int)arrSections.count;
    stepper.numberOfSteps = _totalStep;
    stepper.currentStep = 1;
    stepper.lineThickness = 10.0f;
    stepper.incompleteColor = stepper.completeNumberColor = stepper.activeColor = stepper.borderColor = [UIColor whiteColor];
    stepper.completeColor = [UIColor colorWithHexString:@"#6DC58A"];
    stepper.activeColor = [ConfigurationFile getThemeColor];
   // stepper.incompleteNumberColor
   stepper.incompleteNumberColor = [UIColor blackColor];
     stepper.activeNumberColor = [UIColor whiteColor];
    stepper.numberSize = 20;
    stepper.showLinesBetweenSteps = YES;
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    swipe.numberOfTouchesRequired = 1;
    [swipe setDirection:UISwipeGestureRecognizerDirectionLeft];
    [self.view addGestureRecognizer:swipe];
    
  }

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (IBAction)submitButtonClicked:(UIButton *)sender {
    currentIndex = currentIndex+1;
    if (currentIndex>=arrRows.count) {
        currentIndex = arrSections.count-1;
        BodyAnalysisResultViewController *bodyAnalysisResultVC = [self.storyboard instantiateViewControllerWithIdentifier:@"BodyAnalysisResultViewController"];
        [self.navigationController pushViewController:bodyAnalysisResultVC animated:YES];
        
    }else{
        [SupportingClass animateViewWithType:kCATransitionReveal SubType:kCATransitionFromRight OnView:_tblView Duration:0.5];
        [_tblView setContentOffset:CGPointMake(0, 0)];
        [_tblView reloadData];
        
    }
    stepper.currentStep=(int)currentIndex+1;
    [stepper reloadStepper];
    [ProgressHUD dismiss];
}

#pragma mark - Operational Methods
- (void)handleSwipe:(UISwipeGestureRecognizer *)recognizer {
    if(recognizer.direction == UISwipeGestureRecognizerDirectionLeft){
        currentIndex = currentIndex+1;
        if (currentIndex>=arrRows.count) {
            currentIndex = arrSections.count-1;
           
        }else{
            [SupportingClass animateViewWithType:kCATransitionReveal SubType:kCATransitionFromRight OnView:_tblView Duration:0.5];
            [_tblView reloadData];

        }
       
    }
    else  if(recognizer.direction == UISwipeGestureRecognizerDirectionRight){
        currentIndex = currentIndex-1;
        if (currentIndex<0) {
            currentIndex = 0;
            
        }
        else{
           [SupportingClass animateViewWithType:kCATransitionReveal SubType:kCATransitionFromLeft OnView:_tblView Duration:0.5];
            [_tblView reloadData];
        }
        
    }
   stepper.currentStep=(int)currentIndex+1;
    [stepper reloadStepper];
}

-(void)showQuestions{
    NSString* plistPath = [[NSBundle mainBundle] pathForResource:@"BodyAnalysisQues" ofType:@"plist"];
     NSData *rowsData = [NSData dataWithContentsOfFile:plistPath];
    
   dictQuesList = [NSPropertyListSerialization propertyListWithData:rowsData
                                                                          options:NSPropertyListMutableContainers format:NULL error:NULL];
    currentIndex = -1;
    [SupportingClass animateViewWithType:kCATransitionReveal SubType:kCATransitionFromRight OnView:_tblView Duration:0.1];
    arrSections = [dictQuesList allKeys];
    arrRows = [[NSMutableArray alloc]init];
    
    for (NSString *key in arrSections) {
        NSArray *arr = [dictQuesList objectForKey:key];
        [arrRows addObject:arr];
    }
    [self submitButtonClicked:nil];
   
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    NSMutableArray *arr = [arrRows objectAtIndex:currentIndex];
    return arr.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSMutableArray *arr = [arrRows objectAtIndex:currentIndex];
    NSMutableDictionary *dict = [arr objectAtIndex:indexPath.row];
NSString *quesText = [dict objectForKey:@"QuesText"];
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:18]};
    // NSString class method: boundingRectWithSize:options:attributes:context is
    // available only on ios7.0 sdk.
     CGRect rect = [quesText boundingRectWithSize:CGSizeMake(screenWidth, CGFLOAT_MAX)
                                              options:NSStringDrawingUsesLineFragmentOrigin
                                           attributes:attributes
                                              context:nil];
    
//    
//    CGSize sizeOFString = [[quesText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] sizeWithFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:18] constrainedToSize:CGSizeMake(screenWidth-30, 2000) lineBreakMode:NSLineBreakByWordWrapping];
    
    //CGFloat height = rect.size.height+50;
    
     CGFloat height = rect.size.height+70;
    
    if (height<80)
        return 80;
    return height;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == arrRows.count-1) {
        return vwFooter.frame.size.height;
    }
    return 0;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{

        return vwFooter;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    BodyAnalysisTableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"BodyAnalysisTableViewCell"];
    NSMutableArray *arr = [arrRows objectAtIndex:currentIndex];
    NSLog(@"%@",arr);
    NSMutableDictionary *dict = [arr objectAtIndex:indexPath.row];
    if (![dict objectForKey:@"Value"]) {
        [dict setObject:@"0" forKey:@"Value"];
    }
    NSInteger tag = indexPath.row;
    cell.stepper.tag = tag;
    cell.tag = tag+1000;
    [cell setValueOfCell:dict];
   
   
    cell.stepper.valueChangedCallback = ^(PKYStepper *objStepper, float count) {
        NSInteger tag= objStepper.tag;
        objStepper.countLabel.text = [NSString stringWithFormat:@"%@", @(count)];
        NSMutableArray *arr = [arrRows objectAtIndex:currentIndex];
        NSMutableDictionary *dict = [arr objectAtIndex:tag];
        [dict setValue:[NSString stringWithFormat:@"%ld",(long)count] forKey:@"Value"];
     };    return cell;
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    cell.backgroundColor=[UIColor clearColor];
}

@end
