//
//  ReduceWeightCalculatorViewController.m
//  EGA
//
//  Created by Soniya on 14/11/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "ReduceWeightCalculatorViewController.h"

@interface ReduceWeightCalculatorViewController ()
@property (weak, nonatomic) IBOutlet UILabel *lblWeight;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end

@implementation ReduceWeightCalculatorViewController
#pragma mark - View Controller Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self ScreenDesigningForReduceWeight];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - ScreenDesigning Methods

-(void)ScreenDesigningForReduceWeight{
  
    NSString *strTitle =@"You are of normal weight";
    if (_weightExceed<0) {
        strTitle =@"You are underweight by";
        _weightExceed = (0 - _weightExceed);
    }
    else if (_weightExceed>0){
        strTitle =@"You are overweight by";
    }
   
    _lblTitle.text = strTitle;
      _lblWeight.text = [NSString stringWithFormat:@"%0.1f",_weightExceed];
    
    if (_weightExceed==0){
         _lblWeight.text = @"0";
    }
}

#pragma mark - Operational Methods

@end
