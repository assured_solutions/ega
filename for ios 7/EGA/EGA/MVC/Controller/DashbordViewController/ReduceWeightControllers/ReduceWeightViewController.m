//
//  ReduceWeightViewController.m
//  EGA
//
//  Created by Soniya on 02/11/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "ReduceWeightViewController.h"
#import "SetWeightTargetViewController.h"
@interface ReduceWeightViewController ()
@property (weak, nonatomic) IBOutlet UISegmentedControl *genderSegmentControl;
@property (weak, nonatomic) IBOutlet UITextField *txtHeight;
@property (weak, nonatomic) IBOutlet UITextField *txtWeight;
@end

@implementation ReduceWeightViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     [self screenDesigningForBiologicalCalculator];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - ScreenDesigning Methods
-(void)screenDesigningForBiologicalCalculator{
    self.navigationItem.title = @"Reduce Weight";
    [self createKeyboardAccessoryToolBar:YES];
    
    _txtWeight.inputAccessoryView = accessoryToolBar;
    _txtHeight.inputAccessoryView = accessoryToolBar;
    
}
#pragma mark - Operational Methods
- (IBAction)saveAndSubmitClicked:(id)sender {
    if (appDelegate.isNetAvailable){
        if ([self validateAllFields]){
            SetWeightTargetViewController *setWeightVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SetWeightTargetViewController"];
            setWeightVC.weightExceed = [self getWeightDiff];
            [self.navigationController pushViewController:setWeightVC animated:YES];
            
        }
    }
    else{
        [SupportingClass showErrorMessage:NETWORKERRORMESSAGE];
        
    }
}
-(BOOL)validateAllFields{
    
    _txtWeight.text=[_txtWeight.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    _txtHeight.text=[_txtHeight.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
  
    NSString* weight = _txtWeight.text;
    NSString* height = _txtHeight.text;
    
    //Check whether textField are left empty or not
    NSString *errorMessage = nil;
    
   if (weight.length<=0) {
        errorMessage = @"Please provide your weight";
    }
    else if (height.length<=0) {
        errorMessage = @"Please provide your height";
    }
    else if(_genderSegmentControl.selectedSegmentIndex==0){
       if ([height integerValue]<158||[height integerValue]>193) {
        errorMessage = @"Please provide male normal height from 5'2\" to 6'4\"";
    }
    }
    else if(_genderSegmentControl.selectedSegmentIndex==1){
        if ([height integerValue]<147||[height integerValue]>183) {
            errorMessage = @"Please provide female normal height from 4'10\" to 6'0\"";
        }
    }

    
    [self doneEditingButtonClicked];
    if (errorMessage!=nil) {
        [SupportingClass showErrorMessage:errorMessage];
        return NO;
    }
    return YES;
}
-(NSInteger)getWeightDiff{
    NSInteger height = [_txtHeight.text integerValue];
    float normalWeight = 50;
    float currentWeight = [_txtWeight.text floatValue];
    if (_genderSegmentControl.selectedSegmentIndex == 0) {
        if (height>=158&&height<160) {
            normalWeight = 59;
        }
        else if (height>=160&&height<163) {
            normalWeight = 60;
        }
        else if (height>=163&&height<165) {
            normalWeight = 61;
        }
        else if (height>=165&&height<168) {
            normalWeight = 62;
        }
        else if (height>=168&&height<170) {
            normalWeight = 63;
        }
        else if (height>=170&&height<173) {
            normalWeight = 64;
        }
        else if (height>=173&&height<175) {
            normalWeight = 66;
        }
        else if (height>=175&&height<178) {
            normalWeight = 67;
        }
        else if (height>=178&&height<180) {
            normalWeight = 68;
        }
        else if (height>=180&&height<183) {
            normalWeight = 70;
        }
        else if (height>=183&&height<186) {
            normalWeight = 71;
        }
        else if (height>=186&&height<188) {
            normalWeight = 73;
        }
        else if (height>=188&&height<191) {
            normalWeight = 74;
        }
        else if (height>=191&&height<193) {
            normalWeight = 76;
        }
        else if (height>=193) {
            normalWeight = 78;
        }

    }
    else{
        if (height>=147&&height<150) {
            normalWeight = 49;
        }
        else if (height>=150&&height<153) {
            normalWeight = 50;
        }
        else if (height>=153&&height<155) {
            normalWeight = 51;
        }
        else if (height>=155&&height<158) {
            normalWeight = 52;
        }
        else if (height>=158&&height<160) {
            normalWeight = 54;
        }
        else if (height>=160&&height<163) {
            normalWeight = 55;
        }
        else if (height>=163&&height<165) {
            normalWeight = 56;
        }
        else if (height>=165&&height<168) {
            normalWeight = 58;
        }
        else if (height>=168&&height<170) {
            normalWeight = 59;
        }
        else if (height>=170&&height<173) {
            normalWeight = 60;
        }
        else if (height>=173&&height<175) {
            normalWeight = 62;
        }
        else if (height>=175&&height<178) {
            normalWeight = 63;
        }
        else if (height>=178&&height<180) {
            normalWeight = 64;
        }
        else if (height>=180&&height<183) {
            normalWeight = 66;
        }
        else if (height>=183) {
            normalWeight = 67;
        }
    }
    
    return currentWeight - normalWeight;
}
#pragma mark - TextField Navigation Methodsbase
-(void)doneEditingButtonClicked
{
    [self.txtWeight resignFirstResponder];
    [self.txtHeight resignFirstResponder];
    
}
-(void)prevButtonClicked
{
    if (activeTextField==self.txtWeight) {
        [self.txtHeight becomeFirstResponder];
    }
    else if (activeTextField==self.txtHeight) {
        [self.txtWeight becomeFirstResponder];
    }
}
-(void)nextButtonClicked
{
    if (activeTextField==self.txtHeight) {
        [self.txtWeight becomeFirstResponder];
    }
    else if (activeTextField==self.txtWeight) {
        [self.txtHeight becomeFirstResponder];
    }
    
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    activeTextField = textField;
}


@end
