//
//  BodyAnalysisTableViewCell.m
//  EGA
//
//  Created by Soniya on 17/11/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "BodyAnalysisTableViewCell.h"

@implementation BodyAnalysisTableViewCell

@synthesize lblQuesText;
@synthesize stepper;

- (void)awakeFromNib {
    
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)setValueOfCell:(NSDictionary*)dictCellDetail{
    
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    CGFloat screenWidth = screenSize.width;
    NSString *quesText = [dictCellDetail objectForKey:@"QuesText"];
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:18]};
    // NSString class method: boundingRectWithSize:options:attributes:context is
    // available only on ios7.0 sdk.
    CGRect rect = [quesText boundingRectWithSize:CGSizeMake(screenWidth, CGFLOAT_MAX)
                                         options:NSStringDrawingUsesLineFragmentOrigin
                                      attributes:attributes
                                         context:nil];
    
    lblQuesText.text = quesText;
    CGFloat width = self.frame.size.width;
    lblQuesText.frame = CGRectMake(15, 10, width-30, rect.size.height);
    
    stepper.frame = CGRectMake(width-155, self.frame.size.height-50, 140,40);
    stepper.minimum = 0;
    stepper.maximum = 6;
    UIColor *color = [ConfigurationFile getThemeColor];
    [stepper setBorderColor:color];
    [stepper setLabelTextColor:color];
    [stepper setButtonTextColor:color forState:UIControlStateNormal];
    //    stepper.hidesDecrementWhenMinimum = YES;
    //   stepper.hidesIncrementWhenMaximum = YES;
    stepper.stepInterval = 1.0f;
    NSString *strValue = [NSString stringWithFormat:@"%ld",(long)[[dictCellDetail objectForKey:@"Value"] integerValue]];
    //[self.stepper setup];
    stepper.countLabel.text = strValue;
    stepper.value= [strValue floatValue];
}
@end
