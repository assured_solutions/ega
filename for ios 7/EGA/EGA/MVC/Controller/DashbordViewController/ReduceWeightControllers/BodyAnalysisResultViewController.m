//
//  BodyAnalysisResultViewController.m
//  EGA
//
//  Created by Soniya on 01/10/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "BodyAnalysisResultViewController.h"
#import "SendReportToMailViewController.h"
@interface BodyAnalysisResultViewController ()
@property (weak, nonatomic) IBOutlet UILabel *lblResult;
@property (weak, nonatomic) IBOutlet UITextField *txtEmailID;

@end

@implementation BodyAnalysisResultViewController

#pragma mark - Memory Management
-(void)removeReferences{

}

#pragma mark - View Controller Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
     // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ScreenDesigning Methods
-(void)screenDesignOfBodyAnalysisResult{
  
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - Operational Methods
- (IBAction)sendReportClicked:(UIButton *)sender {
if (appDelegate.isNetAvailable){
    if ([self validateAllFields]){
        SendReportToMailViewController *sendReportVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SendReportToMailViewController"];
        sendReportVC.strEmailID = _txtEmailID.text;
        [self.navigationController pushViewController:sendReportVC animated:YES];
        
    }
}
else{
    [SupportingClass showErrorMessage:NETWORKERRORMESSAGE];
    
}
}
-(BOOL)validateAllFields{
   
    _txtEmailID.text=[_txtEmailID.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSString* loginID = _txtEmailID.text;
    
    NSString *message = nil;
    
    //Check whether textField are left empty or not
    if (loginID.length<=0) {
        message =  [SupportingClass blankEmailIDErrorMessage];
    }
    else if(![SupportingClass isNSStringValidEmail:loginID]){
        message =  [SupportingClass invalidEmaildIDFormatErrorMessage];
    }
    
    [self dismissKeyboard];
    
    if (message!=nil) {
        [SupportingClass showAlertWithTitle:@"Required" Message:message CancelButtonTitle:@"OK"];
        return NO;
    }
    return YES;
}
-(IBAction)dismissKeyboard
{
    [_txtEmailID resignFirstResponder];
 }


@end
