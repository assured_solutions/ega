//
//  AddReminderViewController.m
//  EGA
//
//  Created by Soniya on 28/10/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "AddReminderViewController.h"

#import "ReminderDM.h"


@interface AddReminderViewController ()
@end

@implementation AddReminderViewController
@synthesize txtTitle;
@synthesize txtCategory;
@synthesize dateTime;
@synthesize isEdit;
@synthesize btnAdd;
@synthesize notificationDict;


#pragma mark - Memory Management
-(void)removeReferences{
}
#pragma mark - View Controller Life Cycle
- (void)viewDidLoad {
    
    
    dateTime.backgroundColor=[UIColor clearColor];
    dateTime.layer.borderColor = [UIColor orangeColor].CGColor;
    dateTime.layer.borderWidth = 2 ;
    
    //    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ganeshBorder.png"]];
    //    imgView.frame = CGRectMake(-160,0,453,500);
    //    imgView.backgroundColor = [UIColor clearColor];
    //    [dateTime addSubview:imgView];
    
    
    [self.dateTime setMinimumDate:[NSDate date]];
    if (isEdit)
    {
        [btnAdd setTitle:@"Edit" forState:UIControlStateNormal];
        txtTitle.text= [notificationDict objectForKey:@"Title"];
        NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
        [dateFormat setDateFormat:@"dd-MM-yyyy hh:mm a"];
        dateTime.date=[dateFormat dateFromString:[notificationDict objectForKey:@"DateTime"]];
        
    }
    else
    {
        [btnAdd setTitle:@"Add" forState:UIControlStateNormal];
        
    }
   // [self.navigationController setNavigationBarHidden:YES];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.}
    
    
}


-(IBAction)addNotification:(id)sender
{
    NSInteger IsRepeatAlarm = 0 ;
    NSString *str_Mp3name= @"ganesha";
    
    if (txtTitle.text.length==0)
    {
        
        UIAlertView* dialog = [[UIAlertView alloc] initWithTitle:@"EGA Reminder Alert"
                                                         message:@"Please enter Title"
                                                        delegate:self
                                               cancelButtonTitle:@"Ok"
                                               otherButtonTitles:nil, nil];
        
        [dialog show];
        
        
        
        
        return ;
        
        
        
        
    }
    else
    {
        NSDate *dateSel=self.dateTime.date;
        
        NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
        [dateFormat setDateFormat:@"dd-MM-yyyy hh:mm a"];
        
        NSString *currentDate=[dateFormat stringFromDate:[NSDate date]];
        NSString *currentSelectedDate=[dateFormat stringFromDate:dateSel];
        
        if ([[dateFormat dateFromString:currentDate] isEqualToDate:[dateFormat dateFromString:currentSelectedDate]])
        {
            
            UIAlertView* dialog = [[UIAlertView alloc] initWithTitle:@"EGA Reminder Alert"
                                                             message:@"Please Not select current running Time"
                                                            delegate:self
                                                   cancelButtonTitle:@"Ok"
                                                   otherButtonTitles:nil, nil];
            
            [dialog show];
            
            return ;
            
            
            
            
        }
        else
        {
            
            
            
            UILocalNotification *localNotification = [[UILocalNotification alloc] init];
            
            localNotification.fireDate = dateSel;
            localNotification.alertBody = self.txtTitle.text;
            localNotification.repeatInterval=IsRepeatAlarm;
            
            
            //UILocalNotificationDefaultSoundName
            localNotification.soundName =[NSString stringWithFormat:@"%@.mp3",str_Mp3name];
            
            
            ReminderDM *db=[[ReminderDM alloc]init];
            
            //   [db open];
            
            if ([btnAdd.titleLabel.text isEqualToString:@"Edit"])
            {
                
                NSString *strID=[notificationDict objectForKey:@"NotificationID"];
                BOOL success=   [db updateEvent:txtTitle.text Date:currentSelectedDate AlarmTone:str_Mp3name Repeat:IsRepeatAlarm SoundTone:0 ID:[strID intValue]];
                
                if (success)
                {
                    
                    NSDictionary *infoDict = [NSDictionary dictionaryWithObjectsAndKeys:strID, @"ID", nil];
                    localNotification.userInfo = infoDict;
                    
                    NSArray *notificationsArray = [[UIApplication sharedApplication] scheduledLocalNotifications];
                    
                    int i;
                    for (i=0; i<notificationsArray.count; i++)
                    {
                        UILocalNotification *localNotification1=[notificationsArray objectAtIndex:i];
                        NSDictionary *notiDict=[localNotification1 userInfo];
                        NSString *notiDictID=[notiDict objectForKey:@"ID"];
                        
                        
                        
                        if ([notiDictID isEqualToString:strID])
                        {
                            NSString*key= [NSString stringWithFormat:@"%d",i];
                            [[UIApplication sharedApplication] cancelLocalNotification:localNotification1];
                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:key];
                            break;
                        }
                    }
                    
                    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
                }
                else
                {
                    //                    JSAlertView *alert=[[JSAlertView alloc]initWithTitle:@"Alert" message:@"Sorry Not able to update yet, Please Try Again Later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                    //                    alert.tintColor = [UIColor colorWithRed:0.65882352941176 green:0.26666666666667 blue:0.09411764705882 alpha:1.0];
                    //
                    //                    [alert show];
                    //                    alert=nil;
                    
                    
                    
                    UIAlertView* dialog = [[UIAlertView alloc] initWithTitle:@"EGA Reminder Alert"
                                                                     message:@"Sorry Not able to update yet, Please Try Again Later"
                                                                    delegate:self
                                                           cancelButtonTitle:@"Ok"
                                                           otherButtonTitles:nil, nil];
                    
                    [dialog show];
                    
                    
                    
                    return ;
                }
            }
            else
            {
                
                NSInteger rowID= [db insertEvent:txtTitle.text Date:currentSelectedDate AlarmTone:str_Mp3name Repeat:IsRepeatAlarm SoundTone:0];
                
                NSString *str=[[NSString alloc]initWithFormat:@"%d",rowID];
                
                NSDictionary *infoDict = [NSDictionary dictionaryWithObjectsAndKeys:str, @"ID", nil];
                
                localNotification.userInfo = infoDict;
                
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
                
            }
            //   [db close];
            db=nil;
        }
        // [self BackPressed:nil];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
