//
//  ReminderListViewController.m
//  EGA
//
//  Created by Soniya on 17/10/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "ReminderListViewController.h"
#import "ReminderDetailCell.h"

//@interface UIColor (randomColor)
//
//+ (UIColor *)randomColor;
//
//@end
//
//@implementation UIColor (randomColor)
//
//+ (UIColor *)randomColor {
//    
//    CGFloat comps[3];
//    
//    for (int i = 0; i < 3; i++) {
//        
//        NSUInteger r = arc4random_uniform(256);
//        comps[i] = (CGFloat)r/255.f;
//    }
//    
//    return [UIColor colorWithRed:comps[0] green:comps[1] blue:comps[2] alpha:1.0];
//}
//
//@end


@interface ReminderListViewController ()
@property (nonatomic) NSMutableArray *arrReminder;
@end

@implementation ReminderListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self screenDesigningOfReminderList];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)screenDesigningOfReminderList{
    // Set to NO to prevent a small number
    // of cards from filling the entire
    // view height evenly and only show
    // their -topReveal amount
    //
    self.stackedLayout.fillHeight = YES;
    
    // Set to NO to prevent a small number
    // of cards from being scrollable and
    // bounce
    //
    self.stackedLayout.alwaysBounce = YES;
    
    // Set to NO to prevent unexposed
    // items at top and bottom from
    // being selectable
    //
    self.unexposedItemsAreSelectable = YES;
    
    self.exposedBottomOverlapCount = 4;
    
    self.exposedTopPinningCount = 2;
    self.exposedBottomPinningCount = 5;
    
    self.exposedItemSize = self.stackedLayout.itemSize = CGSizeMake(0.0, 240.0);
    self.exposedPinningMode = TGLExposedLayoutPinningModeBelow;
    self.exposedTopOverlap = 5.0;
    self.exposedBottomOverlap = 5.0;
    
    self.stackedLayout.layoutMargin = UIEdgeInsetsZero;
    self.exposedLayoutMargin = self.exposedPinningMode ? UIEdgeInsetsMake(40.0, 0.0, 0.0, 0.0) : UIEdgeInsetsMake(20.0, 0.0, 0.0, 0.0);
     self.edgesForExtendedLayout = UIRectEdgeNone;
    
    NSDictionary * dictReminder = [[NSDictionary alloc]initWithObjectsAndKeys:@"Hello1",@"Title",@"Hello how r u",@"Desc", nil];
    NSDictionary * dictReminder1 = [[NSDictionary alloc]initWithObjectsAndKeys:@"Hello1",@"Title",@"Hello how r u",@"Desc", nil];
    NSDictionary * dictReminder2 = [[NSDictionary alloc]initWithObjectsAndKeys:@"Hello2",@"Title",@"Hello how r u",@"Desc", nil];
    NSDictionary * dictReminder3 = [[NSDictionary alloc]initWithObjectsAndKeys:@"Hello3",@"Title",@"Hello how r u",@"Desc", nil];
    NSDictionary * dictReminder4 = [[NSDictionary alloc]initWithObjectsAndKeys:@"Hello4",@"Title",@"Hello how r u",@"Desc", nil];
    NSDictionary * dictReminder5 = [[NSDictionary alloc]initWithObjectsAndKeys:@"Hello5",@"Title",@"Hello how r u",@"Desc", nil];
    NSDictionary * dictReminder6 = [[NSDictionary alloc]initWithObjectsAndKeys:@"Hello6",@"Title",@"Hello how r u",@"Desc", nil];
    NSDictionary * dictReminder7 = [[NSDictionary alloc]initWithObjectsAndKeys:@"Hello7",@"Title",@"Hello how r u",@"Desc", nil];
    NSDictionary * dictReminder8 = [[NSDictionary alloc]initWithObjectsAndKeys:@"Hello8",@"Title",@"Hello how r u",@"Desc", nil];
    
    _arrReminder = [[NSMutableArray alloc]initWithObjects:dictReminder,dictReminder1,dictReminder2,dictReminder3,dictReminder4,dictReminder5,dictReminder6,dictReminder7,dictReminder8, nil];
    
    if (self.doubleTapToClose) {
        
        UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
        
        recognizer.delaysTouchesBegan = YES;
        recognizer.numberOfTapsRequired = 2;
        
        [self.collectionView addGestureRecognizer:recognizer];
    }

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - Actions

- (IBAction)handleDoubleTap:(UITapGestureRecognizer *)recognizer {
    
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - CollectionViewDataSource protocol

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return _arrReminder.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ReminderDetailCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ReminderDetailCell" forIndexPath:indexPath];
    cell.lblDate.text =  [NSString stringWithFormat:@"%ld",indexPath.row];
        NSDictionary *card = [_arrReminder objectAtIndex:indexPath.row];
    
    cell.lblDate.text = [card objectForKey:@"Title"];
     cell.lblDetail.text = [card objectForKey:@"Desc"];
    [cell.lblDetail sizeToFit];
    cell.lblType.text = @"Consultation";
    cell.backgroundColor = [self getRandomColor:indexPath.row];
    return cell;
}
#pragma mark - Overloaded methods

- (void)moveItemAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
    
    // Update data source when moving cards around
    
    NSDictionary *card = [_arrReminder objectAtIndex:fromIndexPath.item];

        [self.arrReminder removeObjectAtIndex:fromIndexPath.item];
        [self.arrReminder insertObject:card atIndex:toIndexPath.item];
}
-(UIColor*)getRandomColor:(NSInteger)row{
    
    int randNum = row % 8; //create the random number.
    UIColor *color;
    switch (randNum) {
        case 0:
            color = [UIColor colorWithHexString:@"#8000FF"];
            break;
        case 1:
            color = [UIColor colorWithHexString:@"#FF8000"];
            break;
        case 2:
            color = [UIColor colorWithHexString:@"#008000"];
            break;
        case 3:
            color = [UIColor colorWithHexString:@"#FF0080"];
            break;
        case 4:
            color = [UIColor colorWithHexString:@"#800000"];
            break;
        case 5:
            color = [UIColor colorWithHexString:@"#007AFF"];
            break;
        case 6:
            color = [UIColor colorWithHexString:@"#003366"];
            break;
        case 7:
            color = [UIColor colorWithHexString:@"#FF0000"];
            break;
        default:
            color = [UIColor colorWithHexString:@"#8000FF"];
            break;
    }
    return color;
}

@end
