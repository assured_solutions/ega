//
//  ReminderCalendarViewController.m
//  EGA
//
//  Created by Soniya on 13/10/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "ReminderCalendarViewController.h"
#import "ReminderDetailViewController.h"

@interface ReminderCalendarViewController (){
 NSMutableArray *_eventsByDate;
    NSMutableArray *_datesSelected;
}
@end

@implementation ReminderCalendarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _calendarManager = [JTCalendarManager new];
    _calendarManager.delegate = self;
    
    // Generate random events sort by date using a dateformatter for the demonstration
    [self createRandomEvents];
    
    [_calendarManager setMenuView:_calendarMenuView];
    [_calendarManager setContentView:_calendarContentView];
    [_calendarManager setDate:[NSDate date]];
    
    _datesSelected = [NSMutableArray new];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - CalendarManager delegate

// Exemple of implementation of prepareDayView method
// Used to customize the appearance of dayView
- (void)calendar:(JTCalendarManager *)calendar prepareDayView:(JTCalendarDayView *)dayView
{
    // Today
    if([_calendarManager.dateHelper date:[NSDate date] isTheSameDayThan:dayView.date]){
        dayView.circleView.hidden = NO;
        dayView.circleView.backgroundColor = [UIColor blueColor];
        dayView.dotView.backgroundColor = [UIColor whiteColor];
        dayView.textLabel.textColor = [UIColor whiteColor];
    }
    // Selected date
    else if([self isInDatesSelected:dayView.date]){
        dayView.circleView.hidden = NO;
        dayView.circleView.backgroundColor = [UIColor redColor];
        dayView.dotView.backgroundColor = [UIColor whiteColor];
        dayView.textLabel.textColor = [UIColor whiteColor];
    }
    // Other month
    else if(![_calendarManager.dateHelper date:_calendarContentView.date isTheSameMonthThan:dayView.date]){
        dayView.circleView.hidden = YES;
        dayView.dotView.backgroundColor = [UIColor redColor];
        dayView.textLabel.textColor = [UIColor lightGrayColor];
    }
    // Another day of the current month
    else{
        dayView.circleView.hidden = YES;
        dayView.dotView.backgroundColor = [UIColor redColor];
        dayView.textLabel.textColor = [UIColor blackColor];
    }
    NSString *dateToShow = [[SupportingClass dateFormatter:[SupportingClass dateFormat]] stringFromDate:dayView.date];

    if([self haveEventForDay:dateToShow]){
        dayView.dotView.hidden = NO;

    }
    else{
        dayView.dotView.hidden = YES;
    }
}

- (void)calendar:(JTCalendarManager *)calendar didTouchDayView:(JTCalendarDayView *)dayView
{
    NSString *dateSelected = [[SupportingClass dateFormatter:[SupportingClass dateFormat]] stringFromDate:dayView.date];
    if([self haveEventForDay:dateSelected]){
    
         ReminderDetailViewController *reminderDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ReminderDetailViewController"];
        reminderDetailVC.arrDateOfReminders = _eventsByDate;
        reminderDetailVC.selectedDate = dateSelected;
    [self.navigationController pushViewController:reminderDetailVC animated:YES];
    }
    else{
        if(![_calendarManager.dateHelper date:_calendarContentView.date isTheSameMonthThan:dayView.date]){
            if([_calendarContentView.date compare:dayView.date] == NSOrderedAscending){
                [_calendarContentView loadNextPageWithAnimation];
            }
            else{
                [_calendarContentView loadPreviousPageWithAnimation];
            }
        }
    }
    
//    if([self isInDatesSelected:dayView.date]){
//        [_datesSelected removeObject:dayView.date];
//        
//        [UIView transitionWithView:dayView
//                          duration:.3
//                           options:0
//                        animations:^{
//                            [_calendarManager reload];
//                            dayView.circleView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.1, 0.1);
//                        } completion:nil];
//    }
//    else{
//        [_datesSelected addObject:dayView.date];
//        
//        dayView.circleView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.1, 0.1);
//        [UIView transitionWithView:dayView
//                          duration:.3
//                           options:0
//                        animations:^{
//                            [_calendarManager reload];
//                            dayView.circleView.transform = CGAffineTransformIdentity;
//                        } completion:nil];
    }
    
    
    // Load the previous or next page if touch a day from another month
    



#pragma mark - Date selection

- (BOOL)isInDatesSelected:(NSDate *)date
{
    for(NSDate *dateSelected in _datesSelected){
        if([_calendarManager.dateHelper date:dateSelected isTheSameDayThan:date]){
            return YES;
        }
    }
    
    return NO;
}

#pragma mark - Fake data

// Used only to have a key for _eventsByDate


- (BOOL)haveEventForDay:(NSString *)date
{
    
//    NSString *dateToPoint =
    if ([_eventsByDate containsObject:date]) {
        return YES;
    }
//    if(_eventsByDate[key] && [_eventsByDate[key] count] > 0){
//        return YES;
//    }
    
    return NO;
    
}

- (void)createRandomEvents
{
    _eventsByDate = [NSMutableArray new];
    
    for(int i = 0; i < 30; ++i){
        // Generate 30 random dates between now and 60 days later
        NSDate *randomDate = [NSDate dateWithTimeInterval:(rand() % (3600 * 24 * 60)) sinceDate:[NSDate date]];
        
        // Use the date as key for eventsByDate
        NSString *key = [[SupportingClass dateFormatter:[SupportingClass dateFormat]] stringFromDate:randomDate];
        
//        if(!_eventsByDate[key]){
//            _eventsByDate[key] = [NSMutableArray new];
//        }
//        
        [_eventsByDate addObject:key];
    }
}

@end
