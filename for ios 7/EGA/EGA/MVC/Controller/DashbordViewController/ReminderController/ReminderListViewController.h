//
//  ReminderListViewController.h
//  EGA
//
//  Created by Soniya on 17/10/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "TGLStackedViewController.h"

@interface ReminderListViewController : TGLStackedViewController
@property (nonatomic, assign) BOOL doubleTapToClose;
@end
