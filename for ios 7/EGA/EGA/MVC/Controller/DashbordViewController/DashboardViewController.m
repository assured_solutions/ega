//
//  DashboardViewController.m
//  EGA
//
//  Created by Soniya on 03/10/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "DashboardViewController.h"

@interface DashboardViewController ()
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *vwContainer;

@end

@implementation DashboardViewController
#pragma mark - Memory Management
-(void)removeReferences{
    _scrollView=nil;
    _vwContainer=nil;
}
#pragma mark - View Controller Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self screenDesignOfDashboard];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - ScreenDesigning Methods
-(void)screenDesignOfDashboard{
  
    _scrollView.contentSize = CGSizeMake(screenWidth, _vwContainer.frame.size.height);
    [self setCompnyLogoInNavigationBar];
    [self showListMenu];
 }

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(IBAction)underConstruction:(id)sender{
    [SupportingClass showErrorMessage:@"Under Development Process"];
}
@end
