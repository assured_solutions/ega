//
//  TxtBtnCustomCell.m
//  EGA
//
//  Created by Soniya on 06/10/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "TxtBtnCustomCell.h"

@implementation TxtBtnCustomCell
@synthesize btnReminder;
@synthesize txtValue;
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)hideReminder:(BOOL)hide{
    if (hide) {
        btnReminder.hidden = YES;
        txtValue.frame = CGRectMake(10, 12, self.contentView.frame.size.width-20, 30);
    }
    else{
        btnReminder.hidden = NO;
    }
   
}
@end
