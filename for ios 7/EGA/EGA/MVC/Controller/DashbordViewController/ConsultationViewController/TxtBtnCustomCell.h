//
//  TxtBtnCustomCell.h
//  EGA
//
//  Created by Soniya on 06/10/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TxtBtnCustomCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *txtValue;
@property (weak, nonatomic) IBOutlet UIButton *btnReminder;
-(void)hideReminder:(BOOL)hide;
@end
