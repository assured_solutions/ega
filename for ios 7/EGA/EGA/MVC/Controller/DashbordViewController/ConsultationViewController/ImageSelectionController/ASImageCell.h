//
//  ASImageCell.h
//
//  Created by Soniya on 12/10/15.
//  Copyright (c) 2015 Assure Solutions Group. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ASImageCell : UICollectionViewCell {
    UILabel *text;
}

@property (nonatomic, strong) UIImageView *backgroundImageView;

- (void)styleImage;
- (void)styleAddButton;

@end
