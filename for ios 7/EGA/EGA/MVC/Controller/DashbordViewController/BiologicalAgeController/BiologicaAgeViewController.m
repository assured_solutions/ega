//
//  BiologicaAgeViewController.m
//  EGA
//
//  Created by Soniya on 10/11/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "BiologicaAgeViewController.h"

@interface BiologicaAgeViewController ()
@property (weak, nonatomic) IBOutlet UILabel *lblAge;
@property (weak, nonatomic) IBOutlet UILabel *lblAgeResult;
@property (weak, nonatomic) IBOutlet UIView *vwResultContainer;

@end

@implementation BiologicaAgeViewController
@synthesize actualAge;
@synthesize biologicalAge;
#pragma mark - View Controller Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self screenDesigningOfBiologicalAge];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - ScreenDesigning Methods
-(void)screenDesigningOfBiologicalAge{
    _vwResultContainer.layer.borderWidth = 1.0f;
    _vwResultContainer.layer.borderColor = [UIColor colorWithHexString:@"#d9d9df"].CGColor;
    _vwResultContainer.layer.cornerRadius = 2.0f;
    
    _lblAge.text = [NSString stringWithFormat:@"%.0f",biologicalAge];
    float ageDiff = 0;
    NSString * weight = @"";
    
    if (biologicalAge>actualAge) {
        weight = @"underAge";
        ageDiff = biologicalAge-actualAge;
    }
    else if (biologicalAge<actualAge){
        weight = @"overAge";
        ageDiff = actualAge-biologicalAge;
    }
    
    if (ageDiff==0) {
        _lblAgeResult.hidden = YES;
    }
    else{
        _lblAgeResult.hidden = NO;
        _lblAgeResult.text = [NSString stringWithFormat:@"\"You are %.0f Years %@\"",ageDiff,weight];
    }
}
@end
