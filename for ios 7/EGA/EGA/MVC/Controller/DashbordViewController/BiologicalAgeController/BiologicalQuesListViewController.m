//
//  BiologicalQuesListViewController.m
//  EGA
//
//  Created by Soniya on 01/11/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "BiologicalQuesListViewController.h"
#import "BiologicalAgeHeaderView.h"
#import "ASCustomButton.h"
#import "BiologicaAgeViewController.h"
@interface BiologicalQuesListViewController (){
NSMutableArray *arrQuestions;
    __weak IBOutlet UITableView *tblQuesList;
    
    __weak IBOutlet UIView *vwFooter;
}
@end

@implementation BiologicalQuesListViewController
@synthesize age;
#pragma mark - View Controller Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self screenDesignOfBiologicalQues];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - ScreenDesigning Methods
-(void)screenDesignOfBiologicalQues{
    
    [self showQuestions];
}
#pragma mark - Operational Methods
-(void)showQuestions{
    NSString* plistPath = [[NSBundle mainBundle] pathForResource:@"AgeCalculations" ofType:@"plist"];
    NSData *rowsData = [NSData dataWithContentsOfFile:plistPath];
    
    arrQuestions = [NSPropertyListSerialization propertyListWithData:rowsData
                                                             options:NSPropertyListMutableContainers format:NULL error:NULL];

    [tblQuesList reloadData];
}

-(IBAction)submitButtonClicked:(UIButton*)sender{
    if ([self validateAllFields]) {
         NSInteger sum = 0;
        for (NSDictionary *dictQuestion in arrQuestions ) {
            NSArray *arrOptions = [dictQuestion objectForKey:@"Options"];
             for (NSDictionary *optionsDetail in arrOptions) {
                if( [[optionsDetail objectForKey:@"isSelected"] isEqualToString:@"Yes"]){
                  sum += [[optionsDetail objectForKey:@"ChangeInAge"] integerValue];
                }
                
            }

    }
        float biologicalAge = age+sum;
                BiologicaAgeViewController*biologicalAgeVC=[self.storyboard instantiateViewControllerWithIdentifier:@"BiologicaAgeViewController"];
        biologicalAgeVC.actualAge = age;
        biologicalAgeVC.biologicalAge = biologicalAge;
        [self.navigationController pushViewController:biologicalAgeVC animated:YES];
    }
}
-(BOOL)validateAllFields{
    
    NSString *message=nil;
    for (NSDictionary *dictQuestion in arrQuestions ) {
        NSArray *arrOptions = [dictQuestion objectForKey:@"Options"];
        BOOL notSelected = YES;
        for (NSDictionary *optionsDetail in arrOptions) {
            if( [[optionsDetail objectForKey:@"isSelected"] isEqualToString:@"Yes"]){
                notSelected = NO;
                break;
            }
            
        }
        if (notSelected) {
            message = @"Please answer to all questions";
            break;
        }
    }
    if (message!=nil) {
        [SupportingClass showErrorMessage:message];
        return NO;
    }
    return YES;
}
-(CGFloat)getHeightForQuesText:(NSInteger)section{
    NSDictionary *dictQuestion = [arrQuestions objectAtIndex:section];
    NSString *quesText = [dictQuestion objectForKey:@"Question"];
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:18]};
    // NSString class method: boundingRectWithSize:options:attributes:context is
    // available only on ios7.0 sdk.
    CGRect rect = [quesText boundingRectWithSize:CGSizeMake(tblQuesList.frame.size.width, CGFLOAT_MAX)
                                         options:NSStringDrawingUsesLineFragmentOrigin
                                      attributes:attributes
                                         context:nil];
    
    //
    //    CGSize sizeOFString = [[quesText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] sizeWithFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:18] constrainedToSize:CGSizeMake(screenWidth-30, 2000) lineBreakMode:NSLineBreakByWordWrapping];
    
    //CGFloat height = rect.size.height+50;
    
    CGFloat height = rect.size.height+40;
    
    if (height<=80)
        return 80;
    return height;
}
#pragma mark - Table View DataSource And Delegate Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return arrQuestions.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSDictionary *dictQuestion = [arrQuestions objectAtIndex:section];
    NSArray *arrOptions = [dictQuestion objectForKey:@"Options"];
    return arrOptions.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return [self getHeightForQuesText:section];
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == arrQuestions.count-1) {
        return vwFooter.frame.size.height;
    }
    return 0;
}
- ( UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
     NSDictionary *dictQuestion = [arrQuestions objectAtIndex:section];
    BiologicalAgeHeaderView * headerView = [[BiologicalAgeHeaderView alloc]initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, [self getHeightForQuesText:section])];
    headerView.lblQuesText.text = [dictQuestion objectForKey:@"Question"];
    [headerView.lblQuesText sizeToFit];
    return headerView;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section ==arrQuestions.count-1) {
        return vwFooter;
        
    }
    return nil;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Create an instance of ItemCell
    UITableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"OptionCell"];
    NSDictionary *dictQuestion = [arrQuestions objectAtIndex:indexPath.section];
    NSArray *arrOptions = [dictQuestion objectForKey:@"Options"];
    NSDictionary *optionsDetail = [arrOptions objectAtIndex:indexPath.row];
    cell.textLabel.text = [optionsDetail objectForKey:@"Option"];
    cell.detailTextLabel.text = [optionsDetail objectForKey:@"SubOptionText"];
    UIImage *img = [UIImage imageNamed:@"RadioButton"];
    if ([[optionsDetail objectForKey:@"isSelected"] isEqualToString:@"Yes"]) {
        img = [UIImage imageNamed:@"RadioButtonSelected"];
    }
    cell.imageView.image = img;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dictQuestion = [arrQuestions objectAtIndex:indexPath.section];
    NSArray *arrOptions = [dictQuestion objectForKey:@"Options"];
    NSDictionary *optionsDetail = [arrOptions objectAtIndex:indexPath.row];
    NSString *selected = @"No";
    if ([[optionsDetail objectForKey:@"isSelected"] isEqualToString:@"No"]||![optionsDetail objectForKey:@"isSelected"]) {
       selected = @"Yes";
    }
    
    for (NSDictionary *dict in arrOptions) {
        [dict setValue:@"No" forKey:@"isSelected"];
    }
    [optionsDetail setValue:selected forKey:@"isSelected"];
    [tblQuesList reloadData];
 }


@end
