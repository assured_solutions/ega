//
//  BiologicaAgeViewController.h
//  EGA
//
//  Created by Soniya on 10/11/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "BaseViewController.h"

@interface BiologicaAgeViewController : BaseViewController
@property(nonatomic)float actualAge;
@property(nonatomic)float biologicalAge;
@end
