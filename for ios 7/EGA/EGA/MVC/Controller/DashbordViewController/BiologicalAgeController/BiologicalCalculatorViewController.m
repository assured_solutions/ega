//
//  BiologicalCalculatorViewController.m
//  EGA
//
//  Created by Soniya on 01/11/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "BiologicalCalculatorViewController.h"
#import "BiologicalQuesListViewController.h"

@interface BiologicalCalculatorViewController ()
@property (weak, nonatomic) IBOutlet UISegmentedControl *genderSegmentControl;
@property (weak, nonatomic) IBOutlet UITextField *txtDOB;
@property (weak, nonatomic) IBOutlet UITextField *txtHeight;
@property (weak, nonatomic) IBOutlet UITextField *txtWeight;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIView *contentView;
@end

@implementation BiologicalCalculatorViewController
#pragma mark - View Controller Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self screenDesigningForBiologicalCalculator];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - ScreenDesigning Methods
-(void)screenDesigningForBiologicalCalculator{
    self.navigationItem.title = @"Biological Age Calculator";
    [self createKeyboardAccessoryToolBar:YES];
    
     _txtDOB.inputAccessoryView = accessoryToolBar;
    _txtWeight.inputAccessoryView = accessoryToolBar;
    _txtHeight.inputAccessoryView = accessoryToolBar;
    
    UIDatePicker *datePicker = [[UIDatePicker alloc]init];
    datePicker.datePickerMode = UIDatePickerModeDate;
    datePicker.maximumDate = [NSDate date];
    [datePicker addTarget:self action:@selector(updateDOB:) forControlEvents:UIControlEventValueChanged];
    _txtDOB.inputView = datePicker;
    _scrollView.contentSize = CGSizeMake(_contentView.frame.size.width, _contentView.frame.size.height);
}
#pragma mark - Operational Methods
- (IBAction)saveAndSubmitClicked:(id)sender {
    if (appDelegate.isNetAvailable){
        if ([self validateAllFields]){
            
            NSDate *today = [NSDate date];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"dd MMM yyyy"];
            NSDate *birthdate = [dateFormatter dateFromString: _txtDOB.text];
            NSDateComponents *ageComponents = [[NSCalendar currentCalendar]
                                               components:NSYearCalendarUnit
                                               fromDate:birthdate
                                               toDate:today
                                               options:0];
            
            BiologicalQuesListViewController *biologicalVC = [self.storyboard instantiateViewControllerWithIdentifier:@"BiologicalQuesListViewController"];
            biologicalVC.age = ageComponents.year;
             [self.navigationController pushViewController:biologicalVC animated:YES];
            
        }
    }
    else{
        [SupportingClass showErrorMessage:NETWORKERRORMESSAGE];
        
    }
}
-(BOOL)validateAllFields{
    
     _txtDOB.text=[_txtDOB.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    _txtWeight.text=[_txtWeight.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    _txtHeight.text=[_txtHeight.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
     NSString* dob = _txtDOB.text;
    NSString* weight = _txtWeight.text;
    NSString* height = _txtHeight.text;
    
    //Check whether textField are left empty or not
    NSString *errorMessage = nil;
    
   if(dob.length<=0){
        errorMessage = [SupportingClass blankDOBErrorMessage];
    }
    
    else if (weight.length<=0) {
        errorMessage = @"Please provide your weight";
    }
    else if (height.length<=0) {
        errorMessage = @"Please provide your height";
    }
    
    [self doneEditingButtonClicked];
    if (errorMessage!=nil) {
        [SupportingClass showErrorMessage:errorMessage];
        return NO;
    }
    return YES;
}
-(void)updateDOB:(UIDatePicker*)sender{
    NSDateFormatter *dateFomratter = [[NSDateFormatter alloc]init];
    dateFomratter.dateFormat = @"dd MMM yyyy";
    _txtDOB.text = [dateFomratter stringFromDate:sender.date];
}
#pragma mark - TextField Navigation Methodsbase
-(void)doneEditingButtonClicked
{
   
    [self.txtDOB resignFirstResponder];
    [self.txtWeight resignFirstResponder];
    [self.txtHeight resignFirstResponder];
    
}
-(void)prevButtonClicked
{
    if (activeTextField==self.txtWeight) {
        [self.txtHeight becomeFirstResponder];
    }
    else if (activeTextField==self.txtHeight) {
        [self.txtDOB becomeFirstResponder];
    }
    else if (activeTextField==self.txtDOB) {
        [self.txtWeight becomeFirstResponder];
    }
}
-(void)nextButtonClicked
{
    if (activeTextField==self.txtDOB) {
        [self.txtHeight becomeFirstResponder];
    }
    else if (activeTextField==self.txtHeight) {
        [self.txtWeight becomeFirstResponder];
    }
    else if (activeTextField==self.txtWeight) {
        [self.txtDOB becomeFirstResponder];
    }

}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    activeTextField = textField;
}
@end
