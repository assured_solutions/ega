//
//  BiologicalAgeHeaderView.h
//  EGA
//
//  Created by Soniya on 08/11/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BiologicalAgeHeaderView : UIView
@property (weak, nonatomic) IBOutlet UILabel *lblQuesText;

@end
