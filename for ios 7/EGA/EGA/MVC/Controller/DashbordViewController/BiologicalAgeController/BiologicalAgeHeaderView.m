//
//  BiologicalAgeHeaderView.m
//  EGA
//
//  Created by Soniya on 08/11/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "BiologicalAgeHeaderView.h"

@implementation BiologicalAgeHeaderView

-(id)initWithFrame:(CGRect)frame {
    if ( !(self = [super initWithFrame:frame]) ) return nil;
    [self awakeFromNib];
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self awakeFromNib];
    }
    return self;
}
-(void)awakeFromNib {
    UIView *xibView = [[[NSBundle mainBundle] loadNibNamed:@"BiologicalAgeHeaderView" owner:self options:nil] objectAtIndex:0];
    [self addSubview:xibView];
    xibView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    [self viewDesigning];
   
}
-(void)viewDesigning{

    self.autoresizingMask =  UIViewAutoresizingFlexibleRightMargin |
    UIViewAutoresizingFlexibleLeftMargin |
    UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
