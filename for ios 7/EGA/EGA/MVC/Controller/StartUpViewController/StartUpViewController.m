//
//  StartUpViewController.m
//  EGA
//
//  Created by Soniya on 26/09/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "StartUpViewController.h"
#import "LoginEmailViewController.h"
#import "LoginViewController.h"
#import "DDPageControl.h"

@interface StartUpViewController ()<UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *imgEGALogo;
@end

@implementation StartUpViewController
#pragma mark - Memory Management
-(void)removeReferences{
}

#pragma mark - View Controller Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
  [self screenDesignOfStartUp];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = NO;
}
#pragma mark - ScreenDesigning Methods
-(void)screenDesignOfStartUp{
    [self animateControlOutside];
   self.navigationController.navigationBar.barTintColor = [ConfigurationFile getThemeColor];
  [self performSelector:@selector(animateControlOutside) withObject:nil afterDelay:0.2];
    

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - Operational Methods

-(void) animateControlOutside{

    [UIView animateWithDuration:1.5
                     animations:^
     {
         CGPoint center = self.imgEGALogo.center;
         self.imgEGALogo.transform = CGAffineTransformMakeScale(10,10);
         self.imgEGALogo.center = center;
            }
                     completion:^(BOOL finished) {
                         [self performSelector:@selector(goToLoginEmailViewController:) withObject:nil afterDelay:2.3];
                     } ];
    }

-(IBAction)goToLoginEmailViewController:(UIButton*)sender{
   
    LoginEmailViewController *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginEmailViewController"];
    NSArray *viewController = [NSArray arrayWithObjects:loginVC, nil];
    [self.navigationController setViewControllers:viewController animated:YES];
    
}


@end
