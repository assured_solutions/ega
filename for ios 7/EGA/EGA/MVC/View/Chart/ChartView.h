//
//  ChartView.h
//  LaunchApp
//
//  Created by Soniya on 08/09/15.
//  Copyright (c) 2015 Assure Solutions Group. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChartView : UIView
- (void)drawPieChartWithData:(NSDictionary*)dictDetails Title:(NSString*)titleOfLegend CenterText:(NSString*)centerText;
@end
