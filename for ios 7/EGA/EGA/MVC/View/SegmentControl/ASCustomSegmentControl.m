//
//  ASCustomSegmentControl.m
//  UFSApp
//
//  Created by SONIYA VISHWAKARMA on 07/09/15.
//  Copyright (c) 2015 Epictenet. All rights reserved.
//

#import "ASCustomSegmentControl.h"

@implementation ASCustomSegmentControl
#pragma mark - UILabel Overrides
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self designSegmentControl];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)coder
{
    // Call the parent implementation of initWithCoder
    self = [super initWithCoder:coder];
    
    // Custom drawing methods
    if (self)
    {
        [self designSegmentControl];
    }
    
    return self;
}
#pragma mark - Operational setters

- (void)designSegmentControl
{
    // Get the root layer (any UIView subclass comes with one)
    self.tintColor = [ConfigurationFile getThemeColor];
    [[UISegmentedControl appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]} forState:UIControlStateSelected];
}

@end
