//
//  ASNavigationBar.m
//  LaunchApp
//
//  Created by Soniya on 07/09/15.
//  Copyright (c) 2015 Assure Solutions Group. All rights reserved.
//

#import "ASNavigationBar.h"

@implementation ASNavigationBar

#pragma mark - UINavigationBar Overrides
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self designNavigationBar];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)coder
{
    // Call the parent implementation of initWithCoder
    self = [super initWithCoder:coder];
    
    // Custom drawing methods
    if (self)
    {
        [self designNavigationBar];
    }
    
    return self;
}
#pragma mark - Operational setters

- (void)designNavigationBar
{
    // Get the root layer (any UIView subclass comes with one)
    self.barTintColor = [ConfigurationFile getThemeColor];
    self.tintColor = [UIColor whiteColor];
//    UIImageView *headerImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"NavLogo"]];
//    headerImage.frame = CGRectMake(0, 0, 92, 32);
//    [self.topItem setTitleView:headerImage];
}
@end
