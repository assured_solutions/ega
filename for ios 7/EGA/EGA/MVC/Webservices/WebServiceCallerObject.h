//
//  WebServiceCallerObject.h
//  UFSApp
//
//  Created by EPICTENET CONSULTANCY on 14/09/15.
//  Copyright (c) 2015 Epictenet. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebserviceEnum.h"

@interface WebServiceCallerObject : NSObject


//REQUEST

@property(nonatomic, retain) NSString * requestURL;
@property(nonatomic, readwrite) BOOL isPost;
@property(nonatomic, retain) NSString * jsonStringToPost;
@property(nonatomic, readwrite) WebServiceCallerType webserviceType;


//RESPONSE

@property(nonatomic, readwrite) BOOL success;
@property(nonatomic, retain) id parseData;
@property(nonatomic, retain) NSString *comment;

@end
