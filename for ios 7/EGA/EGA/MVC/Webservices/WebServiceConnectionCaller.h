//
//  WebServiceConnectionCaller.h
//  UFSApp
//
//  Created by EPICTENET CONSULTANCY on 14/09/15.
//  Copyright (c) 2015 Epictenet. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebServiceCallerObject.h"

@interface WebServiceConnectionCaller : NSObject
{
     NSURLConnection *connection;
     NSMutableData *responseData;
    WebServiceCallerObject *serviceResponse;
}
-(void)callWebserviceWithRequest:(WebServiceCallerObject*)request;
@end
