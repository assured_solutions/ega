//
//  WebServiceConnectionCaller.m
//  UFSApp
//
//  Created by EPICTENET CONSULTANCY on 14/09/15.
//  Copyright (c) 2015 Epictenet. All rights reserved.
//

#import "WebServiceConnectionCaller.h"

@implementation WebServiceConnectionCaller
#pragma mark - Webservice Caller

-(void)callWebserviceWithRequest:(WebServiceCallerObject*)request
{
    if (request == nil) {
        return;
    }
    
    
    
    serviceResponse = request;
    
    NSString *urlString = [serviceResponse.requestURL stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    responseData = [NSMutableData data];
    
    NSLog(@"\n%@",urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:60];
    
    if (serviceResponse.isPost) {
        [urlRequest setHTTPMethod:@"POST"];
        if (serviceResponse.jsonStringToPost.length!=0) {
            NSData *postData = [serviceResponse.jsonStringToPost dataUsingEncoding:NSUTF8StringEncoding];
            NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
            [urlRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [urlRequest setValue:postLength forHTTPHeaderField:@"Content-Length"];
            [urlRequest setHTTPBody:postData];
            
        }
        
        
    }
    connection = [[NSURLConnection alloc]initWithRequest:urlRequest delegate:self startImmediately:NO];
    [connection scheduleInRunLoop:[NSRunLoop mainRunLoop] forMode:NSRunLoopCommonModes];
    [connection start];
    
}
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    [responseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSLog(@"User Login Error during connection: %@", [error description]);
    
    serviceResponse.parseData = nil;
    serviceResponse.success = NO;
    serviceResponse.comment = [error localizedDescription];
//    NSInteger errCode = error.code;
//    if (errCode == -1009) {
//        serviceResponse.comment = NETWORKERRORMESSAGE;
//    }
    [[NSNotificationCenter defaultCenter] postNotificationName:webserviceCallerFinishedKey object:serviceResponse userInfo:nil];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    //  NSLog(@"%@",responseString);
    
    NSError *error = nil;
    
    id result=nil;
    if (responseData!=nil) {
        result = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:&error];
    }
    
    BOOL success = YES;
    if (error) {
        success = NO;
        serviceResponse.comment = [error localizedDescription];
    }
    
    
    serviceResponse.parseData = result;
    if (serviceResponse.webserviceType == RegisterTrader) {
        serviceResponse.parseData = responseString;
    }
    serviceResponse.success = success;
    [[NSNotificationCenter defaultCenter] postNotificationName:webserviceCallerFinishedKey object:serviceResponse userInfo:nil];
    
}

- (void) connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    if([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
    {
        [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust]
             forAuthenticationChallenge:challenge];
    }
    
    [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}

- (void) connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace
{
    if([[protectionSpace authenticationMethod] isEqualToString:NSURLAuthenticationMethodServerTrust])
    {
        // return YES;
    }
}
//- (NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse
//{
//    NSURLResponse *response = cachedResponse.response;
//    if ([response isKindOfClass:NSHTTPURLResponse.class]) return cachedResponse;
//
//    NSHTTPURLResponse *HTTPResponse = (NSHTTPURLResponse*)response;
//    NSDictionary *headers = HTTPResponse.allHeaderFields;
//    if (headers[@"Cache-Control"]) return cachedResponse;
//
//    NSMutableDictionary *modifiedHeaders = headers.mutableCopy;
//    modifiedHeaders[@"Cache-Control"] = @"max-age=60";
//    NSHTTPURLResponse *modifiedResponse = [[NSHTTPURLResponse alloc]
//                                           initWithURL:HTTPResponse.URL
//                                           statusCode:HTTPResponse.statusCode
//                                           HTTPVersion:@"HTTP/1.1"
//                                           headerFields:modifiedHeaders];
//
//    cachedResponse = [[NSCachedURLResponse alloc]
//                      initWithResponse:modifiedResponse
//                      data:cachedResponse.data
//                      userInfo:cachedResponse.userInfo
//                      storagePolicy:cachedResponse.storagePolicy];
//    return cachedResponse;
//}
@end
