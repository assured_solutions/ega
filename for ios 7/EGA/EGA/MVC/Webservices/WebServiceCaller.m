//
//  WebServiceCaller.m
//  UFSApp
//
//  Created by EPICTENET CONSULTANCY on 31/08/15.
//  Copyright (c) 2015 Epictenet. All rights reserved.
//

#import "WebServiceCaller.h"
#import "WebServiceCallerObject.h"
#import "WebServiceConnectionCaller.h"

#define SuccessMessage @"Successfully Called"


@interface WebServiceCaller ()
@end

@implementation WebServiceCaller
@synthesize delegate;

typedef enum : NSInteger {
    BaseURL = 0,
    CalculatorBaseURL,
    
} BaseURLType;

- (id)init
{
    self = [super init];
    if (self) {
        operationQueue = [[NSOperationQueue alloc] init];
        [operationQueue setMaxConcurrentOperationCount:10];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(webserviceCallerFinished:) name:webserviceCallerFinishedKey object:nil];
    }
    return self;
}

-(void)cancelWebserviceCall{
    [operationQueue cancelAllOperations];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void)cancelAllCalls{
    [operationQueue cancelAllOperations];
}
//#pragma mark - Calculators
//
//- (void)getHomeLoanForLoanAmount:(NSString *)loanAmount Duration:(NSString *)loanDuration Rate:(NSString *)interestRate LoanType:(NSString*)loanType Frequency:(NSString*)frequency {
//    
//    NSString *finalURLString = [NSString stringWithFormat:[self getURLWithBase:CalculatorBaseURL relativeURL:APIForHomeLoan], loanAmount, loanDuration, interestRate,loanType,frequency];
//    
//    WebServiceCallerObject *objWebserviceCaller = [[WebServiceCallerObject alloc]init];
//    objWebserviceCaller.requestURL = finalURLString;
//    objWebserviceCaller.webserviceType = HomeLoanRepayment;
//    objWebserviceCaller.isPost  = NO;
//    objWebserviceCaller.jsonStringToPost = nil;
//    
//    WebServiceConnectionCaller * webserviceCaller = [[WebServiceConnectionCaller alloc]init];
//    NSInvocationOperation *invocationOperation = [[NSInvocationOperation alloc]initWithTarget:webserviceCaller selector:@selector(callWebserviceWithRequest:) object:objWebserviceCaller];
//    [operationQueue addOperation:invocationOperation];
//}
//
//
//
////---------- BORROWING POWER CALCULATIONS ---------//
//
//- (void)getBorrowingPowerForAnnualIncome:(NSString *)annualIncome FixeCommitments:(NSString *)fixedCommitments LivingExpenses:(NSString *)livingExp {
//   
//     NSString *finalURLString = [NSString stringWithFormat:[self getURLWithBase:CalculatorBaseURL relativeURL:APIForBorrowingPower], annualIncome, fixedCommitments, livingExp];
//    
//    WebServiceCallerObject *objWebserviceCaller = [[WebServiceCallerObject alloc]init];
//    objWebserviceCaller.requestURL = finalURLString;
//    objWebserviceCaller.webserviceType = BorrowingPower;
//    objWebserviceCaller.isPost  = NO;
//    objWebserviceCaller.jsonStringToPost = nil;
//    
//    WebServiceConnectionCaller * webserviceCaller = [[WebServiceConnectionCaller alloc]init];
//    NSInvocationOperation *invocationOperation = [[NSInvocationOperation alloc]initWithTarget:webserviceCaller selector:@selector(callWebserviceWithRequest:) object:objWebserviceCaller];
//    [operationQueue addOperation:invocationOperation];
//    
//    
//}
//
////---- INCOME TAX CALCULATIONS ----//
//
//- (void)getIncomeTaxForAnnualIncome:(NSString *)annualIncome {
//
//    NSString *finalURLString = [NSString stringWithFormat:[self getURLWithBase:CalculatorBaseURL relativeURL:APIForIncomeTax], annualIncome];
//    
//    WebServiceCallerObject *objWebserviceCaller = [[WebServiceCallerObject alloc]init];
//    objWebserviceCaller.requestURL = finalURLString;
//    objWebserviceCaller.webserviceType = IncomeTax;
//    objWebserviceCaller.isPost  = NO;
//    objWebserviceCaller.jsonStringToPost = nil;
//    
//    WebServiceConnectionCaller * webserviceCaller = [[WebServiceConnectionCaller alloc]init];
//    NSInvocationOperation *invocationOperation = [[NSInvocationOperation alloc]initWithTarget:webserviceCaller selector:@selector(callWebserviceWithRequest:) object:objWebserviceCaller];
//    [operationQueue addOperation:invocationOperation];
//    
//}
////----- FIXED TERM DEPOSIT CALCULATIONS --------//
//
//- (void)getFixedTermForAmount:(NSString *)depositAmount Rate:(NSString *)interestRate Years:(NSString *)years {
//   
//      NSString *finalURLString = [NSString stringWithFormat:[self getURLWithBase:CalculatorBaseURL relativeURL:APIForFixedTermDeposit], depositAmount, interestRate, years];
//    
//    WebServiceCallerObject *objWebserviceCaller = [[WebServiceCallerObject alloc]init];
//    objWebserviceCaller.requestURL = finalURLString;
//    objWebserviceCaller.webserviceType = FixedDeposit;
//    objWebserviceCaller.isPost  = NO;
//    objWebserviceCaller.jsonStringToPost = nil;
//    
//    WebServiceConnectionCaller * webserviceCaller = [[WebServiceConnectionCaller alloc]init];
//    NSInvocationOperation *invocationOperation = [[NSInvocationOperation alloc]initWithTarget:webserviceCaller selector:@selector(callWebserviceWithRequest:) object:objWebserviceCaller];
//    [operationQueue addOperation:invocationOperation];
//    
//    
//}
////------- BUDGET PLAN CALCULATIONS -------//
//
//- (void)getBudgetPlanForAnnualIncome:(NSString *)annualIncome OtherIncome:(NSString*)otherIncome LivingExpenses:(NSString *)livingExpenses Insurance:(NSString *)insurance Loans:(NSString *)loans Entertainment:(NSString *)entertainmentCost TravelCost:(NSString *)travelCost OtherExpenses:(NSString*)otherExpenses {
//    
//     NSString *finalURLString = [NSString stringWithFormat:[self getURLWithBase:CalculatorBaseURL relativeURL:APIForBudgetPlan], annualIncome,otherIncome, livingExpenses, insurance, loans, entertainmentCost, travelCost,otherExpenses];
//    
//    WebServiceCallerObject *objWebserviceCaller = [[WebServiceCallerObject alloc]init];
//    objWebserviceCaller.requestURL = finalURLString;
//    objWebserviceCaller.webserviceType = BudgetPlanner;
//    objWebserviceCaller.isPost  = NO;
//    objWebserviceCaller.jsonStringToPost = nil;
//    
//    WebServiceConnectionCaller * webserviceCaller = [[WebServiceConnectionCaller alloc]init];
//    NSInvocationOperation *invocationOperation = [[NSInvocationOperation alloc]initWithTarget:webserviceCaller selector:@selector(callWebserviceWithRequest:) object:objWebserviceCaller];
//    [operationQueue addOperation:invocationOperation];
//    
//    
//}
//
////---- STAMP DUTY CALCULATIONS -----//
//
//- (void)getStampDutyForState:(NSString *)stateOfProperty PropertyValue:(NSString *)propertyValue PropertyType:(NSString *)propType isFirstHome:(BOOL)firstHome {
//   
//    NSString *finalURLString = [NSString stringWithFormat:[self getURLWithBase:CalculatorBaseURL relativeURL:APIForStampDuty], stateOfProperty, propertyValue, propType, firstHome ? @"true" : @"false"];
//
//    
//    WebServiceCallerObject *objWebserviceCaller = [[WebServiceCallerObject alloc]init];
//    objWebserviceCaller.requestURL = finalURLString;
//    objWebserviceCaller.webserviceType = StampDuty;
//    objWebserviceCaller.isPost  = NO;
//    objWebserviceCaller.jsonStringToPost = nil;
//    
//    WebServiceConnectionCaller * webserviceCaller = [[WebServiceConnectionCaller alloc]init];
//    NSInvocationOperation *invocationOperation = [[NSInvocationOperation alloc]initWithTarget:webserviceCaller selector:@selector(callWebserviceWithRequest:) object:objWebserviceCaller];
//    [operationQueue addOperation:invocationOperation];
//    
//}
//
//-(void)getCalulationWebserviceCalledSuccessfully:(BOOL)success WithData:(id)jsonObject WebserviceType:(WebServiceCallerType)type  WithMessage:(NSString*)errorMessage{
//    
//    NSDictionary *dictResultValue = (NSDictionary*)jsonObject;
//  
//    NSString *message = errorMessage;
//    if (success) {
//        message = SuccessMessage;
//    }
//    else{
//        if (message.length==0) {
//            message = NETWORKERRORMESSAGE;
//        }
//    }
//    
//    if(self.delegate!=nil && [(id)[self delegate] respondsToSelector:@selector(getCalculationCompletesSuccessfully:WithResult:WithMessage:Type:)])
//    {
//        [(id)[self delegate] getCalculationCompletesSuccessfully:success WithResult:dictResultValue WithMessage:message Type:type];
//    }
//    
//    
//}
//
//
//#pragma mark - Referral List
//
//-(void)getListOfBrokersForRefer{
//    
//    WebServiceCallerObject *objWebserviceCaller = [[WebServiceCallerObject alloc]init];
//    objWebserviceCaller.requestURL = [self getURLWithBase:BaseURL relativeURL:REFERRALLIST];
//    objWebserviceCaller.webserviceType = BrokersList;
//    objWebserviceCaller.isPost  = NO;
//    objWebserviceCaller.jsonStringToPost = nil;
//    
//    WebServiceConnectionCaller * webserviceCaller = [[WebServiceConnectionCaller alloc]init];
//    NSInvocationOperation *invocationOperation = [[NSInvocationOperation alloc]initWithTarget:webserviceCaller selector:@selector(callWebserviceWithRequest:) object:objWebserviceCaller];
//    [operationQueue addOperation:invocationOperation];
//}
//-(void)getListOfBrokersCalledSuccessfully:(BOOL)success WithData:(id)Data WithMessage:(NSString*)errorMessage{
//    
//    NSMutableArray *arrBroker = (NSMutableArray*)Data;
//    NSString *message = errorMessage;
//    if (success) {
//        message = SuccessMessage;
//    }
//    else{
//        if (message.length==0) {
//            message = NETWORKERRORMESSAGE;
//        }
//    }
//    
//    if(self.delegate!=nil && [(id)[self delegate] respondsToSelector:@selector(getListOfBrokersForReferCompletesSuccessfully:WithList:WithMessage:)])
//    {
//        [(id)[self delegate] getListOfBrokersForReferCompletesSuccessfully:success WithList:arrBroker WithMessage:message];
//    }
//}
//
//
//#pragma mark - Category List
//
//-(void)getListOfCategories{
//    WebServiceCallerObject *objWebserviceCaller = [[WebServiceCallerObject alloc]init];
//    objWebserviceCaller.requestURL = [self getURLWithBase:BaseURL relativeURL:CATEGORIESLIST];
//    objWebserviceCaller.webserviceType = CategoryList;
//    objWebserviceCaller.isPost  = NO;
//    objWebserviceCaller.jsonStringToPost = nil;
//    
//    WebServiceConnectionCaller * webserviceCaller = [[WebServiceConnectionCaller alloc]init];
//    NSInvocationOperation *invocationOperation = [[NSInvocationOperation alloc]initWithTarget:webserviceCaller selector:@selector(callWebserviceWithRequest:) object:objWebserviceCaller];
//    [operationQueue addOperation:invocationOperation];
//}
//-(void)getListOfCategoriesCalledSuccessfully:(BOOL)success WithData:(id)Data WithMessage:(NSString*)errorMessage{
//    
//    NSMutableArray *arrCategortList = (NSMutableArray*)Data;
//    NSString *message = errorMessage;
//    if (success) {
//        message = SuccessMessage;
//    }
//    else{
//        if (message.length==0) {
//            message = NETWORKERRORMESSAGE;
//        }
//        
//    }
//    
//    if(self.delegate!=nil && [(id)[self delegate] respondsToSelector:@selector(getListOfCategoriesCompletesSuccessfully:WithList:WithMessage:)])
//    {
//        [(id)[self delegate] getListOfCategoriesCompletesSuccessfully:success WithList:arrCategortList WithMessage:message];
//    }
//}
//
//#pragma mark - Traders/Service Provider
//
//-(void)registerTraderWithData:(NSDictionary*)userDetail{
//    
//    NSString *postString=[NSString stringWithFormat:@"traderName=%@&phoneNumber=%@&email=%@&abn=%@&bio=%@&description=%@&webUrl=%@&address=%@&categoryId=%@&clientid=%@", [userDetail objectForKey:@"traderName"],[userDetail objectForKey:@"phoneNumber"],[userDetail objectForKey:@"email"],[userDetail objectForKey:@"abn"],[userDetail objectForKey:@"bio"],[userDetail objectForKey:@"description"],[userDetail objectForKey:@"webUrl"],[userDetail objectForKey:@"address"],[userDetail objectForKey:@"categoryId"],[userDetail objectForKey:@"clientId"]];
//    
//    
//    WebServiceCallerObject *objWebserviceCaller = [[WebServiceCallerObject alloc]init];
//    objWebserviceCaller.requestURL = [self getURLWithBase:BaseURL relativeURL:REGISTERTRADEINDEX];
//    objWebserviceCaller.webserviceType = RegisterTrader;
//    objWebserviceCaller.isPost  = YES;
//    objWebserviceCaller.jsonStringToPost = postString;
//    
//    WebServiceConnectionCaller * webserviceCaller = [[WebServiceConnectionCaller alloc]init];
//    NSInvocationOperation *invocationOperation = [[NSInvocationOperation alloc]initWithTarget:webserviceCaller selector:@selector(callWebserviceWithRequest:) object:objWebserviceCaller];
//    [operationQueue addOperation:invocationOperation];
//    
//}
//-(void)registerTradersCalledSuccessfully:(BOOL)success WithData:(NSString*)responseString WithMessage:(NSString*)errorMessage{
//    
//    NSString *message = errorMessage;
//    if (responseString!=nil) {
//        if ([responseString isEqualToString:@"OK"]) {
//            message = @"Registered Successfully.";
//            success = YES;
//        }
//        else{
//            message = @"Not able to Registered.Please try again later.";
//        }
//    }
//    else{
//        if (!success) {
//            if (message.length==0) {
//                message = NETWORKERRORMESSAGE;
//            }
//        }
//        
//    }
//    
//    if(self.delegate!=nil && [(id)[self delegate] respondsToSelector:@selector(registerTraderCompletesSuccessfully:WithMessage:)])
//    {
//        [(id)[self delegate] registerTraderCompletesSuccessfully:success WithMessage:message];
//    }
//}
//
//
//#pragma mark -
//-(void)getTradersListWithRecordCounts:(NSInteger)recordCount OfClientID:(NSString*)clientID{
//    NSString *urlString = [NSString stringWithFormat:FINDTOPNTRADERS,(long)recordCount,ClientID];
//    WebServiceCallerObject *objWebserviceCaller = [[WebServiceCallerObject alloc]init];
//    objWebserviceCaller.requestURL = [self getURLWithBase:BaseURL relativeURL:urlString];
//    objWebserviceCaller.webserviceType = TopNTraders;
//    objWebserviceCaller.isPost  = NO;
//    objWebserviceCaller.jsonStringToPost = nil;
//    
//    WebServiceConnectionCaller * webserviceCaller = [[WebServiceConnectionCaller alloc]init];
//    // [webserviceCaller callWebserviceWithRequest:objWebserviceCaller];
//    NSInvocationOperation *invocationOperation = [[NSInvocationOperation alloc]initWithTarget:webserviceCaller selector:@selector(callWebserviceWithRequest:) object:objWebserviceCaller];
//    [operationQueue addOperation:invocationOperation];
//}
//-(void)getTradersListWithRecordCountsCalledSuccessfully:(BOOL)success WithData:(id)Data WithMessage:(NSString*)errorMessage{
//    
//    NSDictionary *dictTraders = (NSDictionary*)Data;
//    NSMutableArray *arrTradersList = nil;
//    if (dictTraders.allKeys.count!=0) {
//        NSDictionary * dictResult = [dictTraders objectForKey:@"result"];
//        if (dictResult.allKeys.count!=0) {
//            arrTradersList= [dictResult objectForKey:@"registerTradeIndex"];
//        }
//    }
//    NSString *message = errorMessage;
//    if (success) {
//        message = SuccessMessage;
//    }
//    else{
//        if (message.length==0) {
//            message = NETWORKERRORMESSAGE;
//        }
//    }
//    
//    if(self.delegate!=nil && [(id)[self delegate] respondsToSelector:@selector(getTradersListWithRecordCountsCompletesSuccessfully:WithList:WithMessage:)])
//    {
//        [(id)[self delegate] getTradersListWithRecordCountsCompletesSuccessfully:success WithList:arrTradersList WithMessage:message];
//    }
//}
//
//#pragma mark - Team List
//
//-(void)getTeamListWithClientID:(NSString*)clientID{
//    NSString *urlString = [NSString stringWithFormat:APIForTEAM, clientID];
//    WebServiceCallerObject *objWebserviceCaller = [[WebServiceCallerObject alloc]init];
//    objWebserviceCaller.requestURL = [self getURLWithBase:BaseURL relativeURL:urlString];
//    objWebserviceCaller.webserviceType = TeamList;
//    objWebserviceCaller.isPost  = NO;
//    objWebserviceCaller.jsonStringToPost = nil;
//    
//    WebServiceConnectionCaller * webserviceCaller = [[WebServiceConnectionCaller alloc]init];
//    // [webserviceCaller callWebserviceWithRequest:objWebserviceCaller];
//    NSInvocationOperation *invocationOperation = [[NSInvocationOperation alloc]initWithTarget:webserviceCaller selector:@selector(callWebserviceWithRequest:) object:objWebserviceCaller];
//    [operationQueue addOperation:invocationOperation];
//}
//-(void)getTeamListCalledSuccessfully:(BOOL)success WithData:(id)Data WithMessage:(NSString*)errorMessage{
//    
//    NSMutableArray *arrTeamList = (NSMutableArray*)Data;
//
//    NSString *message = errorMessage;
//    if (success) {
//        message = SuccessMessage;
//    }
//    else{
//        if (message.length==0) {
//            message = NETWORKERRORMESSAGE;
//        }
//    }
//    
//    if(self.delegate!=nil && [(id)[self delegate] respondsToSelector:@selector(getTeamListCompletesSuccessfully:WithList:WithMessage:)])
//    {
//        [(id)[self delegate] getTeamListCompletesSuccessfully:success WithList:arrTeamList WithMessage:message];
//    }
//}
//
//#pragma mark - URL creator
//
//-(NSString*)getURLWithBase:(BaseURLType)urlType relativeURL:(NSString*)relativeURL{
//    NSString *baseUrl = BrokerAppBaseURL;
//    if (urlType == CalculatorBaseURL) {
//        baseUrl = CalculatorsBaseURL;
//    }
//    return [NSString stringWithFormat:baseUrl,relativeURL];
//}

#pragma mark - JSON creator

-(NSString*)getJSONStringForData:(id)dataToConvert{
    NSError *error;
    NSString *jsonString = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dataToConvert
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    return jsonString;
}


//-(void)webserviceCallerFinished:(NSNotification *)notification{
//    WebServiceCallerObject * webserviceCallerObject = notification.object;
//    
//    switch (webserviceCallerObject.webserviceType) {
//        case CategoryList:
//            [self getListOfCategoriesCalledSuccessfully:webserviceCallerObject.success WithData:webserviceCallerObject.parseData  WithMessage:webserviceCallerObject.comment];
//            break;
//        case BrokersList:
//            [self getListOfBrokersCalledSuccessfully:webserviceCallerObject.success WithData:webserviceCallerObject.parseData  WithMessage:webserviceCallerObject.comment];
//            break;
//        case RegisterTrader:
//            [self registerTradersCalledSuccessfully:webserviceCallerObject.success WithData:webserviceCallerObject.parseData  WithMessage:webserviceCallerObject.comment];
//            break;
//        case TopNTraders:
//            [self getTradersListWithRecordCountsCalledSuccessfully:webserviceCallerObject.success WithData:webserviceCallerObject.parseData  WithMessage:webserviceCallerObject.comment];
//            break;
//        case TeamList:
//            [self getTeamListCalledSuccessfully:webserviceCallerObject.success WithData:webserviceCallerObject.parseData  WithMessage:webserviceCallerObject.comment];
//            break;
//        case IncomeTax:
//        case StampDuty:
//        case HomeLoanRepayment:
//        case BorrowingPower:
//        case SuperAnnuation:
//        case MedicalLevy:
//        case FixedDeposit:
//        case BudgetPlanner:
//            [self getCalulationWebserviceCalledSuccessfully:webserviceCallerObject.success WithData:webserviceCallerObject.parseData WebserviceType:webserviceCallerObject.webserviceType WithMessage:webserviceCallerObject.comment];
//            break;
//            
//        default:
//            break;
//    }
//}
@end
