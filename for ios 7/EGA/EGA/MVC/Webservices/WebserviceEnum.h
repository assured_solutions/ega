//
//  WebserviceEnum.h
//  UFSApp
//
//  Created by EPICTENET CONSULTANCY on 14/09/15.
//  Copyright (c) 2015 Epictenet. All rights reserved.
//

#ifndef UFSApp_WebserviceEnum_h
#define UFSApp_WebserviceEnum_h

typedef enum : NSInteger {
    HomeLoanRepayment = 0,
    BorrowingPower,
    FixedDeposit,
    BudgetPlanner,
    StampDuty,
    IncomeTax,
    MedicalLevy,
    SuperAnnuation,
    CategoryList,
    BrokersList,
    RegisterTrader,
    TopNTraders,
    TeamList,
} WebServiceCallerType;



#endif
