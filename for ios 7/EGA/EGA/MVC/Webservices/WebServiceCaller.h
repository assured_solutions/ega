
//FileName : WebServiceCaller
//Version : 1.0
//DateOfCreation : 30/08/2015
//Author : Soniya Vishwakarma
//Dependencies :
//Description : This is caller for the JSON webservice which will used in application
/******* Webservice
 @URL :
 
 ****/

#import <Foundation/Foundation.h>
#import "WebserviceEnum.h"

@protocol WebServiceCallerDelegate;

@interface WebServiceCaller : NSObject
{
    __unsafe_unretained id <WebServiceCallerDelegate> delegate;
    NSOperationQueue * operationQueue;
   
}
@property(nonatomic, assign) id <WebServiceCallerDelegate> delegate;

-(void)cancelWebserviceCall;

-(void)cancelAllCalls;

- (void)getHomeLoanForLoanAmount:(NSString *)loanAmount Duration:(NSString *)loanDuration Rate:(NSString *)interestRate LoanType:(NSString*)loanType Frequency:(NSString*)frequency;

- (void)getBorrowingPowerForAnnualIncome:(NSString *)annualIncome FixeCommitments:(NSString *)fixedCommitments LivingExpenses:(NSString *)livingExp;

- (void)getIncomeTaxForAnnualIncome:(NSString *)annualIncome;

- (void)getFixedTermForAmount:(NSString *)depositAmount Rate:(NSString *)interestRate Years:(NSString *)years;

- (void)getBudgetPlanForAnnualIncome:(NSString *)annualIncome OtherIncome:(NSString*)otherIncome LivingExpenses:(NSString *)livingExpenses Insurance:(NSString *)insurance Loans:(NSString *)loans Entertainment:(NSString *)entertainmentCost TravelCost:(NSString *)travelCost OtherExpenses:(NSString*)otherExpenses;

- (void)getStampDutyForState:(NSString *)stateOfProperty PropertyValue:(NSString *)propertyValue PropertyType:(NSString *)propType isFirstHome:(BOOL)firstHome;


-(void)getListOfBrokersForRefer;

-(void)getListOfCategories;

-(void)registerTraderWithData:(NSDictionary*)userDetail;

-(void)getTradersListWithRecordCounts:(NSInteger)recordCount OfClientID:(NSString*)clientID;

-(void)getTeamListWithClientID:(NSString*)clientID;

@end

@protocol WebServiceCallerDelegate <NSObject>

@optional

-(void)getCalculationCompletesSuccessfully:(BOOL)success WithResult:(NSDictionary*)dictResultDetails WithMessage:(NSString*)message Type:(WebServiceCallerType)webserviceType;

-(void)getListOfBrokersForReferCompletesSuccessfully:(BOOL)success WithList:(NSMutableArray*)arrBrokers WithMessage:(NSString*)message;

-(void)getListOfCategoriesCompletesSuccessfully:(BOOL)success WithList:(NSMutableArray*)arrCategories WithMessage:(NSString*)message;

-(void)registerTraderCompletesSuccessfully:(BOOL)success WithMessage:(NSString*)message;

-(void)getTradersListWithRecordCountsCompletesSuccessfully:(BOOL)success WithList:(NSMutableArray*)arrServiceProvider WithMessage:(NSString*)message;

-(void)getInterestValueToPlotChartCompletesSuccessfully:(BOOL)success WithList:(NSMutableArray*)arrInterestPoints WithMessage:(NSString*)message;

-(void)getTeamListCompletesSuccessfully:(BOOL)success WithList:(NSMutableArray*)arrTeam WithMessage:(NSString*)message;

@end
