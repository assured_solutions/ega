//
//  WebServiceCallerObject.m
//  UFSApp
//
//  Created by EPICTENET CONSULTANCY on 14/09/15.
//  Copyright (c) 2015 Epictenet. All rights reserved.
//

#import "WebServiceCallerObject.h"

@implementation WebServiceCallerObject

@synthesize requestURL;
@synthesize isPost;
@synthesize jsonStringToPost;
@synthesize webserviceType;

@synthesize success;
@synthesize parseData;
@synthesize comment;

@end
