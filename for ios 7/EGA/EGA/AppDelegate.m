//
//  AppDelegate.m
//  EGA
//
//  Created by animesh bansal on 9/26/15.
//  Copyright © 2015 Assure Solutions Group. All rights reserved.
//

#import "AppDelegate.h"
#import "AppManager.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [Fabric with:@[[Crashlytics class]]];

    [self copyDatabaseIfNeeded];
    AppManager *manager = [[AppManager alloc]init];
    [manager setAppConfiguration];
     [self checkNetworkStatus];
    return YES;
}














- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
#pragma mark - Reachability

- (void)checkNetworkStatus{
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(reachabilityChanged:) name: kReachabilityChangedNotification object: nil];
}

- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    [self updateInterfaceWithReachability: curReach];
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    switch (netStatus)
    {
        case NotReachable:
        {
            self.isNetAvailable=NO;
            break;
        }
            
        case ReachableViaWiFi:
        {
            self.isNetAvailable=YES;
            break;
        }
            
        case ReachableViaWWAN:
        {
            self.isNetAvailable=YES;
            break;
        }
    }
}




#pragma mark - DataBasePath


-(void)copyDatabaseIfNeeded
{
    NSString *path=[self getDBPath];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    BOOL find = [fileManager fileExistsAtPath:path];
    BOOL success;
    NSError *error;
    
    if (!find) {
        
        
        NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"EGADB.sqlite"];
        success =[fileManager copyItemAtPath:databasePathFromApp toPath:path error:&error];
        if (!success)
        {
            NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
        }
        else
        {
            [self addSkipBackupAttributeToItemAtURL:databasePathFromApp];
        }
        
        databasePathFromApp=nil;
    }
    path=nil;
    error=nil;
    fileManager=nil;
}

-(NSString*)getDBPath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"EGADB.sqlite"];
    paths=nil;
    documentsDirectory=nil;
    return path;
}


-(BOOL)addSkipBackupAttributeToItemAtURL:(NSString *)URLString
{
    NSURL *URL=[[NSURL alloc]initFileURLWithPath:URLString];
    
    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    
    NSError *error = nil;
    
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success)
    {
        
    }
    URL=nil;
    error=nil;
    return success;
}













@end
